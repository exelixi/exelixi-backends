/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.core.backends.templates.exelixirunime

import exelixi.core.backends.templates.ExelixiPrinter

/**
 * 
 * @author Endri Bezati
 */
class MappingParserSource implements ExelixiPrinter {

	override getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 ******************************************************************************/
			
			#include "mapping_parser.h"
			#include <iostream>
			#include <algorithm>
			
			MappingParser::MappingParser(std::string& filename,
					std::vector<Actor*> actors) :
					filename(filename), actors(actors), unmapped_actors(actors) {
				if (filename.empty()) {
					std::vector<Actor*> singlePartition;
			
					std::map<std::string, Actor*>::iterator ait;
			
					partitions["main"] = actors;
				} else {
					parse();
				}
			}
			
			MappingParser::~MappingParser() {
			}
			
			void MappingParser::parse() {
				XMLDocument doc;
				doc.LoadFile(filename.c_str());
				XMLElement* elt =  doc.FirstChildElement("Configuration" )->FirstChildElement("Partitioning");
				parsePartitions(elt);
			
				if(!unmapped_actors.empty()){
					std::cout<<"[Partition Parser ERROR] there are "<< unmapped_actors.size() << " unmapped actors!\n";
					for(Actor* actor : unmapped_actors){
						std::cout<<"  [unmapped] ->" << actor->name()<<"\n";
					}
				}
			}
			
			void MappingParser::parsePartitions(XMLElement* parent) {
				for(XMLElement* child = parent->FirstChildElement("Partition"); child != NULL; child = child->NextSiblingElement("Partition"))
				{
					std::string id = std::string(child->Attribute("id"));
					partitions[id] = parseInstances(id, child);
				}
			}
			
			std::vector<Actor*> MappingParser::parseInstances(std::string partitionId, XMLElement* parent) {
				std::vector<Actor*> instances;
				for(XMLElement* child = parent->FirstChildElement("Instance"); child != NULL; child = child->NextSiblingElement("Instance"))
				{
					XMLElement* elt = child->ToElement();
					std::string instId = std::string(elt->Attribute("id"));
					Actor* actor = find_actor(instId);
					if(actor!=NULL){
						instances.push_back(actor);
						unmapped_actors.erase(std::remove(unmapped_actors.begin(), unmapped_actors.end(), actor), unmapped_actors.end());
					}else{
						std::cout<<"[Partition Parser ERROR] (partition="<<partitionId<<") instance="<<instId<<" not found in the generated network!\n";
					}
				}
			
				return instances;
			}

		'''
	}
	
	override getFileName() {
		return "mapping_parser.cpp"
	}
	
}
