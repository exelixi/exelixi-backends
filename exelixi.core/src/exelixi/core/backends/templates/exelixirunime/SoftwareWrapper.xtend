/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.core.backends.templates.exelixirunime

import exelixi.core.backends.templates.ExelixiPrinter

class SoftwareWrapper implements ExelixiPrinter {

	override getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 ******************************************************************************/
			#ifndef __EXELIXI_SOFTWARE_WRAPPER_H__
			#define __EXELIXI_SOFTWARE_WRAPPER_H__
			
			#include "wrapper.h"
			
			template<typename T>
			class SoftwareWrapper : public Wrapper<T> {
			public:
			    SoftwareWrapper();
			    ~SoftwareWrapper();
			
			    T *write_address() override;
			
			    void write_advance() override;
			
			    void write_advance(unsigned int nb_data) override;
			
			    T *read_address(int reader_id) override;
			
			    T *read_address(int reader_id, unsigned nb_data) override;
			
			    void read_advance(int reader_id, unsigned int nb_data = 1) override;
			
			    unsigned int count(int reader_id) const override{
			        return  fifo->count(reader_id);
			    }
			
			    unsigned int rooms() const override{
			        return fifo->rooms();
			    }
			
			    void set_fifo(Fifo<T> *fifo);
			
			    unsigned int fifo_size() const override;
			
			private:
			    Fifo<T> *fifo;
			};
			
			template<typename T>
			SoftwareWrapper<T>::SoftwareWrapper() {
			}
			
			template<typename T>
			SoftwareWrapper<T>::~SoftwareWrapper() {
			}
			
			template<typename T>
			void SoftwareWrapper<T>::set_fifo(Fifo<T> *fifo) {
			    this->fifo = fifo;
			}
			
			template<typename T>
			unsigned int SoftwareWrapper<T>::fifo_size() const {
			    fifo->fifo_size();
			}
			
			
			template<typename T>
			inline T* SoftwareWrapper<T>::write_address() {
			    return fifo->write_address();
			}
			
			template<typename T>
			void SoftwareWrapper<T>::write_advance() {
			    fifo->write_advance();
			}
			
			template<typename T>
			void SoftwareWrapper<T>::write_advance(unsigned int nb_val) {
			    fifo->write_advance(nb_val);
			}
			
			template<typename T>
			inline T* SoftwareWrapper<T>::read_address(int reader_id) {
			}
			
			template<typename T>
			inline T* SoftwareWrapper<T>::read_address(int reader_id, unsigned uNbVal) {
			}
			
			template<typename T>
			void SoftwareWrapper<T>::read_advance(int reader_id, unsigned int nb_val) {
			}
			
			
			#endif //__EXELIXI_SOFTWARE_WRAPPER_H__
		'''
	}

	override getFileName() {
		return "software_wrapper.h"
	}

}
