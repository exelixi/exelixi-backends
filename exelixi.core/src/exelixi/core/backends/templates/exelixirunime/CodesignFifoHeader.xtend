/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.core.backends.templates.exelixirunime

import exelixi.core.backends.templates.ExelixiPrinter

class CodesignFifoHeader implements ExelixiPrinter {

	override getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 ******************************************************************************/
			
			#ifndef __EXELIXI_SW_FIFO_H__
			#define __EXELIXI_SW_FIFO_H__
			
			#include <string.h>
			
			/*! \class Fifo fifo.h
			 *  \brief A template class that implements a non-bocking ring buffer.
			 */
			template<typename T>
			class Fifo {
			public:
			    virtual ~Fifo() = 0;
			
			    virtual T *write_address() = 0;
			
			    virtual void write_advance() = 0;
			
			    virtual void write_advance(unsigned int nb_data) = 0;
			
			    virtual T *read_address(int reader_id) = 0;
			
			    virtual T *read_address(int reader_id, unsigned nb_data) = 0;
			
			    virtual void read_advance(int reader_id, unsigned int nb_data = 1) = 0;
			
			    virtual unsigned int count(int reader_id) const = 0;
			
			    virtual unsigned int rooms() const = 0;
			
			    virtual unsigned int fifo_size() const = 0;
			
			};
			
			template<typename T>
			Fifo<T>::~Fifo() {
			}
			
			
			#endif // __EXELIXI_SW_FIFO_H__
		'''
	}

	override getFileName() {
		return "fifo.h"
	}

}
