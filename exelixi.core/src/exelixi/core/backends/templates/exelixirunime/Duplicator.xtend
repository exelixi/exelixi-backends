/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.core.backends.templates.exelixirunime

import exelixi.core.backends.templates.ExelixiPrinter

class Duplicator implements ExelixiPrinter {

	override getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 ******************************************************************************/
			
			#ifndef __EXELIXI_DUPLICATOR_H__
			#define __EXELIXI_DUPLICATOR_H__
			
			#include "wrapper.h"
			#include <cstring>
			#include <vector>
			
			template<typename T>
			class Duplicator : public Wrapper<T> {
			public:
			    Duplicator(std::vector<Wrapper<T> *> wrappers);
			
			    ~Duplicator();
			
			    T *write_address() override;
			
			    void write_advance() override;
			
			    void write_advance(unsigned int nb_data) override;
			
			    T *read_address(int reader_id) override;
			
			    T *read_address(int reader_id, unsigned nb_data) override;
			
			    void read_advance(int reader_id, unsigned int nb_data = 1) override;
			
			    unsigned int count(int reader_id) const override {
			
			    }
			
			    unsigned int rooms() const override {
			        unsigned int min_rooms = 0xFFFFFFFF;
			        for (Wrapper<T> *wrapper : wrappers) {
			            unsigned int rooms = wrapper->rooms();
			            min_rooms = min_rooms < rooms ? min_rooms : rooms;
			        }
			        return min_rooms;
			    }
			
			    void set_fifo(Fifo<T> *fifo) override;
			
			    unsigned int fifo_size() const override;
			
			private:
			    std::vector<Wrapper<T> *> wrappers;
			    T *buffer;
			    unsigned int wr_ptr;
			    unsigned int size;
			
			};
			
			template<typename T>
			Duplicator<T>::Duplicator(std::vector<Wrapper<T> *> wrappers) : wrappers(wrappers), buffer(new T[512]), wr_ptr(0) {
			}
			
			template<typename T>
			Duplicator<T>::~Duplicator() {
			}
			
			template<typename T>
			void Duplicator<T>::set_fifo(Fifo<T> *fifo) {
			}
			
			template<typename T>
			unsigned int Duplicator<T>::fifo_size() const {
			}
			
			
			template<typename T>
			inline T *Duplicator<T>::write_address() {
			    return buffer;
			}
			
			template<typename T>
			void Duplicator<T>::write_advance() {
			    for (Wrapper<T> *wrapper : wrappers) {
			        T* w = wrapper->write_address();
			        w[0] = buffer[0];
			        wrapper->write_advance();
			    }
			}
			
			template<typename T>
			void Duplicator<T>::write_advance(unsigned int nb_val) {
			    for (Wrapper<T> *wrapper : wrappers) {
			        T* w = wrapper->write_address();
			        // -- Copy
			        std::memcpy(w,buffer, sizeof(T)*nb_val);
			        wrapper->write_advance(nb_val);
			    }
			}
			
			template<typename T>
			inline T *Duplicator<T>::read_address(int reader_id) {
			}
			
			template<typename T>
			inline T *Duplicator<T>::read_address(int reader_id, unsigned uNbVal) {
			}
			
			template<typename T>
			void Duplicator<T>::read_advance(int reader_id, unsigned int nb_val) {
			}
			
			#endif //__EXELIXI_DUPLICATOR_H__
		'''
	}

	override getFileName() {
		return "duplicator.h"
	}

}
