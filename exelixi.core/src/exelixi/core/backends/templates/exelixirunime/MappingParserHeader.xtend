/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.core.backends.templates.exelixirunime

import exelixi.core.backends.templates.ExelixiPrinter

/**
 * 
 * @author Endri Bezati
 */
class MappingParserHeader implements ExelixiPrinter {
	
	override getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 ******************************************************************************/
			
			#ifndef EXELIXI_MAPPING_PARSER_H_
			#define EXELIXI_MAPPING_PARSER_H_
			
			#include <string>
			#include <map>
			#include <vector>
			#include <set>
			#include "tinyxml2.h"
			
			#include "actor.h"
			
			using namespace tinyxml2;
			
			class MappingParser {
			public:
				MappingParser(std::string& filename, std::vector<Actor*> actors);
			
				virtual ~MappingParser();
			
				std::map<std::string, std::vector<Actor*> > getPartitions() {
					return partitions;
				}
			
				std::vector<Actor*> getActors(std::string partitionId){
					return partitions[partitionId];
				}
			
			private:
			
				void parse();
			
				void parsePartitions(XMLElement* parent);
			
				std::vector<Actor*> parseInstances(std::string id, XMLElement* parent);
			
				std::string& filename;
			
				std::vector<Actor*> actors;
			
				std::vector<Actor*> unmapped_actors;
			
				std::map<std::string, std::vector<Actor*> > partitions;
			
				Actor* find_actor(std::string name){
			
					for(Actor* actor : actors){
						if(actor->name().compare(name) == 0){
							return actor;
						}
					}
			
					return NULL;
				}
			};
			
			#endif // EXELIXI_MAPPING_PARSER_H_

		'''
	}
	
	override getFileName() {
		return "mapping_parser.h"
	}
	
}
