/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.core.backends.templates.exelixirunime

import exelixi.core.backends.templates.ExelixiPrinter

/**
 * 
 * @author Endri Bezati
 * @author Simone Casale-Brunet
 */
class ActorHeader implements ExelixiPrinter {

	private boolean fifoTrace;
	private boolean logSchedulingMiss;

	new(boolean fifoTrace, boolean logSchedulingMiss) {
		this.fifoTrace = fifoTrace
		this.logSchedulingMiss = logSchedulingMiss
	}
	new (){
		this.fifoTrace = false
		this.logSchedulingMiss = false
	}
	
	override def getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _ 
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 ******************************************************************************/	
			
			#ifndef __EXELIXI_ACTOR_H__
			#define __EXELIXI_ACTOR_H__
			#include <string>
			
			enum EStatus {
				None, hasSuspended, hasExecuted, Starvation, Fullness
			};
			
			class Actor {
			
			public:
				virtual void initialize() = 0;
				
				«IF fifoTrace»virtual void enable_fifo_traces() = 0;«ENDIF»
			
				virtual bool action_selection(EStatus&) = 0;
				
				bool has_executed(){ return executed_once; }
				
				virtual std::string name() {return "<unknown>"; }
				
				«IF logSchedulingMiss»long get_scheduling_miss() { return scheduling_miss; } «ENDIF»
				
				virtual ~Actor(){
				}
				
			protected:
			    bool executed_once = false;
			    «IF logSchedulingMiss»long scheduling_miss = 0; «ENDIF»
			
			};
			
			#endif // __EXELIXI_ACTOR_H__
		'''
	}
	
	override getFileName() {
		return "actor.h"
	}
	
}
