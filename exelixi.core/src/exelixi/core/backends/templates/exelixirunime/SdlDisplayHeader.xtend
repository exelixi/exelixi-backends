/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.core.backends.templates.exelixirunime

import exelixi.core.backends.templates.ExelixiPrinter

/**
 * 
 * @author Endri Bezati
 */
class SdlDisplayHeader implements ExelixiPrinter {
	

	override getContent() {
		'''
		#ifndef __SDL_DISPLAY_H__
		#define __SDL_DISPLAY_H__
		
		#ifndef NO_DISPLAY
		#ifdef _WIN32 
		#include <SDL.h>
		#else
		#include <SDL/SDL.h>
		#endif
		#endif
		
		#ifndef NO_DISPLAY
		static void press_a_key(int code);
		#endif
		
		void displayYUV_init();
		
		void displayYUV_setSize(int width, int height);
		
		void displayYUV_displayPicture(unsigned char pictureBufferY[], unsigned char pictureBufferU[], unsigned char pictureBufferV[], short pictureWidth, short pictureHeight);
			
		int displayYUV_getNbFrames();
		
		unsigned char displayYUV_getFlags();
		
		int compareYUV_compareComponent(const int x_size, const int y_size, const int x_size_test_img, 
			const unsigned char *true_img_uchar, const unsigned char *test_img_uchar,
			unsigned char SizeMbSide, char Component_Type);
		
		void compareYUV_init();
		
		void compareYUV_readComponent(unsigned char **Component, unsigned short width, unsigned short height, char sizeChanged);
		
		void compareYUV_comparePicture(unsigned char pictureBufferY[], unsigned char pictureBufferU[],
			unsigned char pictureBufferV[], short pictureWidth,
			short pictureHeight);
		
		
		static void print_fps_avg(void);
		
		void fpsPrintInit();
		
		void fpsPrintNewPicDecoded(void);

		#endif // __SDL_DISPLAY_H__
		'''
	}
	
	override getFileName() {
		return "sdl_display.h"
	}
	
}
