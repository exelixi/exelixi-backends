/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.core.backends.templates.exelixirunime

import exelixi.core.backends.templates.ExelixiPrinter

/**
 * 
 * @author Endri Bezati
 */
class GetOptSource implements ExelixiPrinter {

	override getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _ 
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 ******************************************************************************/
			
			#include "get_opt.h"
			#include <iostream>
			#include <stdlib.h>
			
			std::string config_file;
			std::string input_file;
			std::string write_file;
			
			int nbLoops = -1;
			int nbFrames = -1;
			
			GetOpt::GetOpt(int argc, char* argv[]) {
				parse(argc, argv);
			}
			
			GetOpt::~GetOpt() {
			}
			
			void GetOpt::parse(int argc, char* argv[]) {
				std::vector<std::string> currOptionValues;
				std::string optionName;
				for (int i = 1; i < argc; i++) {
					if (argv[i][0] == '-') {
						optionName = &argv[i][1];
						tokens[optionName] = currOptionValues;
					} else {
						tokens[optionName].push_back(&argv[i][0]);
					}
				}
			}
			
			void GetOpt::get_help() {
				std::cout << "Exelixi CPP" << std::endl;
			
				std::cout << std::endl;
			
				std::cout << "Usage: " << "./" << this->name << " [options]" << std::endl;
			
				std::cout << std::endl;
			
				std::cout << "Common arguments:" << std::endl;
			
				std::cout << "-m <file>"
						<< " Define a predefined actor mapping on multi-core platforms using the given XML file."
						<< std::endl;
			
				std::cout << "-h" << " Print this message." << std::endl;
			
				std::cout << std::endl;
			
				std::cout << "Specific arguments:" << std::endl;
			
				std::cout << "-i <file>" << " Specify an input file." << std::endl;
			
				std::cout << "-l <number>"
						<< " Set the number of readings of the input file before exiting."
						<< std::endl;
			
				std::cout << "-w <file>" << " Specify a file to write the output stream."
						<< std::endl;
			
				std::cout << "-t <instance name(s)>"
						<< " Specify the actor instances to generate the fifo traces."
						<< std::endl;
			
			}
			
			void GetOpt::getOptions() {
				std::string help;
				bool is_help = this->getOptionAs<std::string>("h");
				if (is_help) {
					get_help();
					exit(EXIT_SUCCESS);
				}
				this->getOptionAs<std::string>("i", input_file);
				this->getOptionAs<std::string>("m", config_file);
				this->getOptionAs<std::string>("w", write_file);
				bool is_trace = this->getOptionAs<std::string>("t", this->trace_instance_names);
				if (is_trace) {
					if (!trace_instance_names.empty()) {
						for (auto instance_name : trace_instance_names) {
							Actor* search = findActor(instance_name);
							if (search == NULL) {
								std::cout << "Instance: " << instance_name
										<< " does not exist" << std::endl;
								exit(EXIT_FAILURE);
							}
						}
					} else {
						std::cout << "Info: All traces will be generated, it might take more time!" << std::endl;
						for(Actor* actor : actors){
							trace_instance_names.insert(actor->name());
						}
					}
				}
				bool exists = this->getOptionAs<int>("l", nbLoops);
				if (!exists)
					nbLoops = -1;
				exists = this->getOptionAs<int>("f", nbFrames);
				if (!exists)
					nbFrames = -1;
			}
		'''
	}
	
	override getFileName() {
		return "get_opt.cpp"
	}
	
}
