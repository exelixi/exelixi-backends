/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.core.xcf.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * XCF Memory Element
 * 
 * @author Endri Bezati
 */
@XmlRootElement(name = "Memory")
@XmlType(propOrder = { "medium", "size", "startAddress", "shared", "reserve" })
public class XcfMemory {

	private String medium;

	private Integer size;

	private String startAddress;

	private Boolean shared;

	private Boolean reserve;

	public String getMedium() {
		return medium;
	}

	@XmlAttribute(name = "size")
	public Integer getSize() {
		return size;
	}

	@XmlAttribute(name = "start-address")
	public String getStartAddress() {
		return startAddress;
	}

	@XmlAttribute(name = "shared")
	public Boolean getShared() {
		return shared;
	}

	@XmlAttribute(name = "reserve")
	public Boolean getReserve() {
		return reserve;
	}

	@XmlAttribute(name = "medium")
	public void setMedium(String medium) {
		this.medium = medium;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public void setStartAddress(String startAddress) {
		this.startAddress = startAddress;
	}

	public void setShared(Boolean shared) {
		this.shared = shared;
	}

	public void setReserve(Boolean reserve) {
		this.reserve = reserve;
	}

}
