/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.core.xcf.model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Memory-Connection")
@XmlType(propOrder = { "defaultFifoSize", "fifoConnections", "memoryConnections" })
public class XcfConnections {
	
	private ArrayList<XcfFifoConnection> fifoConnections;
	
	private ArrayList<XcfMemoryConnection> memoryConnections;

	private Integer defaultFifoSize;
	
	@XmlElement(name = "Fifo-Connection", required = false)
	public ArrayList<XcfFifoConnection> getFifoConnections() {
		return fifoConnections;
	}

	public void setFifoConnections(ArrayList<XcfFifoConnection> fifoConnections) {
		this.fifoConnections = fifoConnections;
	}

	@XmlElement(name = "Memory-Connection", required = false)
	public ArrayList<XcfMemoryConnection> getMemoryConnections() {
		return memoryConnections;
	}

	public void setMemoryConnections(ArrayList<XcfMemoryConnection> memoryConnections) {
		this.memoryConnections = memoryConnections;
	}
	
	public void addFifoConnection(XcfFifoConnection connection) {
		try {
			if (fifoConnections == null) {
				fifoConnections = new ArrayList<XcfFifoConnection>();
			}
			fifoConnections.add(connection);
		} catch (Exception e) {
		}
	}
	
	public void addMemoryConnection(XcfMemoryConnection connection) {
		try {
			if (memoryConnections == null) {
				memoryConnections = new ArrayList<XcfMemoryConnection>();
			}
			memoryConnections.add(connection);
		} catch (Exception e) {
		}
	}
	
	@XmlAttribute(name = "default-fifo-size", required = false)
	public Integer getDefaultFifoSize() {
		return defaultFifoSize;
	}

	public void setDefaultFifoSize(Integer defaultFifoSize) {
		this.defaultFifoSize = defaultFifoSize;
	}

}
