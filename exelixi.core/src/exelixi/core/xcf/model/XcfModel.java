/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.core.xcf.model;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/*
 * XML Configuration File Model
 * 
 * @author Endri Bezati
 */

@XmlRootElement(name = "Configuration")
@XmlType(propOrder = { "name", "version", "network", "partitioning", "codeGenerators", "media", "connections" })
public class XcfModel {

	/**
	 * Name of the configuration
	 */
	private String name;

	/**
	 * Version of the configuration
	 */
	private String version;

	/**
	 * The partitioning of the configuration
	 */
	private XcfPartitioning partitioning;

	private XcfNetwork network;

	private XcfMedia media;

	private XcfCodeGenerators codeGenerators;

	/**
	 * XCF connections
	 * 
	 */
	private XcfConnections connections;

	@XmlAttribute(name = "name", required = false)
	public String getName() {
		return name;
	}

	@XmlAttribute(name = "version", required = false)
	public String getVersion() {
		return version;
	}

	@XmlElement(name = "Partitioning")
	public XcfPartitioning getPartitioning() {
		return partitioning;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setPartitioning(XcfPartitioning partitioning) {
		this.partitioning = partitioning;
	}

	@XmlElement(name = "Connections")
	public XcfConnections getConnections() {
		return connections;
	}

	public void setConnections(XcfConnections connections) {
		this.connections = connections;
	}

	@XmlElement(name = "Network")
	public XcfNetwork getNetwork() {
		return network;
	}

	public void setNetwork(XcfNetwork network) {
		this.network = network;
	}

	@XmlElement(name = "Media")
	public XcfMedia getMedia() {
		return media;
	}

	public void setMedia(XcfMedia media) {
		this.media = media;
	}

	@XmlElement(name = "Code-Generators")
	public XcfCodeGenerators getCodeGenerators() {
		return codeGenerators;
	}

	public void setCodeGenerators(XcfCodeGenerators codeGenerators) {
		this.codeGenerators = codeGenerators;
	}

	public Map<XcfInstance, XcfPartition> getPartitionInstanceMap() {
		Map<XcfInstance, XcfPartition> map = new HashMap<>();
		for (XcfPartition p : this.partitioning.getPartitions()) {
			for (XcfInstance instance : p.getInstances()) {
				map.put(instance, p);
			}
		}

		return map;
	}

}
