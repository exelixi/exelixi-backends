/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


package exelixi.zynq.backend.transformations;

import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION;
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION;
import static exelixi.zynq.backend.Constants.ZYNQ_BC_FIFO_ID;
import static exelixi.zynq.backend.Constants.ZYNQ_FIFO_ID;
import static exelixi.zynq.backend.Constants.ZYNQ_NB_READERS;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SINGLE_DMA_POLLING;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import exelixi.hls.partitioner.Partitioner;
import net.sf.orcc.df.Connection;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.graph.Vertex;

public class PartitionedConnectionsIds extends DfVisitor<Void> {

	private Partitioner partitioner;

	private Map<String, Object> options;

	public PartitionedConnectionsIds(Partitioner partitioner, Map<String, Object> options) {
		this.partitioner = partitioner;
		this.options = options;
	}

	@Override
	public Void caseNetwork(Network network) {

		for (Vertex vertex : network.getChildren()) {
			Instance src = (Instance) vertex;
			if (partitioner.isInstanceOnPartition(src, ZYNQ_PS_PARTITION)) {
				for (Port port : src.getOutgoingPortMap().keySet()) {
					boolean portIsAlreadyConnectedToPL = false;
					int nbReaders = 0;
					int fifoId = 0;
					Map<Instance, Integer> plInstances = new HashMap<Instance, Integer>();
					for (Connection connection : src.getOutgoingPortMap().get(port)) {
						Instance tgt = (Instance) connection.getTarget();

						// -- NB readers
						if (partitioner.isInstanceOnPartition(tgt, ZYNQ_PL_PARTITION)) {
							if (!portIsAlreadyConnectedToPL) {
								portIsAlreadyConnectedToPL = true;
								nbReaders++;
							}
						} else {
							nbReaders++;
						}
						// -- FIFO ID
						if (partitioner.isInstanceOnPartition(tgt, ZYNQ_PL_PARTITION)) {
							if (plInstances.containsKey(tgt)) {
								connection.setAttribute(ZYNQ_FIFO_ID, plInstances.get(tgt));
							} else {
								plInstances.put(tgt, fifoId);
								connection.setAttribute(ZYNQ_FIFO_ID, fifoId);
							}
						} else {
							connection.setAttribute(ZYNQ_FIFO_ID, fifoId);
						}
						fifoId++;
					}
					for (Connection connection : src.getOutgoingPortMap().get(port)) {
						connection.setAttribute(ZYNQ_NB_READERS, nbReaders);
					}
				}
			} else if (partitioner.isInstanceOnPartition(src, ZYNQ_PL_PARTITION)) {
				for (Port port : src.getOutgoingPortMap().keySet()) {
					int nbReaders = 0;
					int fifoId = 0;
					for (Connection connection : src.getOutgoingPortMap().get(port)) {
						Instance tgt = (Instance) connection.getTarget();

						if (partitioner.isInstanceOnPartition(tgt, ZYNQ_PS_PARTITION)) {
							// -- NB readers
							nbReaders++;
							// -- FIFO ID
							connection.setAttribute(ZYNQ_FIFO_ID, fifoId);
							fifoId++;
						}
					}
					for (Connection connection : src.getOutgoingPortMap().get(port)) {
						connection.setAttribute(ZYNQ_NB_READERS, nbReaders);
					}
				}
			}
		}

		// -- Broadcast ID
		int i = 0;
		for (Vertex vertex : network.getChildren()) {
			Instance src = (Instance) vertex;
			for (Port port : src.getActor().getOutputs()) {
				if (src.getOutgoingPortMap().get(port) != null) {
					for (Connection connection : src.getOutgoingPortMap().get(port)) {
						connection.setAttribute(ZYNQ_BC_FIFO_ID, i);
					}
					i++;
				}
			}
		}

		// -- Partitioned Connection
		List<Connection> plToPsFifos = new ArrayList<Connection>(
				partitioner.getConnectionToConnectionBd(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION).keySet());

		List<Connection> psToPlFifos = new ArrayList<Connection>(
				partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION).keySet());

		String id = "DMA Polling";
		if (options.containsKey(ZYNQ_OPTION_COMMUNICATION_KIND)) {
			id = (String) options.get(ZYNQ_OPTION_COMMUNICATION_KIND);
		}
		
		String kind = "";
		if(id.equals("AXI-Stream FIFO")){
			kind = ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO;
		}else if (id.equals("DMA Polling")){
			kind = ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING;
		}else if (id.equals("Single DMA")){
			kind = ZYNQ_OPTION_COMMUNICATION_KIND_SINGLE_DMA_POLLING;
		}
		

		for(Connection connection : plToPsFifos){
			connection.setAttribute(ZYNQ_OPTION_COMMUNICATION_KIND, kind);
		}
		
		for(Connection connection : psToPlFifos){
			connection.setAttribute(ZYNQ_OPTION_COMMUNICATION_KIND, kind);
		}
		
		return null;
	}

}
