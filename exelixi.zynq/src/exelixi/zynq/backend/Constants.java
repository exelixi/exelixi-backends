/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend;

/**
 * Zynq Constants
 * @author Endri Bezati
 */
public class Constants {

	public static final String ZYNQ_OPTION_COMMUNICATION_KIND = "exelixi.xronos.options.communicationKind";

	public static final String ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO = "exelixi.xronos.options.communicationKind.llfifo";
	
	public static final String ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING = "exelixi.xronos.options.communicationKind.simpleDma.polling";
	
	public static final String ZYNQ_OPTION_COMMUNICATION_KIND_SINGLE_DMA_POLLING = "exelixi.xronos.options.communicationKind.singleDma.polling";
	
	public static final String ZYNQ_OPTION_COMMUNICATION_KIND_SINGLE_DMA_SG = "exelixi.xronos.options.communicationKind.singleDma.sg";

	//public static final String ZYNQ_OPTION_EVERY_INSTANCE_AN_IP_CORE = "exelixi.xronos.options.everyInstanceIpCore";
	
	public static final String ZYNQ_OPTION_LINKER_SCRIPT = "exelixi.xronos.options.linkerScript";
	
	public static final String ZYNQ_OPTION_LINKER_SCRIPT_STACK_SIZE = "exelixi.xronos.options.linkerScript.stackSize";
	
	public static final String ZYNQ_OPTION_LINKER_SCRIPT_HEAP_SIZE = "exelixi.xronos.options.linkerScript.heapSize";
	
	
	public static final String ZYNQ_OPTION_FPGA_BOARD = "exelixi.xronos.options.zynq.fpgaBoard"; 
	
	public static final String ZYNQ_OPTION_V7_PROFILING = "exelixi.xronos.options.v7Profiling";
	
	public static final String ZYNQ_NB_READERS = "exelixi.xronos.constants.zynqNbReaders";
	
	public static final String ZYNQ_FIFO_ID = "exelixi.xronos.constants.zynqFifoId";
	
	public static final String ZYNQ_BC_FIFO_ID = "exelixi.xronos.constants.zynqBcFifoId";
	
}
