/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 

package exelixi.zynq.backend.templates.vivado

import exelixi.hls.devices.XilinxDevice
import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.tclscripts.TCLTemplate
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import exelixi.zynq.backend.templates.vivado.boardsproperties.PSProperty
import exelixi.zynq.backend.templates.vivado.boardsproperties.SysPsTemplate
import exelixi.zynq.backend.templates.vivado.boardsproperties.zynqps7.OZ745Property
import exelixi.zynq.backend.templates.vivado.boardsproperties.zynqps7.SysPS7
import exelixi.zynq.backend.templates.vivado.boardsproperties.zynqps7.ZC702Property
import exelixi.zynq.backend.templates.vivado.boardsproperties.zynqps7.ZC706Property
import exelixi.zynq.backend.templates.vivado.boardsproperties.zynqps7.ZedBoardProperty
import exelixi.zynq.backend.templates.vivado.boardsproperties.zynqultrascale.SysUltra
import exelixi.zynq.backend.templates.vivado.boardsproperties.zynqultrascale.ZCU102ES1
import java.util.HashMap
import java.util.List
import java.util.Map
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.ir.Var

import static exelixi.hls.backend.Constants.DEVICE
import static exelixi.hls.backend.Constants.NETWORK_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.PROJECTS_PATH
import exelixi.zynq.backend.templates.vivado.boardsproperties.zynqps7.PynqZ1Property

/**
 * 
 * @author Endri Bezati
 */
class SysBlockDesign extends TCLTemplate {

	private Network network

	private String name

	private XilinxDevice xilinxDevice

	private String projectsPath

	private Partitioner partitioner

	private Map<String, Integer> axiInterconnectVar

	private Map<String, Object> options

	private String blockDesignName

	new(Network network, Map<String, Object> options, Partitioner partitioner) {
		this.network = network
		this.options = options
		
		this.name = network.simpleName + "_pl"
		blockDesignName = "sys"

		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		this.projectsPath = options.get(PROJECTS_PATH) as String
	
		this.partitioner = partitioner
		// -- Interconnect for AXI Master Variables
		axiInterconnectVar = new HashMap
		if (network.hasAttribute(NETWORK_EXTERNAL_VARIABLES)) {
			var int i = 0;
			var Map<Instance, List<Var>> instancesExternalVariablesMap = network.getAttribute(
				NETWORK_EXTERNAL_VARIABLES).objectValue as Map<Instance, List<Var>>
			for (instance : instancesExternalVariablesMap.keySet) {
				for (variable : instancesExternalVariablesMap.get(instance)) {
					axiInterconnectVar.put(variable.name + "_" + TemplatesUtils.getInstanceName(instance), i)
					i = i + 1
				}
			}
		}
	}

	public def getContent() {
		var PSProperty boardProperty
		var SysPsTemplate psTemplate
		if (xilinxDevice.name.equals("ZC702 - Zynq 7")) {
			boardProperty = new ZC702Property
			psTemplate = new SysPS7(network, options, partitioner, boardProperty)
		} else if (xilinxDevice.name.equals("ZC706 - Zynq 7")) {
			boardProperty = new ZC706Property
			psTemplate = new SysPS7(network, options, partitioner, boardProperty)
		} else if (xilinxDevice.name.equals("Zedboard - Zynq 7")) {
			boardProperty = new ZedBoardProperty
			psTemplate = new SysPS7(network, options, partitioner, boardProperty)
		} else if (xilinxDevice.name.equals("PYNQ Z1 - Zynq 7")) {
			boardProperty = new PynqZ1Property
			psTemplate = new SysPS7(network, options, partitioner, boardProperty)
		} else if (xilinxDevice.name.equals("OZ745 - Zynq 7")) {
			boardProperty = new OZ745Property
			psTemplate = new SysPS7(network, options, partitioner, boardProperty)
		} else if (xilinxDevice.name.equals("ZCU102 - Zynq Ultrascale+ MPSoC ES1")) {
			boardProperty = new ZCU102ES1
			psTemplate = new SysUltra(network, options, partitioner, boardProperty)
		}

		'''
			«IF xilinxDevice.isZynq»
				«psTemplate.content»
				
				# -- Auto Assign Address
				assign_bd_address
				
				# -- Regenerate Layout
				regenerate_bd_layout
				
				# -- Validate Desing
				validate_bd_design
				
				# -- Save the design
				save_bd_design
				
				# -- Create the wrapper
				make_wrapper -files [get_files vivado_project/«network.simpleName».srcs/sources_1/bd/«blockDesignName»/«blockDesignName».bd] -top
				
				# -- Add the wrapper file and update sources
				add_files -norecurse vivado_project/«network.simpleName».srcs/sources_1/bd/«blockDesignName»/hdl/«blockDesignName»_wrapper.v
				update_compile_order -fileset sources_1
				update_compile_order -fileset sim_1
				
				# -- Generate Output products
				generate_target all [get_files vivado_project/«network.simpleName».srcs/sources_1/bd/«blockDesignName»/«blockDesignName».bd]
			«ENDIF»
		'''
	}

}
