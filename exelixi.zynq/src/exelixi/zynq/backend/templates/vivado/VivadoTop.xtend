/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 

package exelixi.zynq.backend.templates.vivado

import exelixi.hls.devices.XilinxDevice
import exelixi.hls.partitioner.Partitioner
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import java.util.ArrayList
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port

import static exelixi.hls.backend.Constants.DEVICE
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING

class VivadoTop {

	/**
	 * Network
	 */
	private Network network

	/**
	 * Input Connections
	 */
	private List<Connection> inputs

	/**
	 * Output Connections
	 */
	private List<Connection> outputs

	/**
	 * Backend Options
	 */
	private Map<String, Object> options

	/**
	 * Partitioner
	 */
	private Partitioner partitioner

	/**
	 * Xilinx device
	 */
	private XilinxDevice xilinxDevice

	/**
	 * Single DMA interface
	 */
	private boolean singleDma

	new(Network network, Map<String, Object> options, Partitioner partitioner) {
		this.network = network
		this.options = options
		this.inputs = new ArrayList<Connection>(
			partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION).keySet)
		this.outputs = new ArrayList<Connection>(
			partitioner.getConnectionToConnectionBd(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION).keySet)
		this.partitioner = partitioner
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		
		// -- Communication Kind
		singleDma = false;
		val String communicationKind = options.get(ZYNQ_OPTION_COMMUNICATION_KIND) as String
		if (communicationKind.equals("Single DMA")){
			singleDma = true;
		}
	}

	protected def getFileHeader(String comment) {
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- Exelixi Design Systems 
			// -- Copyright (C) 2016 Exelixi. All rights reserved.
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- «comment»
			// ----------------------------------------------------------------------------
		'''
	}

	public def getContent() {
		'''
			«getFileHeader("Top Verilog for Interconnecting PS, Interface and the PL Core")»
			
			`timescale 1 ps / 1 ps
			
			module «network.simpleName»_top(
				«getInOutParameters»
			);
			
				«getInOut()»
				
				«getWires(false)»
				
				«psIntInstance»
			
			endmodule
		'''
	}

	protected def getInOutParameters() {
		'''
			«IF xilinxDevice.isZynq»
				DDR_addr,
				DDR_ba,
				DDR_cas_n,
				DDR_ck_n,
				DDR_ck_p,
				DDR_cke,
				DDR_cs_n,
				DDR_dm,
				DDR_dq,
				DDR_dqs_n,
				DDR_dqs_p,
				DDR_odt,
				DDR_ras_n,
				DDR_reset_n,
				DDR_we_n,
				FIXED_IO_ddr_vrn,
				FIXED_IO_ddr_vrp,
				FIXED_IO_mio,
				FIXED_IO_ps_clk,
				FIXED_IO_ps_porb,
				FIXED_IO_ps_srstb
			«ENDIF»
		'''
	}

	protected def getInOut() {
		'''
			«IF xilinxDevice.isZynq»
				inout [14:0]DDR_addr;
				inout [2:0]DDR_ba;
				inout DDR_cas_n;
				inout DDR_ck_n;
				inout DDR_ck_p;
				inout DDR_cke;
				inout DDR_cs_n;
				inout [3:0]DDR_dm;
				inout [31:0]DDR_dq;
				inout [3:0]DDR_dqs_n;
				inout [3:0]DDR_dqs_p;
				inout DDR_odt;
				inout DDR_ras_n;
				inout DDR_reset_n;
				inout DDR_we_n;
				inout FIXED_IO_ddr_vrn;
				inout FIXED_IO_ddr_vrp;
				inout [53:0]FIXED_IO_mio;
				inout FIXED_IO_ps_clk;
				inout FIXED_IO_ps_porb;
				inout FIXED_IO_ps_srstb;
			«ENDIF»
		'''
	}

	protected def getWires(boolean hasIO) {
		'''
			«IF hasIO»
				wire ap_clk;
				wire ap_rst_n;
				wire start;
				«FOR input : inputs»
					«getWire(input, true)»
				«ENDFOR»
				
				«FOR output : outputs»
					«getWire(output, false)»
				«ENDFOR»
			«ENDIF»
		'''
	}

	protected def getWire(Connection connection, Boolean isWrite) {
		val Integer dataWidth = if (connection.source instanceof Instance)
			connection.sourcePort.type.sizeInBits - 1
		else
			(connection.source as Port).type.sizeInBits - 1
		'''
			wire «TemplateUtils.getQueueNameIO(connection)»«IF isWrite»_full_n«ELSE»_empty_n«ENDIF»;
			wire[«dataWidth»:0]«TemplateUtils.getQueueNameIO(connection)»«IF isWrite»_wr_data«ELSE»_rd_data«ENDIF»;
			wire «TemplateUtils.getQueueNameIO(connection)»«IF isWrite»_wr_en«ELSE»_rd_en«ENDIF»;
			«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || singleDma»
				«IF isWrite»
					wire[31:0] «TemplateUtils.getQueueNameIO(connection)»_size;
				«ENDIF»
				wire[31:0] «TemplateUtils.getQueueNameIO(connection)»_count;
			«ENDIF»
		'''
	}

	protected def getPsIntInstance() {
		'''
			sys_wrapper sys_i
			(
				«IF xilinxDevice.isZynq»
					.DDR_addr(DDR_addr),
					.DDR_ba(DDR_ba),
					.DDR_cas_n(DDR_cas_n),
					.DDR_ck_n(DDR_ck_n),
					.DDR_ck_p(DDR_ck_p),
					.DDR_cke(DDR_cke),
					.DDR_cs_n(DDR_cs_n),
					.DDR_dm(DDR_dm),
					.DDR_dq(DDR_dq),
					.DDR_dqs_n(DDR_dqs_n),
					.DDR_dqs_p(DDR_dqs_p),
					.DDR_odt(DDR_odt),
					.DDR_ras_n(DDR_ras_n),
					.DDR_reset_n(DDR_reset_n),
					.DDR_we_n(DDR_we_n),
					.FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
					.FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
					.FIXED_IO_mio(FIXED_IO_mio),
					.FIXED_IO_ps_clk(FIXED_IO_ps_clk),
					.FIXED_IO_ps_porb(FIXED_IO_ps_porb),
					.FIXED_IO_ps_srstb(FIXED_IO_ps_srstb)
				«ENDIF»
				);
		'''
	}

	protected def getPlCoreInstance() {
		'''
			pl_core_wrapper pl_core_i
			 (
			 	«getModuleIOInterconnection»
			 	.ap_clk(ap_clk),
			 	.ap_rst_n(ap_rst_n),
			 	.start(start));
		'''
	}

	protected def getModuleIOInterconnection() {
		'''
			«FOR input : inputs»
				.«TemplateUtils.getQueueNameIO(input)»_full_n(«TemplateUtils.getQueueNameIO(input)»_full_n),
				.«TemplateUtils.getQueueNameIO(input)»_wr_data(«TemplateUtils.getQueueNameIO(input)»_wr_data),
				.«TemplateUtils.getQueueNameIO(input)»_wr_en(«TemplateUtils.getQueueNameIO(input)»_wr_en),
				«IF TemplateUtils.getConnectionKind(input).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || singleDma»
					.«TemplateUtils.getQueueNameIO(input)»_size(«TemplateUtils.getQueueNameIO(input)»_size),
					.«TemplateUtils.getQueueNameIO(input)»_count(«TemplateUtils.getQueueNameIO(input)»_count),
				«ENDIF»
			«ENDFOR»
			«FOR output : outputs»
				.«TemplateUtils.getQueueNameIO(output)»_empty_n(«TemplateUtils.getQueueNameIO(output)»_empty_n),
				.«TemplateUtils.getQueueNameIO(output)»_rd_data(«TemplateUtils.getQueueNameIO(output)»_rd_data),
				.«TemplateUtils.getQueueNameIO(output)»_rd_en(«TemplateUtils.getQueueNameIO(output)»_rd_en),
				«IF TemplateUtils.getConnectionKind(output).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || singleDma»
					.«TemplateUtils.getQueueNameIO(output)»_count(«TemplateUtils.getQueueNameIO(output)»_count),
				«ENDIF»
			«ENDFOR»
		'''
	}

}
