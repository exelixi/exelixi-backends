/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


package exelixi.zynq.backend.templates.vivado

import exelixi.hls.templates.tclscripts.TCLTemplate
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import java.util.List
import net.sf.orcc.df.Connection

import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING

/**
 * 
 * Block Design Template Utils
 * @author Endri Bezati
 */
class BlockDesignUtils extends TCLTemplate {

	// /////////////////////////////////////////////////////////////////////////
	// -- Create Ports
	static def createBlockDesignPorts(List<Connection> inputs, List<Connection> outputs, String direction, String kind,
		Boolean containsCountSize) {
		'''
			«createPort("ap_rst_n", direction)»
			«createPort("ap_clk", direction)»
			«createPort("start", direction)»
			
			«FOR connection : inputs»
				«createInterfacePort(TemplateUtils.getQueueNameIO(connection), kind,"acc_fifo_write_rtl","1.0")»
				«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || containsCountSize»
					«createPortType(TemplateUtils.getQueueNameIO(connection) + "_count", "I", "data", 31, 0)»
					«createPortType(TemplateUtils.getQueueNameIO(connection) + "_size", "I", "data", 31, 0)»
				«ENDIF»
			«ENDFOR»
			
			«FOR connection : outputs»
				«createInterfacePort(TemplateUtils.getQueueNameIO(connection), kind,"acc_fifo_read_rtl","1.0")»
				«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || containsCountSize»
					«createPortType(TemplateUtils.getQueueNameIO(connection) + "_count", "I", "data", 31, 0)»
				«ENDIF»
			«ENDFOR»
		'''
	}

	static def createPort(String name, String direction) {
		'''create_bd_port -dir «direction» «name»'''
	}

	static def createPort(String name, String direction, String hierarchyName) {
		'''create_bd_pin -dir «direction» «hierarchyName»/«name»'''
	}

	static def createPortType(String name, String direction, String type) {
		'''create_bd_port -dir «direction» -type «type» «name»'''
	}

	static def createPortType(String name, String direction, String type, int vectorFrom, int vectorTo) {
		'''create_bd_port -dir «direction» -from «vectorFrom» -to «vectorTo» -type «type» «name»'''
	}

	static def createPortType(String name, String hierarchy, String direction, String type, int vectorFrom,
		int vectorTo, boolean useHierarchy) {
		'''create_bd_«IF useHierarchy»pin«ELSE»port«ENDIF» -dir «direction» -from «vectorFrom» -to «vectorTo» -type «type» «IF useHierarchy»«hierarchy»/«ENDIF»«name»'''
	}

	static def createPort(String name, String direction, String type, String hierarchyName) {
		'''create_bd_pin -dir «direction» -type «type» «hierarchyName»/«name»'''
	}

	// /////////////////////////////////////////////////////////////////////////
	// -- Get core
	static def getCore(String ipName, String name, String family, String version) {
		'''create_bd_cell -type ip -vlnv xilinx.com:«family»:«ipName»:«version» «name»'''
	}

	static def getCore(String ipName, String name, String family, String version, String hierarchyName) {
		'''create_bd_cell -type ip -vlnv xilinx.com:«family»:«ipName»:«version» «hierarchyName»/«name»'''
	}

	// /////////////////////////////////////////////////////////////////////////
	// -- Interface Port/Pin
	static def createInterfacePort(String name, String mode, String interfaceName, String version) {
		'''create_bd_intf_port -mode «mode» -vlnv xilinx.com:interface:«interfaceName»:«version» «name»'''
	}

	static def createInterfacePort(String name, String mode, String interfaceName, String version,
		String hierarchyName) {
		'''create_bd_intf_pin -mode «mode» -vlnv xilinx.com:interface:«interfaceName»:«version» «hierarchyName»/«name»'''
	}

	// /////////////////////////////////////////////////////////////////////////
	// -- Connection
	static def connectBlockDesignNetHierarchy(String source, String sourcePort, String net, String hierarchyName) {
		'''connect_bd_net [get_bd_pins /«hierarchyName»/«source»/«sourcePort»] [get_bd_pins «hierarchyName»/«net»]'''
	}

	static def connectBlockDesignInterfacePortNet(String sourcePort, String target, String targetPort,
		String hierarchyName) {
		'''connect_bd_intf_net [get_bd_intf_pins «hierarchyName»/«sourcePort»] [get_bd_intf_pins «hierarchyName»/«target»/«targetPort»]'''
	}

	static def connectBlockDesignInterfaceNetPort(String source, String sourcePort, String targetPort,
		String hierarchyName) {
		'''connect_bd_intf_net [get_bd_intf_pins «hierarchyName»/«source»/«sourcePort»] [get_bd_intf_pins «hierarchyName»/«targetPort»]'''
	}

	static def connectBlockDesignInterfaceNet(String source, String sourcePort, String target, String targetPort,
		String hierarchyName) {
		'''connect_bd_intf_net [get_bd_intf_pins «hierarchyName»/«source»/«sourcePort»] [get_bd_intf_pins «hierarchyName»/«target»/«targetPort»]'''
	}

	static def connectBlockDesignNet(String source, String sourcePort, String target, String targetPort,
		String hierarchyName) {
		'''connect_bd_net [get_bd_pins /«hierarchyName»/«source»/«sourcePort»] [get_bd_pins «hierarchyName»/«target»/«targetPort»]'''
	}

	static def connectBlockDesign(String sourcePort, String target, String targetPort, boolean isInterface) {
		'''connect_bd«IF isInterface»_intf«ENDIF»_net [get_bd«IF isInterface»_intf«ENDIF»_pins «sourcePort»] «IF isInterface»-boundary_type upper«ENDIF» [get_bd«IF isInterface»_intf«ENDIF»_pins «target»/«targetPort»]'''
	}

}
