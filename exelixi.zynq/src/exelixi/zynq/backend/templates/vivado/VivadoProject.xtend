/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.vivado

import exelixi.hls.devices.XilinxDevice
import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.tclscripts.TCLTemplate
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import java.util.Map
import net.sf.orcc.df.Network

import static exelixi.hls.backend.Constants.DEVICE
import static exelixi.hls.backend.Constants.EVERY_INSTANCE_AN_IP_CORE
import static exelixi.hls.backend.Constants.NATIVE_IP_CORES
import static exelixi.hls.backend.Constants.NATIVE_IP_CORES_FOLDER
import static exelixi.hls.backend.Constants.RTL_PATH
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION

class VivadoProject extends TCLTemplate {

	private Network network

	private String name

	private XilinxDevice xilinxDevice

	private Map<String, Object> options

	private Partitioner partitioner

	private Boolean everyInstanceAsIPCore
	
	private String rtlPath
	
	private String nativeIPCoresFolder
	
	private boolean useNativeIpCores

	new(Network network, Map<String, Object> options, Partitioner partitioner) {
		this.network = network
		this.options = options
		this.partitioner = partitioner
		this.name = network.simpleName
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		this.everyInstanceAsIPCore = options.get(EVERY_INSTANCE_AN_IP_CORE) as Boolean
		this.rtlPath = options.get(RTL_PATH) as String
		this.useNativeIpCores = options.getOrDefault(NATIVE_IP_CORES, false) as Boolean
		if(useNativeIpCores){
			this.nativeIPCoresFolder = options.get(NATIVE_IP_CORES_FOLDER) as String
		}
	}

	def getContent() {
		val SysBlockDesign psIntBd = new SysBlockDesign(network, options, partitioner)
		val PlCoreBlockDesign plCore = new PlCoreBlockDesign(network, options, partitioner)

		'''
			«getFileHeader("Vivado Project TCL, With Board and IP Core configuration")»
			
			«createProject()»
			
			«ipRepoPaths()»
			
			# -- Create Block Design
			create_bd_design "sys"
			
			«IF partitioner.containsPartition(ZYNQ_PL_PARTITION)»
				## ----------------------------------------------------------------------------
				## ----------------------------------------------------------------------------
				## -- PL Core Block Design
				«plCore.content»
			«ENDIF»
			
			## ----------------------------------------------------------------------------
			## ----------------------------------------------------------------------------
			## -- System Block Design
			«psIntBd.content»
			
			«addTop»
			
			«launchImplementation()»
			
			«exportBitstream()»
			# -- EOF
		'''
	}

	private def createProject() {
		'''
			# -- Create Project «name»
			create_project «network.simpleName» vivado_project -part «xilinxDevice.fpga»
			«IF !xilinxDevice.board.empty»set_property board_part «xilinxDevice.board» [current_project]«ENDIF»
		'''
	}

	private def ipRepoPaths() {
		'''
			«IF partitioner.containsPartition(ZYNQ_PL_PARTITION)»
				# -- Add Vivado HLS IP Core
				«IF !everyInstanceAsIPCore»
					set_property  ip_repo_paths «TemplateUtils.getIpPath(name)» [current_project]
				«ELSE»
					set_property ip_repo_paths { ip_cores «IF useNativeIpCores»«nativeIPCoresFolder» «ENDIF»} [current_project]
				«ENDIF»
				update_ip_catalog
			«ENDIF»
		'''
	}
	
	private def addTop(){
		'''
			# -- Add TOP RTL for interconnecting PS & Interfaces and the PL core
			add_files -norecurse {«TemplatesUtils.getPath(rtlPath)»/«network.simpleName»_top.v}
			set_property top «network.simpleName»_top [current_fileset]
			update_compile_order -fileset sources_1
		'''
	}

	private def launchImplementation() {
		'''
			# -- Launch Implementation
			launch_runs impl_1 -jobs 4 -to_step write_bitstream
			wait_on_run impl_1
		'''
	}
	
	private def exportBitstream(){
		'''
			# -- Export BitStream
			file mkdir vivado_project/«network.simpleName».sdk
			file copy -force vivado_project/«network.simpleName».runs/impl_1/«network.simpleName»_top.sysdef vivado_project/«network.simpleName».sdk/«network.simpleName»_top.hdf
		'''
	}

}
