/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.vivado

import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.tclscripts.TCLTemplate
import exelixi.hls.util.MathUtil
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Set
import java.util.TreeMap
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port
import net.sf.orcc.ir.Var

import static exelixi.hls.backend.Constants.ACTOR_ACTION_SELECTION_PEEKS
import static exelixi.hls.backend.Constants.INSTANCE_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.NETWORK_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.VARIABLE_BASE_ADDRESS
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING
import static net.sf.orcc.OrccLaunchConstants.DEFAULT_FIFO_SIZE
import static net.sf.orcc.OrccLaunchConstants.FIFO_SIZE

/**
 * PL Core Block Design
 * 
 * @author Endri Bezati
 */
class PlCoreBlockDesign extends TCLTemplate {
	
	private Network network
	
	/**
	 * Input Connections
	 */
	private List<Connection> inputs
	
	/**
	 * Output Connections
	 */
	private List<Connection> outputs
	
	/**
	 * Instances
	 */
	private List<Instance> instances
	
	/**
	 * Connections
	 */
	private List<Connection> connections
	
	/**
	 * Backend Options
	 */
	private Map<String, Object> options
	
	/**
	 * Hierarchy name
	 */
	
	private String hierarchyName
	
	/**
	 * The default FIFO Size
	 */
	private Integer fifoSize
	
	private List<String> fanoutCoreNames = new ArrayList
	
	private Map<Connection, List<Connection>> fanoutConnections
	
	private Partitioner partitioner
	
	private String blockDesignName
	
	private boolean singleDma
	
	private Map<Instance, List<Var>> instancesExternalVariablesMap
	
	new(Network network, Map<String, Object> options, Partitioner partitioner){
		this.network = network
		this.options = options
		this.inputs = new ArrayList<Connection>(partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION).keySet)
		this.outputs = new ArrayList<Connection>(partitioner.getConnectionToConnectionBd(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION).keySet)
		this.instances = partitioner.getInstances(ZYNQ_PL_PARTITION)
		this.connections = partitioner.getConnections(ZYNQ_PL_PARTITION, ZYNQ_PL_PARTITION)
		this.partitioner = partitioner
		hierarchyName = network.simpleName + "_pl"
		blockDesignName = "pl_core"
		if (options.containsKey(FIFO_SIZE)) {
			fifoSize = options.get(FIFO_SIZE) as Integer 
		} else {
			fifoSize = DEFAULT_FIFO_SIZE
		}
		
		fanoutConnections = partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION);
		for (Connection connection : fanoutConnections.keySet()) {
			if (fanoutConnections.get(connection).size() > 1) {
				val String fanoutName = "fanout_" + TemplateUtils.getQueueNameIO(connection);
				fanoutCoreNames.add(fanoutName)
			}
		}
		
		// -- Communication Kind
		singleDma = false;
		val String communicationKind = options.get(ZYNQ_OPTION_COMMUNICATION_KIND) as String
		if (communicationKind.equals("Single DMA")){
			singleDma = true;
		}
		
		// -- External Variables
		if(network.hasAttribute(NETWORK_EXTERNAL_VARIABLES)){
			instancesExternalVariablesMap = network.getAttribute(NETWORK_EXTERNAL_VARIABLES).objectValue as TreeMap<Instance, List<Var>>
		}else{
			instancesExternalVariablesMap = new TreeMap
		}
		
	}
	
	def getContent(){
		'''
			create_bd_cell -type hier "«hierarchyName»"
			
			«createPorts(true)»
			
			«ipCores»
			
			«connectCores»
		'''
	}
	

	///////////////////////////////////////////////////////////////////////////
	// -- Create Ports
	private def createPorts(Boolean useHierarchy) {
		'''
			«createPort("ap_rst_n", "I", "rst", useHierarchy)»
			«createPort("ap_clk", "I", "clk", useHierarchy)»
			«createPort("start", "I", useHierarchy)»
			
			«FOR connection : inputs»
				«createInterfacePort(TemplateUtils.getQueueNameIO(connection), "Slave","acc_fifo_write_rtl","1.0", useHierarchy)»
				«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || singleDma»
					«BlockDesignUtils.createPortType(TemplateUtils.getQueueNameIO(connection) + "_count", hierarchyName, "O", "data", 31, 0, useHierarchy)»
					«BlockDesignUtils.createPortType(TemplateUtils.getQueueNameIO(connection) + "_size", hierarchyName, "O", "data", 31, 0, useHierarchy)»
				«ENDIF»
			«ENDFOR»
			
			«FOR connection : outputs»
				«createInterfacePort(TemplateUtils.getQueueNameIO(connection),"Slave","acc_fifo_read_rtl","1.0", useHierarchy)»
				«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || singleDma»
					«BlockDesignUtils.createPortType(TemplateUtils.getQueueNameIO(connection) + "_count", hierarchyName, "O", "data", 31, 0, useHierarchy)»
				«ENDIF»
			«ENDFOR»
			
			«FOR instance : instancesExternalVariablesMap.keySet»
				«FOR variable : instancesExternalVariablesMap.get(instance)»
					«createInterfacePort("m_axi_id_" + instance.number + "_" + variable.name,"Master","aximm_rtl","1.0", useHierarchy)»
				«ENDFOR»
			«ENDFOR»
		'''
	}
	
	protected def connectHierarchy(){
		'''
			# -- Connect clock, reset and start
			«connectBlockDesign("ap_clk", hierarchyName, "ap_clk", false)»
			«connectBlockDesign("ap_rst_n", hierarchyName, "ap_rst_n", false)»
			«connectBlockDesign("start", hierarchyName, "start", false)»
			
			# -- Cores Handshake and hierarchy I/O
			«FOR connection : inputs»
				«connectBlockDesign(TemplateUtils.getQueueNameIO(connection), hierarchyName, TemplateUtils.getQueueNameIO(connection), true)»
				«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || singleDma»
					«connectBlockDesign(TemplateUtils.getQueueNameIO(connection) + "_count", hierarchyName, TemplateUtils.getQueueNameIO(connection) + "_count", false)»
					«connectBlockDesign(TemplateUtils.getQueueNameIO(connection) + "_size", hierarchyName, TemplateUtils.getQueueNameIO(connection) + "_size", false)»
				«ENDIF»
			«ENDFOR»
			
			«FOR connection : outputs»
				«connectBlockDesign(TemplateUtils.getQueueNameIO(connection), hierarchyName, TemplateUtils.getQueueNameIO(connection), true)»
				«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || singleDma»
					«connectBlockDesign(TemplateUtils.getQueueNameIO(connection) + "_count", hierarchyName, TemplateUtils.getQueueNameIO(connection) + "_count", false)»
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// -- Get IP Cores
	private def ipCores(){
		'''
			«IF !fanoutCoreNames.empty»
				## -- Input Connection Fanout Cores
				«FOR fanoutCore : fanoutCoreNames»
					«getCore(fanoutCore, fanoutCore, "hls", "1.0", true)»
				«ENDFOR»
				
				«FOR fanoutConnection : fanoutConnections.keySet»
					«IF fanoutConnections.get(fanoutConnection).size > 1»
						«FOR connection : fanoutConnections.get(fanoutConnection)»
							«getQueueCore(connection,false)»
						«ENDFOR»
					«ENDIF»
				«ENDFOR»
				
			«ENDIF»
			
			## -- Instances Cores
			«FOR instance : instances»
				«getCore(TemplatesUtils.getInstanceName(instance), TemplatesUtils.getInstanceName(instance), "hls", "1.0", true)»
				«IF instance.getActor.hasAttribute(INSTANCE_EXTERNAL_VARIABLES)»
					«FOR variable : instance.getActor.getAttribute(INSTANCE_EXTERNAL_VARIABLES).objectValue  as ArrayList<Var>»
						set_property -dict [list CONFIG.C_M_AXI_ID_«instance.number»_«variable.name.toUpperCase»_TARGET_ADDR {«String.format("0x%08X", (variable.getAttribute(VARIABLE_BASE_ADDRESS).objectValue as Integer))»}] [get_bd_cells «hierarchyName»/«TemplatesUtils.getInstanceName(instance)»]
						«connectBlockDesignInterfaceNetPort(TemplatesUtils.getInstanceName(instance), "m_axi_id_" + instance.number + "_" + variable.name, "m_axi_id_" + instance.number + "_" + variable.name)»
					«ENDFOR»
				«ENDIF»
			«ENDFOR»
			
			## -- Exelixi FIFO Queue Cores
			«FOR connection : inputs»
				«getQueueCore(connection,true)»
			«ENDFOR»
			«FOR connection : outputs»
				«getQueueCore(connection,true)»
			«ENDFOR»
			«FOR connection : connections»
				«getQueueCore(connection,false)»
			«ENDFOR»
		'''
	}
	
	
	private def getQueueCore(Connection connection, boolean isBc) {
		val String name = if (!isBc) TemplateUtils.getQueueName(connection) else TemplateUtils.getQueueNameIO(connection)
		val Integer data_width = if (connection.source instanceof Instance)
			connection.sourcePort.type.sizeInBits
		else
				(connection.source as Port).type.sizeInBits
		val Integer addr_width = MathUtil.log2_ceil(if(connection.size === null) fifoSize else connection.size)
		val Integer depth = if(connection.size === null) fifoSize else connection.size
		'''
			startgroup
			create_bd_cell -type ip -vlnv Exelixi:user:Exelixi_FIFO:1.0 «hierarchyName»/«name»
			set_property -dict [list CONFIG.MEM_STYLE {auto} CONFIG.DATA_WIDTH {«data_width»} CONFIG.ADDR_WIDTH {«addr_width»} CONFIG.DEPTH {«depth»}] [get_bd_cells «hierarchyName»/«name»]
			endgroup
		'''
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// -- Get Connections
	
	private def connectCores(){
		'''
			## -- -------------------------------------------------------------------------
			## -- Connect Clocks
			«FOR instance : instances»
				«connectBlockDesignNetHierarchy(TemplatesUtils.getInstanceName(instance), "ap_clk", "ap_clk")»
			«ENDFOR»
			
			«IF !fanoutCoreNames.empty»
				«FOR fanoutCore : fanoutCoreNames»
					«connectBlockDesignNetHierarchy(fanoutCore, "ap_clk", "ap_clk")»
				«ENDFOR»
				«FOR fanoutConnection : fanoutConnections.keySet»
					«IF fanoutConnections.get(fanoutConnection).size > 1»
						«FOR connection : fanoutConnections.get(fanoutConnection)»
							«connectBlockDesignNetHierarchy(TemplateUtils.getQueueName(connection), "clk", "ap_clk")»
						«ENDFOR»
					«ENDIF»
				«ENDFOR»
			«ENDIF»
			
			«FOR connection : inputs»
				«connectBlockDesignNetHierarchy(TemplateUtils.getQueueNameIO(connection), "clk", "ap_clk")»
			«ENDFOR»
			«FOR connection : connections»
				«connectBlockDesignNetHierarchy(TemplateUtils.getQueueName(connection), "clk", "ap_clk")»
			«ENDFOR»
			«FOR connection : outputs»
				«connectBlockDesignNetHierarchy(TemplateUtils.getQueueNameIO(connection), "clk", "ap_clk")»
			«ENDFOR»
			
			## -- -------------------------------------------------------------------------
			## -- Connect Resets
			«FOR instance : instances»
				«connectBlockDesignNetHierarchy(TemplatesUtils.getInstanceName(instance),"ap_rst_n", "ap_rst_n")»
			«ENDFOR»
			
			«IF !fanoutCoreNames.empty»
				«FOR fanoutCore : fanoutCoreNames»
					«connectBlockDesignNetHierarchy(fanoutCore, "ap_rst_n", "ap_rst_n")»
				«ENDFOR»
				«FOR fanoutConnection : fanoutConnections.keySet»
					«IF fanoutConnections.get(fanoutConnection).size > 1»
						«FOR connection : fanoutConnections.get(fanoutConnection)»
							«connectBlockDesignNetHierarchy(TemplateUtils.getQueueName(connection), "reset_n", "ap_rst_n")»
						«ENDFOR»
					«ENDIF»
				«ENDFOR»
			«ENDIF»
			
			«FOR connection : inputs»
				«connectBlockDesignNetHierarchy(TemplateUtils.getQueueNameIO(connection), "reset_n", "ap_rst_n")»
			«ENDFOR»
			«FOR connection : connections»
				«connectBlockDesignNetHierarchy(TemplateUtils.getQueueName(connection), "reset_n", "ap_rst_n")»
			«ENDFOR»
			«FOR connection : outputs»
				«connectBlockDesignNetHierarchy(TemplateUtils.getQueueNameIO(connection), "reset_n", "ap_rst_n")»
			«ENDFOR»
			
			## -- -------------------------------------------------------------------------
			## -- Connect Start
			«FOR instance : instances»
				«connectBlockDesignNetHierarchy(TemplatesUtils.getInstanceName(instance), "ap_start", "start")»
			«ENDFOR»
			
			«IF !fanoutCoreNames.empty»
				«FOR fanoutCore : fanoutCoreNames»
					«connectBlockDesignNetHierarchy(fanoutCore, "ap_start", "start")»
				«ENDFOR»
			«ENDIF»
			
			## -- -------------------------------------------------------------------------
			## -- Connect Input Connections
			«IF !inputs.empty» 
				## -- Input Connections
				«FOR connection : inputs»
					«IF fanoutConnections.get(connection).size > 1»
						«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || singleDma»
							«connectBlockDesignNet(TemplateUtils.getQueueNameIO(connection) + "_count", TemplateUtils.getQueueNameIO(connection), "count")»
							«connectBlockDesignNet(TemplateUtils.getQueueNameIO(connection) + "_size", TemplateUtils.getQueueNameIO(connection), "size")»
						«ENDIF»
						«connectBlockDesignInterfacePortNet(TemplateUtils.getQueueNameIO(connection), TemplateUtils.getQueueNameIO(connection), "WRITE")»
						«connectBlockDesignInterfaceNet(TemplateUtils.getQueueNameIO(connection), "READ","fanout_" + TemplateUtils.getQueueNameIO(connection), "IN_V")»
						«FOR fanoutConnection : fanoutConnections.get(connection)»
							«connectBlockDesignInterfaceNet("fanout_" + TemplateUtils.getQueueNameIO(connection), "OUT_" + fanoutConnections.get(connection).indexOf(fanoutConnection) + "_V", TemplateUtils.getQueueName(fanoutConnection),"WRITE")»
						«ENDFOR»
					«ELSE»
						«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || singleDma»
							«connectBlockDesignNet(TemplateUtils.getQueueNameIO(connection) + "_count", TemplateUtils.getQueueNameIO(connection), "count")»
							«connectBlockDesignNet(TemplateUtils.getQueueNameIO(connection) + "_size", TemplateUtils.getQueueNameIO(connection), "size")»
						«ENDIF»
						«connectBlockDesignInterfacePortNet(TemplateUtils.getQueueNameIO(connection), TemplateUtils.getQueueNameIO(connection),"WRITE")»
						«connectBlockDesignInterfaceNet(TemplateUtils.getQueueNameIO(connection), "READ", TemplatesUtils.getInstanceName(connection.target as Instance) , connection.targetPort.name + "_V")»
					«ENDIF»
				«ENDFOR»
			«ENDIF»
			«IF !outputs.empty» 
				## -- Output Connections
				«FOR connection : outputs»
					«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING) || singleDma»
						«connectBlockDesignNet(TemplateUtils.getQueueNameIO(connection) + "_count", TemplateUtils.getQueueNameIO(connection), "count")»
					«ENDIF»
					«connectBlockDesignInterfaceNetPort(TemplateUtils.getQueueNameIO(connection), "READ", TemplateUtils.getQueueNameIO(connection))»
				«ENDFOR»
			«ENDIF»
			## -- -------------------------------------------------------------------------
			## -- Instances connection
			«FOR instance : instances»
				«connectInstance(instance)»
			«ENDFOR»
			
		'''
	}
	
	private def connectInstance(Instance instance) {
		val Map<Port, Connection> incoming = instance.incomingPortMap
		val List<Port> inputs = instance.getActor.inputs
		val List<Port> outputs = instance.getActor.outputs
		val Set<Port> portsWithCount = TemplatesUtils.getPortsWithCount(instance)
		
		var Map<Port, Integer> portReaders = new HashMap
		var Map<Port, List<Connection> > portConnectionMap = new HashMap
		
		var Boolean isPlPsBorderInstance
		
		isPlPsBorderInstance = false
		var boolean portAlreadyConnectedToPS = false 
		
		for (port : instance.outgoingPortMap.keySet) {
			var int nbReaders = 0
			var List<Connection> connections = new ArrayList
			for (connection : instance.outgoingPortMap.get(port)) {
				val Instance targetInstance = connection.target as Instance
				if (partitioner.isInstanceOnPartition(targetInstance, ZYNQ_PS_PARTITION)) {
					if (!portAlreadyConnectedToPS) {
						isPlPsBorderInstance = true
						portAlreadyConnectedToPS = true
						connections.add(connection)
						nbReaders++
					}
				} else {
					connections.add(connection)
					nbReaders++
				}
			}
			portAlreadyConnectedToPS = false 
			portConnectionMap.put(port, connections)
			portReaders.put(port, nbReaders)
		}
		var Map<Port, List<Integer>> peekIndexes = new HashMap
		if(instance.getActor().hasAttribute(ACTOR_ACTION_SELECTION_PEEKS)){
			peekIndexes = instance.getActor().getValueAsObject(ACTOR_ACTION_SELECTION_PEEKS)
		}
		
		'''
			«FOR port : inputs»
				«IF !fanoutConnections.containsKey(incoming.get(port))»
					«connectBlockDesignInterfaceNet(TemplateUtils.getQueueName(incoming.get(port)), "READ", TemplatesUtils.getInstanceName(instance),port.name + "_V")»
					«IF portsWithCount.contains(port)»
						«connectBlockDesignNet(TemplateUtils.getQueueName(incoming.get(port)), "count", TemplatesUtils.getInstanceName(instance), port.name + "_count")»
					«ENDIF»
					«IF peekIndexes.containsKey(port)»
						«FOR i : peekIndexes.get(port)»
							«connectBlockDesignNet(TemplateUtils.getQueueName(incoming.get(port)), "peek" + if (i > 0) "_" + i else "", TemplatesUtils.getInstanceName(instance), port.name + if (i > 0) "_" + i else "" + "_peek")»
						«ENDFOR»
					«ENDIF»
				«ELSE»
					«IF fanoutConnections.get(incoming.get(port)).size > 1»
						«connectBlockDesignInterfaceNet(TemplateUtils.getQueueName(incoming.get(port)), "READ", TemplatesUtils.getInstanceName(instance),port.name + "_V")»
						«IF portsWithCount.contains(port)»
							«connectBlockDesignNet(TemplateUtils.getQueueName(incoming.get(port)), "count", TemplatesUtils.getInstanceName(instance), port.name + "_count")»
						«ENDIF»
						«IF peekIndexes.containsKey(port)»
							«FOR i : peekIndexes.get(port)»
								«connectBlockDesignNet(TemplateUtils.getQueueName(incoming.get(port)), "peek" + if (i > 0) "_" + i else "", TemplatesUtils.getInstanceName(instance), port.name + if (i > 0) "_" + i else "" + "_peek")»
							«ENDFOR»
						«ENDIF»
					«ELSE»
						«IF portsWithCount.contains(port)»
							«connectBlockDesignNet(TemplateUtils.getQueueNameIO(incoming.get(port)), "count", TemplatesUtils.getInstanceName(instance), port.name + "_count")»
						«ENDIF»
						«IF peekIndexes.containsKey(port)»
							«FOR i : peekIndexes.get(port)»
								«connectBlockDesignNet(TemplateUtils.getQueueNameIO(incoming.get(port)), "peek" + if (i > 0) "_" + i else "", TemplatesUtils.getInstanceName(instance), port.name + if (i > 0) "_" + i else "" + "_peek")»
							«ENDFOR»
						«ENDIF»
					«ENDIF»
				«ENDIF»	
			«ENDFOR»
			
			«FOR port : outputs»
				«FOR connection : portConnectionMap.get(port)»
					«IF this.outputs.contains(connection)»
						«connectBlockDesignInterfaceNet(TemplatesUtils.getInstanceName(instance), port.name + (if(portConnectionMap.get(port).size > 1) "_" + portConnectionMap.get(port).indexOf(connection) else "" ) + "_V", TemplateUtils.getQueueNameIO(connection), "WRITE")»
						«IF portsWithCount.contains(port)»
							«connectBlockDesignNet(TemplateUtils.getQueueNameIO(connection), "count", TemplatesUtils.getInstanceName(instance), port.name + (if(portConnectionMap.get(port).size > 1) "_" + portConnectionMap.get(port).indexOf(connection) else "" ) + "_count")»
							«connectBlockDesignNet(TemplateUtils.getQueueNameIO(connection), "size", TemplatesUtils.getInstanceName(instance), port.name + (if(portConnectionMap.get(port).size > 1) "_" + portConnectionMap.get(port).indexOf(connection) else "" )  + "_size")»
						«ENDIF»
					«ELSE»
						«connectBlockDesignInterfaceNet(TemplatesUtils.getInstanceName(instance), port.name + (if(portConnectionMap.get(port).size > 1) "_" + portConnectionMap.get(port).indexOf(connection) else "" ) + "_V", TemplateUtils.getQueueName(connection), "WRITE")»
						«IF portsWithCount.contains(port)»
							«connectBlockDesignNet(TemplateUtils.getQueueName(connection), "count", TemplatesUtils.getInstanceName(instance), port.name + (if(portConnectionMap.get(port).size > 1) "_" + portConnectionMap.get(port).indexOf(connection) else "" ) + "_count")»
							«connectBlockDesignNet(TemplateUtils.getQueueName(connection), "size", TemplatesUtils.getInstanceName(instance), port.name + (if(portConnectionMap.get(port).size > 1) "_" + portConnectionMap.get(port).indexOf(connection) else "" )  + "_size")»
						«ENDIF»
					«ENDIF»
				«ENDFOR»
			«ENDFOR»
		'''
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// -- Helper methods
	
	private def createInterfacePort(String name, String mode, String interfaceName, String version, Boolean useHierarchy){
		'''create_bd_intf_«IF useHierarchy»pin«ELSE»port«ENDIF» -mode «mode» -vlnv xilinx.com:interface:«interfaceName»:«version» «IF useHierarchy»«hierarchyName»/«ENDIF»«name»'''	
	}
	
	private def createPort(String name, String direction, String type, Boolean useHierarchy){
		'''create_bd_«IF useHierarchy»pin«ELSE»port«ENDIF» -dir «direction» -type «type» «IF useHierarchy»«hierarchyName»/«ENDIF»«name»'''
	}
	
	private def createPort(String name, String direction, Boolean useHierarchy){
		'''create_bd_«IF useHierarchy»pin«ELSE»port«ENDIF» -dir «direction» «IF useHierarchy»«hierarchyName»/«ENDIF»«name»'''
	}
	
	
	private def getCore(String ipName, String name, String family, String version, Boolean useHierarchy){
		'''create_bd_cell -type ip -vlnv xilinx.com:«family»:«ipName»:«version» «IF useHierarchy»«hierarchyName»/«ENDIF»«name»'''
	}
	
	private def connectBlockDesignNetHierarchy(String source, String sourcePort, String net) {
		'''connect_bd_net [get_bd_pins /«hierarchyName»/«source»/«sourcePort»] [get_bd_pins «hierarchyName»/«net»]'''
	}
	
	private def connectBlockDesignInterfacePortNet(String sourcePort, String target, String targetPort) {
		'''connect_bd_intf_net [get_bd_intf_pins «hierarchyName»/«sourcePort»] [get_bd_intf_pins «hierarchyName»/«target»/«targetPort»]'''
	}
	
	private def connectBlockDesignInterfaceNetPort(String source, String sourcePort, String targetPort) {
		'''connect_bd_intf_net [get_bd_intf_pins «hierarchyName»/«source»/«sourcePort»] [get_bd_intf_pins «hierarchyName»/«targetPort»]'''
	}
	
	private def connectBlockDesignInterfaceNet(String source, String sourcePort, String target, String targetPort) {
		'''connect_bd_intf_net [get_bd_intf_pins «hierarchyName»/«source»/«sourcePort»] [get_bd_intf_pins «hierarchyName»/«target»/«targetPort»]'''
	}
	
	private def connectBlockDesignNet(String source, String sourcePort,  String target, String targetPort) {
		'''connect_bd_net [get_bd_pins /«hierarchyName»/«source»/«sourcePort»] [get_bd_pins «hierarchyName»/«target»/«targetPort»]'''
	}
	
	private def connectBlockDesignNet(String sourcePort,  String target, String targetPort) {
		'''connect_bd_net [get_bd_pins /«hierarchyName»/«sourcePort»] [get_bd_pins «hierarchyName»/«target»/«targetPort»]'''
	}
	
	private def connectBlockDesign(String sourcePort, String target, String targetPort, boolean isInterface) {
		'''connect_bd«IF isInterface»_intf«ENDIF»_net [get_bd«IF isInterface»_intf«ENDIF»_pins «sourcePort»] «IF isInterface»-boundary_type upper«ENDIF» [get_bd«IF isInterface»_intf«ENDIF»_pins «target»/«targetPort»]'''
	}
	
}
