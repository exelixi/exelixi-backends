/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.vivado.boardsproperties.zynqultrascale

import exelixi.zynq.backend.templates.vivado.boardsproperties.PSProperty

/**
 * @author Endri Bezati
 */
class ZynqUltraBoardPreset {
	
	// -- Board Property
	private PSProperty boardProperty

	// -- PS7 Xilinx IP Core version
	private String psUltraVersion = "2.0"
	
	// -- PS-PL Interfaces
	// -- Full Power Domain Masters
	private boolean useMasterFPD_HPM0 = false
	private boolean useMasterFPD_HPM1 = false
	
	// -- Low Power Domain Master
	private boolean useMasterLPD_HPM0 = false
	
	// -- Full Power Domain Slaves HPC
	private boolean useSlaveFPD_HPC0 = false
	private boolean useSlaveFPD_HPC1 = false
	
	// -- Full Power Domain Slaves HP
	private boolean useSlaveFPD_HP0 = false
	private boolean useSlaveFPD_HP1 = false
	private boolean useSlaveFPD_HP2 = false
	private boolean useSlaveFPD_HP3 = false
	
	// -- Low Power Domain Slave
	private boolean useSlaveLPD = false
	
	// -- ACP Slave
	private boolean useSlaveACP = false
	
	// -- ACE Slave
	private boolean useSlaveACE = false
	
	// -- PS Clock
	private String psClock = "1200"

	// -- PL Clock
	private String plClock = "200"

	// -- Enable Interrupt
	private boolean enablePLPSInterrupt = false
	
	new(PSProperty boardProperty) {
		this.boardProperty = boardProperty
	}
	
	// -- Methods
	public def setPsClock(Double frequency){
		psClock = frequency.toString
	}

	public def setPlClock(Double frequency){
		plClock = frequency.toString
	}
	
	public def setMasterFPD_HPM0(boolean flag) {
		this.useMasterFPD_HPM0 = flag
	}
	
	public def getMasterFPD_HPM0() {
		return this.useMasterFPD_HPM0
	}
	
	public def setMasterFPD_HPM1(boolean flag) {
		this.useMasterFPD_HPM1 = flag
	}
	
	public def getMasterFPD_HPM1() {
		return this.useMasterFPD_HPM1
	}
	
	public def setMasterLPD_HPM0(boolean flag) {
		this.useMasterLPD_HPM0 = flag
	}
	
	public def getMasterLPD_HPM0() {
		return this.useMasterLPD_HPM0
	}
	
	public def setSlaveFPD_HPC0(boolean flag) {
		this.useSlaveFPD_HPC0 = flag
	}
	
	public def getSlaveFPD_HPC0() {
		return this.useSlaveFPD_HPC0
	}
	
	public def setSlaveFPD_HPC1(boolean flag) {
		this.useSlaveFPD_HPC1 = flag
	}
	
	public def getSlaveFPD_HPC1() {
		return this.useSlaveFPD_HPC1
	}
	
	public def setSlaveFPD_HP0(boolean flag) {
		this.useSlaveFPD_HP0 = flag
	}
	
	public def getSlaveFPD_HP0() {
		return this.useSlaveFPD_HP0
	}
	
	public def setSlaveFPD_HP1(boolean flag) {
		this.useSlaveFPD_HP1 = flag
	}
	
	public def getSlaveFPD_HP1() {
		return this.useSlaveFPD_HP1
	}
	
	public def setSlaveFPD_HP2(boolean flag) {
		this.useSlaveFPD_HP2 = flag
	}
	
	public def getSlaveFPD_HP2() {
		return this.useSlaveFPD_HP2
	}
	
	public def setSlaveFPD_HP3(boolean flag) {
		this.useSlaveFPD_HP3 = flag
	}
	
	public def getSlaveFPD_HP3() {
		return this.useSlaveFPD_HP3
	}
	
	public def setSlaveLPD(boolean flag) {
		this.useSlaveLPD = flag
	}
	
	public def getSlaveLPD() {
		return this.useSlaveLPD
	}
	
	public def setSlaveACE(boolean flag) {
		this.useSlaveACE = flag
	}
	
	public def getSlaveACE() {
		return this.useSlaveACE
	}
	
	public def setSlaveACP(boolean flag) {
		this.useSlaveACP = flag
	}
	
	public def getSlaveACP() {
		return this.useSlaveACP
	}
	
	public def getPreset() {
		'''
			set sys_ps [ create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:«psUltraVersion» sys_ps ]
			«boardProperty.property»
			
			«IF useMasterFPD_HPM0»
				set_property -dict [list CONFIG.PSU__USE__M_AXI_GP0 {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__M_AXI_GP0 {0}] $sys_ps
			«ENDIF»
			«IF useMasterFPD_HPM1»
				set_property -dict [list CONFIG.PSU__USE__M_AXI_GP1 {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__M_AXI_GP1 {0}] $sys_ps
			«ENDIF»
			«IF useMasterLPD_HPM0»
				set_property -dict [list CONFIG.PSU__USE__M_AXI_GP2 {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__M_AXI_GP2 {0}] $sys_ps
			«ENDIF»
			«IF useSlaveFPD_HPC0»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP0 {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP0 {0}] $sys_ps
			«ENDIF»
			«IF useSlaveFPD_HPC1»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP1 {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP1 {0}] $sys_ps
			«ENDIF»
			«IF useSlaveFPD_HP0»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP2 {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP2 {0}] $sys_ps
			«ENDIF»
			«IF useSlaveFPD_HP1»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP3 {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP3 {0}] $sys_ps
			«ENDIF»
			«IF useSlaveFPD_HP2»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP4 {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP4 {0}] $sys_ps
			«ENDIF»
			«IF useSlaveFPD_HP3»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP5 {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP5 {0}] $sys_ps
			«ENDIF»
			«IF useSlaveLPD»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP6 {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_GP6 {0}] $sys_ps
			«ENDIF»
			«IF useSlaveACP»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_ACP {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_ACP {0}] $sys_ps
			«ENDIF»
			«IF useSlaveACE»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_ACE {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PSU__USE__S_AXI_ACE {0}] $sys_ps
			«ENDIF»
			
			# -- Clock Configuration
			# -- PS Clock
			set_property -dict [list CONFIG.PSU__CRF_APB__ACPU_CTRL__FREQMHZ {«psClock»}] $sys_ps
			# -- PL Clock
			set_property -dict [list CONFIG.PSU__CRL_APB__PL0_REF_CTRL__FREQMHZ {«plClock»}] $sys_ps
			
			# -- Interrupt
			«IF enablePLPSInterrupt»
				set_property -dict [list CONFIG.PSU__USE__IRQ0 {1}] [get_bd_cells sys_ps]
			«ENDIF»
		'''
	}
	
	
}
