/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.vivado.boardsproperties.zynqps7

import exelixi.hls.partitioner.Partitioner
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import exelixi.zynq.backend.templates.vivado.BlockDesignUtils
import exelixi.zynq.backend.templates.vivado.boardsproperties.PSProperty
import exelixi.zynq.backend.templates.vivado.boardsproperties.SysPsTemplate
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.TreeMap
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.ir.Var

import static exelixi.hls.backend.Constants.CLOCK_PERIOD
import static exelixi.hls.backend.Constants.CLOCK_PERIOD_VALUE
import static exelixi.hls.backend.Constants.NETWORK_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING
import static net.sf.orcc.OrccLaunchConstants.DEFAULT_FIFO_SIZE
import static net.sf.orcc.OrccLaunchConstants.FIFO_SIZE

class SysPS7 implements SysPsTemplate{

	private Network network

	private Map<String, Object> options

	private Partitioner partitioner

	private PSProperty boardProperty

	private List<Connection> plToPsFifos

	private List<Connection> psToPlFifos

	private double plFrequency

	private String name

	private int fifoSize

	private List<String> gp0Interconnect

	private List<String> hp0Interconnect

	private List<String> acpInterconnect
	
	private List<String> concatInterrupts

	private boolean hp1Required

	private Map<Connection, String> axiWrappersNames

	private Map<Connection, String> axiInterfaceNames
	
	private Boolean singleDMAInterface

	private Map<Instance, List<Var>> instancesExternalVariablesMap

	new(Network network, Map<String, Object> options, Partitioner partitioner, PSProperty psProperty) {
		this.network = network
		this.options = options
		this.partitioner = partitioner
		this.boardProperty = psProperty

		this.name = network.simpleName + "_pl"
		this.hp1Required = false

		// -- PL Frequency
		var float clockPeriod = 10
		if (options.get(CLOCK_PERIOD) as Boolean) {
			try {
				clockPeriod = Float.parseFloat(options.get(CLOCK_PERIOD_VALUE) as String)
			} catch (NumberFormatException e) {
				clockPeriod = 10
			}
		}

		plFrequency = (1 / (clockPeriod * Math.pow(10, -9))).intValue / 1000 / 1000

		// -- Fifo Size
		if (options.containsKey(FIFO_SIZE)) {
			fifoSize = options.get(FIFO_SIZE) as Integer
		} else {
			fifoSize = DEFAULT_FIFO_SIZE
		}

		// -- PS/PL IO
		this.psToPlFifos = new ArrayList<Connection>(
			partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION).keySet)
		this.plToPsFifos = new ArrayList<Connection>(
			partitioner.getConnectionToConnectionBd(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION).keySet)

		// -- Communication Kind
		singleDMAInterface = false;
		val String communicationKind = options.get(ZYNQ_OPTION_COMMUNICATION_KIND) as String
		if (communicationKind.equals("Single DMA")){
			singleDMAInterface = true;
		}


		// -- AXIS Wrappers Names
		axiWrappersNames = new HashMap
		if(!singleDMAInterface){
			for (connection : psToPlFifos) {
				axiWrappersNames.put(connection, "AxisInput_" + TemplateUtils.getQueueNameIO(connection))
			}
			for (connection : plToPsFifos) {
				axiWrappersNames.put(connection, "AxisOutput_" + TemplateUtils.getQueueNameIO(connection))
			}
		}

		// -- AXIS Interface Core Names
		axiInterfaceNames = new HashMap
		if(!singleDMAInterface){
			for (connection : psToPlFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					axiInterfaceNames.put(connection, "ll_" + TemplateUtils.getQueueNameIO(connection))
				} else if (TemplateUtils.getConnectionKind(connection).equals(
					ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
					axiInterfaceNames.put(connection, "dma_" + TemplateUtils.getQueueNameIO(connection))
				}
			}
			for (connection : plToPsFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					axiInterfaceNames.put(connection, "ll_" + TemplateUtils.getQueueNameIO(connection))
				} else if (TemplateUtils.getConnectionKind(connection).equals(
					ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
					axiInterfaceNames.put(connection, "dma_" + TemplateUtils.getQueueNameIO(connection))
				}
			}
		}
		// -- GP0 Interconnect
		gp0Interconnect = new ArrayList
		hp0Interconnect = new ArrayList
		acpInterconnect = new ArrayList
		if (partitioner.containsPartition(ZYNQ_PL_PARTITION)) {
			// -- Kicker
			gp0Interconnect.add("kicker" + "/" + "s_axi_control");
			// -- Axi Wrappers I/O
			if(!singleDMAInterface){
				for (connection : psToPlFifos) {
					gp0Interconnect.add(axiWrappersNames.get(connection) + "/" + "s_axi_control")
				}
				for (connection : plToPsFifos) {
					gp0Interconnect.add(axiWrappersNames.get(connection) + "/" + "s_axi_control")
				}
			}else{
				if(!psToPlFifos.empty){
					gp0Interconnect.add("AxisInput_Wrapper" + "/" + "s_axi_control")
				}
				
				if(!plToPsFifos.empty){
					gp0Interconnect.add("AxisOutput_Wrapper" + "/" + "s_axi_control")
				}
			}
			// -- AXI Control for DMA or LLFifo
			if(!singleDMAInterface){
				for (connection : psToPlFifos) {
					if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
						gp0Interconnect.add(axiInterfaceNames.get(connection) + "/" + "S_AXI")
						gp0Interconnect.add(axiInterfaceNames.get(connection) + "/" + "S_AXI_FULL")
					} else if (TemplateUtils.getConnectionKind(connection).equals(
						ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
						gp0Interconnect.add(axiInterfaceNames.get(connection) + "/" + "S_AXI_LITE")
					}
				}
				for (connection : plToPsFifos) {
					if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
						gp0Interconnect.add(axiInterfaceNames.get(connection) + "/" + "S_AXI")
						gp0Interconnect.add(axiInterfaceNames.get(connection) + "/" + "S_AXI_FULL")
					} else if (TemplateUtils.getConnectionKind(connection).equals(
						ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
						gp0Interconnect.add(axiInterfaceNames.get(connection) + "/" + "S_AXI_LITE")
					}
				}
			}else{
				if(!psToPlFifos.empty || !plToPsFifos.empty){
					gp0Interconnect.add("single_dma" + "/" + "S_AXI_LITE")
				}
			}
			
			
			// -- External Variables
			if(network.hasAttribute(NETWORK_EXTERNAL_VARIABLES)){
			instancesExternalVariablesMap = network.getAttribute(NETWORK_EXTERNAL_VARIABLES).objectValue as TreeMap<Instance, List<Var>>
			}else{
				instancesExternalVariablesMap = new TreeMap
			}
			
			
			// -- HP0 Interconnect
			
			// -- AXI Control for DMA or LLFifo
			if(!singleDMAInterface){
				for (connection : psToPlFifos) {
					if (TemplateUtils.getConnectionKind(connection).equals(
						ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
						hp0Interconnect.add(axiInterfaceNames.get(connection) + "/" + "M_AXI_MM2S")
					}
				}
				for (connection : plToPsFifos) {
					if (TemplateUtils.getConnectionKind(connection).equals(
						ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
						hp0Interconnect.add(axiInterfaceNames.get(connection) + "/" + "M_AXI_S2MM")
					}
				}
				
				for(instance : instancesExternalVariablesMap.keySet){
					for(varibale : instancesExternalVariablesMap.get(instance)){
						hp0Interconnect.add(name + "/" + "m_axi_id_" + instance.number + "_" + varibale.name)
					}
				}
			}else{
				if(!psToPlFifos.empty){
					hp0Interconnect.add("single_dma" + "/" + "M_AXI_MM2S")
				}
				if(!plToPsFifos.empty){
					hp0Interconnect.add("single_dma" + "/" + "M_AXI_S2MM")
				}
				
				for(instance : instancesExternalVariablesMap.keySet){
					for(varibale : instancesExternalVariablesMap.get(instance)){
						hp0Interconnect.add(name + "/" + "m_axi_id_" + instance.number + "_" + varibale.name)
					}
				}
			}

			// -- ACP Interconnect
			// -- TO BE DONE

			// -- Concat Interrupts
			concatInterrupts = new ArrayList
			concatInterrupts.add("kicker" + "/" + "interrupt")
			if(!singleDMAInterface){
				for (connection : psToPlFifos) {
					concatInterrupts.add(axiWrappersNames.get(connection) + "/" + "interrupt")
					//concatInterrupts.add(axiInterfaceNames.get(connection) + "/" + "mm2s_introut")
					concatInterrupts.add(axiInterfaceNames.get(connection) + "/" + "interrupt")
				}
				for (connection : plToPsFifos) {
					concatInterrupts.add(axiWrappersNames.get(connection) + "/" + "interrupt")
					//concatInterrupts.add(axiInterfaceNames.get(connection) + "/" + "s2mm_introut")
					concatInterrupts.add(axiInterfaceNames.get(connection) + "/" + "interrupt")
				}
			}else{
				if(!psToPlFifos.empty){
					concatInterrupts.add("AxisInput_Wrapper" + "/" + "interrupt")
					concatInterrupts.add("single_dma" + "/" + "mm2s_introut")
				}
				if(!plToPsFifos.empty){
					concatInterrupts.add("AxisOutput_Wrapper" + "/" + "interrupt")
					concatInterrupts.add("single_dma" + "/" + "s2mm_introut")
				}
			}
		}
	}

	override getContent() {
		'''
			«psConfiguration»
			«IF partitioner.containsPartition(ZYNQ_PL_PARTITION)»
				«interfaceCores»
				«hlsCores»
				«GP0Interconnect»
				«HP0Interconnect»
				«ACPInterconnect»
				«getConcatInterrupts»
			«ENDIF»
		'''
	}

	private def getPsConfiguration(){
		var ZynqPS7BoardPreset zynqPreset = new ZynqPS7BoardPreset(boardProperty)
		zynqPreset.plPsInterrupts = true
		zynqPreset.psClock = zynqPreset.property.psFrequency
		zynqPreset.plClock = plFrequency

		if (!gp0Interconnect.empty){
			zynqPreset.setMasterGP0(true)
		}
		
		if(!hp0Interconnect.empty){
			zynqPreset.setSlaveHP0(true)
		}

		if (!acpInterconnect.empty){
			zynqPreset.setSlaveACP(true)
		}
		'''
			# -- Add Zynq PS
			«zynqPreset.preset»
			# -- Add Port DDR
			create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR
			# -- Connect it to the sys_ps DDR port
			connect_bd_intf_net [get_bd_intf_ports DDR] [get_bd_intf_pins sys_ps/DDR]
			# -- Add Port FIXED IO
			create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO
			# -- Connect it to the sys_ps FIXED_IO port
			connect_bd_intf_net [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins sys_ps/FIXED_IO]
			
			# -- Connect Clocks for master and slaves PL-PS
			«IF zynqPreset.masterGP0 || zynqPreset.masterGP1 || zynqPreset.slaveGP0 || zynqPreset.slaveGP1 || zynqPreset.slaveACP || zynqPreset.slaveHP0 || zynqPreset.slaveHP1 || zynqPreset.slaveHP2 || zynqPreset.slaveHP3»
				«IF zynqPreset.masterGP0»
					# -- Connect M_AXI_GP0_ACLK to FCLK_CLK0
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins sys_ps/M_AXI_GP0_ACLK]
				«ENDIF»
				«IF zynqPreset.masterGP1»
					# -- Connect M_AXI_GP1_ACLK to FCLK_CLK0
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins sys_ps/M_AXI_GP1_ACLK]
				«ENDIF»
				«IF zynqPreset.slaveGP0»
					# -- Connect S_AXI_GP0_ACLK to FCLK_CLK0
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins sys_ps/S_AXI_GP0_ACLK]
				«ENDIF»
				«IF zynqPreset.slaveGP1»
					# -- Connect S_AXI_GP1_ACLK to FCLK_CLK0
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins sys_ps/S_AXI_GP1_ACLK]
				«ENDIF»
				«IF zynqPreset.slaveACP»
					# -- Connect S_AXI_ACP_ACLK to FCLK_CLK0
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins sys_ps/S_AXI_ACP_ACLK]
				«ENDIF»
				«IF zynqPreset.slaveHP0»
					# -- Connect S_AXI_HP0_ACLK to FCLK_CLK0
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins sys_ps/S_AXI_HP0_ACLK]
				«ENDIF»
				«IF zynqPreset.slaveHP1»
					# -- Connect S_AXI_HP1_ACLK to FCLK_CLK0
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins sys_ps/S_AXI_HP1_ACLK]
				«ENDIF»
				«IF zynqPreset.slaveHP2»
					# -- Connect S_AXI_HP2_ACLK to FCLK_CLK0
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins sys_ps/S_AXI_HP2_ACLK]
				«ENDIF»
				«IF zynqPreset.slaveHP3»
					# -- Connect S_AXI_HP3_ACLK to FCLK_CLK0
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins sys_ps/S_AXI_HP3_ACLK]
				«ENDIF»
				# -- Add system reset generation
				set sys_rstgen [create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 sys_rstgen]
				set_property -dict [list CONFIG.C_EXT_RST_WIDTH {1}] $sys_rstgen
				# -- Connect FCLK_CLKO to the sys_rstgen slowest_sync_clk
				connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins sys_rstgen/slowest_sync_clk]
				# -- Connect FCLK_RESET0_N to the sys_rstgen ext_reset_in
				connect_bd_net [get_bd_pins sys_ps/FCLK_RESET0_N] [get_bd_pins sys_rstgen/ext_reset_in]
				# -- connect clk and reset to block design ports
				connect_bd_net [get_bd_pins «name»/ap_clk] [get_bd_pins sys_ps/FCLK_CLK0]
				connect_bd_net [get_bd_pins «name»/ap_rst_n] [get_bd_pins sys_rstgen/peripheral_aresetn]
			«ENDIF»
		'''
	}

	private def getHlsCores() {
		'''
			# -- Kicker
			«BlockDesignUtils.getCore("kicker", "kicker", "hls", "1.0")»
			# -- Connect start
			connect_bd_net [get_bd_pins «name»/start] [get_bd_pins kicker/start_net]
			# -- Connect CLK and reset
			connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins kicker/ap_rst_n]
			connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins kicker/ap_clk]
			
			# -- Wrappers
			«IF !singleDMAInterface»
				«FOR connection : psToPlFifos»
					«BlockDesignUtils.getCore(axiWrappersNames.get(connection), axiWrappersNames.get(connection), "hls", "1.0")»
					connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins «axiWrappersNames.get(connection)»/ap_rst_n]
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins «axiWrappersNames.get(connection)»/ap_clk]
					# -- TO PL
					connect_bd_intf_net [get_bd_intf_pins «axiWrappersNames.get(connection)»/OUT_V] [get_bd_intf_ports «name»/«TemplateUtils.getQueueNameIO(connection)»] 
					«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
						# -- MM2S
						connect_bd_intf_net [get_bd_intf_pins «axiInterfaceNames.get(connection)»/AXI_STR_TXD] [get_bd_intf_pins «axiWrappersNames.get(connection)»/IN_r]
					«ELSEIF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
						# -- MM2S
						connect_bd_intf_net [get_bd_intf_pins «axiInterfaceNames.get(connection)»/M_AXIS_MM2S] [get_bd_intf_pins «axiWrappersNames.get(connection)»/IN_r]
						# -- size and count
						connect_bd_net [get_bd_pins «name»/«TemplateUtils.getQueueNameIO(connection)»_count] [get_bd_pins «axiWrappersNames.get(connection)»/count]
						connect_bd_net [get_bd_pins «name»/«TemplateUtils.getQueueNameIO(connection)»_size] [get_bd_pins «axiWrappersNames.get(connection)»/size]
					«ENDIF»
				«ENDFOR»
				
				«FOR connection : plToPsFifos»
					«BlockDesignUtils.getCore(axiWrappersNames.get(connection), axiWrappersNames.get(connection), "hls", "1.0")»
					connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins «axiWrappersNames.get(connection)»/ap_rst_n]
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins «axiWrappersNames.get(connection)»/ap_clk]
					# -- FROM PL
					connect_bd_intf_net [get_bd_intf_ports «name»/«TemplateUtils.getQueueNameIO(connection)»] [get_bd_intf_pins «axiWrappersNames.get(connection)»/IN_V]
					«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
						# -- S2MM
						connect_bd_intf_net [get_bd_intf_pins «axiWrappersNames.get(connection)»/OUT_r] [get_bd_intf_pins «axiInterfaceNames.get(connection)»/AXI_STR_RXD]
					«ELSEIF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
						# -- S2MM
						connect_bd_intf_net [get_bd_intf_pins «axiWrappersNames.get(connection)»/OUT_r] [get_bd_intf_pins «axiInterfaceNames.get(connection)»/S_AXIS_S2MM]
						connect_bd_net [get_bd_pins «name»/«TemplateUtils.getQueueNameIO(connection)»_count] [get_bd_pins «axiWrappersNames.get(connection)»/count]
					«ENDIF»
				«ENDFOR»
			«ELSE»
				«IF !psToPlFifos.empty»
					«BlockDesignUtils.getCore("AxisInput_Wrapper", "AxisInput_Wrapper", "hls", "1.0")»
					# -- Connect CLK and reset
					connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins AxisInput_Wrapper/ap_rst_n]
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins AxisInput_Wrapper/ap_clk]
					
					# -- MM2S
					connect_bd_intf_net [get_bd_intf_pins single_dma/M_AXIS_MM2S] [get_bd_intf_pins AxisInput_Wrapper/IN_r]
					«FOR connection : psToPlFifos»
						connect_bd_intf_net [get_bd_intf_pins AxisInput_Wrapper/«TemplateUtils.getQueueNameIO(connection)»_V] -boundary_type upper [get_bd_intf_pins «name»/«TemplateUtils.getQueueNameIO(connection)»]
						# -- size and count
						connect_bd_net [get_bd_pins «name»/«TemplateUtils.getQueueNameIO(connection)»_count] [get_bd_pins AxisInput_Wrapper/«TemplateUtils.getQueueNameIO(connection)»_count]
						connect_bd_net [get_bd_pins «name»/«TemplateUtils.getQueueNameIO(connection)»_size] [get_bd_pins AxisInput_Wrapper/«TemplateUtils.getQueueNameIO(connection)»_size]
					«ENDFOR»
					
				«ENDIF»
				
				«IF !plToPsFifos.empty»
					«BlockDesignUtils.getCore("AxisOutput_Wrapper", "AxisOutput_Wrapper", "hls", "1.0")»
					# -- Connect CLK and reset
					connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins AxisOutput_Wrapper/ap_rst_n]
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins AxisOutput_Wrapper/ap_clk]
					
					# -- S2MM
					connect_bd_intf_net [get_bd_intf_pins AxisOutput_Wrapper/OUT_r] [get_bd_intf_pins single_dma/S_AXIS_S2MM]
					«FOR connection : plToPsFifos»
						connect_bd_intf_net [get_bd_intf_pins «name»/«TemplateUtils.getQueueNameIO(connection)»] -boundary_type upper [get_bd_intf_pins AxisOutput_Wrapper/«TemplateUtils.getQueueNameIO(connection)»_V]
						connect_bd_net [get_bd_pins «name»/«TemplateUtils.getQueueNameIO(connection)»_count] [get_bd_pins AxisOutput_Wrapper/«TemplateUtils.getQueueNameIO(connection)»_count]
					«ENDFOR»
				«ENDIF»
			«ENDIF»
		'''
	}

	private def getInterfaceCores() {
		'''
			«IF !singleDMAInterface»
				«FOR connection : psToPlFifos»
					«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
						# -- Configuration
						startgroup
						create_bd_cell -type ip -vlnv xilinx.com:ip:axi_fifo_mm_s:4.1 «axiInterfaceNames.get(connection)»
						set_property -dict [list CONFIG.C_USE_RX_DATA {0} CONFIG.C_USE_TX_CTRL {0} CONFIG.C_DATA_INTERFACE_TYPE {1}] [get_bd_cells «axiInterfaceNames.get(connection)»]
						set_property -dict [list CONFIG.C_TX_FIFO_DEPTH {«IF connection.size === null»«fifoSize»«ELSE»«connection.size»«ENDIF»} CONFIG.C_TX_FIFO_PF_THRESHOLD {«IF connection.size === null»«fifoSize-5»«ELSE»«connection.size-5»«ENDIF»} CONFIG.C_TX_FIFO_PE_THRESHOLD {2}] [get_bd_cells «axiInterfaceNames.get(connection)»]
						endgroup
						# -- Connections
						connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins «axiInterfaceNames.get(connection)»/s_axi_aresetn]
						connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins «axiInterfaceNames.get(connection)»/s_axi_aclk]
					«ELSEIF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
						# -- Configuration
						startgroup
						create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 «axiInterfaceNames.get(connection)»
						set_property -dict [list CONFIG.c_include_sg {0} CONFIG.c_sg_length_width {23} CONFIG.c_include_mm2s {1} CONFIG.c_mm2s_burst_size {256} CONFIG.c_include_s2mm {0} CONFIG.c_sg_include_stscntrl_strm {0}] [get_bd_cells «axiInterfaceNames.get(connection)»]
						endgroup
						# -- Connections
						connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins «axiInterfaceNames.get(connection)»/axi_resetn]
						connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins «axiInterfaceNames.get(connection)»/s_axi_lite_aclk]
						connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins «axiInterfaceNames.get(connection)»/m_axi_mm2s_aclk]
					«ENDIF»
				«ENDFOR»
				
				«FOR connection : plToPsFifos»
					«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
						# -- Configuration
						startgroup
						create_bd_cell -type ip -vlnv xilinx.com:ip:axi_fifo_mm_s:4.1 «axiInterfaceNames.get(connection)»
						set_property -dict [list CONFIG.C_USE_TX_DATA {0} CONFIG.C_DATA_INTERFACE_TYPE {1} CONFIG.C_USE_TX_CTRL {0}] [get_bd_cells «axiInterfaceNames.get(connection)»]
						set_property -dict [list CONFIG.C_TX_FIFO_DEPTH {«IF connection.size === null»«fifoSize»«ELSE»«connection.size»«ENDIF»} CONFIG.C_TX_FIFO_PF_THRESHOLD {«IF connection.size === null»«fifoSize-5»«ELSE»«connection.size-5»«ENDIF»} CONFIG.C_TX_FIFO_PE_THRESHOLD {2}] [get_bd_cells «axiInterfaceNames.get(connection)»]
						endgroup
						# -- Connections
						connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins «axiInterfaceNames.get(connection)»/s_axi_aresetn]
						connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins «axiInterfaceNames.get(connection)»/s_axi_aclk]
					«ELSEIF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
						# -- Configuration
						startgroup
						create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 «axiInterfaceNames.get(connection)»
						set_property -dict [list CONFIG.c_include_sg {0} CONFIG.c_sg_length_width {23} CONFIG.c_sg_include_stscntrl_strm {0} CONFIG.c_include_mm2s {0} CONFIG.c_s2mm_burst_size {256}] [get_bd_cells «axiInterfaceNames.get(connection)»]
						endgroup
						# -- Connections
						connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins «axiInterfaceNames.get(connection)»/axi_resetn]
						connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins «axiInterfaceNames.get(connection)»/s_axi_lite_aclk]
						connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins «axiInterfaceNames.get(connection)»/m_axi_s2mm_aclk]
					«ENDIF»
				«ENDFOR»
			«ELSE»
				«IF !psToPlFifos.empty || !plToPsFifos.empty»
					startgroup
					create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 single_dma
					set_property -dict [list CONFIG.c_include_sg {0} CONFIG.c_sg_length_width {23} CONFIG.c_sg_include_stscntrl_strm {0} CONFIG.c_include_s2mm {«IF plToPsFifos.empty»0«ELSE»1«ENDIF»} CONFIG.c_include_mm2s {«IF psToPlFifos.empty»0«ELSE»1«ENDIF»} CONFIG.c_s2mm_burst_size {256}] [get_bd_cells single_dma]
					endgroup
					# -- Connections
					connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins single_dma/axi_resetn]
					connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins single_dma/s_axi_lite_aclk]
					«IF !psToPlFifos.empty»
						connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins single_dma/m_axi_mm2s_aclk]
					«ENDIF»
					«IF !plToPsFifos.empty»
						connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins single_dma/m_axi_s2mm_aclk]
					«ENDIF»	
				«ENDIF»
			«ENDIF»
			
		'''
	}

	private def getGP0Interconnect() {
		'''
			# -----------------------------------------------------------------------------
			# -- Add AXI Smartconnect for GP0 
			
			set gp0_smc [create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 gp0_smc]
			set_property -dict [list CONFIG.NUM_SI {1}] [get_bd_cells $gp0_smc]
			set_property -dict [list CONFIG.NUM_MI {«gp0Interconnect.size»}] [get_bd_cells $gp0_smc]
			
			# -- Connect CLK and reset
			connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins $gp0_smc/aresetn]
			connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins $gp0_smc/aclk]
			
			# -- Connect Zynq GP0
			connect_bd_intf_net [get_bd_intf_pins sys_ps/M_AXI_GP0] -boundary_type upper [get_bd_intf_pins $gp0_smc/S00_AXI]
			
			# -- Connect Masters to Core Slaves
			«FOR i : 0 .. gp0Interconnect.size - 1»
				connect_bd_intf_net [get_bd_intf_pins $gp0_smc/M«IF i < 10»0«ENDIF»«i»_AXI] [get_bd_intf_pins «gp0Interconnect.get(i)»]
			«ENDFOR»
		'''
	}

	private def getHP0Interconnect() {
		'''
			«IF !hp0Interconnect.empty»
				# -----------------------------------------------------------------------------
				# -- Add AXI Smartconnect for HP0 
				
				set hp0_smc [create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 hp0_smc]
				set_property -dict [list CONFIG.NUM_SI {«hp0Interconnect.size»}] [get_bd_cells $hp0_smc]
				set_property -dict [list CONFIG.NUM_MI {1}] [get_bd_cells $hp0_smc]
				
				# -- Connect CLK and reset
				connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins $hp0_smc/aresetn]
				connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins $hp0_smc/aclk]
				
				# -- Connect Zynq HP0
				connect_bd_intf_net [get_bd_intf_pins sys_ps/S_AXI_HP0] -boundary_type upper [get_bd_intf_pins $hp0_smc/M00_AXI]
				
				# -- Connect to SmartConnect Slaves
				«FOR i : 0 .. hp0Interconnect.size - 1»
					connect_bd_intf_net [get_bd_intf_pins «hp0Interconnect.get(i)»] [get_bd_intf_pins $hp0_smc/S«IF i < 10»0«ENDIF»«i»_AXI]
				«ENDFOR»
			«ENDIF»
		'''
	}

	private def getACPInterconnect() {
		'''
			«IF !acpInterconnect.empty»
				# -----------------------------------------------------------------------------
				# -- Add AXI Smartconnect for ACP 
				
				set acp_smc [create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 acp_smc]
				set_property -dict [list CONFIG.NUM_SI {«acpInterconnect.size»}] [get_bd_cells $acp_smc]
				set_property -dict [list CONFIG.NUM_MI {1}] [get_bd_cells $hp0_smc]
				
				# -- Connect CLK and reset
				connect_bd_net [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins $acp_smc/aresetn]
				connect_bd_net [get_bd_pins sys_ps/FCLK_CLK0] [get_bd_pins $acp_smc/aclk]
				
				# -- Connect Zynq HP0
				connect_bd_intf_net [get_bd_intf_pins sys_ps/S_AXI_HP0] -boundary_type upper [get_bd_intf_pins $acp_smc/M00_AXI]
				
				# -- Connect to SmartConnect Slaves
				«FOR i : 0 .. hp0Interconnect.size - 1»
					connect_bd_intf_net [get_bd_intf_pins «acpInterconnect.get(i)»] [get_bd_intf_pins $acp_smc/S«IF i < 10»0«ENDIF»«i»_AXI]
				«ENDFOR»
			«ENDIF»
		''' 
	}
	
	private def getConcatInterrupts(){
		'''
			# -----------------------------------------------------------------------------
			# -- Concat Interrupts
			startgroup
			create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 sys_concat_interrupts
			set_property -dict [list CONFIG.NUM_PORTS {«concatInterrupts.size»}] [get_bd_cells sys_concat_interrupts]
			endgroup
			
			# -- Connect Concat output to PS Interrupt
			connect_bd_net [get_bd_pins sys_concat_interrupts/dout] [get_bd_pins sys_ps/IRQ_F2P]
			# -- Connect Interrupts
			«FOR i : 0 .. concatInterrupts.size - 1»
				connect_bd_net [get_bd_pins «concatInterrupts.get(i)»] [get_bd_pins sys_concat_interrupts/In«i»]
			«ENDFOR»
		'''
	}

}
