/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.vivado.boardsproperties.zynqps7

import exelixi.zynq.backend.templates.vivado.boardsproperties.PSProperty

/**
 * @author Endri Bezati
 */
class ZynqPS7BoardPreset {

	// -- Board Property
	private PSProperty boardProperty

	// -- PS7 Xilinx IP Core version
	private String ps7Version = "5.5"

	// -- PS-PL Interfaces
	private boolean useMasterGP0 = false
	private boolean useMasterGP1 = false

	private boolean useSlaveGP0 = false
	private boolean useSlaveGP1 = false

	private boolean useSlaveACP = false
	private boolean useSlaveHP0 = false
	private boolean useSlaveHP1 = false
	private boolean useSlaveHP2 = false
	private boolean useSlaveHP3 = false

	private boolean usePlPsInterrupts = false

	// -- PS Clock
	private String psClock = "666.666666"

	// -- PL Clock
	private String plClock = "100"

	// -- Enable Interrupt
	private boolean enablePLPSInterrupt = false

	new(PSProperty boardProperty) {
		this.boardProperty = boardProperty
	}

	public def setMasterGP0(boolean flag) {
		this.useMasterGP0 = flag
	}

	public def boolean getMasterGP0() {
		return this.useMasterGP0 
	}

	public def setMasterGP1(boolean flag) {
		this.useMasterGP1 = flag
	}
	
	public def boolean getMasterGP1() {
		return this.useMasterGP1 
	}
	
	public def setSlaveGP0(boolean flag) {
		this.useSlaveGP0 = flag
	}

	public def boolean getSlaveGP0() {
		return this.useSlaveGP0 
	}

	public def setSlaveGP1(boolean flag) {
		this.useSlaveGP1 = flag
	}
	
	public def boolean getSlaveGP1() {
		return this.useSlaveGP1 
	}

	public def setSlaveACP(boolean flag) {
		this.useSlaveACP = flag
	}
	
	public def boolean getSlaveACP() {
		return this.useSlaveACP
	}

	public def setSlaveHP0(boolean flag) {
		this.useSlaveHP0 = flag
	}

	public def boolean getSlaveHP0() {
		return this.useSlaveHP0
	}

	public def setSlaveHP1(boolean flag) {
		this.useSlaveHP1 = flag
	}
	
	public def boolean getSlaveHP1() {
		return this.useSlaveHP1
	}

	public def setSlaveHP2(boolean flag) {
		this.useSlaveHP2 = flag
	}
	
	public def setPlPsInterrupts(boolean flag){
		this.usePlPsInterrupts = flag
	}
	
	public def boolean getSlaveHP2() {
		return this.useSlaveHP2
	}

	public def setSlaveHP3(boolean flag) {
		this.useSlaveHP3 = flag
	}
	
	public def boolean getSlaveHP3() {
		return this.useSlaveHP3
	}

	public def boolean getPlPsInterrupts(){
		return this.usePlPsInterrupts
	}

	public def setPsClock(Double frequency){
		psClock = frequency.toString
	}

	public def setPlClock(Double frequency){
		plClock = frequency.toString
	}

	public def getPreset() {
		'''
			set sys_ps [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:«ps7Version» sys_ps ]
			«boardProperty.property»
			
			# -- PS-PL Configuration
			«IF useMasterGP0»
				set_property -dict [list CONFIG.PCW_USE_M_AXI_GP0 {1}] $sys_ps
			«ELSE»
				set_property -dict [list CONFIG.PCW_USE_M_AXI_GP0 {0}] $sys_ps
			«ENDIF»
			«IF useMasterGP1»
				set_property -dict [list CONFIG.PCW_USE_M_AXI_GP1 {1}] $sys_ps
			«ENDIF»
			«IF useSlaveGP0»
				set_property -dict [list CONFIG.PCW_USE_S_AXI_GP0 {1}] $sys_ps
			«ENDIF»
			«IF useSlaveGP1»
				set_property -dict [list CONFIG.PCW_USE_S_AXI_GP1 {1}] $sys_ps
			«ENDIF»
			«IF useSlaveACP»
				set_property -dict [list CONFIG.PCW_USE_S_AXI_ACP {1}] $sys_ps
			«ENDIF»
			«IF useSlaveHP0»
				set_property -dict [list CONFIG.PCW_USE_S_AXI_HP0 {1}] $sys_ps
			«ENDIF»
			«IF useSlaveHP1»
				set_property -dict [list CONFIG.PCW_USE_S_AXI_HP1 {1}] $sys_ps
			«ENDIF»
			«IF useSlaveHP2»
				set_property -dict [list CONFIG.PCW_USE_S_AXI_HP2 {1}] $sys_ps
			«ENDIF»
			«IF useSlaveHP3»
				set_property -dict [list CONFIG.PCW_USE_S_AXI_HP3 {1}] $sys_ps
			«ENDIF»
			
			«IF usePlPsInterrupts»
				set_property -dict [list CONFIG.PCW_USE_FABRIC_INTERRUPT {1} CONFIG.PCW_IRQ_F2P_INTR {1}] $sys_ps
			«ENDIF»
			
			# -- Clock Configuration
			# -- PS Clock
			set_property -dict [list CONFIG.PCW_APU_PERIPHERAL_FREQMHZ {«psClock»}] $sys_ps
			# -- PL Clock
			set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {«plClock»} CONFIG.PCW_EN_CLK0_PORT {1}] $sys_ps
			
			# -- Interrupt
			«IF enablePLPSInterrupt»
				set_property -dict [list CONFIG.PCW_USE_FABRIC_INTERRUPT {1} CONFIG.PCW_IRQ_F2P_INTR {1}] $sys_ps
			«ENDIF»
		'''
	}
	
	public def PSProperty getProperty(){
		return boardProperty
	}
}
