/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.vivado.boardsproperties.zynqps7

import exelixi.zynq.backend.templates.vivado.boardsproperties.PSProperty

/**
 * @author Endri Bezati
 */
class OZ745Property implements PSProperty {

	override getProperty() {
		'''
			# -- Peripheral I/O Pins
			# -- Quad SPI Flash
			set_property -dict [list CONFIG.PCW_QSPI_PERIPHERAL_ENABLE {1} CONFIG.PCW_QSPI_QSPI_IO {MIO 1 .. 6} CONFIG.PCW_QSPI_GRP_FBCLK_ENABLE {0}] $sys_ps
			# -- Ethernet 0
			set_property -dict [list CONFIG.PCW_ENET0_ENET0_IO {MIO 16 .. 27}] $sys_ps
			# -- USB 0
			set_property -dict [list CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39}] $sys_ps
			# -- SD 0
			set_property -dict [list CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} CONFIG.PCW_SD0_GRP_CD_ENABLE {1} CONFIG.PCW_SD0_GRP_CD_IO {MIO 0} CONFIG.PCW_SD0_GRP_WP_ENABLE {1} CONFIG.PCW_SD0_GRP_WP_IO {MIO 9}] $sys_ps
			# -- SPI 1
			set_property -dict [list CONFIG.PCW_SPI1_PERIPHERAL_ENABLE {1} CONFIG.PCW_SPI1_SPI1_IO {MIO 10 .. 15}] $sys_ps
			# -- UART 0
			set_property -dict [list CONFIG.PCW_UART0_PERIPHERAL_ENABLE {1} CONFIG.PCW_UART0_UART0_IO {MIO 46 .. 47}] $sys_ps
			# -- UART 1
			set_property -dict [list CONFIG.PCW_UART1_PERIPHERAL_ENABLE {1} CONFIG.PCW_UART1_UART1_IO {MIO 48 .. 49}] $sys_ps
			# -- I2C 0
			set_property -dict [list CONFIG.PCW_I2C0_PERIPHERAL_ENABLE {1} CONFIG.PCW_I2C0_I2C0_IO {MIO 50 .. 51}] $sys_ps
			# -- TTC 0
			set_property -dict [list CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {1}] $sys_ps
			# -- SWDT
			set_property -dict [list CONFIG.PCW_WDT_PERIPHERAL_ENABLE {1}] $sys_ps
			# -- PJTAG
			set_property -dict [list CONFIG.PCW_PJTAG_PERIPHERAL_ENABLE {1}] $sys_ps
			
			# -- DDR Controller Configuration
			# -- Memory Part
			set_property -dict [list CONFIG.PCW_UIPARAM_DDR_PARTNO {MT41J256M16 RE-125}] $sys_ps
			# -- Training/Board Details
			# -- DQS to Clock Delay (ns)
			set_property -dict [list CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_0 {-0.005} CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_1 {-0.004} CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_2 {-0.008} CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_3 {-0.058}] $sys_ps
			# -- Board Delay
			set_property -dict [list CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY0 {0.075} CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY1 {0.076} CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY2 {0.082} CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY3 {0.1}] $sys_ps
		'''
	}

	override double getPsFrequency() {
		return 1000
	}

}
