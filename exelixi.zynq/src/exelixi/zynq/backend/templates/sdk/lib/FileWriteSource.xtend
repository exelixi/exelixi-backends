/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.sdk.lib

class FileWriteSource {
	def getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 ******************************************************************************/
			
			#include <iostream>
			#include <fstream>
			#include <string>
			
			#include "file_write.h"
			#include "file_fs.h"
			
			#include <stdlib.h>
			#include <string.h>
			#include <vector>
			#include "ff.h"
			
			static FIL fil; /* File object */
			
			void Writer_init() {
				if (!mount_sd_card()) {
					std::cout << "Error mounting the SD card file system \n";
					return;
				}
			
				std::cout << "== Set the output file name ==\n";
				char fileName[256];
				fflush(stdin);
				for (;;) {
					std::cout << "choice: ";
					scanf("%s", fileName);
					if ((unsigned)strlen(fileName)) {
						std::cout << fileName << " [OK]\n";
						break;
					} else {
						std::cout << " [ERROR] can not be an empty string!\n";
					}
				}
			
				if (f_open(&fil, fileName, FA_CREATE_ALWAYS | FA_WRITE)) {
					std::cout << "Write file can not be opened\n";
					return;
				}
			}
			
			void Writer_write(uint8_t byte) {
				UINT bw;
				f_write(&fil, &byte, 1, &bw);
			}
			
			void Writer_close() {
				if (f_close(&fil)) {
					std::cout << " Error while closing the output file \n";
					return;
				}
			}
		'''
	}
}
