/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk.lib

import exelixi.cpp.backend.templates.common.ExprAndTypePrinter
import java.util.LinkedHashMap
import java.util.Map
import net.sf.orcc.df.Network

/**
 * 
 * @author Endri Bezati
 */
class NoDisplaySource extends ExprAndTypePrinter {
	
	protected Network network
	protected Map<String, Boolean> addtionalNativeDisplayProcs = new LinkedHashMap<String, Boolean>();

	new(Network topNetwork) {
		network = topNetwork
	}

	def getContent() {
		'''
			#include <stdio.h>
			#include <iostream>
			#include <stdlib.h>
			
			#include "no_display.h"
			
			#define TIMEDIFF(t1,t2) (t2 - t1)
			#define MILLISECONDS(t) (1000.0 * t / COUNTS_PER_SECOND)
			
			static XTime startTime;
			static XTime relativeStartTime;
			static int lastNumPic;
			static int numPicturesDecoded;
			
			void displayYUV_init()
			{
				//XTime_StartTimer();
			}
			
			void displayYUV_setSize(int width, int height)
			{
			}
			
			void displayYUV_displayPicture(unsigned char pictureBufferY[], unsigned char pictureBufferU[], unsigned char pictureBufferV[], short pictureWidth, short pictureHeight) 
			{
			}
			
			/**
			 * @brief Return the number of frames the user want to decode before exiting the application.
			 * If user didn't use the -f flag, it returns -1 (DEFAULT_INFINITEà).
			 * @return The
			 */
			int displayYUV_getNbFrames() 
			{
				return -1;
			}
			
			unsigned char displayYUV_getFlags()
			{
				return 3;
			}
			
			int compareYUV_compareComponent(const int x_size, const int y_size, const int x_size_test_img, 
				const unsigned char *true_img_uchar, const unsigned char *test_img_uchar,
				unsigned char SizeMbSide, char Component_Type) 
			{
				return 0;
			}
			
			void compareYUV_init()
			{
			}
			
			void compareYUV_readComponent(unsigned char **Component, unsigned short width, unsigned short height, char sizeChanged)
			{
			}
			
			void compareYUV_comparePicture(unsigned char pictureBufferY[], unsigned char pictureBufferU[],
				unsigned char pictureBufferV[], short pictureWidth,
				short pictureHeight)
			{
			}
			
			static void print_fps_avg(void) {
				XTime endTime;
				XTime_GetTime(&endTime);
				XTime time_diff =  TIMEDIFF(startTime, endTime);
				float time_diff_ms = MILLISECONDS(time_diff);
			
				printf("%i images in %f seconds: %f FPS\n", numPicturesDecoded,
					(float) time_diff_ms / 1000.0f,
					1000.0f * (float) numPicturesDecoded / time_diff_ms);
			}
			
			void fpsPrintInit() {
				XTime_GetTime(&startTime);
				relativeStartTime = startTime;
				numPicturesDecoded = 0;
				lastNumPic = 0;
				atexit(print_fps_avg);
			}
			
			void fpsPrintNewPicDecoded(void) {
				numPicturesDecoded++;
				XTime endTime;
				XTime_GetTime(&endTime);
			
				XTime time_diff =  TIMEDIFF(relativeStartTime, endTime);
				float time_diff_ms = MILLISECONDS(time_diff);
			
				if (time_diff_ms > 5000.0f) {
					printf("%f images/sec\n", 1000.0f * (float) (numPicturesDecoded - lastNumPic) / (float) time_diff_ms);
					relativeStartTime = endTime;
					lastNumPic = numPicturesDecoded;
				}
			}
		'''
	}
}
