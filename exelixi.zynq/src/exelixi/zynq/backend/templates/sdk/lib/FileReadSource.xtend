/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk.lib

/**
 * 
 * @author Endri Bezati
 */
class FileReadSource {
	def getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _ 
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 ******************************************************************************/
			
			#define _CRT_SECURE_NO_WARNINGS
			
			#include <iostream>
			#include <fstream>
			#include <string>
			
			#include "file_read.h"
			#include "file_fs.h"
			#include <stdlib.h>
			#include <string.h>
			#include <vector>
			#include "ff.h"
			
			static int nbLoops;
			
			static int loopsCount;
			
			static int fileSize;
			
			static FIL fil; /* File object */
			
			FRESULT __source_compute_file_size() {
				fileSize = 0;
				UINT byteRead;
				uint8_t tmp[256];
			
				FRESULT res = f_lseek(&fil, 0);
				if (res) {
					return res;
				}
			
				do {
					f_read(&fil, tmp, 256, &byteRead);
					fileSize += byteRead;
				} while (byteRead > 0);
				res = f_lseek(&fil, 0);
			
				return res;
			}
			
			FRESULT __source_scan_files(char* path, /* Start node to be scanned (***also used as work area***) */
										std::vector<std::string>* files /* the list of files */
			) {
				FRESULT res;
				DIR dir;
				UINT i;
				static FILINFO fno;
			
				res = f_opendir(&dir, path); /* Open the directory */
				if (res == FR_OK) {
					for (;;) {
						res = f_readdir(&dir, &fno); /* Read a directory item */
						if (res != FR_OK || fno.fname[0] == 0)
							break; /* Break on error or end of dir */
						if (fno.fattrib & AM_DIR) {
							/* It is a directory */
							sprintf(&path[i = strlen(path)], "/%s", fno.fname);
							res = __source_scan_files(path, files); /* Enter the directory */
							if (res != FR_OK)
								break;
							path[i] = 0;
						} else {
							/* It is a file. */
							char fileName[256];
							sprintf(fileName, "%s/%s", path, fno.fname);
							files->push_back(fileName);
						}
					}
					f_closedir(&dir);
				}
			
				return res;
			}
			
			void source_init() {
				if (!mount_sd_card()) {
					std::cout << "Error mounting the SD card file system \n";
					return;
				}
			
				char path[256] = "/";
				std::vector<std::string> files;
				if (__source_scan_files(path, &files)) {
					std::cout << "Error while listing the SD card files \n";
					return;
				}
			
				std::cout << "== Select the source file ==\n";
				int files_size = files.size();
				for (int i = 0; i < files_size; i++) {
					std::cout << "[" << i << "] " << files.at(i) << "\n";
				}
			
				int file_id;
				fflush(stdin);
				for (;;) {
					std::cout << "choice: ";
					scanf("%d", &file_id);
					if (file_id >= 0 && file_id < files_size) {
						std::cout << file_id << " [OK]\n";
						break;
					} else {
						std::cout << file_id << " [ERROR] is an invalid file number!\n";
					}
				}
			
				std::cout << "== Select the number of loops ==\n";
				fflush(stdin);
				for (;;) {
					std::cout << "choice: ";
					scanf("%d", &nbLoops);
					if (nbLoops > 0) {
						std::cout << nbLoops << " [OK]\n";
						break;
					} else {
						std::cout << nbLoops << " [ERROR] should be a positive number!\n";
					}
				}
			
				loopsCount = nbLoops;
			
				std::cout << "=====================================\n";
			
				std::string file = files.at(file_id);
				file = file.substr(2, file.size()); /* Remove the "//" initial characters*/
				if (f_open(&fil, file.c_str(), FA_READ)) {
					std::cout << "File " << file << " can not be opened\n";
					return;
				}
			
				// compute the file size
				if (__source_compute_file_size()) {
					std::cout << "The size of " << file << " can not be evaluated\n";
					return;
				}
			
				if (f_lseek(&fil, 0)) {
					std::cout << "File " << file << " can not be correctly accessed \n";
					return;
				}
			
			}
			
			int source_sizeOfFile() {
				return fileSize;
			}
			
			void source_rewind() {
				if (f_lseek(&fil, 0)) {
					std::cout << " Error while rewinding the input file \n";
					return;
				}
			}
			
			unsigned int source_readByte() {
				unsigned int result;
				UINT byteRead;
				f_read(&fil, &result, 1, &byteRead);
				return result;
			}
			
			void source_readNBytes(unsigned char outTable[], unsigned int nbTokenToRead) {
				UINT byteRead;
				f_read(&fil, outTable, nbTokenToRead, &byteRead);
			}
			
			unsigned int source_getNbLoop(void) {
				return nbLoops;
			}
			
			void source_decrementNbLoops() {
				--loopsCount;
			}
			
			bool source_isMaxLoopsReached() {
				return nbLoops != -1 && loopsCount <= 0;
			}
			
			void source_exit(int exitCode) {
				if (f_close(&fil)) {
					std::cout << " Error while closing the input file \n";
					return;
				}
			}
		'''
	}
}
