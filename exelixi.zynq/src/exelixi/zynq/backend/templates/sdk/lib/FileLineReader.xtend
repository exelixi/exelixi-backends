/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.sdk.lib

/**
 * 
 * @author Endri Bezati
 */
class FileLineReader {
	def getContent() {
		'''
			#ifndef SRC_FILE_LINE_READER_H_
			#define SRC_FILE_LINE_READER_H_
			
			#include "ff.h"
			#include <stdio.h>
			#include <string.h>
			
			class FileLineReader {
			public:
			
				FileLineReader() {
					fileSize = 0;
					overallByteRead = 0;
				}
				;
			
				~FileLineReader() {
			
				}
			
				bool open(char *fileName) {
					FRESULT res = f_open(&fil, fileName, FA_READ);
					if (res != FR_OK) {
						return false;
					}
			
					res = f_lseek(&fil, 0);
					if (res != FR_OK) {
						return false;
					}
			
					UINT byteRead;
					char tmp[256];
					do {
						f_read(&fil, tmp, 256, &byteRead);
						fileSize += byteRead;
					} while (byteRead > 0);
			
					res = f_lseek(&fil, 0);
					if (res != FR_OK) {
						return false;
					}
			
					return true;
			
				}
			
				unsigned int size() {
					return fileSize;
				}
			
				bool has_next() {
					return overallByteRead < fileSize;
				}
			
				bool read_line(char *buff) {
					if (overallByteRead >= fileSize) {
						return false;
					}
			
					char tmp;
					UINT byteRead;
			
					unsigned int ptr = 0;
					do {
						f_read(&fil, &tmp, 1, &byteRead);
						if (!byteRead) {
							tmp = '\n';
						}
						buff[ptr] = tmp;
						ptr++;
						overallByteRead++;
					} while (tmp != '\n' && byteRead > 0);
			
					buff[strcspn(buff, "\r")] = '\0';
					buff[strcspn(buff, "\n")] = '\0';
			
					return true;
				}
			
				bool close() {
					return f_close(&fil) == FR_OK;
				}
			
			private:
				FIL fil; // File object
				unsigned int fileSize; // in bytes
				unsigned int overallByteRead;
			};
			
			#endif /* SRC_FILE_LINE_READER_H_ */
		'''
	}
}
