/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk.network

import exelixi.cpp.backend.templates.common.ExprAndTypePrinter
import exelixi.hls.partitioner.Partitioner
import exelixi.zynq.backend.templates.sdk.SdkUtil
import exelixi.zynq.backend.templates.sdk.utils.SDKTemplateUtils
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date
import java.util.HashMap
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network

import static exelixi.hls.backend.Constants.NETWORK_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING

/**
 * 
 * @author Endri Bezati
 */
class MainPrinter extends ExprAndTypePrinter {

	protected Network network

	protected Partitioner partitioner

	protected Boolean networkHasHeterogeneousPartitions

	protected List<Connection> plToPsFifos

	protected List<Connection> psToPlFifos

	private Map<String, Object> options

	protected List<Instance> plInstances;

	private List<Connection> externalFifos

	private Map<Connection, String> axiWrappersNames

	private Map<Connection, String> axiInterfaceNames

	private boolean containsLlfifo

	private boolean containsLlfifoTransmit

	private boolean containsLlfifoReceive

	private boolean containsDma

	private boolean containsDmaTransmit

	private boolean containsDmaReceive
	
	private Boolean singleDMAInterface

	new(Network network, Partitioner partitioner, Map<String, Object> options) {
		this.network = network
		this.options = options
		this.partitioner = partitioner
		this.networkHasHeterogeneousPartitions = partitioner.containsPartition(ZYNQ_PS_PARTITION) &&
			partitioner.containsPartition(ZYNQ_PL_PARTITION)
		this.plToPsFifos = new ArrayList<Connection>(partitioner.getConnectionToConnectionBd(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION).keySet)
		this.psToPlFifos = new ArrayList<Connection>(partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION).keySet)
		this.plInstances = partitioner.getInstances(ZYNQ_PL_PARTITION);
		var List<Connection> interfaceConnections = new ArrayList<Connection>(partitioner.getConnections(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION))
		interfaceConnections.addAll(partitioner.getConnections(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION))
		this.externalFifos = SDKTemplateUtils.getConnectionsWithDifferentBroadcast(interfaceConnections)

		// -- Communication Kind
		singleDMAInterface = false;
		val String communicationKind = options.get(ZYNQ_OPTION_COMMUNICATION_KIND) as String
		if (communicationKind.equals("Single DMA")){
			singleDMAInterface = true;
		}
		

		// -- Contains or not llfifo or dma	
		containsLlfifo = false
		containsDma = false
		if(!singleDMAInterface){
			for (connection : psToPlFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					containsLlfifo = true
					containsLlfifoTransmit = true
				} else if (TemplateUtils.getConnectionKind(connection).equals(
					ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
					containsDma = true
					containsDmaTransmit = true
				}
			}
	
			for (connection : plToPsFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					containsLlfifo = true
					containsLlfifoReceive = true
				} else if (TemplateUtils.getConnectionKind(connection).equals(
					ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
					containsDma = true
					containsDmaReceive = true
				}
			}
		}else{
			containsDma = true;
			if(!psToPlFifos.empty){
				containsDmaTransmit = true
			}
			if(!plToPsFifos.empty){
				containsDmaReceive = true
			}
		}
		
		// -- AXIS Wrappers Names
		axiWrappersNames = new HashMap
		if(!singleDMAInterface){
			for (connection : psToPlFifos) {
				axiWrappersNames.put(connection, "AxisInput_" + TemplateUtils.getQueueNameIO(connection))
			}
			for (connection : plToPsFifos) {
				axiWrappersNames.put(connection, "AxisOutput_" + TemplateUtils.getQueueNameIO(connection))
			}
		}

		// -- AXIS Interface Core Names
		axiInterfaceNames = new HashMap
		if(!singleDMAInterface){
			for (connection : psToPlFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					axiInterfaceNames.put(connection, "ll_" + TemplateUtils.getQueueNameIO(connection))
				} else if (TemplateUtils.getConnectionKind(connection).equals(
					ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
					axiInterfaceNames.put(connection, "dma_" + TemplateUtils.getQueueNameIO(connection))
				}
			}
			for (connection : plToPsFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					axiInterfaceNames.put(connection, "ll_" + TemplateUtils.getQueueNameIO(connection))
				} else if (TemplateUtils.getConnectionKind(connection).equals(
					ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
					axiInterfaceNames.put(connection, "dma_" + TemplateUtils.getQueueNameIO(connection))
				}
			}
		}

	}

	// ////////////////////////////////////////////////////////////////////////
	// -- Content
	// ////////////////////////////////////////////////////////////////////////
	def getContent() {
		'''
			«getFileHeader("Exelixi CPP Code Generation for Xilinx SDK")»
			
			«headers»
			
			«main»
		'''
	}

	// ////////////////////////////////////////////////////////////////////////
	// -- Preprocessor
	// ////////////////////////////////////////////////////////////////////////
	def getHeaders() {
		'''
			// -- Xilinx Headers
			#include <stdint.h>
			#include "xparameters.h"
			#include "xil_cache.h"	
			#include "xstatus.h"
			#include "xtime_l.h"
			«IF containsLlfifo»
				#include "xllfifo.h"
			«ENDIF»
			
			«IF containsDma»
				#include "xaxidma.h"
				«IF !singleDMAInterface»
					«FOR conn : psToPlFifos»
						«IF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
							#include "input_wrapper_«TemplateUtils.getQueueNameIO(conn)».h"
						«ENDIF»
					«ENDFOR»
					«FOR conn : plToPsFifos»
						«IF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
							#include "output_wrapper_«TemplateUtils.getQueueNameIO(conn)».h"
						«ENDIF»
					«ENDFOR»
				«ELSE»
					«IF !psToPlFifos.empty»
						#include "single_input_wrapper.h"
					«ENDIF»
					«IF !plToPsFifos.empty»
						#include "single_output_wrapper.h"
					«ENDIF»
				«ENDIF»	
			«ENDIF»
			
			«IF partitioner.containsPartition(ZYNQ_PS_PARTITION)»
				// -- PS Software Headers
				#include "fifo.h"
				#include "«network.simpleName»_ps.h"
			«ENDIF»
			«IF partitioner.containsPartition(ZYNQ_PL_PARTITION)» 
				#include "«network.simpleName»_pl.h"
			«ENDIF»
			«IF containsLlfifoTransmit»
				#include "llfifo_transmit.h"
			«ENDIF»
			«IF containsLlfifoReceive»
				#include "llfifo_receive.h"
			«ENDIF»
			«IF containsDmaTransmit»
				#include "dma_transmit.h"
			«ENDIF»
			«IF containsDmaReceive»
				#include "dma_receive.h"
			«ENDIF»
			
			// -- Timers
			static XTime startTime;
			
			#define TIMEDIFF(t1,t2) (t2 - t1)
			#define MILLISECONDS(t) (1000.0 * t / COUNTS_PER_SECOND)
		'''
	}

	// ////////////////////////////////////////////////////////////////////////
	// -- Network Class
	// ////////////////////////////////////////////////////////////////////////
	def getNetworkInstatiation() {
		'''
			// -- Network Instantiation
			«network.simpleName»_ps *ps_design = new «network.simpleName»_ps(«FOR conn : externalFifos SEPARATOR ", "»«TemplateUtils.getQueueNameIO(conn)»«ENDFOR»);
		'''
	}

	/**
	 *  note that the threshold is set as the size (could be size - 2)
	 */
	def instatiateExternalSoftwareFifos() {
		'''
			«FOR conn : externalFifos»
				«val size = getSize(conn)»
				Fifo<«conn.sourcePort.type.doSwitch»> *«TemplateUtils.getQueueNameIO(conn)» = new Fifo<«conn.sourcePort.type.doSwitch»>(«size», «size», «SdkUtil.getNbReadersConnection(conn)»);
			«ENDFOR»
		'''
	}

	// ////////////////////////////////////////////////////////////////////////
	// -- Main
	// ////////////////////////////////////////////////////////////////////////
	def getMain() {
		'''
			int main(){
				«IF networkHasHeterogeneousPartitions || network.hasAttribute(NETWORK_EXTERNAL_VARIABLES)»
					// -- Xilinx Status of success or failure
					int status;
				«ENDIF»
				
				«IF partitioner.containsPartition(ZYNQ_PS_PARTITION)»
					// -- Status of Actor Exeuction
					EStatus e_status;
				«ENDIF»
				
				«IF networkHasHeterogeneousPartitions»
					«IF !plToPsFifos.empty || !psToPlFifos.empty»
						// -- Instantiate & Initialize Interfaces
					«ENDIF»
					«IF !singleDMAInterface»
						«FOR conn : externalFifos»
							«IF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
								XLlFifo «axiInterfaceNames.get(conn)»;
							«ELSEIF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
								XAxiDma «axiInterfaceNames.get(conn)»;
							«ENDIF»
						«ENDFOR»
						«FOR conn : externalFifos»
							«IF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
								status = initLLFifo(&«axiInterfaceNames.get(conn)»,«SdkUtil.getXparDeviceId(axiInterfaceNames.get(conn))»);
								if (status != XST_SUCCESS) {
									xil_printf("Initialization of LLFifo «axiInterfaceNames.get(conn)» failed\n\r");
									return XST_FAILURE;
								}
							«ELSEIF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
								status = initAxiDma(&«axiInterfaceNames.get(conn)»,«SdkUtil.getXparDeviceId(axiInterfaceNames.get(conn))»);
								if (status != XST_SUCCESS) {
									xil_printf("Initialization of AXI DMA «axiInterfaceNames.get(conn)» failed\n\r");
									return XST_FAILURE;
								}
							«ENDIF»
							
						«ENDFOR»
					«ELSE»
						XAxiDma single_dma;
						
						status = initAxiDma(&single_dma,«SdkUtil.getXparDeviceId("single_dma")»);
						if (status != XST_SUCCESS) {
							xil_printf("Initialization of AXI DMA single_dma failed\n\r");
							return XST_FAILURE;
						}
					«ENDIF»
				«ENDIF»
				
				«IF partitioner.containsPartition(ZYNQ_PL_PARTITION)»	
					«IF network.hasAttribute(NETWORK_EXTERNAL_VARIABLES)»
						// -- TODO
					«ENDIF»
					«SdkUtil.getHwCoreType("kicker")» kicker_core;
					status = initCore(&kicker_core, «SdkUtil.getXparDeviceId("kicker")»);
					if (status != XST_SUCCESS) {
							xil_printf("Initialization of kicker IP Core failed\n\r");
					return XST_FAILURE;
					}
					«IF !singleDMAInterface»
						«FOR conn : externalFifos SEPARATOR "\n"»
							«SdkUtil.getHwCoreType(axiWrappersNames.get(conn))» wr«axiWrappersNames.get(conn)»;
							status = initCore(&wr«axiWrappersNames.get(conn)», «SdkUtil.getXparDeviceId(axiWrappersNames.get(conn))»);
							if (status != XST_SUCCESS) {
								xil_printf("Initialization of «axiWrappersNames.get(conn)» IP Core failed\n\r");
								return XST_FAILURE;
							}
						«ENDFOR»
					«ELSE»
						«IF !psToPlFifos.empty»
							«SdkUtil.getHwCoreType("AxisInput_Wrapper")» axis_input_wrapper;
							status = initCore(&axis_input_wrapper, «SdkUtil.getXparDeviceId("AxisInput_Wrapper")»);
							if (status != XST_SUCCESS) {
								xil_printf("Initialization of AxisInput_Wrapper IP Core failed\n\r");
								return XST_FAILURE;
							}
						«ENDIF»
						«IF !plToPsFifos.empty»
							«SdkUtil.getHwCoreType("AxisOutput_Wrapper")» axis_output_wrapper;
							status = initCore(&axis_output_wrapper, «SdkUtil.getXparDeviceId("AxisOutput_Wrapper")»);
							if (status != XST_SUCCESS) {
								xil_printf("Initialization of AxisOutput_Wrapper IP Core failed\n\r");
								return XST_FAILURE;
							}
						«ENDIF»
					«ENDIF»
				«ENDIF»
				
				«IF partitioner.containsPartition(ZYNQ_PL_PARTITION)»
					«IF containsDma»
						// -- AXIS to HLS fifo Wrappers
					«ENDIF»
					«IF !singleDMAInterface»
						«FOR conn : psToPlFifos»
							«IF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
								InputWrapper *wrapper_«axiInterfaceNames.get(conn)» = new InputWrapper_«TemplateUtils.getQueueIdIO(conn)»(&wr«axiWrappersNames.get(conn)»);
							«ENDIF»
						«ENDFOR»
						
						«FOR conn : plToPsFifos»
							«IF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
								OutputWrapper *wrapper_«axiInterfaceNames.get(conn)» = new OutputWrapper_«TemplateUtils.getQueueIdIO(conn)»(&wr«axiWrappersNames.get(conn)»);
							«ENDIF»
						«ENDFOR»
					«ELSE»
						«IF !psToPlFifos.empty»
							SingleInputWrapper *wrapper_input = new SingleInputWrapper(&axis_input_wrapper);
						«ENDIF»
						«IF !plToPsFifos.empty»
							SingleOutputWrapper *wrapper_output = new SingleOutputWrapper(&axis_output_wrapper);
						«ENDIF»
					«ENDIF»
				«ENDIF»
				
				
				// -- Instantiate Software
				«IF networkHasHeterogeneousPartitions»
					«instatiateExternalSoftwareFifos»
				«ENDIF»
				
				«IF partitioner.containsPartition(ZYNQ_PS_PARTITION)»
					«getNetworkInstatiation»
					
					«IF !psToPlFifos.empty»
						// -- Instantiate Transmit
						«IF !singleDMAInterface»
							«FOR conn : psToPlFifos»
								«IF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
									LLTransmit<«conn.sourcePort.type.doSwitch»> *transmit_«TemplateUtils.getQueueNameIO(conn)» = new LLTransmit<«conn.sourcePort.type.doSwitch»> (&ll_«TemplateUtils.getQueueNameIO(conn)», «TemplateUtils.getQueueNameIO(conn)», «conn.getAttribute("fifoId").objectValue»);
								«ELSEIF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
									AxiDMATransmit<«conn.sourcePort.type.doSwitch»> *transmit_«TemplateUtils.getQueueNameIO(conn)» = new AxiDMATransmit<«conn.sourcePort.type.doSwitch»> («TemplateUtils.getQueueNameIO(conn)», &«axiInterfaceNames.get(conn)»,wrapper_«axiInterfaceNames.get(conn)», «conn.getAttribute("fifoId").objectValue»);
								«ENDIF»	
							«ENDFOR»
						«ELSE»
							«FOR conn : psToPlFifos»
								AxiDMATransmit<«conn.sourcePort.type.doSwitch»> *transmit_«TemplateUtils.getQueueNameIO(conn)» = new AxiDMATransmit<«conn.sourcePort.type.doSwitch»> («TemplateUtils.getQueueNameIO(conn)», &single_dma, wrapper_input, «conn.getAttribute("fifoId").objectValue», «TemplateUtils.getQueueBroadcastId(conn)»);
							«ENDFOR»
						«ENDIF»
					«ENDIF»
					
					«IF !plToPsFifos.empty»
						// -- Instantiate Receivers
						«IF !singleDMAInterface»
							«FOR conn : plToPsFifos»
								«IF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
									LLReceive<«conn.sourcePort.type.doSwitch»> *receive_«TemplateUtils.getQueueNameIO(conn)» = new LLReceive<«conn.sourcePort.type.doSwitch»>(«TemplateUtils.getQueueNameIO(conn)», &ll_«TemplateUtils.getQueueNameIO(conn)»);
								«ELSEIF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
									AxiDMAReceive<«conn.sourcePort.type.doSwitch»> *receive_«TemplateUtils.getQueueNameIO(conn)» = new AxiDMAReceive<«conn.sourcePort.type.doSwitch»>(«TemplateUtils.getQueueNameIO(conn)», &«axiInterfaceNames.get(conn)»,wrapper_«axiInterfaceNames.get(conn)»);
								«ENDIF»	
							«ENDFOR»
						«ELSE»
							«FOR conn : plToPsFifos»
								AxiDMAReceive<«conn.sourcePort.type.doSwitch»> *receive_«TemplateUtils.getQueueNameIO(conn)» = new AxiDMAReceive<«conn.sourcePort.type.doSwitch»>(«TemplateUtils.getQueueNameIO(conn)», &single_dma, wrapper_output, «TemplateUtils.getQueueBroadcastId(conn)»);
							«ENDFOR»
						«ENDIF»
					«ENDIF»
					
					«IF networkHasHeterogeneousPartitions»
						// -- Start Cores
						«SdkUtil.getHwCoreType("kicker")»_Set_kick(&kicker_core, 1);
						«SdkUtil.getHwCoreType("kicker")»_Start(&kicker_core);
						«FOR conn : externalFifos»
							«IF TemplateUtils.getConnectionKind(conn).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
								«SdkUtil.getHwCoreType(axiWrappersNames.get(conn))»_Start(&wr«axiWrappersNames.get(conn)»);
							«ENDIF»	
						«ENDFOR»
					«ENDIF»
					
					ps_design->initialize();
					
					bool stop = false;
					
					XTime_GetTime(&startTime);
					do{
						e_status = None;
						«FOR conn : externalFifos»
							«IF partitioner.isInstanceOnPartition(conn.source as Instance, ZYNQ_PL_PARTITION) && partitioner.isInstanceOnPartition(conn.target as Instance, ZYNQ_PS_PARTITION)»
								receive_«TemplateUtils.getQueueNameIO(conn)»->receive(e_status);
							«ENDIF»
						«ENDFOR»
						ps_design->run(e_status);
						«FOR conn : externalFifos»
							«IF partitioner.isInstanceOnPartition(conn.source as Instance, ZYNQ_PS_PARTITION) && partitioner.isInstanceOnPartition(conn.target as Instance, ZYNQ_PL_PARTITION)»
								transmit_«TemplateUtils.getQueueNameIO(conn)»->transmit(e_status);
							«ENDIF»
						«ENDFOR»
						if(e_status == None){
							XTime endTime;
							XTime_GetTime(&endTime);
							XTime time_diff = TIMEDIFF(startTime, endTime);
							float time_diff_ms = MILLISECONDS(time_diff);
							if(time_diff_ms > 5000.0f){
								stop = 1;
							}
						}else{
							XTime_GetTime(&startTime);
						}
					}while(!stop);
				«ELSE»
					while(true);	
				«ENDIF»
				return XST_SUCCESS;
			}
		'''
	}

	// ////////////////////////////////////////////////////////////////////////
	// -- Helper Methods
	// ////////////////////////////////////////////////////////////////////////
	def getFileHeader(String comment) {
		var dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		var date = new Date();
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- Exelixi Design Systems 
			// -- All rights reserved, 2015 
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- «comment»
			// -- Date: «dateFormat.format(date)»
			// ----------------------------------------------------------------------------
		'''
	}

	def int getSize(Connection connection) {
		if (connection.size !== null) {
			return connection.size;
		} else {
			return fifoSize;
		}
	}
}
