/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk.network

import exelixi.hls.partitioner.Partitioner
import java.util.List
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network

/**
 * 
 * @author Endri Bezati
 */
class TestbenchMainPrinter {

	List<Instance> instances;
	Network network;

	new(Network network, Partitioner partitioner) {
		this.network = network;
		this.instances = partitioner.getInstances("PS");
	}

	def getContent() {
		'''
			#include "ff.h"
			«FOR instance : instances»
				#include "«instance.simpleName»_tb.h"
			«ENDFOR»
			
			FATFS fatfs; // fatfs pointer
			
			bool mount_disk();
			
			int main()
			{
				// mount the SD disk
				if(!mount_disk()){
					std::cout << "[ERROR] The SD card can not be mounted!\n";
					return -1;
				}
				
				int passed = 0;
				int not_passed = 0;
				
				«FOR instance : instances SEPARATOR "\n"»
					«instance.simpleName»_tb* «instance.simpleName» = new «instance.simpleName»_tb();
					if(«instance.simpleName»->run_test()){
						std::cout << "[OK] «instance.simpleName» PASSED! \n";
						passed++;
					}else{
						std::cout << "[ERROR] «instance.simpleName» NOT PASSED! \n";
						not_passed++;
					}
				«ENDFOR»
				
				std::cout << "PASSED....: "<< passed << "\n";
				std::cout << "NOT PASSED: "<< not_passed << "\n";
			
				return not_passed == 0;
			}
			
			bool mount_disk(){
				FRESULT res = f_mount(&fatfs, "0:/", 0);
				return (res == FR_OK);
			}
		'''
	}

}
