/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk.network

import exelixi.cpp.backend.templates.common.ExprAndTypePrinter
import exelixi.hls.partitioner.Partitioner
import exelixi.zynq.backend.templates.sdk.SdkUtil
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date
import java.util.List
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.ir.Var

import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION

/**
 * 
 * @author Endri Bezati
 */
class NetworkPrinter extends ExprAndTypePrinter {

	protected Network network

	List<Connection> externalFifos

	Partitioner partitioner;

	new(Network network, Partitioner partitioner) {
		this.network = network
		this.partitioner = partitioner;
		var List<Connection> interfaceConnections = partitioner.getConnections(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION)
		interfaceConnections.addAll(partitioner.getConnections(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION))
		this.externalFifos = getConnectionsWithDifferentBroadcast(interfaceConnections)
	}

	def getContent() {
		'''
			«getFileHeader("Exelixi CPP Code Generation for Xilinx SDK")»
			«getHeaders»
			«networkClass»
		'''
	}

	// ////////////////////////////////////////////////////////////////////////
	// -- Headers
	// ////////////////////////////////////////////////////////////////////////
	def getHeaders() {
		'''
			#include <stdint.h>
			#include "actor.h"
			#include "fifo.h"
			
			«FOR instance : network.children.filter(typeof(Instance))»
				«IF partitioner.isInstanceOnPartition(instance, ZYNQ_PS_PARTITION)»
					#include "«instance.simpleName».h"
				«ENDIF»
			«ENDFOR»
			
		'''
	}

	// ////////////////////////////////////////////////////////////////////////
	// -- Network Class
	// ////////////////////////////////////////////////////////////////////////
	def getNetworkClass() {

		'''
			class «network.simpleName»_ps{
				public:
					// -- Constructor
					«network.simpleName»_ps(«FOR conn : externalFifos SEPARATOR ", "»Fifo<«conn.sourcePort.type.doSwitch»> *«TemplateUtils.getQueueNameIO(conn)»«ENDFOR»){
						// -- Instantiate Fifos
						«instatiateFifos»
						
						// -- Instantiate Actors
						«FOR instance : network.children.filter(typeof(Instance))»
							«IF partitioner.isInstanceOnPartition(instance, ZYNQ_PS_PARTITION)»
								«instatiateActor(instance)»
							«ENDIF»
						«ENDFOR»
						
						// -- Connect Instances and Fifos
						«connectInstanceFifo»
					}
					
					«initializeMethod»	
					«getRunMethod»
				
					
				private:
					// -- Instances
					«instances»
					// -- Fifos
					«fifos»
			};
		'''
	}

	def getInstances() {
		'''
			«FOR instance : network.children.filter(typeof(Instance))»
				«IF partitioner.isInstanceOnPartition(instance, ZYNQ_PS_PARTITION)»
					«instance.simpleName» *act_«instance.simpleName»;
				«ENDIF»
			«ENDFOR»
		'''
	}

	def getFifos() {
		
		var List<Connection> psPsConnections = getConnectionsWithDifferentBroadcast(partitioner.getConnections(ZYNQ_PS_PARTITION,ZYNQ_PS_PARTITION))
		var List<Connection> plPsConnections = getConnectionsWithDifferentBroadcast(partitioner.getConnections(ZYNQ_PL_PARTITION,ZYNQ_PS_PARTITION))
		var List<Connection> psPlConnections = getConnectionsWithDifferentBroadcast(partitioner.getConnections(ZYNQ_PS_PARTITION,ZYNQ_PL_PARTITION))
		
		var List<Connection> connections = new ArrayList
		var List<Connection> fifos = new ArrayList		
		
		connections.addAll(psPsConnections)
		connections.addAll(plPsConnections)
		connections.addAll(psPlConnections)
		
		for(connection : connections){
			if(fifos.empty){
				fifos.add(connection)
			}else{
				var boolean itContainsBdId = false
				for (Connection c : fifos) {
					val Integer connectionBdId = connection.getAttribute("idNoBcast").objectValue as Integer;
					val Integer cBdId = c.getAttribute("idNoBcast").objectValue as Integer;
					if (cBdId.equals(connectionBdId)) {
						itContainsBdId = true;
					}
				}
				if (!itContainsBdId) {
					fifos.add(connection);
				}
			}
		}
				
		'''
			«FOR conn : fifos»
				Fifo<«conn.sourcePort.type.doSwitch»> *fifo_«conn.getAttribute("idNoBcast").objectValue»;
			«ENDFOR»
		'''
	}

	def instatiateActor(Instance instance) {
		val List<Var> parameters = instance.getActor.parameters
		'''
			act_«instance.simpleName» = new «instance.simpleName»(«FOR parameter : parameters SEPARATOR ", "»«IF instance.getArgument(parameter.name) !== null»«instance.getArgument(parameter.name).value.doSwitch»«ELSE»«parameter.initialValue.doSwitch»«ENDIF»«ENDFOR»);
		'''
	}

	/**
	 *  note that the threshold is set as the size
	 */
	def instatiateFifos() {
		var List<Connection> psPsConnections = getConnectionsWithDifferentBroadcast(partitioner.getConnections(ZYNQ_PS_PARTITION,ZYNQ_PS_PARTITION))
		var List<Connection> plPsConnections = getConnectionsWithDifferentBroadcast(partitioner.getConnections(ZYNQ_PL_PARTITION,ZYNQ_PS_PARTITION))
		var List<Connection> psPlConnections = getConnectionsWithDifferentBroadcast(partitioner.getConnections(ZYNQ_PS_PARTITION,ZYNQ_PL_PARTITION))
		
		var List<Connection> connections = new ArrayList
		var List<Connection> fifos = new ArrayList		
		
		connections.addAll(plPsConnections)
		connections.addAll(psPlConnections)
		connections.addAll(psPsConnections)
		
		for(connection : connections){
			if(fifos.empty){
				fifos.add(connection)
			}else{
				var boolean itContainsBdId = false
				for (Connection c : fifos) {
					val Integer connectionBdId = connection.getAttribute("idNoBcast").objectValue as Integer;
					val Integer cBdId = c.getAttribute("idNoBcast").objectValue as Integer;
					if (cBdId.equals(connectionBdId)) {
						itContainsBdId = true;
					}
				}
				if (!itContainsBdId) {
					fifos.add(connection);
				}
			}
		}
		
		'''
			«FOR conn : fifos»
				«IF partitioner.isInstanceOnPartition( conn.source as Instance, ZYNQ_PS_PARTITION) && partitioner.isInstanceOnPartition(conn.target as Instance, ZYNQ_PS_PARTITION)»
					this->fifo_«conn.getAttribute("idNoBcast").objectValue» = new Fifo<«conn.sourcePort.type.doSwitch»>(«getSize(conn)», «conn.getAttribute("threshold").objectValue», «SdkUtil.getNbReadersConnection(conn)»);
				«ELSE»
					this->fifo_«conn.getAttribute("idNoBcast").objectValue» = «TemplateUtils.getQueueNameIO(conn)»;
				«ENDIF»
			«ENDFOR»
		'''
	}

	def connectInstanceFifo() {
		'''
			«FOR instance : network.children.filter(typeof(Instance))»
				«FOR port : instance.outgoingPortMap.keySet»
					«IF partitioner.isInstanceOnPartition(instance, ZYNQ_PS_PARTITION)»
						act_«instance.name»->port_«port.name» = fifo_«instance.outgoingPortMap.get(port).get(0).getAttribute("idNoBcast").objectValue»;
					«ENDIF»
					«FOR connection : instance.outgoingPortMap.get(port)»
						«IF partitioner.isInstanceOnPartition(connection.target as Instance, ZYNQ_PS_PARTITION)»
							act_«(connection.target as Instance).name»->port_«connection.targetPort.name» = fifo_«connection.getAttribute("idNoBcast").objectValue»;
						«ENDIF»
					«ENDFOR»
				«ENDFOR»
			«ENDFOR»
		'''
	}

	def getRunMethod() {
		'''
			void run(EStatus& status){
				// -- Run 
				«FOR instance : network.children.filter(typeof(Instance))»
					«IF partitioner.isInstanceOnPartition(instance, ZYNQ_PS_PARTITION)»
						act_«instance.simpleName»->action_selection(status);
					«ENDIF»
				«ENDFOR»
			}
		'''
	}

	def getInitializeMethod() {
		'''
			void initialize(){
				// -- Initialize instance
				«FOR instance : network.children.filter(typeof(Instance))»
					«IF partitioner.isInstanceOnPartition(instance, ZYNQ_PS_PARTITION)»
						act_«instance.simpleName»->initialize();
					«ENDIF»
				«ENDFOR»
			}
		'''
	}

	// ////////////////////////////////////////////////////////////////////////
	// -- Helper Methods
	// ////////////////////////////////////////////////////////////////////////
	def getFileHeader(String comment) {
		var dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		var date = new Date();
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- «comment»
			// -- Date: «dateFormat.format(date)»
			// ----------------------------------------------------------------------------
		'''
	}

	def int getSize(Connection connection) {
		if (connection.size !== null) {
			return connection.size;
		} else {
			return fifoSize;
		}
	}


	def List<Connection> getConnectionsWithDifferentBroadcast(List<Connection> connections) {
		var List<Connection> returnConnections = new ArrayList

		for (Connection connection : connections) {
			if (returnConnections.isEmpty) {
				returnConnections.add(connection);
			} else {
				var boolean itContainsBdId = false
				for (Connection c : returnConnections) {
					val Integer connectionBdId = connection.getAttribute("idNoBcast").objectValue as Integer;
					val Integer cBdId = c.getAttribute("idNoBcast").objectValue as Integer;
					if (cBdId.equals(connectionBdId)) {
						itContainsBdId = true;
					}
				}
				if (!itContainsBdId) {
					returnConnections.add(connection);
				}
			}

		}

		return returnConnections
	}

}
