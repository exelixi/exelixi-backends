/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.sdk.instance

import exelixi.cpp.backend.templates.common.ExprAndTypePrinter
import java.text.SimpleDateFormat
import java.util.Date
import java.util.List
import java.util.Map
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.graph.Edge
import org.eclipse.emf.common.util.EList

/**
 * 
 * @author Endri Bezati
 */
class TestbenchInstancePrinter extends ExprAndTypePrinter {

	private Instance instance;
	protected Actor actor;
	private String instanceName;
	private EList<Edge> incoming;
	private Map<Port, List<Connection>> outgoingPortMap;

	new(Instance instance) {
		this.instance = instance;
		this.instanceName = instance.simpleName;
		this.actor = instance.getActor;
		this.incoming = instance.incoming
		this.outgoingPortMap = instance.outgoingPortMap
	}

	def getContent() {
		'''
					#ifndef __«instanceName.toUpperCase»_TB_H__
					#define __«instanceName.toUpperCase»_TB_H__
					«getFileHeader»
					
					#include <sstream>
					#include <string>
					#include <iostream>
					#include "file_line_reader.h"
					#include <«instanceName».h>
					
					class «instanceName»_tb{
				
					public:
						«instanceName»_tb(){
						}
						
						bool run_test() {
						
							if (!open_files()) {
								return false;
							}
							
							// create the actor
							actor = new «instanceName»();
					
							// create and connect the fifos
							«initPorts»
							
							// initialize the actor
							actor->initialize();
					
							// launch the execution
							EStatus status;
							bool error = false;
							do {
								status = None;
								
								// populate the input buffers
								feed_input_tokens(status);
								
								// execute the actor
								actor->action_selection(status);
								
								// consume and check the output data
								error = !consume_output_tokens(status); 
			
							} while (status != None && !error);
			
							// check if all tokens have been consumed
							// it will give only a warning in case of unconsumed tokens
							check_unused_tokens();
							
							close_files();
					
							return !error;
						}
					
					private:
						
						«instanceName» * actor;
						char string_buff[256];
						
						«definePorts»
						
						«consumeOutputTokens»
						
						«feedInputTokens»
						
						«openPortFile»
						
						«checkUnusedTokens»
						
						«closePortFile»
						
						
					};
					
					
					#endif // __«instanceName.toUpperCase»_TB_H__
			'''
	}
	
	def checkUnusedTokens() {
		val connectedOutput = [Port port|instance.outgoingPortMap.get(port) !== null]
		'''
			bool check_unused_tokens(){
				bool unused_tokens = false;
				
				«FOR edge : incoming»
					«(edge as Connection).targetPort.checkUnusedTokens(edge as Connection)»
				«ENDFOR»			
				
				«FOR port : actor.outputs.filter(connectedOutput)»
					«FOR connection : outgoingPortMap.get(port)»
						«port.checkUnusedTokens(connection)»
					«ENDFOR»
				«ENDFOR»
				
				return unused_tokens;
			}
		'''
	}
	
	def checkUnusedTokens(Port port, Connection connection){
		var name = "port_" + port.name + "_fifo_" + connection.getAttribute("id").objectValue;
		'''
			unsigned int unused_tokens_«name» = actor->«name»->count(0);
			if(unused_tokens_«name»){
				std::cout << " [WARNING] " << unused_tokens_«name» << "unused tokens on port «port.name»\n";
				unused_tokens = true; 
			}
		'''
	}
	
	 

	// -- Get Content For each Top Level
	private def getFileHeader() {
		var dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		var date = new Date();
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- Date: «dateFormat.format(date)»
			// ----------------------------------------------------------------------------
		'''
	}

	private def definePorts() {
		val connectedOutput = [Port port|instance.outgoingPortMap.get(port) !== null]
		'''
			«FOR edge : incoming»
				«(edge as Connection).targetPort.definePort(edge as Connection)»
			«ENDFOR»			
			
			«FOR port : actor.outputs.filter(connectedOutput)»
				«FOR connection : outgoingPortMap.get(port)»
					«port.definePort(connection)»
				«ENDFOR»
			«ENDFOR»
		'''
	}

	private def initPorts() {
		val connectedOutput = [Port port|instance.outgoingPortMap.get(port) !== null]
		'''
			«FOR edge : incoming»
				«(edge as Connection).targetPort.initPort(edge as Connection)»
			«ENDFOR»			
			
			«FOR port : actor.outputs.filter(connectedOutput)»
				«FOR connection : outgoingPortMap.get(port)»
					«port.initPort(connection)»
				«ENDFOR»
			«ENDFOR»
		'''
	}

	private def initPort(Port port, Connection connection) {
		var name = "port_" + port.name + "_fifo_" + connection.getAttribute("id").objectValue;
		'''
			actor->«name» = new Fifo<«port.type.doSwitch», 1>(512,512);
			tokens_«name» = 0;
		'''

	}

	private def definePort(Port port, Connection connection) {
		var name = "port_" + port.name + "_fifo_" + connection.getAttribute("id").objectValue;
		'''
			FileLineReader* filer_«name»;
			int tokens_«name»;
		'''

	}

	private def consumeOutputTokens() {
		val connectedOutput = [Port port|instance.outgoingPortMap.get(port) !== null]
		'''
			bool consume_output_tokens(EStatus& status){
				
				«FOR port : actor.outputs.filter(connectedOutput)»
					«FOR connection : outgoingPortMap.get(port)»
						«var portName = "port_" + port.name + "_fifo_" + connection.getAttribute("id").objectValue»
						while(filer_«portName»->has_next() && actor->«portName»->count(0)){
							status = hasExecuted;
							
							filer_«portName»->read_line(string_buff);
							std::string buff_s(string_buff);
							
							int tmp;
							std::istringstream iss(buff_s);
							iss >> tmp;
							
							«port.type.doSwitch» golden = («port.type.doSwitch») tmp;
							«port.type.doSwitch» value = actor->«portName»->read_address(0)[0];
							actor->«portName»->read_advance(0,1);
							tokens_«portName»++;
							
							if(golden != value){
								std::cout << "[ERROR]: Token " << tokens_«portName» << " on port «portName» does not match: " << value <<"/" << golden<<"\n";
								return false;
							}
							
							
						}
						
					«ENDFOR»
				«ENDFOR»
				
				return true;
			
			}
		'''
	}

	private def feedInputTokens() {
		'''
			void feed_input_tokens(EStatus& status){
			«FOR edge : incoming»
				«var conn = edge as Connection»
				«var port = conn.targetPort»
				«var portName = "port_" + port.name + "_fifo_" + conn.getAttribute("id").objectValue»	
					while(filer_«portName»->has_next() && actor->«portName»->rooms()>0){
						status = hasExecuted;
						
						filer_«portName»->read_line(string_buff);
						std::string buff_s(string_buff);
						
						int tmp;
						std::istringstream iss(buff_s);
						iss >> tmp;
						
						actor->«portName»->write_address()[0] = («port.type.doSwitch») tmp;
						actor->«portName»->write_advance();
						tokens_«portName»++;
						
					}
			«ENDFOR»
			}
		'''
	}

	private def openPortFile() {
		val connectedOutput = [Port port|instance.outgoingPortMap.get(port) !== null]
		'''
			bool open_files() {
				
				bool all_opened = true;				
				«FOR edge : incoming»
					«(edge as Connection).targetPort.openPortFile(edge as Connection)»
				«ENDFOR»			
				
				«FOR port : actor.outputs.filter(connectedOutput)»
					«FOR connection : outgoingPortMap.get(port)»
						«port.openPortFile(connection)»
					«ENDFOR»
				«ENDFOR»
				return all_opened;
			}
		'''
	}
	
	
	private def closePortFile() {
		val connectedOutput = [Port port|instance.outgoingPortMap.get(port) !== null]
		'''
			void close_files() {
				
				«FOR edge : incoming»
					«(edge as Connection).targetPort.closePortFile(edge as Connection)»
				«ENDFOR»			
				
				«FOR port : actor.outputs.filter(connectedOutput)»
					«FOR connection : outgoingPortMap.get(port)»
						«port.closePortFile(connection)»
					«ENDFOR»
				«ENDFOR»
			}
		'''
	}

	private def openPortFile(Port port, Connection connection) {
		'''
			filer_port_«port.name»_fifo_«connection.getAttribute("id").objectValue» = new FileLineReader();
			if(!filer_port_«port.name»_fifo_«connection.getAttribute("id").objectValue»->open("«portFilePath(instance, port)»")){
				std::cout << "Cannot open file: «port.name».txt\n";
				all_opened = false;
			}
		'''
	}
	
	private def closePortFile(Port port, Connection connection) {
		'''
			filer_port_«port.name»_fifo_«connection.getAttribute("id").objectValue»->close();
		'''
	}
	
	private def String portFilePath(Instance instance, Port port) {
		var dir = "fifo-traces/";
		if (instance !== null) {
			for (String sp : instance.hierarchicalId.subList(1, instance.hierarchicalId.size)) {
				dir += (sp + "/");
			}
		}
		return dir + port.name + ".txt";
	}
}
