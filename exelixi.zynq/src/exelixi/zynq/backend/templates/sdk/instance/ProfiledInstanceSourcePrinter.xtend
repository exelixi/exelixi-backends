/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.zynq.backend.templates.sdk.instance

import exelixi.hls.partitioner.Partitioner
import java.util.Map
import net.sf.orcc.df.Action
import net.sf.orcc.df.Instance
import net.sf.orcc.df.State

class ProfiledInstanceSourcePrinter extends SDKInstanceSourcePrinter {

	new(Partitioner partitioner, Instance instance, Map<String, Object> options) {
		super(partitioner, instance, options)
	}

	override compileScheduler(
		Action action,
		State state
	) '''
	(«FOR e : action.inputPattern.numTokensMap»«IF instance.incomingPortMap.get(e.key) !== null»status_«e.key.name»_ >= «e.value» && «ENDIF»«ENDFOR»«action.scheduler.name»()) {
		«IF !action.outputPattern.empty»
		if(«FOR e : action.outputPattern.numTokensMap SEPARATOR " &&" »«IF instance.outgoingPortMap.get(e.key) !== null»status_«e.key.name»_ >= «e.value» «ENDIF»«ENDFOR») {
			reset_ccnt();                   // Reset the CCNT (cycle counter)
			reset_pmn();                    // Reset the configurable counters
			«action.body.name»();
			v7_cycles[«actor.actions.indexOf(action)»] += read_ccnt();     // Read CCNT
			v7_overflows[«actor.actions.indexOf(action)»] = read_flags(); // Check for overflow flag
			v7_executions[«actor.actions.indexOf(action)»]++;
			
			res = true;
			status = hasExecuted;
			«IF state !== null»state_ = state_«state.name»;«ENDIF»
		}
		«ELSE»
		reset_ccnt();                   // Reset the CCNT (cycle counter)
		reset_pmn();                    // Reset the configurable counters
		«action.body.name»();
		v7_cycles[«actor.actions.indexOf(action)»] += read_ccnt();     // Read CCNT
		v7_overflows[«actor.actions.indexOf(action)»] = read_flags(); // Check for overflow flag
		v7_executions[«actor.actions.indexOf(action)»]++;
		res = true;
		status = hasExecuted;
		«IF state !== null»state_ = state_«state.name»;«ENDIF»
		«ENDIF»
	}'''

}
