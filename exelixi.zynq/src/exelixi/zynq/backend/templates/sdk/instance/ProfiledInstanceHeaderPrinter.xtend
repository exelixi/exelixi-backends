/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk.instance

import java.util.HashSet
import java.util.Map
import net.sf.orcc.df.Instance
import net.sf.orcc.ir.Var
import exelixi.hls.partitioner.Partitioner

/**
 * 
 * @author Endri Bezati
 */
class ProfiledInstanceHeaderPrinter extends SDKInstanceHeaderPrinter {

	new(Partitioner partitioner, Instance instance, Map<String, Object> options) {
		super(partitioner, instance, options)
	}

	override getContent() {
		'''
			«getFileHeader»
			«getIncludes»
			«IF !actor.procs.filter(p | p.native && !"print".equals(p.name)).isEmpty()»
				#include "native.h"
			«ENDIF»
			
			«IF actor.hasAttribute("actor_shared_variables")»
				// -- Shared Variables
				«FOR v : actor.getAttribute("actor_shared_variables").objectValue as HashSet<Var>»
					extern «v.type.doSwitch» «v.name»«FOR dim : v.type.dimensions»[«dim»]«ENDFOR»;
				«ENDFOR»
			«ENDIF»
					
			class «instanceName»: public Actor
			{
			public:
				
				// ARM v7 Profiling
				unsigned int v7_cycles[«actor.actions.size»];
				unsigned int v7_overflows[«actor.actions.size»];
				unsigned int v7_executions[«actor.actions.size»];
				
				«instanceName»(«FOR param : instance.getActor.parameters SEPARATOR ", "»«param.type.doSwitch»«FOR dim : param.type.dimensions»[«dim»]«ENDFOR» «param.name»«ENDFOR»)
				«FOR param : instance.getActor.parameters BEFORE ":" SEPARATOR "\n,"»«param.name»(«param.name»)«ENDFOR»
				{
				«IF actor.fsm !== null»state_ = state_«actor.fsm.initialState.name»;«ENDIF»
				
				«FOR i : 0 .. actor.actions.size-1»
				v7_executions[«i»] = 0;
				«ENDFOR»
				«FOR i : 0 .. actor.actions.size-1»
				v7_cycles[«i»] = 0;
				«ENDFOR»
				}
			
				«getPorts»
				«getObjects»
				
				
			
				«actor.procs.filter(p | !p.native).map[compileProcedure].join»
				«actor.initializes.map[compileAction].join»
			
				
				void initialize() {
					«FOR action : actor.initializes»
						«action.name»();
					«ENDFOR»
				}
			
				«actor.actions.map[compileAction].join»
			
				void action_selection(EStatus& status);
				
				void printActorProfiling() {
					std::cout << "Profiling for Actor: «actor.simpleName» " << std::endl;
					std::cout << "Actions : "<< std::endl;
					«FOR action : actor.actions»
						std::cout << "\t- «action.name», Mean Cycles:" <<  v7_cycles[«actor.actions.indexOf(action)»] / v7_executions[«actor.actions.indexOf(action)»] <<", Executions: " << v7_executions[«actor.actions.indexOf(action)»] << std::endl; 
					«ENDFOR»
				}
				
				void printActorProfilingCSV() {
					«FOR action : actor.actions»
						std::cout << "«actor.simpleName»; «action.name»; " << v7_cycles[«actor.actions.indexOf(action)»] / v7_executions[«actor.actions.indexOf(action)»] << "; " << v7_executions[«actor.actions.indexOf(action)»] << std::endl;
					«ENDFOR»
				}
			
			
				void getState() {
					«IF actor.hasFsm»
						switch(state_) {
						«FOR state : actor.fsm.states»
							case state_«state.name»:
								std::cout << "Actor :«instanceName», Last State: «state.name»" << std::endl;
								break;
						«ENDFOR»
						}
					«ELSE»
						std::cout << "Actor :«actor.simpleName» has no state" << std::endl;
					«ENDIF»
				}
				
			private:	
				«FOR param : actor.parameters SEPARATOR "\n"»«param.varDecl»;«ENDFOR»
				«FOR variable : actor.stateVars SEPARATOR "\n"»«IF !variable.hasAttribute("shared")»«variable.varDecl»«IF variable.initialValue !==null» = «variable.initialValue.doSwitch»«ENDIF»;«ENDIF»«ENDFOR»
				«FOR port : actor.inputs SEPARATOR "\n"»int status_«port.name»_;«ENDFOR»
				«FOR port : actor.outputs SEPARATOR "\n"»int status_«port.name»_;«ENDFOR»
				«IF actor.fsm !== null»
				enum states {
					«FOR state : actor.fsm.states SEPARATOR ","»
						state_«state.name»
					«ENDFOR»
				} state_;
				«ENDIF»
			};
			#endif
		'''
	}

	override getIncludes() {
		'''
			«super.includes»
			#include "v7_pmu.h"
		'''
	}
}
