/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk.plcore

import exelixi.cpp.backend.templates.common.ExprAndTypePrinter
import exelixi.hls.partitioner.Partitioner
import exelixi.zynq.backend.templates.sdk.SdkUtil
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date
import java.util.HashMap
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.ir.Type
import net.sf.orcc.ir.Var

import static exelixi.hls.backend.Constants.NETWORK_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING

class PLCoreSourcePrinter extends ExprAndTypePrinter {

	Network network

	private Map<String, Object> options

	protected Partitioner partitioner

	var Map<Instance, List<Var>> instancesExternalVariablesMap

	protected List<Instance> partitionInstances;

	private List<Connection> plToPsFifos

	private List<Connection> psToPlFifos

	private boolean containsLlfifo

	private boolean containsDma

	private boolean singleDma

	new(Network network, Partitioner partitioner, Map<String, Object> options) {
		this.network = network
		this.options = options
		this.partitioner = partitioner
		this.instancesExternalVariablesMap = new HashMap
		this.partitionInstances = partitioner.getInstances(ZYNQ_PL_PARTITION);

		// -- PS/PL IO
		this.psToPlFifos = new ArrayList<Connection>(
			partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION).keySet)
		this.plToPsFifos = new ArrayList<Connection>(
			partitioner.getConnectionToConnectionBd(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION).keySet)

		if (network.hasAttribute(NETWORK_EXTERNAL_VARIABLES)) {
			this.instancesExternalVariablesMap = network.getAttribute(NETWORK_EXTERNAL_VARIABLES).
				objectValue as Map<Instance, List<Var>>

		}

		// -- Communication Kind
		singleDma = false;
		val String communicationKind = options.get(ZYNQ_OPTION_COMMUNICATION_KIND) as String
		if (communicationKind.equals("Single DMA")) {
			singleDma = true;
		}

		// -- Contains or not llfifo or dma	
		containsLlfifo = false
		containsDma = false
		if (!singleDma) {
			for (connection : psToPlFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					containsLlfifo = true
				} else if (TemplateUtils.getConnectionKind(connection).equals(
					ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
					containsDma = true
				}
			}

			for (connection : plToPsFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					containsLlfifo = true
				} else if (TemplateUtils.getConnectionKind(connection).equals(
					ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
					containsDma = true
				}
			}
		} else {
			containsDma = true;
		}

	}

	def getContent() {
		'''
			«getFileHeader("PL Initialization Source")»
			
			«headers»
			
			«IF !instancesExternalVariablesMap.empty»«externalMemories»«ENDIF»
			«functions»
		'''
	}

	def getHeaders() {
		'''
			#include "«network.simpleName»_pl.h"
		'''
	}

	def getExternalMemories() {
		'''
			«FOR instance : instancesExternalVariablesMap.keySet»
				«FOR variable : instancesExternalVariablesMap.get(instance)»
					volatile «variable.type.doSwitch» «variable.name»_«instance.simpleName»[«getFlatListDepth(variable.type)»];
				«ENDFOR»
			«ENDFOR»
		'''
	}

	def getFunctions() {
		'''
			«IF partitioner.containsPartition(ZYNQ_PL_PARTITION)»
				«IF containsLlfifo»
					«initLLFifo»
				«ENDIF»
				
				«IF containsDma»
					«initDma»
				«ENDIF»
				
				«getInitCore("kicker", false)»
				«IF !singleDma»
					«FOR connection : psToPlFifos»
						«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
							«getInitCore("AxisInput_" + TemplateUtils.getQueueNameIO(connection), true)»
						«ELSEIF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
							«getInitCore("AxisInput_" + TemplateUtils.getQueueNameIO(connection), false)»
						«ENDIF»
					«ENDFOR»
					
					«FOR connection : plToPsFifos»
						«IF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
							«getInitCore("AxisOutput_" + TemplateUtils.getQueueNameIO(connection), true)»
						«ELSEIF TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
							«getInitCore("AxisOutput_" + TemplateUtils.getQueueNameIO(connection), false)»
						«ENDIF»
					«ENDFOR»	
				«ELSE»
					«IF !psToPlFifos.empty»
						«getInitCore("AxisInput_Wrapper", false)»
					«ENDIF»
					«IF !plToPsFifos.empty»
						«getInitCore("AxisOutput_Wrapper", false)»
					«ENDIF»
				«ENDIF»
				«IF network.hasAttribute(NETWORK_EXTERNAL_VARIABLES)»
					// -- TODO
				«ENDIF»
			«ENDIF»
		'''
	}

	def getInitLLFifo() {
		'''
			int initLLFifo(XLlFifo *instancePtr, u16 deviceId) {
				int status;
			
				// -- Look Up Configuration
				XLlFifo_Config *cfg;
				cfg = XLlFfio_LookupConfig(deviceId);
				if (!cfg) {
					xil_printf("No configuration found for %d\r\n", deviceId);
					return XST_FAILURE;
				}
			
				// -- Initialize Device
				status = XLlFifo_CfgInitialize(instancePtr, cfg, cfg->BaseAddress);
				if (status != XST_SUCCESS) {
					xil_printf("Initialization failed\n\r");
					return status;
				}
				/* Check for the Reset value */
				status = XLlFifo_Status(instancePtr);
				XLlFifo_IntClear(instancePtr, 0xffffffff);
				status = XLlFifo_Status(instancePtr);
				if (status != 0x0) {
					xil_printf("\n ERROR : Reset value of ISR0 : 0x%x\t"
							"Expected : 0x0\n\r", XLlFifo_Status(instancePtr));
					return XST_FAILURE;
				}
			
				return status;
			}
		'''
	}

	def getInitDma() {
		'''
			int initAxiDma(XAxiDma *instancePtr, u16 deviceId) {
				int status;
				XAxiDma_Config *cfg;
			
				cfg = XAxiDma_LookupConfig(deviceId);
				if (!cfg) {
					xil_printf("No config found for %d\r\n", deviceId);
					return XST_FAILURE;
				}
			
				status = XAxiDma_CfgInitialize(instancePtr, cfg);
				if (status != XST_SUCCESS) {
					xil_printf("Initialization failed %d\r\n", status);
					return XST_FAILURE;
				}
			
				if (XAxiDma_HasSg(instancePtr)) {
					xil_printf("Device configured as SG mode \r\n");
					return XST_FAILURE;
				}
			
				XAxiDma_IntrDisable(instancePtr, XAXIDMA_IRQ_ALL_MASK,
						XAXIDMA_DEVICE_TO_DMA);
			
				return status;
			}
		'''
	}

	def getInitCore(Instance instance) {
		'''
			int initCore(«SdkUtil.getHwCoreType(instance)» *instancePtr, u16 deviceId){
				int status;
				
				// -- Get Configuration
				«SdkUtil.getHwCoreType(instance)»_Config *cfg;
				cfg = «SdkUtil.getHwCoreType(instance)»_LookupConfig(deviceId);
				if (!cfg) {
					xil_printf("No configuration found for %d\r\n", deviceId);
					return XST_FAILURE;
				}
				
				// -- Initialize the Device
				status = «SdkUtil.getHwCoreType(instance)»_CfgInitialize(instancePtr, cfg);
				if (status != XST_SUCCESS) {
					xil_printf("Initialization of «SdkUtil.getHwCoreType(instance)» failed\n\r");
					return status;
				}
				
				«IF !instancesExternalVariablesMap.empty»
					// -- Set AXI Master Core memory offset
					«IF instancesExternalVariablesMap.containsKey(instance)»
						«FOR variable : instancesExternalVariablesMap.get(instance)»
							«SdkUtil.getHwCoreType(instance)»_Set_«variable.name»_«instance.simpleName»_offset(instancePtr, (UINTPTR) «variable.name»_«instance.simpleName»);
						«ENDFOR»
					«ENDIF»
				«ENDIF»
				
					«SdkUtil.getHwCoreType(instance)»_EnableAutoRestart(instancePtr);
					
				return status;
			}
		'''
	}

	def getInitCore(String core, boolean enableAutoRestart) {
		'''
			int initCore(«SdkUtil.getHwCoreType(core)» *instancePtr, u16 deviceId){
				int status;
				
				// -- Get Configuration
				«SdkUtil.getHwCoreType(core)»_Config *cfg;
				cfg = «SdkUtil.getHwCoreType(core)»_LookupConfig(deviceId);
				if (!cfg) {
					xil_printf("No configuration found for %d\r\n", deviceId);
					return XST_FAILURE;
				}
				
				// -- Initialize the Device
				status = «SdkUtil.getHwCoreType(core)»_CfgInitialize(instancePtr, cfg);
				if (status != XST_SUCCESS) {
					xil_printf("Initialization of «SdkUtil.getHwCoreType(core)» failed\n\r");
					return status;
				}
				«IF enableAutoRestart»
					«SdkUtil.getHwCoreType(core)»_EnableAutoRestart(instancePtr);
				«ENDIF»
				
				return status;
			}
		'''
	}

	// ////////////////////////////////////////////////////////////////////////
	// -- Helper Methods
	// ////////////////////////////////////////////////////////////////////////
	def getFileHeader(String comment) {
		var dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		var date = new Date();
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- «comment»
			// -- Date: «dateFormat.format(date)»
			// ----------------------------------------------------------------------------
		'''
	}

	def getHwCoreHeader() {
		'''
			#include "x«network.simpleName.toLowerCase»_pl.h"
		'''
	}

	def int getFlatListDepth(Type type) {
		var int r = 1;
		if (type.isList) {
			for (int dim : type.getDimensions) {
				r = r * dim;
			}
		}
		return r;
	}
}
