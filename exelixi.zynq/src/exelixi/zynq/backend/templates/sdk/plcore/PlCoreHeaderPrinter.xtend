/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk.plcore

import exelixi.hls.partitioner.Partitioner
import exelixi.zynq.backend.templates.sdk.SdkUtil
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network

import static exelixi.hls.backend.Constants.NETWORK_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING

/**
 * 
 * @author Endri Bezati
 */
class PlCoreHeaderPrinter {

	private Network network

	private Map<String, Object> options

	protected Partitioner partitioner

	protected List<Instance> partitionInstances;

	private List<Connection> plToPsFifos

	private List<Connection> psToPlFifos

	private boolean containsLlfifo

	private boolean containsDma
	
	private boolean singleDma

	new(Network network, Partitioner partitioner, Map<String, Object> options) {
		this.network = network
		this.options = options
		this.partitioner = partitioner
		this.partitionInstances = partitioner.getInstances(ZYNQ_PL_PARTITION);

		// -- PS/PL IO
		this.psToPlFifos = new ArrayList<Connection>(
			partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION).keySet)
		this.plToPsFifos = new ArrayList<Connection>(
			partitioner.getConnectionToConnectionBd(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION).keySet)

		// -- Communication Kind
		singleDma = false;
		val String communicationKind = options.get(ZYNQ_OPTION_COMMUNICATION_KIND) as String
		if (communicationKind.equals("Single DMA")){
			singleDma = true;
		}

		// -- Contains or not llfifo or dma	
		containsLlfifo = false
		containsDma = false
		if(!singleDma){
			for (connection : psToPlFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					containsLlfifo = true
				} else if (TemplateUtils.getConnectionKind(connection).equals(
					ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
					containsDma = true
				}
			}
	
			for (connection : plToPsFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					containsLlfifo = true
				} else if (TemplateUtils.getConnectionKind(connection).equals(
					ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
					containsDma = true
				}
			}
		}else{
			containsDma = true;
		}
	}

	def getContent() {
		'''
			«getFileHeader("PL Initialization Header")»
			#ifndef __«SdkUtil.getHwCoreType(network).toUpperCase»_H__
			#define __«SdkUtil.getHwCoreType(network).toUpperCase»_H__
			
			«headers»
			
			«staticVariables»
			
			«prototypes»
			
			#endif
		'''
	}

	def getHeaders() {
		'''
			#include "xil_types.h"
			#include "xstatus.h"
			«IF containsLlfifo»
				#include "xllfifo.h"
			«ENDIF»
			«IF containsDma»
				#include "xaxidma.h"
			«ENDIF»
			«IF partitioner.containsPartition(ZYNQ_PL_PARTITION)»
				«hwCoreHeader»
			«ENDIF»
			«IF network.hasAttribute(NETWORK_EXTERNAL_VARIABLES) »
				// -- TODO
			«ENDIF»
		'''
	}

	def getPrototypes() {
		'''
			«IF partitioner.containsPartition(ZYNQ_PL_PARTITION)»
				«IF containsLlfifo»
					// -- Initialize LLFifo
					int initLLFifo(XLlFifo *instancePtr, u16 deviceId);
				«ENDIF»
				«IF containsDma»
					// -- Initialize DMA
					int initAxiDma(XAxiDma *instancePtr, u16 deviceId);
				«ENDIF»
			«ENDIF»
			int initCore(«SdkUtil.getHwCoreType("kicker")» *instancePtr, u16 deviceId);
			«IF !singleDma»
				«FOR connection : psToPlFifos»
					int initCore(«SdkUtil.getHwCoreType("AxisInput_" + TemplateUtils.getQueueNameIO(connection))» *instancePtr, u16 deviceId);
				«ENDFOR»
				«FOR connection : plToPsFifos»
					int initCore(«SdkUtil.getHwCoreType("AxisOutput_" + TemplateUtils.getQueueNameIO(connection))» *instancePtr, u16 deviceId);
				«ENDFOR»
			«ELSE»
				«IF !psToPlFifos.empty»
					int initCore(«SdkUtil.getHwCoreType("AxisInput_Wrapper")» *instancePtr, u16 deviceId);
				«ENDIF»
				«IF !plToPsFifos.empty»
					int initCore(«SdkUtil.getHwCoreType("AxisOutput_Wrapper")» *instancePtr, u16 deviceId);
				«ENDIF»
			«ENDIF»
			«IF network.hasAttribute(NETWORK_EXTERNAL_VARIABLES) »
				// -- Initialize PL Core
				// -- TODO
			«ENDIF»
		'''
	}

	// ////////////////////////////////////////////////////////////////////////
	// -- Helper Methods
	// ////////////////////////////////////////////////////////////////////////
	def getFileHeader(String comment) {
		var dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		var date = new Date();
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- «comment»
			// -- Date: «dateFormat.format(date)»
			// ----------------------------------------------------------------------------
		'''
	}

	def getHwCoreHeader() {
		'''
			#include "xkicker.h"
			«IF !singleDma»
				«FOR connection : psToPlFifos»
					#include "x«("AxisInput_" + TemplateUtils.getQueueNameIO(connection)).toLowerCase».h"
				«ENDFOR»
				«FOR connection : plToPsFifos»
					#include "x«("AxisOutput_" + TemplateUtils.getQueueNameIO(connection)).toLowerCase».h"
				«ENDFOR»
			«ELSE»
				«IF !psToPlFifos.empty»
					#include "x«("AxisInput_Wrapper").toLowerCase».h"
				«ENDIF»
				«IF !plToPsFifos.empty»
					#include "x«("AxisOutput_Wrapper").toLowerCase».h"
				«ENDIF»
			«ENDIF»
		'''
	}

	def getStaticVariables(){
		'''
			«IF singleDma»
				«IF !psToPlFifos.empty»
					static unsigned int tx_buffer[2097152]; // 8 MBytes
				«ENDIF»
				«IF !plToPsFifos.empty»
					static unsigned int rx_buffer[2097152]; // 8 MBytes
				«ENDIF»
			«ENDIF»
		'''
	}

}
