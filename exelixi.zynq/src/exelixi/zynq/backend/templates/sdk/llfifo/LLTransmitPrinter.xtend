/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.sdk.llfifo

import java.text.SimpleDateFormat
import java.util.Date

/**
 * 
 * @author Endri Bezati
 */
class LLTransmitPrinter {
	
	
	def getContent(){
		'''
			«getFileHeader("LLFifo Transmit class for PS")»
			#ifndef __LLTRANSMIT_H__
			#define __LLTRANSMIT_H__
			
			#include <stdint.h>
			#include "fifo.h"
			#include "actor.h"
			#include "xllfifo.h"
			
			#define min(x1, x2)(x1 < x2 ? x1 : x2)
			
			template<typename T, int nb_reader>
			class LLTransmit {
			public:
				LLTransmit(XLlFifo *llfifo, Fifo<T, nb_reader> *fifo, u32 reader_id) :
						llfifo(llfifo), fifo(fifo), reader_id(reader_id), word_size(
								sizeof(int)) {
				}
			
				void transmit(EStatus& status) {
					u32 available_data = fifo->count(reader_id);
					u32 vacany = XLlFifo_iTxVacancy(llfifo);
			
					if (available_data && vacany) {
						// -- Data to transmit
						u32 data_to_transmit = min(available_data, vacany);
						T *ptr = fifo->read_address(reader_id, data_to_transmit);
			
						if (sizeof(T) != word_size) {
							int buffer[data_to_transmit];
							
							// -- Convert Data Type
							for (u32 i = 0; i < data_to_transmit; i++) {
								buffer[i] = ptr[i];
							}
			
							// -- Transmit the data
							XLlFifo_Write(llfifo, buffer, word_size * data_to_transmit);
						} else {
							XLlFifo_Write(llfifo, ptr, word_size * data_to_transmit);
						}
						// -- Transmit Length
						XLlFifo_TxSetLen(llfifo, word_size * data_to_transmit);
			
						// -- Transmit done
						//while (!XLlFifo_IsTxDone(llfifo))
						//	;
			
						// -- Advance fifo pointer
						fifo->read_advance(reader_id, data_to_transmit);
			
						status = hasExecuted;
					}
				}
			
			private:
				// -- PL Fifo
				XLlFifo *llfifo;
			
				// -- PS Fifo
				Fifo<T, nb_reader> *fifo;
			
				// -- Reader Id
				const u32 reader_id;
			
				// -- Word size in Bytes
				const u32 word_size;
			
			};
			#endif //__LLTRANSMIT_H__
		'''
	}
	
	// ////////////////////////////////////////////////////////////////////////
	// -- Helper Methods
	// ////////////////////////////////////////////////////////////////////////
	def getFileHeader(String comment) {
		var dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		var date = new Date();
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- «comment»
			// -- Date: «dateFormat.format(date)»
			// ----------------------------------------------------------------------------
		'''
	}
}
