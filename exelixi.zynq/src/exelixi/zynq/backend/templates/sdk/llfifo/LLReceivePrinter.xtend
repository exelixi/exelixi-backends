/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.sdk.llfifo

import java.text.SimpleDateFormat
import java.util.Date

/**
 * 
 * @author Endri Bezati
 */
class LLReceivePrinter{
	
	
	def getContent(){
		'''
			«getFileHeader("LLFifo Receive class for PS")»
			#ifndef __LLRECEIVE_H__
			#define __LLRECEIVE_H__
			
			#include <stdint.h>
			#include "fifo.h"
			#include "actor.h"
			#include "xllfifo.h"
			
			#define min(x1, x2)(x1 < x2 ? x1 : x2)
			
			template<typename T, int nb_reader>
			class LLReceive {
			public:
				LLReceive(Fifo<T, nb_reader> *fifo, XLlFifo *llfifo) :
						fifo(fifo), llfifo(llfifo), word_size(sizeof(int)) {
				}
			
				void receive(EStatus& status) {
					if (!XLlFifo_IsRxEmpty(llfifo)) {
						// -- Get the fifo rooms
						u32 rooms = fifo->rooms();
			
						// -- Get Occupancy
						u32 occupancy = XLlFifo_RxOccupancy(llfifo);
			
						// -- Data to receive
						u32 data_read = min(occupancy, rooms);
						if (data_read) {
			
							// -- Get Write address of Fifo
							T* ptr = fifo->write_address();
			
							// -- Flash the memory zone
							//Xil_DCacheFlushRange((T) ptr, word_size * data_read);
			
							if (sizeof(T) != word_size) {
								int buffer[data_read];
			
								// -- Read the data
								XLlFifo_Read(llfifo, buffer, word_size * data_read);
								
								// -- Convert Data Type
								for (u32 i = 0; i < data_read; i++) {
									ptr[i] = (T) buffer[i];
								}
							} else {
								XLlFifo_Read(llfifo, ptr, word_size * data_read);
							}
			
							// -- Wait for the data to be fully received
							while (!XLlFifo_IsRxDone(llfifo));
			
							// -- Advance the fifo pointer
							fifo->write_advance(data_read);
			
							status = hasExecuted;
						}
					}
				}
			
			private:
				// -- PS Fifo
				Fifo<T, nb_reader> *fifo;
			
				// -- PL Fifo
				XLlFifo *llfifo;
			
				// -- Word size in Bytes
				const u32 word_size;
			};
			#endif //__LLRECEIVE_H__
		'''
	}
	
	// ////////////////////////////////////////////////////////////////////////
	// -- Helper Methods
	// ////////////////////////////////////////////////////////////////////////
	def getFileHeader(String comment) {
		var dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		var date = new Date();
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- «comment»
			// -- Date: «dateFormat.format(date)»
			// ----------------------------------------------------------------------------
		'''
	}
}
