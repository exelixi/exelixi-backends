/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.sdk

import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network

import static exelixi.zynq.backend.Constants.ZYNQ_FIFO_ID
import static exelixi.zynq.backend.Constants.ZYNQ_NB_READERS

class SdkUtil {
	
	static def String getHwCoreType(Instance instance){
		val String nameLowerCase = instance.simpleName.toLowerCase
		val String firstLetter = (nameLowerCase.charAt(0).toString).toUpperCase
		val String coreName =  "X" + firstLetter + nameLowerCase.substring(1)
		
		return coreName
	}
	
	static def String getHwCoreType(String core){
		val String nameLowerCase = core.toLowerCase
		val String firstLetter = (nameLowerCase.charAt(0).toString).toUpperCase
		val String coreName =  "X" + firstLetter + nameLowerCase.substring(1)
		
		return coreName
	}
	
	static def String getHwCoreType(Network network){
		val String nameLowerCase = network.simpleName.toLowerCase + "_pl"
		val String firstLetter = (nameLowerCase.charAt(0).toString).toUpperCase
		val String coreName =  "X" + firstLetter + nameLowerCase.substring(1)
		
		return coreName
	}
	
	static def String getXparDeviceId(String string){
		return "XPAR_" + string.toUpperCase + "_DEVICE_ID"
	}
	
	static def Integer getNbReadersConnection(Connection connection){
		return connection.getAttribute(ZYNQ_NB_READERS).objectValue as Integer
	}
	
	static def Integer getFifoIdConnection(Connection connection){
		return connection.getAttribute(ZYNQ_FIFO_ID).objectValue as Integer
	}
}
