/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 

package exelixi.zynq.backend.templates.sdk.wrappers

import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import exelixi.zynq.backend.interconnect.InterconnectionType
import net.sf.orcc.df.Connection

/**
 * Input Wrapper implementation
 * 
 * @author Endri Bezati
 */
class InputWrapperImpl {

	
	Connection connection

	InterconnectionType interconnectionType
	
	new(InterconnectionType interconnectionType, Connection connection){
		this.interconnectionType = interconnectionType
		this.connection = connection
	}

	new(InterconnectionType interconnectionType){
		this.interconnectionType = interconnectionType
	}

	def getContent() {
		'''
			«IF interconnectionType.isSignleDMA»
				«singleDMA»
			«ELSE»
				«multiDMA»
			«ENDIF»
		'''
	}

	def multiDMA() {
		val Integer id = TemplateUtils.getQueueIdIO(connection)
		'''
			#ifndef _INPUT_«id»_H_
			#define _INPUT_«id»_H_
			
			#include "input_wrapper.h"
			#include "xaxisinput_fifo_bcid_«id».h"
			
			class InputWrapper_«id»: public InputWrapper {
			public:
			
				InputWrapper_«id»(XAxisinput_fifo_bcid_«id» *wrapper) :
					wrapper(wrapper){
				}
			
				u32 get_vacancy() {
					XAxisinput_fifo_bcid_«id»_Set_transmit(wrapper, 0);
					XAxisinput_fifo_bcid_«id»_Set_length_r(wrapper, 0);
					XAxisinput_fifo_bcid_«id»_Start(wrapper);
					while(!XAxisinput_fifo_bcid_«id»_IsDone(wrapper));
					u32 vacancy = XAxisinput_fifo_bcid_«id»_Get_vacancy(wrapper);
					return vacancy;
				}
			
				void set_length(u32 nbr) {
					XAxisinput_fifo_bcid_«id»_Set_transmit(wrapper, 1);
					XAxisinput_fifo_bcid_«id»_Set_length_r(wrapper, nbr);
					XAxisinput_fifo_bcid_«id»_Start(wrapper);
					while(!XAxisinput_fifo_bcid_«id»_IsDone(wrapper));
				}
			
			private:
				XAxisinput_fifo_bcid_«id» *wrapper;
			};
			
			#endif
		'''
	}

	def singleDMA() {
		'''
			#ifndef _SINGLE_INPUT_WRAPPER_H_s
			#define _SINGLE_INPUT_WRAPPER_H_
			
			#include "input_wrapper.h"
			#include "xaxisinput_wrapper.h"
			
			class SingleInputWrapper: public InputWrapper {
			public:
			
				SingleInputWrapper(XAxisinput_wrapper *wrapper) :
					wrapper(wrapper){
				}
			
				u32 get_vacancy(u32 selection) {
					XAxisinput_wrapper_Set_selection(wrapper, selection);
					XAxisinput_wrapper_Set_transmit(wrapper, 0);
					XAxisinput_wrapper_Set_length_r(wrapper, 0);
					XAxisinput_wrapper_Start(wrapper);
					while(!XAxisinput_wrapper_IsDone(wrapper));
					u32 vacancy = XAxisinput_wrapper_Get_vacancy(wrapper);
					return vacancy;
				}
			
				void set_length(u32 selection, u32 nbr) {
					XAxisinput_wrapper_Set_selection(wrapper, selection);
					XAxisinput_wrapper_Set_transmit(wrapper, 1);
					XAxisinput_wrapper_Set_length_r(wrapper, nbr);
					XAxisinput_wrapper_Start(wrapper);
					while(!XAxisinput_wrapper_IsDone(wrapper));
				}
			
			private:
				XAxisinput_wrapper *wrapper;
			};
			
			#endif
		'''
	}

}
