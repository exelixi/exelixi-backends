/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk.wrappers

import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import exelixi.zynq.backend.interconnect.InterconnectionType
import net.sf.orcc.df.Connection

/**
 * Output Wrapper implementation
 * 
 * @author Endri Bezati
 */
class OutputWrapperImpl {

	Connection connection

	InterconnectionType interconnectionType
	
	new(InterconnectionType interconnectionType, Connection connection){
		this.interconnectionType = interconnectionType
		this.connection = connection
	}

	new(InterconnectionType interconnectionType){
		this.interconnectionType = interconnectionType
	}

	def getContent() {
		'''
			«IF interconnectionType.isSignleDMA»
				«singleDMA»
			«ELSE»
				«multiDMA»
			«ENDIF»
		'''
	}

	def multiDMA() {
		val Integer id = TemplateUtils.getQueueIdIO(connection)
		'''
			#ifndef _OUTPUT_«id»_H_
			#define _OUTPUT_«id»_H_
			
			#include "output_wrapper.h"
			#include "xaxisoutput_fifo_bcid_«id».h"
			
			class OutputWrapper_«id»: public OutputWrapper {
			public:
			
				OutputWrapper_«id»(XAxisoutput_fifo_bcid_«id» *wrapper) :
						wrapper(wrapper) {
				}
			
				u32 get_occupancy() {
					XAxisoutput_fifo_bcid_«id»_Set_receive(wrapper, 0);
					XAxisoutput_fifo_bcid_«id»_Set_length_r(wrapper, 0);
					XAxisoutput_fifo_bcid_«id»_Start(wrapper);
					while (!XAxisoutput_fifo_bcid_«id»_IsDone(wrapper))
						;
			
					u32 occupancy = XAxisoutput_fifo_bcid_«id»_Get_occupancy(wrapper);
			
					return occupancy;
				}
			
				void set_length(u32 nbr) {
					XAxisoutput_fifo_bcid_«id»_Set_receive(wrapper, 1);
					XAxisoutput_fifo_bcid_«id»_Set_length_r(wrapper, nbr);
					XAxisoutput_fifo_bcid_«id»_Start(wrapper);
					while (!XAxisoutput_fifo_bcid_«id»_IsDone(wrapper))
						;
				}
			
			private:
				XAxisoutput_fifo_bcid_«id» *wrapper;
			
			};
			
			#endif
		'''
	}
	
	def singleDMA(){
		'''
			#ifndef _SINGLE_OUTPUT_WRAPPER_H_
			#define _SINGLE_OUTPUT_WRAPPER_H_
			
			#include "output_wrapper.h"
			#include "xaxisoutput_wrapper.h"
			
			class SingleOutputWrapper: public OutputWrapper {
			public:
			
				SingleOutputWrapper(XAxisoutput_wrapper *wrapper) :
						wrapper(wrapper) {
				}
			
				u32 get_occupancy(u32 selection) {
					XAxisoutput_wrapper_Set_selection(wrapper, selection);
					XAxisoutput_wrapper_Set_receive(wrapper, 0);
					XAxisoutput_wrapper_Set_length_r(wrapper, 0);
					XAxisoutput_wrapper_Start(wrapper);
					while (!XAxisoutput_wrapper_IsDone(wrapper))
						;
			
					u32 occupancy = XAxisoutput_wrapper_Get_occupancy(wrapper);
			
					return occupancy;
				}
			
				void set_length(u32 selection, u32 nbr) {
					XAxisoutput_wrapper_Set_selection(wrapper, selection);
					XAxisoutput_wrapper_Set_receive(wrapper, 1);
					XAxisoutput_wrapper_Set_length_r(wrapper, nbr);
					XAxisoutput_wrapper_Start(wrapper);
					while (!XAxisoutput_wrapper_IsDone(wrapper))
						;
				}
			
			private:
				XAxisoutput_wrapper *wrapper;
			
			};
			
			#endif
		'''
	}
}
