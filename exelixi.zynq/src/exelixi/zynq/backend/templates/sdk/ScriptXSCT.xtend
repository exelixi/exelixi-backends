/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk

import exelixi.hls.devices.XilinxDevice
import java.util.Map
import net.sf.orcc.df.Network

import static exelixi.hls.backend.Constants.CODE_GEN_PATH_SW
import static exelixi.hls.backend.Constants.DEVICE

/**
 * 
 * @author Endri Bezati
 */
class ScriptXSCT {

	Map<String, Object> options;

	String partialPath
	
	XilinxDevice xilinxDevice
	
	Network network

	new(Network network, Map<String, Object> options){
		this.network = network
		this.options = options
		this.partialPath = options.get(CODE_GEN_PATH_SW) as String
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
	}

	def getContent() {
		'''
			#!/usr/bin/tclsh
			
			# vivado -mode batch -nojournal -nolog -notrace -source source.tcl
			
			sdk setws vivado_project/«network.simpleName».sdk
			sdk createhw -name hw_0 -hwspec vivado_project/«network.simpleName».sdk/«network.simpleName»_top.hdf
			# Temporary solution for supporting xillfs, xsct can not set libraries for the moment
			createapp -name fsbl -app {«IF xilinxDevice.isZynqUltrascale»Zynq MP FSBL«ELSE»Zynq FSBL«ENDIF»} -hwproject hw_0 -proc «IF xilinxDevice.isZynqUltrascale»psu_cortexa53_0«ELSE»ps7_cortexa9_0«ENDIF»
			#sdk createbsp -name bsp_0 -hwproject hw_0 -proc «IF xilinxDevice.isZynqUltrascale»psu_cortexa53_0«ELSE»ps7_cortexa9_0«ENDIF» -os standalone
			#setlib -hw hw_0 -bsp bsp_0 -lib xilffs -ver 3.3
			#configbsp -hw hw_0 -bsp bsp_0 use_lfn true
			sdk projects -build -type bsp -name fsbl_bsp
			sdk createapp -name ps -hwproject hw_0 -proc «IF xilinxDevice.isZynqUltrascale»psu_cortexa53_0«ELSE»ps7_cortexa9_0«ENDIF» -os standalone -lang CPP -app {Empty Application} -bsp fsbl_bsp
			sdk importsources -name ps -path «partialPath»/include
			sdk importsources -name ps -path «partialPath»/src
			sdk importsources -name ps -linker-script -path «partialPath»/src
			sdk configapp -app ps compiler-misc -fsigned-char
			sdk configapp -app ps compiler-misc -std=c++11 
			sdk projects -build -type app -name ps
			
			exit
		'''
	}
}
