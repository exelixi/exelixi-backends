/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk.dma

import exelixi.zynq.backend.interconnect.InterconnectionType

/**
 * DMA transmit in polling mode
 * 
 * @author Endri Bezati
 */
class DMATransmitPolling {

	InterconnectionType interconnectionType

	new(InterconnectionType interconnectionType) {
		this.interconnectionType = interconnectionType
	}

	def getContent() {
		'''
			«IF interconnectionType.isSignleDMA»
				«singleDMA»
			«ELSE»
				«multiDMA»
			«ENDIF»
		'''
	}

	def multiDMA() {
		'''
			#ifndef __AXI_DMA_TRANSMIT_H_
			#define __AXI_DMA_TRANSMIT_H_
			
			#include <iostream>
			#include "fifo.h"
			#include "actor.h"
			#include "xaxidma.h"
			#include "input_wrapper.h"
			
			#define min(x1, x2)(x1 < x2 ? x1 : x2)
			
			template<typename T>
			class AxiDMATransmit {
			public:
				AxiDMATransmit(Fifo<T> *fifo, XAxiDma *axiDMA,
						InputWrapper *wrapper, u32 reader_id) :
						word_size(sizeof(int)) {
					this->fifo = fifo;
					this->axiDMA = axiDMA;
					this->wrapper = wrapper;
					this->length = 512;
					this->counter = 0;
					this->reader_id = reader_id;
				}
			
				void transmit(EStatus& status) {
					u32 available_data = fifo->count(this->reader_id);
					u32 vacany = wrapper->get_vacancy();
					if (available_data && vacany) {
						// -- Data to transmit
						u32 length = min(available_data, vacany);
			
						u32 num_bytes = length * sizeof(int);
			
						T *ptr = fifo->read_address(this->reader_id, length);
			
						int buffer[length];
						if (sizeof(T) != word_size) {
			
							// -- Convert Data Type
							for (u32 i = 0; i < length; i++) {
								buffer[i] = ptr[i];
							}
						}else{
							memcpy(buffer, ptr, length * sizeof(int));
						}
			
						Xil_DCacheFlushRange((UINTPTR) buffer, num_bytes);
			
						u32 dma_status = XAxiDma_SimpleTransfer(axiDMA, (UINTPTR) buffer,
								num_bytes, XAXIDMA_DMA_TO_DEVICE);
						if (dma_status != XST_SUCCESS) {
							std::cout << "Error: DMA transfer from Vivado HLS IP failed"
									<< std::endl;
							std::cout << "counter := " << counter << std::endl;
						} else {
			
							wrapper->set_length(length);
			
							/* Wait for transfer to be done */
							while (XAxiDma_Busy(axiDMA, XAXIDMA_DMA_TO_DEVICE))
								;
			
							fifo->read_advance(reader_id, length);
							counter+= length;
							status = hasExecuted;
						}
					}
				}
			
			private:
				// -- PS Fifo
				Fifo<T> *fifo;
				// -- DMA
				XAxiDma *axiDMA;
				// -- Wrapper
				InputWrapper *wrapper;
				// -- The available rooms
				unsigned int status;
				// -- Package Length
				unsigned int length;
				// -- Word size in Bytes
				const u32 word_size;
			
				u32 reader_id;
			
				int counter;
			};
			
			#endif
		'''
	}

	def getSingleDMA() {
		'''
			#ifndef __AXI_DMA_TRANSMIT_H_
			#define __AXI_DMA_TRANSMIT_H_
			
			#include <iostream>
			#include "fifo.h"
			#include "actor.h"
			#include "xaxidma.h"
			#include "input_wrapper.h"
			
			#define min(x1, x2)(x1 < x2 ? x1 : x2)
			
			extern unsigned int tx_buffer[2097152];
			
			template<typename T>
			class AxiDMATransmit {
			public:
				AxiDMATransmit(Fifo<T> *fifo, XAxiDma *axiDMA,
						InputWrapper *wrapper, u32 reader_id, u32 selection) :
						word_size(sizeof(int)) {
					this->fifo = fifo;
					this->axiDMA = axiDMA;
					this->wrapper = wrapper;
					this->length = 512;
					this->counter = 0;
					this->reader_id = reader_id;
					this->selection = selection;
				}
			
				void transmit(EStatus& status) {
					u32 available_data = fifo->count(this->reader_id);
					u32 vacany = wrapper->get_vacancy(selection);
					if (available_data && vacany) {
						// -- Data to transmit
						u32 length = min(available_data, vacany);
			
						u32 num_bytes = length * sizeof(int);
			
						T *ptr = fifo->read_address(this->reader_id, length);
			
						if (sizeof(T) != word_size) {
			
							// -- Convert Data Type
							for (u32 i = 0; i < length; i++) {
								tx_buffer[i] = ptr[i];
							}
						} else {
							memcpy(tx_buffer, ptr, length * sizeof(int));
						}
			
						Xil_DCacheFlushRange((UINTPTR) tx_buffer, num_bytes);
			
						u32 dma_status = XAxiDma_SimpleTransfer(axiDMA, (UINTPTR) tx_buffer,
								num_bytes, XAXIDMA_DMA_TO_DEVICE);
						if (dma_status == XST_FAILURE) {
							std::cout << "Error: DMA transfer from Vivado HLS IP failed"
									<< std::endl;
							std::cout << "counter := " << counter << std::endl;
						} else {
			
							wrapper->set_length(selection, length);
			
							/* Wait for transfer to be done */
							while (XAxiDma_Busy(axiDMA, XAXIDMA_DMA_TO_DEVICE))
								;
			
							fifo->read_advance(reader_id, length);
							counter += length;
							status = hasExecuted;
						}
					}
				}
			
			private:
				// -- PS Fifo
				Fifo<T> *fifo;
				// -- DMA
				XAxiDma *axiDMA;
				// -- Wrapper
				InputWrapper *wrapper;
				// -- The available rooms
				unsigned int status;
				// -- Package Length
				unsigned int length;
				// -- Word size in Bytes
				const u32 word_size;
			
				u32 reader_id;
			
				int counter;
			
				u32 selection;
			};
			
			#endif
		'''
	}

}
