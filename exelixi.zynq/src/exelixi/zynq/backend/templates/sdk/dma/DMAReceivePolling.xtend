/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend.templates.sdk.dma

import exelixi.zynq.backend.interconnect.InterconnectionType

/**
 * DMA Receive in polling mode
 * 
 * @author Endri Bezati
 */
class DMAReceivePolling {

	InterconnectionType interconnectionType

	new(InterconnectionType interconnectionType) {
		this.interconnectionType = interconnectionType
	}

	def getContent() {
		'''
			«IF interconnectionType.isSignleDMA»
				«singleDMA»
			«ELSE»
				«multiDMA»
			«ENDIF»
		'''
	}

	def multiDMA() {
		'''
			#ifndef __AXI_DMA_RECEIVE_H__
			#define __AXI_DMA_RECEIVE_H__
			
			#include <iostream>
			#include "fifo.h"
			#include "actor.h"
			#include "xaxidma.h"
			#include "output_wrapper.h"
			
			#define min(x1, x2)(x1 < x2 ? x1 : x2)
			
			template<typename T>
			class AxiDMAReceive {
			public:
				AxiDMAReceive(Fifo<T> *fifo, XAxiDma *axiDMA,
						OutputWrapper *wrapper) :
						word_size(sizeof(int)) {
					this->fifo = fifo;
					this->axiDMA = axiDMA;
					this->wrapper = wrapper;
					this->length = 512;
					this->buffer = new unsigned int[length];
					this->counter = 0;
				}
			
				void receive(EStatus& status) {
					this->rooms = fifo->rooms();
			
					u32 occupancy = wrapper->get_occupancy();
					if (occupancy > 0) {
			
						u32 length = min(occupancy, this->rooms);
			
						u32 num_bytes = length * sizeof(int);
			
			
						Xil_DCacheFlushRange((UINTPTR) buffer, num_bytes);
						u32 dma_status = XAxiDma_SimpleTransfer(axiDMA, (UINTPTR) buffer,
								num_bytes, XAXIDMA_DEVICE_TO_DMA);
						
						wrapper->set_length(length);
						
						if (dma_status != XST_SUCCESS) {
							std::cout << "Error: DMA transfer from Vivado HLS IP failed"
									<< std::endl;
							std::cout << "counter := " << counter << std::endl;
						} else {
							
			
							/* Wait for transfer to be done */
							while (XAxiDma_Busy(axiDMA, XAXIDMA_DEVICE_TO_DMA))
								;
			
							T *fifo_ptr = fifo->write_address();
			
							if (sizeof(T) != word_size) {
								// -- Convert Data Type
								for (u32 i = 0; i < length; i++) {
									fifo_ptr[i] = (T) buffer[i];
									counter++;
								}
							} else {
								memcpy(fifo_ptr, buffer, length * sizeof(T));
							}
							fifo->write_advance(length);
							this->rooms -= length;
							status = hasExecuted;
						}
					}
				}
			
				int get_counter(){
					return counter;
				}
			
			private:
			// -- PS Fifo
				Fifo<T> *fifo;
			// -- DMA
				XAxiDma *axiDMA;
				OutputWrapper *wrapper;
			// -- Local Buffer
				unsigned int *buffer;
			// -- The available rooms
				unsigned int rooms;
			// -- Package Length
				unsigned int length;
				// -- Word size in Bytes
				const u32 word_size;
			
				int counter;
			
			};
			#endif
		'''
	}

	def singleDMA() {
		'''
			#ifndef __AXI_DMA_RECEIVE_H__
			#define __AXI_DMA_RECEIVE_H__
			
			#include <iostream>
			#include "fifo.h"
			#include "actor.h"
			#include "xaxidma.h"
			#include "output_wrapper.h"
			
			#define min(x1, x2)(x1 < x2 ? x1 : x2)
			
			extern unsigned int rx_buffer[2097152];
			
			template<typename T>
			class AxiDMAReceive {
			public:
				AxiDMAReceive(Fifo<T> *fifo, XAxiDma *axiDMA,
						OutputWrapper *wrapper,u32 selection) :
						word_size(sizeof(int)) {
					this->fifo = fifo;
					this->axiDMA = axiDMA;
					this->wrapper = wrapper;
					this->counter = 0;
					this->selection =selection;
				}
			
				void receive(EStatus& status) {
					this->rooms = fifo->rooms();
			
					u32 occupancy = wrapper->get_occupancy(selection);
					if (occupancy > 0) {
			
						u32 length = min(occupancy, this->rooms);
			
						u32 num_bytes = length * sizeof(int);
			
						Xil_DCacheFlushRange((UINTPTR) rx_buffer, length * sizeof(u32));
						u32 dma_status = XAxiDma_SimpleTransfer(axiDMA, (UINTPTR) rx_buffer,
								num_bytes, XAXIDMA_DEVICE_TO_DMA);
						
						wrapper->set_length(selection,length);
						
						if (dma_status == XST_FAILURE) {
							std::cout << "Error: DMA transfer from Vivado HLS IP failed"
									<< std::endl;
							std::cout << "counter := " << counter << std::endl;
						} else {
							
			
							/* Wait for transfer to be done */
							while (XAxiDma_Busy(axiDMA, XAXIDMA_DEVICE_TO_DMA))
								;
			
							T *fifo_ptr = fifo->write_address();
			
							if (sizeof(T) != word_size) {
								// -- Convert Data Type
								for (u32 i = 0; i < length; i++) {
									fifo_ptr[i] = (T) rx_buffer[i];
									counter++;
								}
							} else {
								memcpy(fifo_ptr, rx_buffer, length * sizeof(T));
							}
							fifo->write_advance(length);
							this->rooms -= length;
							status = hasExecuted;
						}
					}
				}
			
				int get_counter(){
					return counter;
				}
			
			private:
			// -- PS Fifo
				Fifo<T> *fifo;
			// -- DMA
				XAxiDma *axiDMA;
				OutputWrapper *wrapper;
			// -- The available rooms
				unsigned int rooms;
			
				// -- Word size in Bytes
				const u32 word_size;
			
				u32 selection;
				int counter;
			
			};
			#endif
		'''
	}

}
