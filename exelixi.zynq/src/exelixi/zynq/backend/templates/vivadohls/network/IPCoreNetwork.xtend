/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 

package exelixi.zynq.backend.templates.vivadohls.network

import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port
import net.sf.orcc.ir.Var

import static exelixi.hls.backend.Constants.INSTANCE_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.NETWORK_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ZYNQ_OPTION_AXI_LITE_CONTROL
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION
import static net.sf.orcc.OrccLaunchConstants.*

/**
 * 
 * @author Endri Bezati
 */
class IPCoreNetwork extends HLSTemplate {

	/** Network Inputs **/
	private List<Port> inputs;

	/** Network Outputs **/
	private List<Port> outputs;

	private Network network;

	protected TypePrinterVisitor typePrinter;

	private Partitioner partitioner;

	protected Boolean networkHasHeterogeneousPartitions

	protected List<Connection> plToPsFifos;

	protected List<Connection> psToPlFifos;

	protected List<Connection> plToPlFifos;

	protected List<Instance> partitionInstances;

	private Boolean usesAxiLiteControl

	new(Network network, TypePrinterVisitor typePrinter, Map<String, Object> options, Partitioner partitioner) {
		super(typePrinter)
		this.network = network
		this.typePrinter = typePrinter
		this.inputs = network.inputs
		this.outputs = network.outputs
		this.partitioner = partitioner
		this.networkHasHeterogeneousPartitions = partitioner.hasHeterogeneousPartitions
		this.plToPlFifos = partitioner.getConnections(ZYNQ_PL_PARTITION, ZYNQ_PL_PARTITION)
		this.plToPsFifos = partitioner.getConnections(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION)
		this.psToPlFifos = partitioner.getConnections(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION)
		this.partitionInstances = partitioner.getInstances(ZYNQ_PL_PARTITION);
		this.usesAxiLiteControl = options.get(ZYNQ_OPTION_AXI_LITE_CONTROL) as Boolean

		if (options.containsKey(FIFO_SIZE)) {
			fifoSize = options.get(FIFO_SIZE) as Integer
		} else {
			fifoSize = DEFAULT_FIFO_SIZE
		}
	}

	def getContent() {
		var List<Connection> interfaces = new ArrayList
		interfaces.addAll(partitioner.getConnections(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION))
		interfaces.addAll(
			partitioner.getConnections(ZYNQ_PL_PARTITION,
				ZYNQ_PS_PARTITION))
		var Map<Instance, List<Var>> instancesExternalVariablesMap = new HashMap;
		if(network.hasAttribute(NETWORK_EXTERNAL_VARIABLES)){
			instancesExternalVariablesMap = network.getAttribute(NETWORK_EXTERNAL_VARIABLES).objectValue as Map<Instance, List<Var>> 
		}		
			'''
			«getFileHeader("Exilixi for Vivado HLS, Network Code Generation, Network: " + network.simpleName)»
			«headers»
			
			«structures»
			
			«readersWriters»
			
			void «network.simpleName»(«FOR conn : interfaces SEPARATOR ", "»«typePrinter.channelClassName»< «conn.sourcePort.type.doSwitch» > &«getQueueName(conn)»«ENDFOR»«IF !instancesExternalVariablesMap.empty»«IF !interfaces.empty», «ENDIF»«getExternalVarsDeclaration(instancesExternalVariablesMap)»«ENDIF»){
			#pragma HLS INTERFACE ap_ctrl_hs port=return
			«IF !instancesExternalVariablesMap.empty»
				«FOR actor: instancesExternalVariablesMap.keySet»
					«FOR variable : instancesExternalVariablesMap.get(actor)»
						#pragma HLS INTERFACE m_axi depth=«getFlatListDepth(variable.type)» port=«variable.name»_«actor.simpleName» offset=slave bundle=«variable.name»_«actor.simpleName»
					«ENDFOR»
				«ENDFOR»
			«ENDIF»
			#pragma HLS INLINE
			#pragma HLS DATAFLOW
				«getInternalStreams»
			
			«internalStreamSize»
			
				«getInstances»
			
				return;
			}
			
			void «network.simpleName»_pl(«FOR conn : interfaces SEPARATOR ", "»hls::stream<AxiWord<32> > &«getQueueName(conn)»_axis«ENDFOR»«IF !instancesExternalVariablesMap.empty»«IF !interfaces.empty», «ENDIF»«getExternalVarsDeclaration(instancesExternalVariablesMap)»«ENDIF»){
			#pragma HLS DATAFLOW
			«IF usesAxiLiteControl»
				#pragma HLS INTERFACE s_axilite port=return
			«ELSE»
				#pragma HLS INTERFACE ap_ctrl_hs port=return
			«ENDIF»
			«IF !instancesExternalVariablesMap.empty»
				«FOR actor: instancesExternalVariablesMap.keySet»
					«FOR variable : instancesExternalVariablesMap.get(actor)»
						#pragma HLS INTERFACE m_axi depth=«getFlatListDepth(variable.type)» port=«variable.name»_«actor.simpleName» offset=slave bundle=«variable.name»_«actor.simpleName»
					«ENDFOR»
				«ENDFOR»
			«ENDIF»
			«FOR conn : psToPlFifos»
				#pragma HLS INTERFACE axis port=«getQueueName(conn)»_axis
			«ENDFOR»
			«FOR conn : plToPsFifos»
				#pragma HLS INTERFACE axis port=«getQueueName(conn)»_axis
			«ENDFOR»
			
				«externalStreams»
			
			«externalStreamSize»
				«IF !psToPlFifos.empty»// -- Readers«ENDIF»
				«FOR conn : psToPlFifos»
					«getQueueName(conn)»_reader(«getQueueName(conn)»_axis,«getQueueName(conn)»);
				«ENDFOR»
				// -- Network
				«network.simpleName»(«FOR conn : interfaces SEPARATOR ", "»«getQueueName(conn)»«ENDFOR»«IF !instancesExternalVariablesMap.empty»«IF !interfaces.empty», «ENDIF»«getExternalVarsArguments(instancesExternalVariablesMap)»«ENDIF»);
				«IF !plToPsFifos.empty»// -- Writers«ENDIF»
				«FOR conn : plToPsFifos»
				«getQueueName(conn)»_writer(«getQueueName(conn)», «getQueueName(conn)»_axis);
				«ENDFOR»
				return;
			}
		'''
		}

		def getExternalVarsDeclaration(Map<Instance, List<Var>> actorsExternalVariablesMap){
			'''«FOR instance: actorsExternalVariablesMap.keySet SEPARATOR ", "»«FOR variable : actorsExternalVariablesMap.get(instance) SEPARATOR ", "»volatile «variable.type.doSwitch» * «variable.name»_«instance.simpleName»«ENDFOR»«ENDFOR»'''
		}
		
		def getExternalVarsArguments(Map<Instance, List<Var>> actorsExternalVariablesMap){
			'''«FOR instance: actorsExternalVariablesMap.keySet SEPARATOR ", "»«FOR variable : actorsExternalVariablesMap.get(instance) SEPARATOR ", "»«variable.name»_«instance.simpleName»«ENDFOR»«ENDFOR»'''
		}

		def getHeaders() {
			'''
			#include <ap_axi_sdata.h>
			#include "«typePrinter.channelIncludeHeaderFile»"
			«FOR instance : partitionInstances»
				#include "«instance.simpleName».h"
			«ENDFOR»
		'''
		}
		
		def getStructures(){
			'''
			template<int D>
			struct AxiWord {
				ap_int<D> data;
				ap_uint<1> last;
			};
		'''
		}
		
		def getInternalStreams() {
			'''
			«FOR connection : plToPlFifos»
				static hls::stream<«if (connection.source instanceof Instance) connection.sourcePort.type.doSwitch else (connection.source as Port).type.doSwitch»> «getQueueName(connection)»;
			«ENDFOR»
		'''
		}
		
		def getExternalStreams() {
			'''
			
			// -- FIFO Queue Channels
			«FOR connection : psToPlFifos»
				static hls::stream<«if (connection.source instanceof Instance) connection.sourcePort.type.doSwitch else (connection.source as Port).type.doSwitch»> «getQueueName(connection)»;
			«ENDFOR»
			
			«FOR connection : plToPsFifos»
				static hls::stream<«if (connection.source instanceof Instance) connection.sourcePort.type.doSwitch else (connection.source as Port).type.doSwitch»> «getQueueName(connection)»;
			«ENDFOR»
		'''
		}
		
		def getInternalStreamSize() {
			'''
			«FOR connection : plToPlFifos»
				#pragma HLS STREAM variable=«getQueueName(connection)» depth=«IF connection.size === null»«fifoSize»«ELSE»«connection.size»«ENDIF» dim=1
			«ENDFOR»
		'''
		}
		
		def getExternalStreamSize() {
			'''
			«FOR connection : psToPlFifos»
				#pragma HLS STREAM variable=«getQueueName(connection)» depth=«IF connection.size === null»«fifoSize»«ELSE»«connection.size»«ENDIF» dim=1
			«ENDFOR»
			«FOR connection : plToPsFifos»
				#pragma HLS STREAM variable=«getQueueName(connection)» depth=«IF connection.size === null»«fifoSize»«ELSE»«connection.size»«ENDIF» dim=1
			«ENDFOR»
		'''
		}

		def getReadersWriters() {
			'''
			«FOR conn : psToPlFifos»
				void «getQueueName(conn)»_reader(hls::stream<AxiWord<32> >  &«getQueueName(conn)»_axis, «typePrinter.channelClassName»< «conn.sourcePort.type.doSwitch» > &«getQueueName(conn)»){
					if(!«getQueueName(conn)»_axis.empty() && !«getQueueName(conn)».full()){
						AxiWord<32> axi_word = «getQueueName(conn)»_axis.read();
						int data_read = axi_word.data;
						«getQueueName(conn)».write(data_read);
					}
				}
			«ENDFOR»
			
			«FOR conn : plToPsFifos»
				void «getQueueName(conn)»_writer(«typePrinter.channelClassName»< «conn.sourcePort.type.doSwitch» > &«getQueueName(conn)», hls::stream<AxiWord<32> > &«getQueueName(conn)»_axis){
					if(!«getQueueName(conn)».empty() && !«getQueueName(conn)»_axis.full()){
						int data = «getQueueName(conn)».read();
						AxiWord<32> axi_word = {data, 1};
						«getQueueName(conn)»_axis.write(axi_word);
					}
				}
			«ENDFOR»
			
		'''
		}
		
		def getInstances() {
			'''
			// -- Instances
			«FOR instance : partitionInstances»
				«getInstance(instance)»
			«ENDFOR»
		'''
		}

		def getInstance(Instance instance) {
			val Map<Port, Connection> incoming = instance.incomingPortMap
			val Map<Port, List<Connection>> outgoing = instance.outgoingPortMap
			val List<Port> inputs = instance.getActor.inputs
			val List<Port> outputs = instance.getActor.outputs
			
			var List<Var> externalVariables = new ArrayList<Var>
			if(instance.getActor.hasAttribute(INSTANCE_EXTERNAL_VARIABLES)){
				externalVariables = instance.getActor.getAttribute(INSTANCE_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
			}
			
			'''
			«instance.simpleName»(«FOR port : inputs SEPARATOR ", "»«getQueueName(incoming.get(port))»«ENDFOR»«IF !inputs.empty && !outputs.empty», «ENDIF»«FOR port : outputs SEPARATOR ", "»«FOR connection: outgoing.get(port) SEPARATOR ", "»«getQueueName(connection)»«ENDFOR»«ENDFOR»«IF !externalVariables.empty»«IF !inputs.empty || !outputs.empty», «ENDIF»«FOR variable: externalVariables SEPARATOR ", "»«variable.name»_«instance.simpleName»«ENDFOR»«ENDIF»);
		'''
		}

		def getQueueName(Connection connection) {
			'''fifo_«connection.getAttribute("id").objectValue»'''
		}

	}
	
