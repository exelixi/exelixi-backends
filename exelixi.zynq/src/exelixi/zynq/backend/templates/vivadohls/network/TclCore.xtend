/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.zynq.backend.templates.vivadohls.network

import exelixi.hls.partitioner.Partitioner
import exelixi.hls.vivadohls.backend.scripts.VivadoHLSProjectCoreTCLPrinter
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import exelixi.zynq.backend.interconnect.InterconnectionType
import java.util.ArrayList
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network

import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND

class TclCore extends VivadoHLSProjectCoreTCLPrinter {

	new(Network network, Map<String, Object> options, Partitioner partitioner) {
		super(network, options, partitioner)
	}

	override multipleIpCores() {
		val Map<Connection, List<Connection>> bcConnections = partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION,
			ZYNQ_PL_PARTITION);
		var List<String> fanoutFifoNames = new ArrayList
		for (Connection connection : bcConnections.keySet()) {
			if (bcConnections.get(connection).size() > 1) {
				val String name = "fanout_" + TemplateUtils.getQueueNameIO(connection)
				fanoutFifoNames.add(name)
			}
		}
		val String interconnectionOtion = options.get(ZYNQ_OPTION_COMMUNICATION_KIND) as String
		val interrconnectionType = new InterconnectionType(interconnectionOtion);
		
		val List<Connection> coreInputs = new ArrayList<Connection>(partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION).keySet)
		val List<Connection> coreOutputs = new ArrayList<Connection>(partitioner.getConnectionToConnectionBd(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION).keySet)
		
		'''
			set src_path "../«partialPath»/src"
			set tbsrc_path "../«partialPath»/src-tb"
			set header_path "../«partialPath»/include"
			
			cd ip_cores
			
			## ----------------------------------------------------------------------------
			## -- Wrapper Cores
			«IF interrconnectionType.isSignleDMA»
				«IF !coreInputs.empty»
					«wrapperCore("AxisInput_Wrapper")»
				«ENDIF»
				«IF !coreOutputs.empty»
					«wrapperCore("AxisOutput_Wrapper")»
				«ENDIF»
			«ELSE»
				«FOR connection : coreInputs»
					«wrapperCore("AxisInput_" + TemplateUtils.getQueueNameIO(connection))»
				«ENDFOR»
				«FOR connection : coreOutputs»
					«wrapperCore("AxisOutput_" + TemplateUtils.getQueueNameIO(connection))»
				«ENDFOR»
			«ENDIF»
			
			## ----------------------------------------------------------------------------
			## -- Kicker Core
			«kickerCore»
			
			«IF !fanoutFifoNames.empty»
				## ----------------------------------------------------------------------------
				## -- Fanout Cores
				«FOR fanoutFifo : fanoutFifoNames»
					«fanoutCore(fanoutFifo)»
				«ENDFOR»
			«ENDIF»
			
			## ----------------------------------------------------------------------------
			## -- Instances
			«FOR instance : instances»
				«IF !instance.getActor.isNative»
					«instanceCore(instance)»
				«ENDIF»
			«ENDFOR»
			
			cd ..
			
			exit
			## EOF
		'''
	}

	private def kickerCore() {
		'''
			## -- Create Project
			open_project hls_kicker 
			
			## -- Add file
			add_files $src_path/kicker.cpp
			
			## -- Set Top Level
			set_top kicker
			
			## -- Create Solution
			open_solution -reset kicker_solution
			
			## -- Define Xilinx Technology
			set_part {«xilinxDevice.fpga»} «IF xilinxDevice.isZynqUltrascale»-tool vivado«ENDIF»
			
			## -- Define Clock period
			create_clock -period «clockPeriod» -name clk
			
			## -- Config RTL
			config_rtl -reset_level low
			
			## -- Run Synthesis
			csynth_design
				
			## -- Export Design
			export_design -format ip_catalog
		'''
	}

	private def fanoutCore(String fanoutFifo) {
		'''
			## -- Create Project
			open_project hls_«fanoutFifo» 
			
			## -- Add file
			add_files $src_path/«fanoutFifo».cpp
			
			## -- Set Top Level
			set_top «fanoutFifo»
			
			## -- Create Solution
			open_solution -reset «fanoutFifo»_solution
			
			## -- Define Xilinx Technology
			set_part {«xilinxDevice.fpga»} «IF xilinxDevice.isZynqUltrascale»-tool vivado«ENDIF»
			
			## -- Define Clock period
			create_clock -period «clockPeriod» -name clk
			
			## -- Config RTL
			config_rtl -reset_level low
			
			## -- Run Synthesis
			csynth_design
				
			## -- Export Design
			export_design -format ip_catalog
		'''
	}

	private def instanceCore(Instance instance) {
		'''
			## -- Create Project
			open_project hls_«instance.simpleName» 
			
			## -- Add file
			add_files $src_path/«instance.simpleName».cpp -cflags "-I$header_path"
			
			## -- Add TestBench files directory
			add_files -tb ../../../fifo-traces
			
			## -- Add TestBench source file
			add_files -tb $tbsrc_path/«instance.simpleName»_tb.cpp -cflags "-I$header_path"
			
			## -- Set Top Level
			set_top «TemplatesUtils.getInstanceName(instance)»
			
			## -- Create Solution
			open_solution -reset «instance.simpleName»_solution
			
			## -- Define Xilinx Technology
			set_part {«xilinxDevice.fpga»} «IF xilinxDevice.isZynqUltrascale»-tool vivado«ENDIF»
			
			## -- Define Clock period
			create_clock -period «clockPeriod» -name clk
			
			## -- Config RTL
			config_rtl -reset_level low
			
			## -- Run Synthesis
			csynth_design
				
			## -- Export Design
			export_design -format ip_catalog
		'''
	}
	
	private def wrapperCore(String name){
		'''
			## -- Create Project
			open_project hls_«name» 
			
			## -- Add file
			add_files $src_path/«name».cpp -cflags "-I$header_path"
			
			## -- Add TestBench files directory
			add_files -tb ../../../fifo-traces
			
			## -- Set Top Level
			set_top «name»
			
			## -- Create Solution
			open_solution -reset «name»_solution
			
			## -- Define Xilinx Technology
			set_part {«xilinxDevice.fpga»} «IF xilinxDevice.isZynqUltrascale»-tool vivado«ENDIF»
			
			## -- Define Clock period
			create_clock -period «clockPeriod» -name clk
			
			## -- Config RTL
			config_rtl -reset_level low
			
			## -- Run Synthesis
			csynth_design
				
			## -- Export Design
			export_design -format ip_catalog
		'''
	}

}
