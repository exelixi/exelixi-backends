/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 

package exelixi.zynq.backend.templates.vivadohls.wrapper

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import java.util.ArrayList
import java.util.List
import net.sf.orcc.df.Connection
import net.sf.orcc.ir.Type

import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING

class AxisOutputWrapper extends HLSTemplate {

	protected Connection outputConnection

	protected List<Connection> outputConnections

	protected Type type

	protected Boolean isSingleDMA

	new(TypePrinterVisitor typePrinter, Connection connection) {
		super(typePrinter)
		this.outputConnection = connection
		this.outputConnections = new ArrayList
		type = connection.sourcePort.type
	}

	new(TypePrinterVisitor typePrinter, List<Connection> connections) {
		super(typePrinter)
		this.outputConnections = connections
	}

	new(TypePrinterVisitor typePrinter, Connection connection, Boolean isSingleDMA) {
		this(typePrinter, connection)
		this.isSingleDMA = isSingleDMA
	}

	new(TypePrinterVisitor typePrinter, List<Connection> connections, Boolean isSingleDMA) {
		this(typePrinter, connections)
		this.isSingleDMA = isSingleDMA
	}

	public def getContent() {
		'''
			«getFileHeader("Exilixi for Vivado HLS, AXI-Stream Output Wrapper Code Generation")»
			
			«IF isSingleDMA»
				«getWrapperSingleDMA(outputConnections)»
			«ELSEIF TemplateUtils.getConnectionKind(outputConnection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
				«getWrapperDMA(outputConnection)»
			«ELSEIF TemplateUtils.getConnectionKind(outputConnection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
				«wrapperLLfifo»
			«ENDIF»
		'''
	}

	private def getWrapperLLfifo() {
		'''
			#include <ap_axi_sdata.h>
			#include <hls_stream.h>
			
			template<int D>
			struct AxiWord {
				ap_uint<D> data;
				ap_uint<1> last;
			};
			
			
			void AxisOutput_«TemplateUtils.getQueueNameIO(outputConnection)»(hls::stream< «doSwitch(type)» > &IN, hls::stream< AxiWord< 32 > > &OUT) {
			#pragma HLS INTERFACE axis register both port=OUT
			#pragma HLS INTERFACE s_axilite port=return bundle=control
			
				if(!IN.empty() && !OUT.full()){
					int data = («doSwitch(type)») IN.read();
					AxiWord<32> value;
					value.data = data;
					value.last = 1;
					OUT.write(value);
				}
			
				return;
			}
		'''
	}

	private def getWrapperDMA(Connection connection) {
		val Type type = connection.sourcePort.type
		'''
			#include <ap_axi_sdata.h>
			#include <hls_stream.h>
			
			template<int D>
			struct AxiWord {
				ap_int<D> data;
				ap_uint<1> last;
			};
			
			void AxisOutput_«TemplateUtils.getQueueNameIO(connection)»(unsigned int length, bool receive,
					unsigned int &occupancy, unsigned int count,
					hls::stream<«doSwitch(type)»> &IN, hls::stream<AxiWord<32> > &OUT) {
			#pragma HLS INTERFACE s_axilite port=length bundle=control
			#pragma HLS INTERFACE s_axilite port=receive bundle=control
			#pragma HLS INTERFACE s_axilite port=occupancy bundle=control
			#pragma HLS INTERFACE axis register both port=OUT
			#pragma HLS INTERFACE s_axilite port=return bundle=control
			
				if (receive) {
					unsigned int i = 0;
					while (i < length) {
						if (!IN.empty() && !OUT.full()) {
							if(i < (length - 1)){
								unsigned int data = (unsigned int) IN.read();
								AxiWord< 32 > value;
								value.data = data;
								value.last = 0;
								OUT.write(value);
								i++;
							}else{
								unsigned int data = (unsigned int) IN.read();
								AxiWord< 32 > value;
								value.data = data;
								value.last = 1;
								OUT.write(value);
								i++;
							}
						}
					}
				} else {
					if (count ==  0 && !IN.empty()){
						occupancy = 1;
					}else{
						occupancy = count;
					}
				}
			
				return;
			}
		'''
	}

	private def getWrapperSingleDMA(List<Connection> connections) {
		'''
			#include <ap_axi_sdata.h>
			#include <hls_stream.h>
			
			template<int D>
			struct AxiWord {
				ap_int<D> data;
				ap_uint<1> last;
			};
			
			void AxisOutput_Wrapper(
				unsigned int length,
				bool receive,
				unsigned int selection,
				unsigned int &occupancy,
				«FOR connection : connections SEPARATOR ", "»unsigned int «TemplateUtils.getQueueNameIO(connection) + "_count"»«ENDFOR»,
				«FOR connection : connections SEPARATOR ", "»hls::stream< «doSwitch(connection.sourcePort.type)» > &«TemplateUtils.getQueueNameIO(connection)»«ENDFOR»,
				hls::stream<AxiWord<32> > &OUT) {
			#pragma HLS INTERFACE s_axilite port=length bundle=control
			#pragma HLS INTERFACE s_axilite port=receive bundle=control
			#pragma HLS INTERFACE s_axilite port=selection bundle=control
			#pragma HLS INTERFACE s_axilite port=occupancy bundle=control
			#pragma HLS INTERFACE axis register both port=OUT
			#pragma HLS INTERFACE s_axilite port=return bundle=control
			
				if (receive) {
					«IF connections.size == 1»
						unsigned int i = 0;
						while (i < length) {
							if (!«TemplateUtils.getQueueNameIO(connections.get(0))».empty() && !OUT.full()) {
								if(i < (length - 1)){
									int data = (unsigned int) «TemplateUtils.getQueueNameIO(connections.get(0))».read();
									AxiWord<32> value;
									value.data = data;
									value.last = 0;
									OUT.write(value);
									i++;
								}else{
									int data = (unsigned int) «TemplateUtils.getQueueNameIO(connections.get(0))».read();
									AxiWord<32> value;
									value.data = data;
									value.last = 1;
									OUT.write(value);
									i++;
								}
							}
						}
					«ELSE»
						switch(selection){
						«FOR connection : connections»
							case «TemplateUtils.getQueueBroadcastId(connection)»:{
									unsigned int i = 0;
									while (i < length) {
										if (!«TemplateUtils.getQueueNameIO(connection)».empty() && !OUT.full()) {
											if(i < (length - 1)){
												int data = (unsigned int) «TemplateUtils.getQueueNameIO(connection)».read();
												AxiWord<32> value;
												value.data = data;
												value.last = 0;
												OUT.write(value);
												i++;
											}else{
												int data = (unsigned int) «TemplateUtils.getQueueNameIO(connection)».read();
												AxiWord<32> value;
												value.data = data;
												value.last = 1;
												OUT.write(value);
												i++;
											}
										}
									}
							}
							break;
						«ENDFOR»	
						default:
							break;
						}
					«ENDIF»
				} else {
					«IF connections.size == 1»
						if («TemplateUtils.getQueueNameIO(connections.get(0)) + "_count"» ==  0 && !«TemplateUtils.getQueueNameIO(connections.get(0))».empty()){
							occupancy = 1;
						}else{
							occupancy = «TemplateUtils.getQueueNameIO(connections.get(0)) + "_count"»;
						}
					«ELSE»
						switch(selection){
						«FOR connection : connections»
							case «TemplateUtils.getQueueBroadcastId(connection)»:
								if («TemplateUtils.getQueueNameIO(connection) + "_count"» ==  0 && !«TemplateUtils.getQueueNameIO(connection)».empty()){
									occupancy = 1;
								}else{
									occupancy = «TemplateUtils.getQueueNameIO(connection) + "_count"»;
								}
								break;
						«ENDFOR»
						default:
							break;
						}
					«ENDIF»
				}
			
				return;
			}
		'''
	}

	public def String getFileName() {
		return "AxisOutput_" + TemplateUtils.getQueueNameIO(outputConnection) + ".cpp"
	}

}
