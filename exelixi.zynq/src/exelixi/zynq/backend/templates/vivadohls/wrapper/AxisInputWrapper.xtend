/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.vivadohls.wrapper

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils
import java.util.ArrayList
import java.util.List
import net.sf.orcc.df.Connection
import net.sf.orcc.ir.Type

import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING

class AxisInputWrapper extends HLSTemplate {

	protected Connection inputConnection

	protected List<Connection> inputConnections

	protected Type type

	protected Boolean isSingleDMA

	new(TypePrinterVisitor typePrinter, Connection connection) {
		super(typePrinter)
		this.inputConnection = connection
		this.inputConnections = new ArrayList
		type = connection.sourcePort.type
	}

	new(TypePrinterVisitor typePrinter, List<Connection> connections) {
		super(typePrinter)
		this.inputConnections = connections
	}

	new(TypePrinterVisitor typePrinter, Connection connection, Boolean isSingleDMA) {
		this(typePrinter, connection)
		this.isSingleDMA = isSingleDMA
	}

	new(TypePrinterVisitor typePrinter, List<Connection> connections, Boolean isSingleDMA) {
		this(typePrinter, connections)
		this.isSingleDMA = isSingleDMA
	}

	public def getContent() {
		'''
			«getFileHeader("Exilixi for Vivado HLS, AXI-Stream Input Wrapper Code Generation")»
			«IF isSingleDMA»
				«getWrapperSingleDMA(inputConnections)»
			«ELSEIF TemplateUtils.getConnectionKind(inputConnection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)»
				«getWrapperDMA(inputConnection)»
			«ELSEIF TemplateUtils.getConnectionKind(inputConnection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)»
				«wrapperLLfifo»
			«ENDIF»
		'''
	}

	private def getWrapperLLfifo() {
		'''
			#include <ap_axi_sdata.h>
			#include <hls_stream.h>
			
			template<int D>
			struct AxiWord {
				ap_int<D> data;
				ap_uint<1> last;
			};
			
			
			void AxisInput_«TemplateUtils.getQueueNameIO(inputConnection)»(hls::stream< AxiWord< 32 > > &IN, hls::stream< «doSwitch(type)» > &OUT) {
			#pragma HLS INTERFACE axis register both port=IN
			#pragma HLS INTERFACE s_axilite port=return bundle=control
			
				if(!IN.empty() && !OUT.full()){
					«doSwitch(type)» data = («doSwitch(type)») IN.read().data;
					OUT.write(data);
				}
			
				return;
			}
		'''
	}

	private def getWrapperDMA(Connection connection) {
		val Type type = connection.sourcePort.type
		'''
			#include <ap_axi_sdata.h>
			#include <hls_stream.h>
			
			template<int D>
			struct AxiWord {
				ap_int<D> data;
				ap_uint<1> last;
			};
			
			void AxisInput_«TemplateUtils.getQueueNameIO(connection)»(bool transmit, unsigned int length,
					unsigned int &vacancy, unsigned int size, unsigned int count, hls::stream<AxiWord<32> > &IN,
					hls::stream< «doSwitch(type)» > &OUT) {
			#pragma HLS INTERFACE s_axilite port=transmit bundle=control
			#pragma HLS INTERFACE s_axilite port=vacancy bundle=control
			#pragma HLS INTERFACE s_axilite port=length bundle=control
			#pragma HLS INTERFACE s_axilite port=return bundle=control
			#pragma HLS INTERFACE axis register both port=IN
			
				if (transmit) {
					unsigned int i = 0;
					while (i < length) {
						if (!IN.empty() && !OUT.full()) {
							«doSwitch(type)» data = ( «doSwitch(type)» ) IN.read().data;
							OUT.write(data);
							i++;
						}
					}
				} else {
					vacancy = size - count;
				}
			
				return;
			}
			
		'''
	}

	private def getWrapperSingleDMA(List<Connection> connections) {
		'''
			#include <ap_axi_sdata.h>
			#include <hls_stream.h>
			
			template<int D>
			struct AxiWord {
				ap_int<D> data;
				ap_uint<1> last;
			};
			
			void AxisInput_Wrapper(
				bool transmit, 
				unsigned int selection,
				unsigned int length,
				unsigned int &vacancy,
				hls::stream<AxiWord<32> > &IN,
				«FOR connection : connections SEPARATOR ", "»unsigned int «TemplateUtils.getQueueNameIO(connection) + "_size"»«ENDFOR»,
				«FOR connection : connections SEPARATOR ", "»unsigned int «TemplateUtils.getQueueNameIO(connection) + "_count"»«ENDFOR»,
				«FOR connection : connections SEPARATOR ", "»hls::stream< «doSwitch(connection.sourcePort.type)» > &«TemplateUtils.getQueueNameIO(connection)»«ENDFOR») {
			#pragma HLS INTERFACE s_axilite port=transmit bundle=control
			#pragma HLS INTERFACE s_axilite port=selection bundle=control
			#pragma HLS INTERFACE s_axilite port=vacancy bundle=control
			#pragma HLS INTERFACE s_axilite port=length bundle=control
			#pragma HLS INTERFACE s_axilite port=return bundle=control
			#pragma HLS INTERFACE axis register both port=IN
			
				if (transmit) {
					«IF connections.size == 1»
						unsigned int i = 0;
						while (i < length) {
							if (!IN.empty() && !«TemplateUtils.getQueueNameIO(connections.get(0))».full()) {
								«doSwitch(connections.get(0).sourcePort.type)» data = («doSwitch(connections.get(0).sourcePort.type)») IN.read().data;
								«TemplateUtils.getQueueNameIO(connections.get(0))».write(data);
								i++;
							}
						}
					«ELSE»
						switch(selection){
						«FOR connection : connections»
							case «TemplateUtils.getQueueBroadcastId(connection)»:{
									unsigned int i = 0;
									while (i < length) {
										if (!IN.empty() && !«TemplateUtils.getQueueNameIO(connection)».full()) {
											«doSwitch(connection.sourcePort.type)» data = («doSwitch(connection.sourcePort.type)») IN.read().data;
											«TemplateUtils.getQueueNameIO(connection)».write(data);
											i++;
										}
									}
							}
							break;
						«ENDFOR»	
						default:
							break;
						}
					«ENDIF»
				} else {
					«IF connections.size == 1»
						vacancy = «TemplateUtils.getQueueNameIO(connections.get(0)) + "_size"» - «TemplateUtils.getQueueNameIO(connections.get(0)) + "_count"»;
					«ELSE»
						switch(selection){
						«FOR connection : connections»
							case «TemplateUtils.getQueueBroadcastId(connection)»:
								vacancy = «TemplateUtils.getQueueNameIO(connection) + "_size"» - «TemplateUtils.getQueueNameIO(connection) + "_count"»;
								break;
						«ENDFOR»
						default:
							break;
						}
					«ENDIF»
				}
			
				return;
			}
		'''
	}

	public def String getFileName() {
		return "AxisInput_" + TemplateUtils.getQueueNameIO(inputConnection) + ".cpp"
	}

}
