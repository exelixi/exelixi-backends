/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
 
package exelixi.zynq.backend.templates.vivadohls.fanout

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.List
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Port
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils

class FanoutCore extends HLSTemplate {
	
	Connection source
	
	List<Connection> targets
	
	String name
	
	Port port
	
	Integer fanoutNumber
	
	new(TypePrinterVisitor typePrinter, Connection connection, List<Connection> bcConnections) {
		super(typePrinter)
		source = connection
		targets = bcConnections
		name =  "fanout_" + TemplateUtils.getQueueNameIO(connection)
		port = source.sourcePort
		fanoutNumber = targets.size
	}
	
	def getContent(){
		'''
			«getFileHeader("Exelixi for Vivado HLS,  Fan-out Code Generation, Fifo: " + name)»
			#include <«typePrinter.intIncludeHeaderFile»>
			#include <«typePrinter.channelIncludeHeaderFile»>
			
			void «name»(«typePrinter.channelClassName»< «port.type.doSwitch» > &IN, «FOR int i : 0 .. fanoutNumber-1 SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &OUT_«i»«ENDFOR»){
			#pragma HLS INTERFACE ap_ctrl_hs port=return
			
				if(!IN.empty() && «FOR int i : 0 .. fanoutNumber -1 SEPARATOR " && "»!OUT_«i».full()«ENDFOR»){
					«port.type.doSwitch» value = IN.read();
					«FOR int i : 0 .. fanoutNumber - 1»
						OUT_«i».write(value);
					«ENDFOR»
				}
			
				return;
			}
		'''
	}
}
