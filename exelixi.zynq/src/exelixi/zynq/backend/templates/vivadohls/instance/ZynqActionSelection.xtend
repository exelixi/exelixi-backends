/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 

package exelixi.zynq.backend.templates.vivadohls.instance

import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.types.TypePrinterVisitor
import exelixi.hls.vivadohls.backend.instance.actionselection.ActionSelectionGeneric
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Set
import net.sf.orcc.df.Action
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.ir.Var

import static exelixi.hls.backend.Constants.ACTION_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ACTOR_ACTION_SELECTION_PEEKS
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION

class ZynqActionSelection extends ActionSelectionGeneric {

	boolean isPlPsBorderInstance

	Map<Port, Integer> portReaders

	new(Instance instance, TypePrinterVisitor typePrinter, Partitioner partitioner, Map<String, Object> options) {
		super(instance, typePrinter, partitioner, options)
		portReaders = new HashMap
		isPlPsBorderInstance = false
		var boolean portAlreadyConnectedToPS = false
		for (port : instance.outgoingPortMap.keySet) {
			var int nbReaders = 0
			for (connection : instance.outgoingPortMap.get(port)) {
				val Instance targetInstance = connection.target as Instance
				if (partitioner.isInstanceOnPartition(targetInstance, ZYNQ_PS_PARTITION)) {
					if (!portAlreadyConnectedToPS) {
						isPlPsBorderInstance = true
						portAlreadyConnectedToPS = true
						nbReaders++
					}
				} else {
					nbReaders++
				}
			}
			portAlreadyConnectedToPS = false
			portReaders.put(port, nbReaders)
		}
	}

	override List<String> getActionSelectionParameters() {
		var List<String> parameters = new ArrayList

		val Set<Port> portsWithCount = TemplatesUtils.getPortsWithCount(instance);
		// -- Input parameters
		for (port : actor.inputs) {
			val String value = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" + " &" + port.name
			parameters.add(value)
			if (portsWithCount.contains(port)) {
				val String count = "int " + port.name + "_count"
				parameters.add(count)
			}

		}

		// -- Peeks
		if (actor.hasAttribute(ACTOR_ACTION_SELECTION_PEEKS)) {
			val Map<Port, List<Integer>> indexes = actor.getValueAsObject(ACTOR_ACTION_SELECTION_PEEKS)

			for (Port port : indexes.keySet) {
				for (Integer index : indexes.get(port)) {
					val String value = doSwitch(port.type) + " " + port.name + "_peek" +
						if(index > 1) "_" + index else ""
					parameters.add(value)
				}
			}
		}

		// -- Output parameters
		for (port : actor.outputs) {
			val nbReaders = portReaders.get(port)
			if (nbReaders === 1) {
				val String value = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" + " &" + port.name
				parameters.add(value)
				if (portsWithCount.contains(port)) {
					val String count = "int " + port.name + "_count"
					parameters.add(count)
					val String size = "int " + port.name + "_size"
					parameters.add(size)
				}
			} else {
				for (int i : 0 .. nbReaders - 1) {
					val String value = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" + " &" +
						port.name + "_" + i
					parameters.add(value)
					if (portsWithCount.contains(port)) {
						val String count = "int " + port.name + "_" + i + "_count"
						parameters.add(count)
						val String size = "int " + port.name + "_" + i + "_size"
						parameters.add(size)
					}
				}
			}
		}

		// -- External Variables
		for (variable : externalVariables) {
			val String value = "volatile " + variable.type.doSwitch + " * " + variable.name
			parameters.add(value)
		}

		return parameters
	}

	override List<String> getTransitionOutputConditions(Action action) {
		val List<String> conditions = new ArrayList<String>

		for (port : action.outputPattern.ports) {
			val nbReaders = portReaders.get(port)
			if (nbReaders === 1) {
				if (action.outputPattern.numTokensMap.get(port) == 1) {
					val String outputPortCondition = "!" + port.name + ".full()"
					conditions.add(outputPortCondition)
				} else {
					val String outputPortCondition = "(" + "!" + port.name + ".full()" + " && " + "(" + port.name +
						"_size" + " - " + port.name + "_count" + " >= " + action.outputPattern.numTokensMap.get(port) +
						")" + ")"
					conditions.add(outputPortCondition)
				}
			} else {
				for (int i : 0 .. nbReaders - 1) {
					if (action.outputPattern.numTokensMap.get(port) == 1) {
						val String outputPortCondition = "!" + port.name + "_" + i + ".full()"
						conditions.add(outputPortCondition)
					} else {
						val String outputPortCondition = "(" + "!" + port.name + "_" + i + ".full()" + " && " + "(" +
							port.name + "_" + i + "_size" + " - " + port.name + "_" + i + "_count" + " >= " +
							action.outputPattern.numTokensMap.get(port) + ")" + ")"
						conditions.add(outputPortCondition)
					}
				}
			}
		}
		return conditions
	}

	override List<String> getTransitionArguments(Action action) {
		val List<String> transitionArguments = new ArrayList<String>
		var externalVariables = new ArrayList<Var>
		if (action.hasAttribute(ACTION_EXTERNAL_VARIABLES)) {
			externalVariables = action.getAttribute(ACTION_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
		}
		// -- Inputs
		if (!isActorComplex) {
			for (port : action.inputPattern.ports) {
				transitionArguments.add(port.name)
			}
		}

		// -- Outputs
		for (port : action.outputPattern.ports) {
			val nbReaders = portReaders.get(port)
			if (nbReaders === 1) {
				transitionArguments.add(port.name)
			} else {
				for (int i : 0 .. nbReaders - 1) {
					transitionArguments.add(port.name + "_" + i)
				}
			}
		}

		// -- External variables
		for (variable : externalVariables) {
			transitionArguments.add(variable.name)
		}

		return transitionArguments

	}

}
