/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.zynq.backend.templates.vivadohls.instance

import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.instance.actionselection.ActionSelection
import exelixi.hls.templates.types.TypePrinterVisitor
import exelixi.hls.vivadohls.backend.instance.VivadoHLSInstance
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import net.sf.orcc.df.Action
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.ir.Var

import static exelixi.hls.backend.Constants.ACTION_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION

class ZynqVivadoHLSInstance extends VivadoHLSInstance {

	boolean isPlPsBorderInstance

	Map<Port, Integer> portReaders = new HashMap

	new(Instance instance, TypePrinterVisitor typePrinter, Map<String, Object> options, Partitioner partitioner) {
		super(instance, typePrinter, options, partitioner)

		isPlPsBorderInstance = false
		var boolean portAlreadyConnectedToPS = false 
		for (port : instance.outgoingPortMap.keySet) {
			var int nbReaders = 0
			for (connection : instance.outgoingPortMap.get(port)) {
				val Instance targetInstance = connection.target as Instance
				if (partitioner.isInstanceOnPartition(targetInstance, ZYNQ_PS_PARTITION)) {
					if (!portAlreadyConnectedToPS) {
						isPlPsBorderInstance = true
						portAlreadyConnectedToPS = true
						nbReaders++
					}
				} else {
					nbReaders++
				}
			}
			portAlreadyConnectedToPS = false 
			portReaders.put(port, nbReaders)
		}
	}

	override print(Action action) {
		var List<String> parameters = new ArrayList
		var externalVariables = new ArrayList<Var>
		if (action.hasAttribute(ACTION_EXTERNAL_VARIABLES)) {
			externalVariables = action.getAttribute(ACTION_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
		}

		// -- Treat Inputs
		if (!isActorComplex) {
			for (port : action.inputPattern.ports) {
				val String inputParameter = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" + " &" +
					port.name
				parameters.add(inputParameter)
			}
		}
		// -- Treat Outputs
		for (port : action.outputPattern.ports) {
			val nbReaders = portReaders.get(port)
			if (nbReaders === 1) {
				val String outputParameter = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" + " &" +
					port.name
				parameters.add(outputParameter)
			} else {
				for (int i : 0 .. nbReaders - 1) {
					val String outputParameter = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" +
						" &" + port.name + "_" + i
					parameters.add(outputParameter)
				}
			}
		}

		// -- Treat External variables
		for (variable : externalVariables) {
			val String externalVariable = "volatile " + variable.type.doSwitch + " *" + variable.name
			parameters.add(externalVariable)
		}

		'''
			«printActionScheduler(action)»
			static void «action.body.name»(«FOR parameter : parameters SEPARATOR ", "»«parameter»«ENDFOR»){
			«printActionBody(action)»
			}
		'''
	}

	override actionOutputs(Action action) {
		var maxOutputRepeat = 1
		for(port : action.outputPattern.ports){
			val repeat = action.outputPattern.getNumTokens(port)
			if(repeat > maxOutputRepeat)
				maxOutputRepeat = repeat
		}
		'''
			«IF !action.outputPattern.ports.empty»
				«IF maxOutputRepeat == 1»
					«FOR port : action.outputPattern.ports»
						«IF portReaders.get(port) === 1»
							«getWrite(port, action.outputPattern.portToVarMap, "0")»
						«ELSE»
							«FOR int i : 0 .. portReaders.get(port) - 1»
								«getWriteWithBroadcast(port, action.outputPattern.portToVarMap, "0", i)»
							«ENDFOR»
						«ENDIF»
					«ENDFOR»
				«ELSE»
					for(int i = 0; i < «maxOutputRepeat»; i++){
						«FOR port: action.outputPattern.ports»
							«IF action.outputPattern.ports.size > 1»
								if(i < «action.outputPattern.getNumTokens(port)»){
									«IF portReaders.get(port) === 1»
										«getWrite(port, action.outputPattern.portToVarMap, "i")»
									«ELSE»
										«FOR int i : 0 .. portReaders.get(port) - 1»
											«getWriteWithBroadcast(port, action.outputPattern.portToVarMap, "i", i)»
										«ENDFOR»
									«ENDIF»
								}
							«ELSE»
								«IF portReaders.get(port) === 1»
									«getWrite(port, action.outputPattern.portToVarMap, "i")»
								«ELSE»
									«FOR int i : 0 .. portReaders.get(port) - 1»
										«getWriteWithBroadcast(port, action.outputPattern.portToVarMap, "i", i)»
									«ENDFOR»
								«ENDIF»
							«ENDIF»
						«ENDFOR»
					}
				«ENDIF»
			«ENDIF»
			«IF isActorComplex»	
				«getActionComplexInput(action)»
			«ENDIF»
		'''
	}

	override ActionSelection getActionSelection(){
		return new ZynqActionSelection(instance, typePrinter, partitioner, options)
	}

}
