/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 

package exelixi.zynq.backend.templates.vivadohls.splittermerger

import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Network

import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION

/**
 * An AXI Stream Slave Splitter
 * 
 * @author Endri Bezati
 */
class AxiStreamSplitterCore extends HLSTemplate {

	Network network

	int packageSize

	List<Connection> psToPlFifos

	TypePrinterVisitor typePrinter

	new(Network network, TypePrinterVisitor typePrinter, Partitioner partitioner, Map<String, Object> options) {
		super(typePrinter)
		this.network = network
		this.typePrinter = typePrinter
		this.psToPlFifos = partitioner.getConnections(ZYNQ_PS_PARTITION,
			ZYNQ_PL_PARTITION)

		packageSize = 4096

	}

	def getContent() {
		'''
			«getFileHeader("Vivado HLS AXI Stream Slave")»
			#include <ap_axi_sdata.h>
			#include <hls_stream.h>
			
			#define PACKAGE_SIZE «packageSize»
			
			template <typename T, int U, int TI, int TD>
			T pop_stream(ap_axiu <sizeof(T)*8,U,TI,TD> const &e)
			{
			#pragma HLS INLINE
			
				union
				{
					int ival;
					T oval;
				} converter;
				converter.ival = e.data;
				T ret = converter.oval;
			
				volatile ap_uint<sizeof(T)> strb = e.strb;
				volatile ap_uint<sizeof(T)> keep = e.keep;
				volatile ap_uint<U> user = e.user;
				volatile ap_uint<1> last = e.last;
				volatile ap_uint<TI> id = e.id;
				volatile ap_uint<TD> dest = e.dest;
			
				return ret;
			}
			
			
			void Splitter_«network.simpleName»(ap_axiu<32,4,5,5> AXIS_IN[PACKAGE_SIZE], «FOR conn : psToPlFifos SEPARATOR ", "»«typePrinter.channelClassName»< «conn.sourcePort.type.doSwitch» > &«getQueueName(conn)»«ENDFOR»){
			#pragma HLS INTERFACE s_axilite port=return bundle=CONTROL_BUS
			#pragma HLS INTERFACE axis port=AXIS_IN
				static int elements_to_read = 0;
			
				for(int i = 0; i < PACKAGE_SIZE; i++){
					int data = pop_stream<unsigned int,4,5,5>(AXIS_IN[i]);
					«FOR int i : 0 .. (psToPlFifos.size - 1)»
						«IF i == 0»if«ELSE»}else if«ENDIF»( i == «i * packageSize/psToPlFifos.size» ){
							elements_to_read = data;
						} else if ( (i > «i * packageSize/psToPlFifos.size») && (i <= « (i+1) * packageSize/psToPlFifos.size - 1») ){
							if( i < ( «i*packageSize/psToPlFifos.size + 1» + elements_to_read ) ){
								«getQueueName(psToPlFifos.get(i))».write(pop_stream<«psToPlFifos.get(i).sourcePort.type.doSwitch», 4, 5, 5>(data, i == elements_to_read ));
							}
					«ENDFOR»
					}
				}
				
				return;
			}
		'''
	}

	def getQueueName(Connection connection) {
		'''fifo_«connection.getAttribute("id").objectValue»'''
	}

}
