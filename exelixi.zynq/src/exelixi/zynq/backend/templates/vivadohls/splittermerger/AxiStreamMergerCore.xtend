/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 

package exelixi.zynq.backend.templates.vivadohls.splittermerger

import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Network

import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION

/**
 * 
 * @author Endri Bezati
 */
class AxiStreamMergerCore extends HLSTemplate {
	Network network

	int packageSize

	List<Connection> plToPsFifos

	TypePrinterVisitor typePrinter

	new(Network network, TypePrinterVisitor typePrinter, Partitioner partitioner, Map<String, Object> options) {
		super(typePrinter)
		this.network = network
		this.typePrinter = typePrinter
		this.plToPsFifos = partitioner.getConnections(ZYNQ_PL_PARTITION,
			ZYNQ_PS_PARTITION)

		packageSize = 4096

	}

	def getContent() {
		'''
			#include <ap_axi_sdata.h>
			#include <hls_stream.h>
			
			#define PACKAGE_SIZE «packageSize»
			
			template <typename T, int U, int TI, int TD> ap_axiu <sizeof(T)*8,U,TI,TD> push_stream(T const &v, bool last = false)
			{
			#pragma HLS INLINE
				ap_axiu<sizeof(T)*8,U,TI,TD> e;
			
				union
				{
					int oval;
					T ival;
				} converter;
				converter.ival = v;
				e.data = converter.oval;
			
				// set it to sizeof(T) ones
				e.strb = -1;
				e.keep = 15; //e.strb;
				e.user = 0;
				e.last = last ? 1 : 0;
				e.id = 0;
				e.dest = 0;
				return e;
			}
			
			void Merger_«network.simpleName»(«FOR conn : plToPsFifos SEPARATOR ", "»«typePrinter.channelClassName»< «conn.sourcePort.type.doSwitch» > &«getQueueName(conn)»«ENDFOR», hls::stream< ap_axiu<32,4,5,5> > AXIS_OUT){
			#pragma HLS INTERFACE s_axilite port=return bundle=CONTROL_BUS
			
			#pragma HLS INTERFACE axis depth=«packageSize» port=AXIS_OUT
			
				«FOR conn : plToPsFifos»
					int «getQueueName(conn)»_counter = 0;
					int «getQueueName(conn)»_wait_counter = «packageSize / plToPsFifos.size»;
					bool «getQueueName(conn)»_wait = true;
				«ENDFOR»
				
				int i = 0;
				while(i < PACKAGE_SIZE){
					«FOR int i : 0 .. (plToPsFifos.size - 1)»
						«IF i == 0»if«ELSE»else if«ENDIF»( i >= «i * packageSize/plToPsFifos.size» && i == «IF i != 0»«(i + 1) * packageSize/plToPsFifos.size - 2»«ELSE»«packageSize/plToPsFifos.size - 2»«ENDIF» ){
							if(«getQueueName(plToPsFifos.get(i))»_wait){
								if(!«getQueueName(plToPsFifos.get(i))».empty() && «getQueueName(plToPsFifos.get(i))»_wait){
									AXIS_OUT.write(push_stream<int,4,5,5>(«getQueueName(plToPsFifos.get(i))».read().data, false));
									«getQueueName(plToPsFifos.get(i))»_wait_counter = PACKAGE_SIZE;
									«getQueueName(plToPsFifos.get(i))»_counter++;
									i++;
								} else {
									«getQueueName(plToPsFifos.get(i))»_wait_counter++;
									«getQueueName(plToPsFifos.get(i))»_wait = «getQueueName(plToPsFifos.get(i))»_wait_counter == «(packageSize/plToPsFifos.size) - 1» ? false : true;
								}
							} else {
						if ( i > 0«IF plToPsFifos.size > 1» && «FOR int j : i+1 .. plToPsFifos.size - 1 SEPARATOR " && "»!«getQueueName(plToPsFifos.get(i))».empty()«ENDFOR»«ENDIF» ){
							AXIS_OUT.write(push_stream<int,4,5,5>(0, false));
							i++;
							}
						}
						} else if(i == «IF i != 0»«(i + 1) * packageSize/plToPsFifos.size - 1»«ELSE»«packageSize/plToPsFifos.size - 1»«ENDIF»){
							AXIS_OUT.write(push_stream<int,4,5,5>(«getQueueName(plToPsFifos.get(i))»_counter, true));
							i++;
					}«ENDFOR»
				}
			
				return;
			}
		'''
	}

	def getQueueName(Connection connection) {
		'''fifo_«connection.getAttribute("id").objectValue»'''
	}
}
