/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.backend;

import static exelixi.hls.backend.Constants.AXI_STREAM_ACTOR_COMMUNICATION;
import static exelixi.hls.backend.Constants.BIT_ACCURATE_TYPES;
import static exelixi.hls.backend.Constants.CODE_GEN_PATH_HW;
import static exelixi.hls.backend.Constants.CODE_GEN_PATH_SW;
import static exelixi.hls.backend.Constants.DEVICE;
import static exelixi.hls.backend.Constants.EVERY_INSTANCE_AN_IP_CORE;
import static exelixi.hls.backend.Constants.INLINE_ACTIONS;
import static exelixi.hls.backend.Constants.PROFILE_DATAFLOW_NETWORK;
import static exelixi.hls.backend.Constants.PROJECTS_PATH;
import static exelixi.hls.backend.Constants.RTL_PATH;
import static exelixi.hls.backend.Constants.ZYNQ_OPTION_AXI_LITE_CONTROL;
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION;
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_COMMUNICATION_KIND_SINGLE_DMA_POLLING;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_FPGA_BOARD;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_LINKER_SCRIPT;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_LINKER_SCRIPT_HEAP_SIZE;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_LINKER_SCRIPT_STACK_SIZE;
import static exelixi.zynq.backend.Constants.ZYNQ_OPTION_V7_PROFILING;
import static net.sf.orcc.OrccLaunchConstants.MAPPING;
import static net.sf.orcc.OrccLaunchConstants.XDF_FILE;
import static net.sf.orcc.backends.BackendsConstants.BXDF_FILE;
import static net.sf.orcc.backends.BackendsConstants.IMPORT_BXDF;
import static net.sf.orcc.util.OrccUtil.getFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import exelixi.cpp.backend.templates.lib.ActorHeader;
import exelixi.cpp.backend.templates.lib.FifoHeader;
import exelixi.cpp.backend.templates.lib.FileReadHeader;
import exelixi.cpp.backend.transformations.ConnectionsThresholds;
import exelixi.hls.devices.XilinxDevice;
import exelixi.hls.partitioner.Partitioner;
import exelixi.hls.templates.types.TypePrinterVisitor;
import exelixi.hls.transformations.ActorAddFSM;
import exelixi.hls.transformations.ActorPeekComplexity;
import exelixi.hls.transformations.ConnectionLabeler;
import exelixi.hls.transformations.LoopLabeler;
import exelixi.hls.transformations.UniquePortMemory;
import exelixi.hls.transformations.VarInitializer;
import exelixi.hls.vivadohls.backend.fifo.FifoPrinter;
import exelixi.hls.vivadohls.backend.instance.VivadoHLSInstanceHeader;
import exelixi.hls.vivadohls.backend.scripts.FifoCoreTCLPrinter;
import exelixi.hls.vivadohls.backend.scripts.VivadoHLSProjectTCLPrinter;
import exelixi.hls.vivadohls.backend.testbench.TestbenchCPP;
import exelixi.hls.vivadohls.backend.transformation.ExternalArray;
import exelixi.hls.vivadohls.backend.transformation.InstanceIndentifier;
import exelixi.hls.vivadohls.backend.transformation.StateVariablesInGuards;
import exelixi.hls.vivadohls.backend.type.VivadoHLSTypePrinter;
import exelixi.zynq.backend.interconnect.InterconnectionType;
import exelixi.zynq.backend.templates.sdk.LinkerScript;
import exelixi.zynq.backend.templates.sdk.ScriptXSCT;
import exelixi.zynq.backend.templates.sdk.dma.DMAReceivePolling;
import exelixi.zynq.backend.templates.sdk.dma.DMATransmitPolling;
import exelixi.zynq.backend.templates.sdk.instance.ProfiledInstanceHeaderPrinter;
import exelixi.zynq.backend.templates.sdk.instance.ProfiledInstanceSourcePrinter;
import exelixi.zynq.backend.templates.sdk.instance.SDKInstanceHeaderPrinter;
import exelixi.zynq.backend.templates.sdk.instance.SDKInstanceSourcePrinter;
import exelixi.zynq.backend.templates.sdk.instance.TestbenchInstancePrinter;
import exelixi.zynq.backend.templates.sdk.lib.FileFsHeader;
import exelixi.zynq.backend.templates.sdk.lib.FileFsSource;
import exelixi.zynq.backend.templates.sdk.lib.FileLineReader;
import exelixi.zynq.backend.templates.sdk.lib.FileReadSource;
import exelixi.zynq.backend.templates.sdk.lib.FileWriteHeader;
import exelixi.zynq.backend.templates.sdk.lib.FileWriteSource;
import exelixi.zynq.backend.templates.sdk.lib.NativeHeader;
import exelixi.zynq.backend.templates.sdk.lib.NoDisplayHeader;
import exelixi.zynq.backend.templates.sdk.lib.NoDisplaySource;
import exelixi.zynq.backend.templates.sdk.llfifo.LLReceivePrinter;
import exelixi.zynq.backend.templates.sdk.llfifo.LLTransmitPrinter;
import exelixi.zynq.backend.templates.sdk.network.MainPrinter;
import exelixi.zynq.backend.templates.sdk.network.NetworkPrinter;
import exelixi.zynq.backend.templates.sdk.network.TestbenchMainPrinter;
import exelixi.zynq.backend.templates.sdk.plcore.PLCoreSourcePrinter;
import exelixi.zynq.backend.templates.sdk.plcore.PlCoreHeaderPrinter;
import exelixi.zynq.backend.templates.sdk.profiling.V7pmuHeader;
import exelixi.zynq.backend.templates.sdk.profiling.V7pmuSource;
import exelixi.zynq.backend.templates.sdk.wrappers.InputWrapper;
import exelixi.zynq.backend.templates.sdk.wrappers.InputWrapperImpl;
import exelixi.zynq.backend.templates.sdk.wrappers.OutputWrapper;
import exelixi.zynq.backend.templates.sdk.wrappers.OutputWrapperImpl;
import exelixi.zynq.backend.templates.vivado.VivadoProject;
import exelixi.zynq.backend.templates.vivado.VivadoTop;
import exelixi.zynq.backend.templates.vivadohls.fanout.FanoutCore;
import exelixi.zynq.backend.templates.vivadohls.instance.ZynqVivadoHLSInstance;
import exelixi.zynq.backend.templates.vivadohls.network.IPCoreNetwork;
import exelixi.zynq.backend.templates.vivadohls.network.TclCore;
import exelixi.zynq.backend.templates.vivadohls.utils.TemplateUtils;
import exelixi.zynq.backend.templates.vivadohls.wrapper.AxisInputWrapper;
import exelixi.zynq.backend.templates.vivadohls.wrapper.AxisOutputWrapper;
import exelixi.zynq.backend.templates.vivadohls.wrapper.Kicker;
import exelixi.zynq.backend.transformations.PartitionedConnectionsIds;
import exelixi.zynq.transformations.ConnectionReaders;
import exelixi.zynq.transformations.StateVariableRenamer;
import exelixi.zynq.transformations.UniqueInstance;
import net.sf.orcc.OrccRuntimeException;
import net.sf.orcc.backends.AbstractBackend;
import net.sf.orcc.backends.llvm.tta.transform.PrintRemoval;
import net.sf.orcc.backends.transform.DisconnectedOutputPortRemoval;
import net.sf.orcc.backends.util.Validator;
import net.sf.orcc.df.Connection;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.transform.Instantiator;
import net.sf.orcc.df.transform.NetworkFlattener;
import net.sf.orcc.df.transform.TypeResizer;
import net.sf.orcc.df.transform.UnitImporter;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.df.util.NetworkValidator;
import net.sf.orcc.ir.transform.RenameTransformation;
import net.sf.orcc.tools.mapping.XmlBufferSizeConfiguration;
import net.sf.orcc.util.FilesManager;
import net.sf.orcc.util.OrccLogger;
import net.sf.orcc.util.OrccUtil;
import net.sf.orcc.util.Result;
import net.sf.orcc.util.util.EcoreHelper;

/**
 * Zynq Co-Design Backend
 * 
 * @author Endri Bezati
 */
public class ZynqBackend extends AbstractBackend {

	/**
	 * Code Generation and data Paths
	 */

	private String psSrcPath;

	private String plSrcPath;

	private String plRtlPath;

	private String plSrcTbPath;

	private String psIncludePath;

	private String plIncludePath;

	private String psSrcTbPath;

	private String projectsPath;

	private String projectsTopPath;

	private String projectsInstancePath;

	private String dataPath;

	private String fifoTracePath;

	/**
	 * Partitioner
	 */

	private Partitioner partitioner;

	private InterconnectionType interrconnectionType;

	/**
	 * Replacement Map
	 */

	private Map<String, String> replacementMap;

	/**
	 * List of transformations to apply on each child for PS.
	 */
	protected List<DfVisitor<?>> childrenTransfosPS;
	/**
	 * List of transformations to apply on each child for PL.
	 */
	protected List<DfVisitor<?>> childrenTransfosPL;

	/**
	 * PS options
	 */

	private boolean v7Profiling;

	/**
	 * PL options
	 */

	private TypePrinterVisitor typePrinter;

	private Boolean everyInstanceIpCore;

	public ZynqBackend() {
		childrenTransfosPS = new ArrayList<DfVisitor<?>>();
		childrenTransfosPL = new ArrayList<DfVisitor<?>>();
	}

	@Override
	protected void beforeGeneration(Network network) {
		if (getOption(IMPORT_BXDF, false)) {
			File f = new File(getOption(BXDF_FILE, ""));
			new XmlBufferSizeConfiguration(true, false).load(f, network);
		}
	}

	@Override
	public void compile(IProgressMonitor progressMonitor) {

		// New ResourceSet for a new compilation
		currentResourceSet = new ResourceSetImpl();

		// Initialize the monitor. Can be used to stop the back-end
		// execution and provide feedback to user
		monitor = progressMonitor;

		// -- Get Network
		final Network network;
		final String networkQName = getOption(XDF_FILE, "");
		final IFile xdfFile = getFile(project, networkQName, OrccUtil.NETWORK_SUFFIX);
		if (xdfFile == null) {
			throw new OrccRuntimeException(
					"Unable to find the XDF file " + "corresponding to the network " + networkQName + ".");
		} else {
			network = EcoreHelper.getEObject(currentResourceSet, xdfFile);
		}

		// -- Initialize Options
		// doInitializeOptions();

		// -- Check if Mapping is not empty, otherwise throw an OrccExcpetion
		if (mapping.isEmpty()) {
			throw new OrccRuntimeException("Mapping is empty, please fill it!");
		} else {
			// -- Transform Network

			if (!getOption(BIT_ACCURATE_TYPES, true)) {
				(new TypeResizer(true, false, false, false)).doSwitch(network);
			}
			new Instantiator(true).doSwitch(network);
			new NetworkFlattener().doSwitch(network);
			new UniqueInstance().doSwitch(network);
			new InstanceIndentifier().doSwitch(network);
			new DisconnectedOutputPortRemoval().doSwitch(network);
			new ConnectionsThresholds().doSwitch(network);
			new ConnectionLabeler().doSwitch(network);
			partitioner = new Partitioner(network, mapping);
			new ConnectionReaders(partitioner).doSwitch(network);
			new UnitImporter().doSwitch(network);
			new StateVariableRenamer().doSwitch(network);
			new ExternalArray(getOptions()).doSwitch(network);
			new RenameTransformation(replacementMap).doSwitch(network);

			new ActorPeekComplexity().doSwitch(network);
			new PartitionedConnectionsIds(partitioner, getOptions()).doSwitch(network);

			String interconnectionOtion = getOption(ZYNQ_OPTION_COMMUNICATION_KIND, "AXI-Stream FIFO");
			interrconnectionType = new InterconnectionType(interconnectionOtion);

			// -- Call Before Generation
			beforeGeneration(network);
			doValidate(network);

			OrccLogger
					.traceln("*********************************************" + "************************************");
			OrccLogger.traceln("* Exelixi Co-Design for Zynq Platform");
			OrccLogger.traceln("* Project:        " + project.getName());
			String topNetwork = getOption(XDF_FILE, "<unknown>");
			OrccLogger.traceln("* Network:        " + topNetwork);
			OrccLogger.traceln("* Output folder:  " + outputPath);
			OrccLogger
					.traceln("*********************************************" + "************************************");

			// -- Code generation
			if (partitioner.containsPartition("PS")) {
				OrccLogger.traceln("Code generation for PS");
				OrccLogger.traceln("\t Generating PS Network");
				long t0 = System.currentTimeMillis();

				final Result result = doGenerateNetworkPS(network);
				result.merge(doAdditionalGenerationPS(network));
				OrccLogger.traceln("\t Generating PS Instances");
				applyTransformations(partitioner.getInstances("PS"), childrenTransfosPS, debug);

				for (Instance instance : partitioner.getInstances("PS")) {
					result.merge(doGenerateInstancePS(instance));
				}

				// -- Generate the Board if not PL partition is found
				if (!partitioner.containsPartition("PL")) {
					result.merge(doAdditionalGenerationPL(network));
				}

				OrccLogger.traceln("PS Code Generation done in " + getDuration(t0) + "s. " + result);
			} else {
				OrccLogger.traceln("Code generation for PS");
				OrccLogger.traceln("\t Generating PS Main");
				long t0 = System.currentTimeMillis();

				final Result result = doGenerateNetworkPS(network);
				result.merge(doAdditionalGenerationPS(network));

				OrccLogger.traceln("PS Code Generation done in " + getDuration(t0) + "s. " + result);
			}

			if (partitioner.containsPartition("PL")) {
				OrccLogger.traceln("Code generation for PL");

				// -- Network
				OrccLogger.traceln("\t Generating PL Network");
				long t1 = System.currentTimeMillis();
				final Result result = doGenerateNetworkPL(network);
				result.merge(doAdditionalGenerationPL(network));

				// -- Instances
				OrccLogger.traceln("\t Generating PL Instances");
				applyTransformations(partitioner.getInstances("PL"), childrenTransfosPL, debug);
				for (Instance instance : partitioner.getInstances("PL")) {
					result.merge(doGenerateInstancePL(instance));
				}
				OrccLogger.traceln("PL Code Genertion done in " + getDuration(t1) + "s. " + result);
			}

		}
		// -- Clear transformation List
		childrenTransfosPL.clear();
		childrenTransfosPS.clear();
		OrccLogger.traceln("Exelixi Zynq Code Generation done.");
	}

	protected Result doAdditionalGenerationPL(final Network network) {
		final Result result = Result.newInstance();
		VivadoProject vivadoProject = new VivadoProject(network, getOptions(), partitioner);
		result.merge(FilesManager.writeFile(vivadoProject.getContent(), projectsTopPath,
				network.getSimpleName() + "_pl_vivado.tcl"));

		VivadoTop vivadoTop = new VivadoTop(network, getOptions(), partitioner);
		result.merge(FilesManager.writeFile(vivadoTop.getContent(), plRtlPath, network.getSimpleName() + "_top.v"));

		return result;
	}

	protected Result doAdditionalGenerationPS(final Network network) {
		final Result result = Result.newInstance();
		// -- Actor header
		ActorHeader actorHeaderPrinter = new ActorHeader(getOptions());
		result.merge(FilesManager.writeFile(actorHeaderPrinter.getContent(), psIncludePath, "actor.h"));

		// -- Native libraries
		NativeHeader nativeHeaderPrinter = new NativeHeader();
		result.merge(FilesManager.writeFile(nativeHeaderPrinter.getContent(), psIncludePath, "native.h"));

		FileReadHeader fileReadHeaderPrinter = new FileReadHeader();
		result.merge(FilesManager.writeFile(fileReadHeaderPrinter.getContent(), psIncludePath, "file_read.h"));

		FileReadSource fileReadSourcePrinter = new FileReadSource();
		result.merge(FilesManager.writeFile(fileReadSourcePrinter.getContent(), psSrcPath, "file_read.cc"));

		FileFsHeader fileFsHeaderPrinter = new FileFsHeader();
		result.merge(FilesManager.writeFile(fileFsHeaderPrinter.getContent(), psIncludePath, "file_fs.h"));

		FileFsSource fileFsSourcePrinter = new FileFsSource();
		result.merge(FilesManager.writeFile(fileFsSourcePrinter.getContent(), psSrcPath, "file_fs.cc"));

		FileWriteHeader fileWriteHeaderPrinter = new FileWriteHeader();
		result.merge(FilesManager.writeFile(fileWriteHeaderPrinter.getContent(), psIncludePath, "file_write.h"));

		FileWriteSource fileWriteSourcePrinter = new FileWriteSource();
		result.merge(FilesManager.writeFile(fileWriteSourcePrinter.getContent(), psSrcPath, "file_write.cc"));

		NoDisplayHeader noDisplayHeader = new NoDisplayHeader(network);
		result.merge(FilesManager.writeFile(noDisplayHeader.getContent(), psIncludePath, "no_display.h"));

		NoDisplaySource noDisplaySource = new NoDisplaySource(network);
		result.merge(FilesManager.writeFile(noDisplaySource.getContent(), psSrcPath, "no_display.cc"));

		// -- Profiling files
		if (v7Profiling) {
			V7pmuHeader pmuHeaderWriter = new V7pmuHeader();
			result.merge(FilesManager.writeFile(pmuHeaderWriter.getContent(), psIncludePath, "v7_pmu.h"));
			V7pmuSource pmuSourceWriter = new V7pmuSource();
			result.merge(FilesManager.writeFile(pmuSourceWriter.getContent(), psSrcPath, "v7_pmu.s"));
		}

		// -- Testbench source files
		FileLineReader fileLineReader = new FileLineReader();
		result.merge(FilesManager.writeFile(fileLineReader.getContent(), psSrcTbPath, "file_line_reader.h"));

		// -- Fifo header
		FifoHeader fifoHeaderPrinter = new FifoHeader();
		result.merge(FilesManager.writeFile(fifoHeaderPrinter.getContent(), psIncludePath, "fifo.h"));

		if (partitioner.containsPartition("PL")) {
			// -- PL Core Header
			PlCoreHeaderPrinter plHeaderPrinter = new PlCoreHeaderPrinter(network, partitioner, getOptions());
			result.merge(FilesManager.writeFile(plHeaderPrinter.getContent(), psIncludePath,
					network.getSimpleName() + "_pl.h"));

			// -- PL Core Source
			PLCoreSourcePrinter plSourcePriner = new PLCoreSourcePrinter(network, partitioner, getOptions());
			result.merge(
					FilesManager.writeFile(plSourcePriner.getContent(), psSrcPath, network.getSimpleName() + "_pl.cc"));
		}
		// -- XSCT SDK Script
		ScriptXSCT scriptXSCT = new ScriptXSCT(network, getOptions());
		result.merge(FilesManager.writeFile(scriptXSCT.getContent(), projectsTopPath,
				network.getSimpleName() + "_ps_xsct.tcl"));

		// -- Linker Script
		LinkerScript linkerScript = new LinkerScript(getOptions());
		result.merge(FilesManager.writeFile(linkerScript.getContent(), psSrcPath, "lscript.ld"));

		return result;
	}

	protected Result doGenerateInstancePL(final Instance instance) {
		final Result result = Result.newInstance();

		// -- Create a directory for Vivado HLS TCL Script
		String name = instance.getSimpleName();
		String projectInstancePath = projectsInstancePath + File.separator + name;
		createDirectory(projectInstancePath);

		// -- Create the Vivado HLS TCL script
		VivadoHLSProjectTCLPrinter projectPrinter = new VivadoHLSProjectTCLPrinter(instance, getOptions());
		result.merge(FilesManager.writeFile(projectPrinter.getContent(), projectInstancePath,
				instance.getSimpleName() + "_hls.tcl"));

		// -- Generate Instance Header file
		VivadoHLSInstanceHeader headerPrinter = new VivadoHLSInstanceHeader(instance, typePrinter, partitioner,
				getOptions());
		result.merge(FilesManager.writeFile(headerPrinter.getContent(), plIncludePath, name + ".h"));

		// -- Generate Instance C++ file
		ZynqVivadoHLSInstance instancePrinter = new ZynqVivadoHLSInstance(instance, typePrinter, getOptions(),
				partitioner);
		result.merge(FilesManager.writeFile(instancePrinter.getContent(), plSrcPath, name + ".cpp"));

		// -- Testbench CPP
		TestbenchCPP testbenchPrinter = new TestbenchCPP(instance, typePrinter, getOptions());
		result.merge(FilesManager.writeFile(testbenchPrinter.getContent(), plSrcTbPath,
				instance.getSimpleName() + "_tb.cpp"));

		return result;
	}

	protected Result doGenerateInstancePS(final Instance instance) {
		final Result result = Result.newInstance();

		// -- Instance source and include files generation
		SDKInstanceHeaderPrinter instancePrinter = v7Profiling
				? new ProfiledInstanceHeaderPrinter(partitioner, instance, getOptions())
				: new SDKInstanceHeaderPrinter(partitioner, instance, getOptions());
		instancePrinter.setOptions(getOptions());
		result.merge(
				FilesManager.writeFile(instancePrinter.getContent(), psIncludePath, instance.getSimpleName() + ".h"));

		SDKInstanceSourcePrinter instanceSourcePrinter = v7Profiling
				? new ProfiledInstanceSourcePrinter(partitioner, instance, getOptions())
				: new SDKInstanceSourcePrinter(partitioner, instance, getOptions());
		instanceSourcePrinter.setOptions(getOptions());
		result.merge(FilesManager.writeFile(instanceSourcePrinter.getContent(), psSrcPath,
				instance.getSimpleName() + ".cpp"));

		// -- Testbench source file generation
		TestbenchInstancePrinter tbInstancePrinter = new TestbenchInstancePrinter(instance);
		result.merge(FilesManager.writeFile(tbInstancePrinter.getContent(), psSrcTbPath,
				instance.getSimpleName() + "_tb.h"));
		return result;
	}

	protected Result doGenerateNetworkPL(Network network) {
		final Result result = Result.newInstance();

		// List<Connection> plToPsFifos =
		// partitioner.getConnections(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION);
		// List<Connection> psToPlFifos =
		// partitioner.getConnections(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION);

		if (!everyInstanceIpCore) {
			if (interrconnectionType.isLLFIFO()) {
				// -- LLFifo Network
				IPCoreNetwork networkCPP = new IPCoreNetwork(network, typePrinter, getOptions(), partitioner);
				result.merge(FilesManager.writeFile(networkCPP.getContent(), plSrcPath,
						network.getSimpleName() + "_pl.cpp"));
			} else if (interrconnectionType.isDMA()) {
				// -- To be done
			}
		} else {
			// -- Kicker
			Kicker kickerPrinter = new Kicker(typePrinter);
			result.merge(FilesManager.writeFile(kickerPrinter.getContent(), plSrcPath, "kicker.cpp"));

			// -- Exelixi Fifo
			FifoPrinter fifoPrinter = new FifoPrinter();
			result.merge(FilesManager.writeFile(fifoPrinter.getContent(), plRtlPath, "fifo.v"));

			FifoCoreTCLPrinter fifoCoreTCLPrinter = new FifoCoreTCLPrinter(getOptions(), plRtlPath);
			result.merge(
					FilesManager.writeFile(fifoCoreTCLPrinter.getContent(), projectsTopPath, "exelixi_fifo_core.tcl"));

			// -- Axis Wrappers
			List<Connection> coreInputs = new ArrayList<Connection>(
					partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION).keySet());
			List<Connection> coreOutputs = new ArrayList<Connection>(
					partitioner.getConnectionToConnectionBd(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION).keySet());

			if (interrconnectionType.isDMA()) {
				for (Connection connection : coreInputs) {
					AxisInputWrapper inputWrapper = new AxisInputWrapper(typePrinter, connection);
					result.merge(
							FilesManager.writeFile(inputWrapper.getContent(), plSrcPath, inputWrapper.getFileName()));
				}

				for (Connection connection : coreOutputs) {
					AxisOutputWrapper outputWrapper = new AxisOutputWrapper(typePrinter, connection);
					result.merge(
							FilesManager.writeFile(outputWrapper.getContent(), plSrcPath, outputWrapper.getFileName()));
				}
			} else if (interrconnectionType.isSignleDMA()) {
				if (!coreInputs.isEmpty()) {
					AxisInputWrapper inputWrapper = new AxisInputWrapper(typePrinter, coreInputs, true);
					result.merge(FilesManager.writeFile(inputWrapper.getContent(), plSrcPath, "AxisInput_Wrapper.cpp"));
				}
				if (!coreOutputs.isEmpty()) {
					AxisOutputWrapper outputWrapper = new AxisOutputWrapper(typePrinter, coreOutputs, true);
					result.merge(
							FilesManager.writeFile(outputWrapper.getContent(), plSrcPath, "AxisOutput_Wrapper.cpp"));
				}

			}

			// -- Fanout
			final Map<Connection, List<Connection>> bcConnections = partitioner
					.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION);
			for (Connection connection : bcConnections.keySet()) {
				if (bcConnections.get(connection).size() > 1) {
					String name = "fanout_" + TemplateUtils.getQueueNameIO(connection);
					FanoutCore fanoutCore = new FanoutCore(typePrinter, connection, bcConnections.get(connection));
					result.merge(FilesManager.writeFile(fanoutCore.getContent(), plSrcPath, name + ".cpp"));
				}
			}
		}

		// -- Vivado HLS Project TCL

		TclCore hlsTcl = new TclCore(network, getOptions(), partitioner);
		result.merge(
				FilesManager.writeFile(hlsTcl.getContent(), projectsTopPath, network.getSimpleName() + "_pl_hls.tcl"));

		return result;
	}

	protected Result doGenerateNetworkPS(Network network) {
		final Result result = Result.newInstance();

		// -- Main file
		MainPrinter mainPrinter = new MainPrinter(network, partitioner, getOptions());
		mainPrinter.setOptions(getOptions());
		result.merge(FilesManager.writeFile(mainPrinter.getContent(), psSrcPath, "main.cc"));

		if (partitioner.containsPartition("PS")) {
			// -- Network Class
			NetworkPrinter printer = new NetworkPrinter(network, partitioner);
			printer.setOptions(getOptions());
			result.merge(
					FilesManager.writeFile(printer.getContent(), psIncludePath, network.getSimpleName() + "_ps.h"));

			// -- Testbenh Class
			TestbenchMainPrinter tbMainPrinter = new TestbenchMainPrinter(network, partitioner);
			result.merge(FilesManager.writeFile(tbMainPrinter.getContent(), psSrcTbPath, "main.cc"));
		}

		if (partitioner.containsPartition("PL")) {
			List<Connection> plToPsFifos = new ArrayList<Connection>(
					partitioner.getConnectionToConnectionBd(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION).keySet());
			List<Connection> psToPlFifos = new ArrayList<Connection>(
					partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION).keySet());

			boolean containsLlFifoTransmit = false;
			boolean containsLlFifoReceive = false;

			boolean containsDmaTransmit = false;
			boolean containsDmaReceive = false;

			for (Connection connection : psToPlFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					containsLlFifoTransmit = true;
				} else if (TemplateUtils.getConnectionKind(connection)
						.equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)
						|| TemplateUtils.getConnectionKind(connection)
								.equals(ZYNQ_OPTION_COMMUNICATION_KIND_SINGLE_DMA_POLLING)) {
					containsDmaTransmit = true;
				}
			}

			for (Connection connection : plToPsFifos) {
				if (TemplateUtils.getConnectionKind(connection).equals(ZYNQ_OPTION_COMMUNICATION_KIND_LLFIFO)) {
					containsLlFifoReceive = true;
				} else if (TemplateUtils.getConnectionKind(connection)
						.equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)
						|| TemplateUtils.getConnectionKind(connection)
								.equals(ZYNQ_OPTION_COMMUNICATION_KIND_SINGLE_DMA_POLLING)) {
					containsDmaReceive = true;
				}
			}

			if (containsLlFifoTransmit) {
				// -- LLFifo PS to PL Transmit
				LLTransmitPrinter llTransmitPrinter = new LLTransmitPrinter();
				result.merge(
						FilesManager.writeFile(llTransmitPrinter.getContent(), psIncludePath, "llfifo_transmit.h"));
			}

			if (containsLlFifoReceive) {
				// -- LLFifo PL to PS Receive
				LLReceivePrinter llReceivePrinter = new LLReceivePrinter();
				result.merge(FilesManager.writeFile(llReceivePrinter.getContent(), psIncludePath, "llfifo_receive.h"));
			}

			if (containsDmaTransmit) {
				// -- DMA Transmit
				DMATransmitPolling dmaTransmitPrinter = new DMATransmitPolling(interrconnectionType);
				result.merge(FilesManager.writeFile(dmaTransmitPrinter.getContent(), psIncludePath, "dma_transmit.h"));
				InputWrapper inputWrapperPrinter = new InputWrapper(interrconnectionType);
				result.merge(
						FilesManager.writeFile(inputWrapperPrinter.getContent(), psIncludePath, "input_wrapper.h"));

				if (interrconnectionType.isSignleDMA()) {
					InputWrapperImpl inputWrapperImpl = new InputWrapperImpl(interrconnectionType);
					result.merge(FilesManager.writeFile(inputWrapperImpl.getContent(), psIncludePath,
							"single_input_wrapper.h"));
				} else {
					for (Connection connection : psToPlFifos) {
						if (TemplateUtils.getConnectionKind(connection)
								.equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
							InputWrapperImpl inputWrapperImpl = new InputWrapperImpl(interrconnectionType, connection);
							result.merge(FilesManager.writeFile(inputWrapperImpl.getContent(), psIncludePath,
									"input_wrapper_" + TemplateUtils.getQueueNameIO(connection) + ".h"));
						}
					}
				}
			}

			if (containsDmaReceive) {
				// -- DMA Receive
				DMAReceivePolling dmaReceivePrinter = new DMAReceivePolling(interrconnectionType);
				result.merge(FilesManager.writeFile(dmaReceivePrinter.getContent(), psIncludePath, "dma_receive.h"));
				OutputWrapper outputWrapperPrinter = new OutputWrapper(interrconnectionType);
				result.merge(
						FilesManager.writeFile(outputWrapperPrinter.getContent(), psIncludePath, "output_wrapper.h"));

				if (interrconnectionType.isSignleDMA()) {
					OutputWrapperImpl outputWrapperImpl = new OutputWrapperImpl(interrconnectionType);
					result.merge(FilesManager.writeFile(outputWrapperImpl.getContent(), psIncludePath,
							"single_output_wrapper.h"));
				} else {
					for (Connection connection : plToPsFifos) {
						if (TemplateUtils.getConnectionKind(connection)
								.equals(ZYNQ_OPTION_COMMUNICATION_KIND_SIMPLE_DMA_POLLING)) {
							OutputWrapperImpl outputWrapperImpl = new OutputWrapperImpl(interrconnectionType,
									connection);
							result.merge(FilesManager.writeFile(outputWrapperImpl.getContent(), psIncludePath,
									"output_wrapper_" + TemplateUtils.getQueueNameIO(connection) + ".h"));
						}
					}
				}
			}

		}

		return Result.newInstance();
	}

	@Override
	protected void doValidate(Network network) {
		Validator.checkMinimalFifoSize(network, fifoSize);

		new NetworkValidator().doSwitch(network);
		network.computeTemplateMaps();
	}

	@Override
	protected void doInitializeOptions() {
		childrenTransfosPL.clear();
		childrenTransfosPS.clear();
		// -- Create Folders
		createDirectories();
		// -- Options
		String name = getOption(ZYNQ_OPTION_FPGA_BOARD, "ZC702 - Zynq 7");
		XilinxDevice xilinxDevice = new XilinxDevice(name);
		getOptions().put(DEVICE, xilinxDevice);

		// -- Code Gen Path option
		getOptions().put(CODE_GEN_PATH_HW, "../../code-gen/pl");
		getOptions().put(CODE_GEN_PATH_SW, "../../code-gen/ps");
		getOptions().put(PROJECTS_PATH, projectsTopPath);

		// -- Get Mapping
		mapping = getOption(MAPPING, new HashMap<String, String>());

		// -- Fill replacement map
		replacementMap = new HashMap<String, String>();
		replacementMap.put("abs", "abs_");
		replacementMap.put("getw", "getw_");
		replacementMap.put("index", "index_");
		replacementMap.put("max", "max_");
		replacementMap.put("min", "min_");
		replacementMap.put("select", "select_");
		replacementMap.put("bitand", "bitand_");
		replacementMap.put("bitor", "bitor_");
		replacementMap.put("not", "not_");
		replacementMap.put("and", "and_");
		replacementMap.put("OUT", "OUT_");
		replacementMap.put("IN", "IN_");
		replacementMap.put("DEBUG", "DEBUG_");
		replacementMap.put("INT_MIN", "INT_MIN_");
		replacementMap.put("INT_MAX", "INT_MAX_");
		replacementMap.put("SHORT_MIN", "SHORT_MIN_");
		replacementMap.put("SHORT_MAX", "SHORT_MAX_");

		// -- Instance Transformation for PS
		childrenTransfosPS.add(new VarInitializer());
		childrenTransfosPS.add(new TypeResizer(true, false, false, false));
		childrenTransfosPS.add(new RenameTransformation(replacementMap));

		// -- PS Initialize Options
		v7Profiling = getOption(ZYNQ_OPTION_V7_PROFILING, false);
		getOptions().put(ZYNQ_OPTION_LINKER_SCRIPT, getOption(ZYNQ_OPTION_LINKER_SCRIPT, false));
		getOptions().put(ZYNQ_OPTION_LINKER_SCRIPT_STACK_SIZE, getOption(ZYNQ_OPTION_LINKER_SCRIPT_STACK_SIZE, "100"));
		getOptions().put(ZYNQ_OPTION_LINKER_SCRIPT_HEAP_SIZE, getOption(ZYNQ_OPTION_LINKER_SCRIPT_HEAP_SIZE, "100"));

		// -- PL Initialize Options
		getOptions().put(ZYNQ_OPTION_AXI_LITE_CONTROL, getOption(ZYNQ_OPTION_AXI_LITE_CONTROL, true));
		getOptions().put(AXI_STREAM_ACTOR_COMMUNICATION, getOption(AXI_STREAM_ACTOR_COMMUNICATION, true));
		everyInstanceIpCore = getOption(EVERY_INSTANCE_AN_IP_CORE, true);
		getOptions().put(EVERY_INSTANCE_AN_IP_CORE, getOption(EVERY_INSTANCE_AN_IP_CORE, true));
		getOptions().put(BIT_ACCURATE_TYPES, getOption(BIT_ACCURATE_TYPES, false));
		getOptions().put(INLINE_ACTIONS, getOption(INLINE_ACTIONS, true));
		getOptions().put(PROFILE_DATAFLOW_NETWORK, false);
		getOptions().put(RTL_PATH, plRtlPath);

		// -- Instance Transformation for PL
		childrenTransfosPL.add(new RenameTransformation(replacementMap));
		childrenTransfosPL.add(new StateVariablesInGuards());

		childrenTransfosPL.add(new UniquePortMemory(fifoSize));

		childrenTransfosPL.add(new ActorAddFSM());
		childrenTransfosPL.add(new VarInitializer());
		childrenTransfosPL.add(new LoopLabeler());
		childrenTransfosPL.add(new PrintRemoval());

		// -- Vivado HLS Type Printer
		typePrinter = new VivadoHLSTypePrinter(getOptions());
	}

	private void createDirectories() {
		// -- Code Generation Directory
		String codeGen = outputPath + File.separator + "code-gen";
		createDirectory(codeGen);

		// -- Code Generation PS Directories
		String psCodeGen = codeGen + File.separator + "ps";
		createDirectory(psCodeGen);

		psSrcPath = psCodeGen + File.separator + "src";
		createDirectory(psSrcPath);

		psSrcTbPath = psCodeGen + File.separator + "src-tb";
		createDirectory(psSrcTbPath);

		psIncludePath = psCodeGen + File.separator + "include";
		createDirectory(psIncludePath);

		// -- Code Generation PL Directories
		String plCodeGen = codeGen + File.separator + "pl";
		createDirectory(plCodeGen);

		plSrcPath = plCodeGen + File.separator + "src";
		createDirectory(plSrcPath);

		plRtlPath = plCodeGen + File.separator + "rtl";
		createDirectory(plRtlPath);

		plSrcTbPath = plCodeGen + File.separator + "src-tb";
		createDirectory(plSrcTbPath);

		plIncludePath = plCodeGen + File.separator + "include";
		createDirectory(plIncludePath);

		// -- Projects Directories
		projectsPath = outputPath + File.separator + "projects";
		createDirectory(projectsPath);

		projectsTopPath = projectsPath + File.separator + "top";
		createDirectory(projectsTopPath);

		String ipCoresPath = projectsTopPath + File.separator + "ip_cores";
		createDirectory(ipCoresPath);

		projectsInstancePath = projectsPath + File.separator + "instances";
		createDirectory(projectsInstancePath);

		// -- Data Directory
		dataPath = outputPath + File.separator + "data";
		createDirectory(dataPath);

		fifoTracePath = dataPath + File.separator + "fifo-traces";
		createDirectory(fifoTracePath);

	}

	private void createDirectory(String pathName) {
		File dir = new File(pathName);
		if (!dir.exists()) {
			dir.mkdir();
		}
	}

}
