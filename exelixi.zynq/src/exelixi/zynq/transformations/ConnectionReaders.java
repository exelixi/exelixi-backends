/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.zynq.transformations;

import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION;

import java.util.List;
import java.util.Map;

import exelixi.hls.partitioner.Partitioner;
import net.sf.orcc.df.Connection;
import net.sf.orcc.df.Entity;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.graph.Edge;
import net.sf.orcc.graph.Vertex;

/**
 * 
 * @author Endri Bezati
 */
public class ConnectionReaders extends DfVisitor<Void> {
	
	private Partitioner partitioner;
	

	public ConnectionReaders(Partitioner partitioner){
		this.partitioner = partitioner;
	}
	
	@Override
	public Void caseNetwork(Network network) {

		// -- Actor Output Ports
		for (Vertex vertex : network.getChildren()) {
			Entity entity = vertex.getAdapter(Entity.class);
			Map<Port, List<Connection>> map = entity.getOutgoingPortMap();
			for (List<Connection> connections : map.values()) {
				for (Connection connection : connections) {
					connection.setAttribute("nbReaders", connections.size());

					Vertex src = connection.getSource();
					Port srcPort = connection.getSourcePort();
					Vertex tgt = connection.getTarget();
					Vertex tgtPort = connection.getTargetPort();

					String fifoName = "";

					if (srcPort != null && tgtPort != null) {
						fifoName = src.getLabel() + "_" + srcPort.getLabel() + "_" + tgt.getLabel() + "_"
								+ tgtPort.getLabel();
					} else if (srcPort != null && tgtPort == null) {
						fifoName = src.getLabel() + "_" + srcPort.getLabel() + "_" + tgt.getLabel();
					} else if (srcPort == null && tgtPort != null) {
						fifoName = src.getLabel() + "_" + tgt.getLabel() + "_" + tgtPort.getLabel();
					}
					connection.setAttribute("fifoName", fifoName);
				}
			}
		}

		// -- Network Input Ports
		for (Port port : network.getInputs()) {
			port.setAttribute("nbReaders", port.getOutgoing().size());
			int j = 0;
			int bc = 0;
			for (Edge edge : port.getOutgoing()) {
				edge.setAttribute("nbReaders", port.getOutgoing().size());
				if (edge instanceof Connection) {
					edge.setAttribute("fifoBcId", bc);
					Connection connection = (Connection) edge;
					Vertex src = connection.getSource();
					Vertex tgt = connection.getTarget();
					Vertex tgtPort = connection.getTargetPort();
					String fifoName = src.getLabel() + "_" + tgt.getLabel() + "_" + tgtPort.getLabel();
					connection.setAttribute("fifoName", fifoName);
					edge.setAttribute("fifoId", j);
					if (partitioner.isInstanceOnPartition((Instance) tgt, ZYNQ_PS_PARTITION)){
						j++;	
					}
				}
				bc++;
			}
		}

		return null;
	}

}
