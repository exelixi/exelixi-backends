/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.cpp.profiling.common.template.network

import exelixi.cpp.backend.templates.network.CPP11Main
import java.io.File
import java.util.Map
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.util.OrccUtil

import static exelixi.cpp.profiling.ProfilingConstants.*

/**
 * 
 * @author Endri Bezati
 */
class ProfiledNetwork extends CPP11Main {
	
	private String path;
	private boolean storeFirings;
	private boolean useFilter;

	new(Network network, Map<String, Object> options) {
		super(network, options)

		path = options.get(PROFILING_RESULTS_PATH) as String;
		if (!path.endsWith(File.separator)) {
			path = path + File.separator;
		}
		
		storeFirings = options.getOrDefault(PROFILING_FIRINGS_CYCLES, false) as Boolean;
		useFilter = options.getOrDefault(PROFILING_FIRINGS_CYCLES_FILTER, true) as Boolean;
	}

	override instantiateActorsFifos() {
		'''
			«super.instantiateActorsFifos»
					// -- CONNECT PROFILING DATA
					«FOR instance : network.children.filter(typeof(Instance))»
						act_«instance.simpleName»->profilingData = profilingData;
					«ENDFOR»
		'''
	}

	override def terminate() {
		'''
			«super.terminate» 
			profilingData->generate_results("«path»", «storeFirings», «useFilter»);
		'''
	}

	override getActorsAndFifos() {
		'''
			«super.actorsAndFifos»
			// -- PROFILING DATA
			// create the actor actions map
			ProfilingData* profilingData = new ProfilingData("«OrccUtil.getQualifiedName(network.getFile())»");
			
		'''
	}

	override def delete() {
		'''
			«super.delete»
			delete profilingData;
		'''
	}

}
