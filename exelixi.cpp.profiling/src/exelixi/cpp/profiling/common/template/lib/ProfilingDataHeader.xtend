/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Simone Casale Brunet
 * web   : http://gramm.epfl.ch
 * email : simone.casalebrunet@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.cpp.profiling.common.template.lib

/**
 * 
 * @author Simone Casale Brunet
 */
class ProfilingDataHeader {

	def getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _ 
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 ******************************************************************************/
			
			#ifndef __EXELIXI_SW_PROFILING_DATA_H__
			#define __EXELIXI_SW_PROFILING_DATA_H__
			
			#include <stdlib.h>
			#include <inttypes.h>
			#include <stdint.h>
			#include <sys/types.h>
			#include <sys/stat.h>
			#include <vector>
			#include <map>
			#include "math.h"
			#include "tinyxml2.h"
			
			using namespace std;
			using namespace tinyxml2;
			
			class ProfilingData {
			public:
			
				ProfilingData(string networkName) {
					this->networkName = networkName;
				}
			
				void addFiring(string actor, string action,
						unsigned int cycles) {
					cyclesMap[actor][action].push_back(cycles);
				}
			
				void addScheduling(string actor, string lastAction, string selectedAction, unsigned int cycles){
					schedulerCyclesMap[actor][lastAction][selectedAction].push_back(cycles);
				}
			
				void generate_results(const char* path, bool storeFiringsCycles,
						bool filter) {
					if (storeFiringsCycles) {
						string res_path = path;
						res_path.append("firings/");
						mkdir(res_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
						for (auto const &ent1 : cyclesMap) {
							string actor = ent1.first;
							for (auto const &ent2 : ent1.second) {
								string action = ent2.first;
								vector<unsigned int> cycles = ent2.second;
								string tpath = res_path;
								const char* fileName = tpath.append(actor).append("-").append(
										action).append(".csv").c_str();
								FILE* file = fopen(fileName, "w");
								for (int i = 0; i < cycles.size(); i++) {
									fprintf(file, "%u\n", cycles.at(i));
								}
								fclose(file);
							}
						}
					}
			
					// create XML file
					string tpath = path;
					FILE* file = fopen(tpath.append("weights.sxdf").c_str(), "w");
					XMLPrinter sprinter(file);
					sprinter.OpenElement("network");
					sprinter.PushAttribute("name", networkName.c_str());
			
					for (auto const &ent1 : schedulerCyclesMap) {
						// string actor = ent1.first;
						sprinter.OpenElement("actor");
						sprinter.PushAttribute("id", ent1.first.c_str());
						for (auto const &ent2 : ent1.second) {
							//string lastAction = ent2.first;
							for (auto const &ent3 : ent2.second) {
								//string action = ent3.first;
			
								sprinter.OpenElement("scheduling");
								sprinter.PushAttribute("source", ent2.first.c_str());
								sprinter.PushAttribute("target", ent3.first.c_str());
			
								vector<unsigned int> cycles = ent3.second;
								int size = cycles.size();
			
								bool use_filter = filter && size > 3;
			
								double mean = 0.0;
								double var = 0;
								unsigned long sum = 0;
								unsigned int min = 0;
								unsigned int max = 0;
								int discarded = 0;
			
								if (use_filter) {
									// get statistics
									statistics(cycles, &mean, &var);
			
									// filter the data
									// compute the mean
									double long _sum = 0;
									double _threshold = mean + 2 * sqrt(var);
									int _filtered_size = 0;
			
									min = UINT_MAX;
									max = 0;
									for (int i = 0; i < size; i++) {
										unsigned int v = cycles.at(i);
										if (v <= _threshold) {
											if (v > max) {
												max = v;
											}
											if (v < min) {
												min = v;
											}
											_sum += v;
											_filtered_size++;
										}
									}
			
									mean = _sum / (double) _filtered_size;
									discarded = size - _filtered_size;
			
									// now compute the variance
									if (_filtered_size > 1) {
										long double _sum2 = 0.0;
										for (int i = 0; i < size; i++) {
											unsigned int v = cycles.at(i);
											if (v <= _threshold) {
												_sum2 += sqr(mean - v);
											}
										}
										var = _sum2 / (_filtered_size - 1);
									} else {
										var = 0;
										if (_filtered_size == 0) {
											min = max = 0;
											mean = 0.0;
										}
									}
								} else if (size > 0) {
									min = UINT_MAX;
									max = 0;
									sum = 0;
									for (int i = 0; i < size; i++) {
										unsigned int v = cycles.at(i);
										sum += v;
										if (v > max) {
											max = v;
										}
										if (v < min) {
											min = v;
										}
									}
			
									mean = sum / (double) size;
			
									// compute the variance
									if (size > 1) {
										long double _sum2 = 0.0;
										for (int i = 0; i < size; i++) {
											_sum2 += sqr(mean - cycles.at(i));
										}
										var = _sum2 / (size - 1);
									}
								}
			
								sprinter.PushAttribute("clockcycles", format("%f", mean).c_str());
								sprinter.PushAttribute("clockcycles-min", format("%u", min).c_str());
								sprinter.PushAttribute("clockcycles-max", format("%u", max).c_str());
								sprinter.PushAttribute("clockcycles-var", format("%f", var).c_str());
			
								sprinter.CloseElement();
							}
						}
						sprinter.CloseElement();
			
					}
			
					sprinter.CloseElement();
					fclose(file);
			
					// create XML file
					tpath = path;
					file = fopen(tpath.append("weights.exdf").c_str(), "w");
			
					XMLPrinter printer(file);
					printer.OpenElement("network");
					printer.PushAttribute("name", networkName.c_str());
			
					for (auto const &ent1 : cyclesMap) {
						printer.OpenElement("actor");
						printer.PushAttribute("id", ent1.first.c_str());
						for (auto const &ent2 : ent1.second) {
							printer.OpenElement("action");
							printer.PushAttribute("id", ent2.first.c_str());
							vector<unsigned int> cycles = ent2.second;
							int size = cycles.size();
			
							bool use_filter = filter && size > 3;
			
							double mean = 0.0;
							double var = 0;
							unsigned long sum = 0;
							unsigned int min = 0;
							unsigned int max = 0;
							int discarded = 0;
			
							if (use_filter) {
								// get statistics
								statistics(cycles, &mean, &var);
			
								// filter the data
								// compute the mean
								double long _sum = 0;
								double _threshold = mean + 2 * sqrt(var);
								int _filtered_size = 0;
			
								min = UINT_MAX;
								max = 0;
								for (int i = 0; i < size; i++) {
									unsigned int v = cycles.at(i);
									if (v <= _threshold) {
										if (v > max) {
											max = v;
										}
										if (v < min) {
											min = v;
										}
										_sum += v;
										_filtered_size++;
									}
								}
			
								mean = _sum / (double) _filtered_size;
								discarded = size - _filtered_size;
			
								// now compute the variance
								if (_filtered_size > 1) {
									long double _sum2 = 0.0;
									for (int i = 0; i < size; i++) {
										unsigned int v = cycles.at(i);
										if (v <= _threshold) {
											_sum2 += sqr(mean - v);
										}
									}
									var = _sum2 / (_filtered_size - 1);
								} else {
									var = 0;
									if (_filtered_size == 0) {
										min = max = 0;
										mean = 0.0;
									}
								}
							} else if (size > 0) {
								min = UINT_MAX;
								max = 0;
								sum = 0;
								for (int i = 0; i < size; i++) {
									unsigned int v = cycles.at(i);
									sum += v;
									if (v > max) {
										max = v;
									}
									if (v < min) {
										min = v;
									}
								}
			
								mean = sum / (double) size;
			
								// compute the variance
								if (size > 1) {
									long double _sum2 = 0.0;
									for (int i = 0; i < size; i++) {
										_sum2 += sqr(mean - cycles.at(i));
									}
									var = _sum2 / (size - 1);
								}
			
							}
			
							printer.PushAttribute("clockcycles", format("%f", mean).c_str());
							printer.PushAttribute("clockcycles-min", format("%u", min).c_str());
							printer.PushAttribute("clockcycles-max", format("%u", max).c_str());
							printer.PushAttribute("clockcycles-var", format("%f", var).c_str());
							//printer.PushAttribute("firings", format("%d", size).c_str());
							//printer.PushAttribute("discarded", format("%d", discarded).c_str());
			
							printer.CloseElement();
			
						}
						printer.CloseElement();
					}
					printer.CloseElement();
					fclose(file);
			
				}
			
			private:
			
				string networkName;
				map<string, map<string, vector<unsigned int>>> cyclesMap;
				map<string, map<string, map<string, vector<unsigned int>>>> schedulerCyclesMap;
			
				template <typename... Ts>
				static string format(const std::string &fmt, Ts... vs) {
					char b;
					unsigned required = std::snprintf(&b, 0, fmt.c_str(), vs...) + 1;
					char bytes[required];
					std::snprintf(bytes, required, fmt.c_str(), vs...);
			
					return std::string(bytes);
				}
			
				inline static double sqr(double x) {
					return x*x;
				}
			
				int statistics(vector<unsigned int> y, double* mean, double* var)
				{
					long double _sum = 0.0;
					double _mean = 0.0;
			
					int n = y.size();
			
					if(n < 2) {
						return 1;
					}
			
					// average
					for (int i=0;i<n;i++)
					{
						_sum += y.at(i);
					}
					*mean = _sum / (double) n;
			
					// variance
					long double _sum2 = 0.0;
					for(int i = 0; i <n; i++) {
						_sum2 += sqr(*mean - y.at(i));
					}
			
					*var = _sum2/(n-1);
			
					return 0;
				}
			};
			
			
			#endif // __EXELIXI_SW_PROFILING_DATA_H__

		'''
	}
}
