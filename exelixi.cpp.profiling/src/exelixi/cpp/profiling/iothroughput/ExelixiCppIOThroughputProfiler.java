/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.profiling.iothroughput;

import java.io.File;

import static exelixi.cpp.backend.constants.Constants.GENERATE_FIFO_TRACES;
import static exelixi.cpp.backend.constants.Constants.GENERATE_FIFO_TRACES_FOLDER;
import static exelixi.cpp.profiling.ProfilingConstants.PROFILING_RESULTS_PATH;

import exelixi.cpp.backend.ExelixiCPP;
import exelixi.cpp.backend.templates.actor.InstanceHeaderPrinter;
import exelixi.cpp.backend.templates.cmake.CMakePrinter;
import exelixi.cpp.profiling.common.template.lib.ProfilingHeader;
import exelixi.cpp.profiling.iothroughput.templates.instance.InstanceIOTopPriner;
import exelixi.cpp.profiling.iothroughput.templates.lib.CMakeIOPrinter;
import exelixi.cpp.profiling.iothroughput.templates.lib.FifoHistogramReader;
import exelixi.cpp.profiling.iothroughput.templates.lib.FifoHistogramWriter;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.util.FilesManager;
import net.sf.orcc.util.Result;

/**
 * 
 * @author Endri Bezati
 */
public class ExelixiCppIOThroughputProfiler extends ExelixiCPP {

	private String srcIO;

	private String resultsPath;
	
	private String fifoTracePath;

	private String instanceInludePath;

	@Override
	protected void createDirectories() {
		super.createDirectories();
		// -- Source IO Path
		srcIO = outputPath + File.separator + "src_io";
		File srcIODir = new File(srcIO);
		if (!srcIODir.exists()) {
			srcIODir.mkdir();
		}

		instanceInludePath = srcIO + File.separator + "include";
		File instanceInludeDir = new File(instanceInludePath);
		if (!instanceInludeDir.exists()) {
			instanceInludeDir.mkdir();
		}
		
		// -- Fifo trace path
		fifoTracePath = outputPath + File.separator + "fifo_traces";
		File fifoTarcesDir = new File(fifoTracePath);
		if (!fifoTarcesDir.exists()) {
			fifoTarcesDir.mkdir();
		}
		

		// -- Fifo trace path
		resultsPath = outputPath + File.separator + "results";
		File resultsDir = new File(resultsPath);
		if (!resultsDir.exists()) {
			resultsDir.mkdir();
		}

	}

	@Override
	protected void doInitializeOptions() {
		super.doInitializeOptions();
		
		fifoTrace = true;
		getOptions().put(GENERATE_FIFO_TRACES, false);
		getOptions().put(GENERATE_FIFO_TRACES_FOLDER, fifoTracePath);
		getOptions().put(PROFILING_RESULTS_PATH, resultsPath);

	}

	@Override
	protected Result doAdditionalGeneration(Network network) {
		Result result = super.doAdditionalGeneration(network);

		// -- Profiling Header
		ProfilingHeader profilingHeader = new ProfilingHeader();
		result.merge(FilesManager.writeFile(profilingHeader.getContent(), libIncludePath, "profiling.h"));

		// -- Fifo Histogram Reader
		FifoHistogramReader histogramReader = new FifoHistogramReader();
		result.merge(FilesManager.writeFile(histogramReader.getContent(), libIncludePath, "fifo_histogram_reader.h"));

		// -- Fifo Histrogram Writer
		FifoHistogramWriter histogramWriter = new FifoHistogramWriter();
		result.merge(FilesManager.writeFile(histogramWriter.getContent(), libIncludePath, "fifo_histogram_writer.h"));

		return result;
	}

	@Override
	protected Result getCMakePrinters(Network network) {
		final Result result = Result.newInstance();

		CMakeIOPrinter cmakeIOPrinter = new CMakeIOPrinter(network, getOptions());
		result.merge(FilesManager.writeFile(cmakeIOPrinter.getContentTop(), outputPath, "CMakeLists.txt"));

		CMakePrinter cmakePrinter = new CMakePrinter(network, getOptions());
		result.merge(FilesManager.writeFile(cmakePrinter.getContentLib(), libPath, "CMakeLists.txt"));
		result.merge(FilesManager.writeFile(cmakePrinter.getContentSrc(), srcPath, "CMakeLists.txt"));

		return result;
	}

	@Override
	protected Result doGenerateNetwork(Network network) {
		Result result = super.doGenerateNetwork(network);
		return result;
	}

	@Override
	protected Result doGenerateInstance(Instance instance) {
		Result result = super.doGenerateInstance(instance);

		// -- Instance Headers wihtout fifoTrace
		InstanceHeaderPrinter instancePrinter = new InstanceHeaderPrinter(instance, getOptions());
		instancePrinter.setOptions(getOptions());
		result.merge(FilesManager.writeFile(instancePrinter.getContent(), instanceInludePath, instance.getSimpleName() + ".h"));

		String instancePath = srcIO + File.separator + instance.getSimpleName();
		File instanceDir = new File(instancePath);
		if (!instanceDir.exists()) {
			instanceDir.mkdir();
		}

		// -- Instance
		InstanceIOTopPriner instanceIOTopPriner = new InstanceIOTopPriner(instance, getOptions());
		instanceIOTopPriner.setOptions(getOptions());
		result.merge(FilesManager.writeFile(instanceIOTopPriner.getContent(), instancePath,
				instance.getSimpleName() + "_top_io.cpp"));

		// -- Instance CMake
		CMakeIOPrinter cmakeIOInstancePrinter = new CMakeIOPrinter(instance, getOptions());
		result.merge(
				FilesManager.writeFile(cmakeIOInstancePrinter.getContentInstance(), instancePath, "CMakeLists.txt"));

		return result;
	}

}
