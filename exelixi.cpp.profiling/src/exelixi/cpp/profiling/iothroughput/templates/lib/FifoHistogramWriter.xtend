/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.cpp.profiling.iothroughput.templates.lib

/**
 * 
 * @author Endri Bezati
 */
class FifoHistogramWriter {

	def getContent() {
		'''
			#ifndef __FIFO_HISTOGRAM_WRITER__
			#define __FIFO_HISTOGRAM_WRITER__
			#include <vector>
			#include <string>
			#include <iostream>
			#include <fstream>
			#include <inttypes.h>
			#include <stdint.h> 
			#include "fifo.h"
			#include "profiling.h"
			#include "file_utils.h"
			
			template<typename T, int nb_reader>
			class FifoHistogramWriter: public Fifo<T, nb_reader> {
			public:
				FifoHistogramWriter(std::string input, std::string output);
				~FifoHistogramWriter();
			
				T* write_address() const;
			
				void write_advance();
			
				void write_advance(unsigned int nb_data);
			
				void write_csv() {
					std::ofstream csv_file;
					std::cout << "Writing file : " << output << std::endl;
					csv_file.open(output.c_str(), std::fstream::out);
					for (unsigned int i = 0; i < time_points_pointer; i++) {
						csv_file << (time_points[i] - time_points[0]) << ", " << bits[i]
								<< std::endl;
					}
					csv_file.close();
				}
			
				unsigned int rooms() const {
					unsigned int min_rooms = 65536;
					return min_rooms;
				}
			
			private:
				int* bits;
				uint64_t* time_points;
				int time_points_pointer;
				std::string output;
			};
			
			template<typename T, int nb_reader>
			FifoHistogramWriter<T, nb_reader>::FifoHistogramWriter(std::string input, std::string output) :
					Fifo<T, nb_reader>(65536, 0) {
				this->output = output;
			
				std::cout << "Allocating memory : " << input << std::endl;
				int lines = file_get_lines(input.c_str());
				bits = new int[lines];
				time_points = new uint64_t[lines];
				time_points_pointer = 0;
			}
			
			template<typename T, int nb_reader>
			FifoHistogramWriter<T, nb_reader>::~FifoHistogramWriter() {
			}
			
			template<typename T, int nb_reader>
			inline T* FifoHistogramWriter<T, nb_reader>::write_address() const {
				return Fifo<T, nb_reader>::buffer;
			}
			
			template<typename T, int nb_reader>
			void FifoHistogramWriter<T, nb_reader>::write_advance() {
				write_advance(1);
			}
			
			template<typename T, int nb_reader>
			void FifoHistogramWriter<T, nb_reader>::write_advance(unsigned int nb_val) {
				unsigned long long time_point = rdtscp();
				time_points[time_points_pointer] = time_point;
				bits[time_points_pointer]  = sizeof(T) * 8 * nb_val;
				time_points_pointer++;
			}
			#endif /* _FIFO_HISTOGRAM_WRITER__ */
		'''
	}
}
