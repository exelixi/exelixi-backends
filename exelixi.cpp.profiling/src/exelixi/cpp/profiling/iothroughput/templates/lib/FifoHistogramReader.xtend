/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.cpp.profiling.iothroughput.templates.lib

/**
 * 
 * @author Endri Bezati
 */
class FifoHistogramReader {

	def getContent() {
		'''
			#ifndef _FIFO_HISTOGRAM_READER__
			#define _FIFO_HISTOGRAM_READER__
			
			#include <chrono>
			#include <vector>
			#include <iostream>
			#include <ctime>
			#include <fstream>
			#include <string>
			#include "profiling.h"
			#include "fifo.h"
			#include "file_utils.h"
			
			template<typename T, int nb_reader>
			class FifoHistogramReader: public Fifo<T, nb_reader> {
			public:
				FifoHistogramReader(std::string input, std::string output);
				~FifoHistogramReader();
			
				T* read_address(int reader_id);
			
				T* read_address(int reader_id, unsigned nb_data);
			
				void read_advance(int reader_id, unsigned int nb_data = 1);
			
				unsigned int count(int reader_id) const;
			
				void write_csv() {
					std::ofstream csv_file;
					std::cout << "Writing file : " << output << std::endl;
					csv_file.open(output.c_str(), std::fstream::out);
					for (unsigned int i = 0; i < time_points_pointer; i++) {
						csv_file << (time_points[i] - time_points[0]) << ", " << bits[i] << std::endl;
					}
					csv_file.close();
				}
			
			private:
				std::string output;
				T* base_vector;
				int time_points_pointer;
				unsigned int base_vector_size;
				unsigned int base_vector_read_pointer;
				int* bits;
				uint64_t* time_points;
			
			};
			
			template<typename T, int nb_reader>
			FifoHistogramReader<T, nb_reader>::FifoHistogramReader(std::string input, std::string output) :
					Fifo<T, nb_reader>(1, 0) {
				this->output = output;
				std::cout << "Allocating memory : " << input << std::endl;
				int lines = file_get_lines(input.c_str());
				base_vector = new T[lines];
				bits = new int[lines];
				time_points = new uint64_t[lines];
			
				std::cout << "Reading file : " << input << std::endl;
				std::ifstream infile;
				infile.open(input.c_str());
				// fill the vector
				int value = 0;
				base_vector_size = 0;
				while (infile >> value) {
					base_vector[base_vector_size] = value;
					base_vector_size++;
				}
			
				time_points_pointer = 0;
				base_vector_read_pointer = 0;
			}
			
			template<typename T, int nb_reader>
			FifoHistogramReader<T, nb_reader>::~FifoHistogramReader() {
			}
			
			template<typename T, int nb_reader>
			inline void FifoHistogramReader<T, nb_reader>::read_advance(int reader_id,
					unsigned int nb_data) {
				base_vector_read_pointer += nb_data;
				bits[time_points_pointer] = sizeof(T) * nb_data * 8;
				time_points_pointer++;
			}
			
			
			template<typename T, int nb_reader>
			inline T* FifoHistogramReader<T, nb_reader>::read_address(int reader_id) {
				return FifoHistogramReader::read_address(reader_id, 1);
			}
			
			template<typename T, int nb_reader>
			inline T* FifoHistogramReader<T, nb_reader>::read_address(int reader_id,
					unsigned nb_data) {
				time_points[time_points_pointer] = rdtscp();
			
				return base_vector + base_vector_read_pointer;
			
			}
			
			template<typename T, int nb_reader>
			inline unsigned int FifoHistogramReader<T, nb_reader>::count(
					int reader_id) const {
				return base_vector_size - base_vector_read_pointer;
			}
			
			#endif /* _FIFO_HISTOGRAM_READER__ */
		'''
	}
}
