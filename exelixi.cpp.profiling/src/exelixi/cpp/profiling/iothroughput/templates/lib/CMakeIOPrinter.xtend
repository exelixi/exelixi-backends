/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.cpp.profiling.iothroughput.templates.lib

import static exelixi.cpp.backend.constants.Constants.LINK_NATIVE_LIBRARY
import static exelixi.cpp.backend.constants.Constants.LINK_NATIVE_LIBRARY_FOLDER

import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import java.util.Map

/**
 * 
 * @author Endri Bezati
 */
class CMakeIOPrinter {
	
	Network network
	
	Instance instance
	
	boolean linkNativeLib;
	String linkNativeLibFolder;
	
	new(Instance instance, Map<String, Object> options){
		this.instance = instance
		setOptions(options)
	}
	
	new(Network network, Map<String, Object> options){
		this.network = network
		setOptions(options)
	}
	
	def setOptions(Map<String, Object> options) {
		linkNativeLib = options.getOrDefault(LINK_NATIVE_LIBRARY, false) as Boolean;
		linkNativeLibFolder = options.getOrDefault(LINK_NATIVE_LIBRARY_FOLDER, "") as String;

		if (linkNativeLib && linkNativeLibFolder != "")
			linkNativeLib = true
		else
			linkNativeLib = false
	}
	
	def getContentTop() {
		'''
			cmake_minimum_required (VERSION 2.6)
			
			project («network.simpleName»)
			
			set(extra_definitions)
			option(NO_DISPLAY "Disable SDL display." 0)
			if(NO_DISPLAY)
				list(APPEND extra_definitions -DNO_DISPLAY)
			else()
				find_package(SDL REQUIRED)
			endif()
			
			«IF linkNativeLib»
				set(external_definitions)
				set(external_include_paths)
				set(external_library_paths)
				set(external_libraries)
				
				# All external vars should be set by the CMakeLists.txt inside the following folder.
				add_subdirectory(«linkNativeLibFolder» «linkNativeLibFolder»)
			«ENDIF»
			
			# Requiered Packages
			find_package(Threads REQUIRED)
			
			# Place the executable on bin directory
			set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)
			
			if(CMAKE_COMPILER_IS_GNUCXX)
				set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g")
				set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")
				set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -std=c++11 -fpermissive -Wno-narrowing -w")
			endif()
			«IF linkNativeLib»
				
				if(external_definitions)
					list(APPEND extra_definitions ${external_definitions})
				endif()
				add_definitions(${extra_definitions})
				
				if(external_library_paths)
					link_directories(${external_library_paths})
				endif()
			«ENDIF»
			
			include_directories(
				lib/include
				«IF linkNativeLib»
					${external_include_paths}
				«ENDIF»
			)
			
			# Compile Exelixi runtime
			add_subdirectory(lib)
			
			# Compiler dataflow CPP application
			add_subdirectory(src)
			
			# Add IO Profiling Executables
			«FOR instance : network.children.filter(typeof(Instance))»
				add_subdirectory(src_io/«instance.simpleName»)
			«ENDFOR»
		'''
	}
	
	def getContentInstance(){
		'''
			set(filenames
				«instance.simpleName»_top_io.cpp
			)
			
			«IF linkNativeLib»
				if(external_definitions)
					add_definitions(${external_definitions})
				endif()	

				if(external_library_paths)
					link_directories(${external_library_paths})
				endif()
			«ENDIF»	

			include_directories(../include«IF linkNativeLib» ${external_include_paths}«ENDIF»)
			
			add_executable(«instance.simpleName»_top_io ${filenames})
			
			set(libraries«IF linkNativeLib» ${external_libraries}«ENDIF» exelixi-runtime)
			
			#Exelixi CPP library files
			if(NO_DISPLAY)
					add_definitions(-DNO_DISPLAY)
			else()
					find_package(SDL)
					include_directories(
						${SDL_INCLUDE_DIR} 
					)
					set(libraries ${libraries} ${SDL_LIBRARY})
			endif()
			
			set(libraries ${libraries} ${CMAKE_THREAD_LIBS_INIT})
			target_link_libraries(«instance.simpleName»_top_io ${libraries})
		'''
	}
}
