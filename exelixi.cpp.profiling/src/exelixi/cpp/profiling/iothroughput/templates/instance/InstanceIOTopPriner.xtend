/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.cpp.profiling.iothroughput.templates.instance

import exelixi.cpp.backend.templates.common.ExprAndTypePrinter
import java.io.File
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.graph.Edge
import net.sf.orcc.ir.Var
import org.eclipse.emf.common.util.EList
import static exelixi.cpp.backend.constants.Constants.*

import static exelixi.cpp.backend.constants.Constants.GENERATE_FIFO_TRACES_FOLDER
import static exelixi.cpp.profiling.ProfilingConstants.PROFILING_RESULTS_PATH
import java.util.HashSet
import net.sf.orcc.df.Actor

/**
 * 
 * @author Endri Bezati
 */
class InstanceIOTopPriner extends ExprAndTypePrinter {

	protected Instance instance

	protected String resultsPath

	private String fifoTracePath

	private EList<Edge> incoming;

	private Map<Port, List<Connection>> outgoingPortMap;

	boolean linkNativeLib;
	String linkNativeLibHeaders;

	new(Instance instance, Map<String, Object> options) {
		this.instance = instance
		this.resultsPath = options.get(PROFILING_RESULTS_PATH) as String
		this.incoming = instance.incoming
		this.outgoingPortMap = instance.outgoingPortMap
		this.fifoTracePath = options.get(GENERATE_FIFO_TRACES_FOLDER) as String
		
		linkNativeLib = options.getOrDefault(LINK_NATIVE_LIBRARY, false) as Boolean;
		linkNativeLibHeaders =	options.getOrDefault(LINK_NATIVE_LIBRARY_HEADERS,"") as String;		
	}

	def getContent() {
		'''
			«headers»
			
			«main»
		'''
	}

	def getHeaders() {
		'''
			#include <string>
			«IF !instance.getActor.inputs.empty»
				#include "fifo_histogram_reader.h"
			«ENDIF»
			«IF !instance.getActor.outputs.empty»
				#include "fifo_histogram_writer.h"
			«ENDIF»

			«IF linkNativeLib && linkNativeLibHeaders != ""»
			«nativeLibHeaders»
			«ENDIF»
			// -- Actors Headers
			#include "«instance.simpleName».h"
		'''
	}

	def getNativeLibHeaders() {
		'''
			// -- Native lib headers
			«FOR header : linkNativeLibHeaders.split(";")»
				#include "«header.trim()»"
			«ENDFOR»
			
		'''
	}

	def getMain() {
		val List<Var> parameters = instance.getActor.parameters
		val Actor actor = instance.getActor
		'''
			«IF actor.hasAttribute("actor_shared_variables")»
				// -- Shared Variables
				«FOR v : actor.getAttribute("actor_shared_variables").objectValue as HashSet<Var>»
					«v.type.doSwitch» «v.name»«FOR dim : v.type.dimensions»[«dim»]«ENDFOR»;
				«ENDFOR»
			«ENDIF»

			int main(){
				// -- Instatiate Instance «instance.simpleName»
				«instance.simpleName» *act_«instance.simpleName» = new «instance.simpleName»(«FOR parameter : parameters SEPARATOR ", "»«IF instance.getArgument(parameter.name) !== null»«instance.getArgument(parameter.name).value.doSwitch»«ELSE»«parameter.initialValue.doSwitch»«ENDIF»«ENDFOR»);			
				
				// -- Instatiate fifos
				«FOR edge : incoming SEPARATOR "\n"»
					std::string fifo_«(edge as Connection).getAttribute("idNoBcast").objectValue»_input = "«(instancePortFilePath(instance,(edge as Connection).targetPort)).replaceAll("\\\\", "/")»";
					std::string fifo_«(edge as Connection).getAttribute("idNoBcast").objectValue»_output = "«(resultsPath + File.separator + instance.simpleName + "_" + (edge as Connection).targetPort.name + ".csv").replaceAll("\\\\", "/")»";
					FifoHistogramReader< «(edge as Connection).sourcePort.type.doSwitch», «(edge as Connection).getAttribute("nbReaders").objectValue» > *fifo_«(edge as Connection).getAttribute("idNoBcast").objectValue» = new FifoHistogramReader< «(edge as Connection).sourcePort.type.doSwitch», «(edge as Connection).getAttribute("nbReaders").objectValue» >(fifo_«(edge as Connection).getAttribute("idNoBcast").objectValue»_input, fifo_«(edge as Connection).getAttribute("idNoBcast").objectValue»_output);
				«ENDFOR»
				
				«FOR edges : outgoingPortMap.values SEPARATOR "\n"»
					std::string fifo_«edges.get(0).getAttribute("idNoBcast").objectValue»_input = "«(instancePortFilePath(instance,edges.get(0).sourcePort)).replaceAll("\\\\", "/")»";
					std::string fifo_«edges.get(0).getAttribute("idNoBcast").objectValue»_output = "«(resultsPath + File.separator + instance.simpleName + "_" + edges.get(0).sourcePort.name +".csv").replaceAll("\\\\", "/")»";
					FifoHistogramWriter<«edges.get(0).sourcePort.type.doSwitch», «edges.get(0).getAttribute("nbReaders").objectValue»> *fifo_«edges.get(0).getAttribute("idNoBcast").objectValue» = new FifoHistogramWriter<«edges.get(0).sourcePort.type.doSwitch», «edges.get(0).getAttribute("nbReaders").objectValue»>(fifo_«edges.get(0).getAttribute("idNoBcast").objectValue»_input, fifo_«edges.get(0).getAttribute("idNoBcast").objectValue»_output);
				«ENDFOR»
				
				// -- Connect Instance and Fifo(s)
				«FOR edges : outgoingPortMap.values»
					act_«(edges.get(0).source as Instance).name»->port_«edges.get(0).sourcePort.name» = fifo_«edges.get(0).getAttribute("idNoBcast").objectValue»;
				«ENDFOR»
				«FOR edge : incoming»
					act_«((edge as Connection).target as Instance).name»->port_«(edge as Connection).targetPort.name» = fifo_«(edge as Connection).getAttribute("idNoBcast").objectValue»;
				«ENDFOR»
				
				std::cout << "Starting Execution of instance : «instance.simpleName» " << std::endl;
				// -- Run
				
				// -- Call initialize action
				act_«instance.simpleName»->initialize();
				
				EStatus status = None;
				do{
					status = None;
					act_«instance.simpleName»->action_selection(status);
				}while(status != None);
				
				// -- Write CSV
				«FOR edge : incoming»
					fifo_«(edge as Connection).getAttribute("idNoBcast").objectValue»->write_csv();;
				«ENDFOR»
				«FOR edges : outgoingPortMap.values»
					fifo_«edges.get(0).getAttribute("idNoBcast").objectValue»->write_csv();;
				«ENDFOR»
				
				
				delete act_«instance.simpleName»;
				«FOR edge : incoming»
					delete fifo_«(edge as Connection).getAttribute("idNoBcast").objectValue»;
				«ENDFOR»
				«FOR edges : outgoingPortMap.values»
					delete fifo_«edges.get(0).getAttribute("idNoBcast").objectValue»;
				«ENDFOR»
				return 0;
			}
		'''
	}

	def int getSize(Connection connection) {
		if (connection.size !== null) {
			return connection.size;
		} else {
			return fifoSize;
		}
	}

	private def String instancePortFilePath(Instance instance, Port port) {
		return instancePath(instance) + port.name + ".txt";
	}

	private def String instancePath(Instance instance) {
		var dir = fifoTracePath + "/";
		for (String sp : instance.hierarchicalId.subList(1, instance.hierarchicalId.size)) {
			dir += (sp + "/");
		}
		return dir;
	}

}
