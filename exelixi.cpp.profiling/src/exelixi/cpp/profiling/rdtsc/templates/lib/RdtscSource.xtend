/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.cpp.profiling.rdtsc.templates.lib

/**
 * 
 * @author Endri Bezati
 * @author scb
 */
class RdtscSource {
	def getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _ 
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 * (c) Copyright 2016 Exelixi. All rights reserved.
			 * 
			 * This generated file contains confidential and proprietary information of 
			 * Exelixi and is protected under international copyright and other
			 * intellectual property laws.
			 ******************************************************************************/
			 
			#ifndef __EXELIXI_SW_RDTSC_H__
			#define __EXELIXI_SW_RDTSC_H__
			
			
			#include <inttypes.h>
			#include <stdint.h>
			#include "rdtsc.h"
			
			
			uint64_t cycles(unsigned int cycles_low, unsigned int cycles_high){
					return ( ((uint64_t)cycles_high << 32) | cycles_low );
			}
			
			unsigned int d_cycles(unsigned int cycles_low_0, unsigned int cycles_high_0, unsigned int cycles_low_1, unsigned int cycles_high_1){
					return static_cast<unsigned int>((cycles(cycles_low_1, cycles_high_1) - cycles(cycles_low_0, cycles_high_0)));
			}
				
				
			#endif // __EXELIXI_SW_RDTSC_H__
		'''
	}
}
