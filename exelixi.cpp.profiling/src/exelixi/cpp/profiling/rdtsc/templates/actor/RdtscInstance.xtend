/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.cpp.profiling.rdtsc.templates.actor

import exelixi.cpp.backend.templates.actor.InstanceSourcePrinter
import java.util.Map
import net.sf.orcc.df.Action
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Instance
import net.sf.orcc.df.State

/**
 * 
 * @author Endri Bezati
 * @author scb
 */
class RdtscInstance extends InstanceSourcePrinter {

	new(Instance instance, Map<String, Object> options) {
		super(instance, options)
	}
	
	 
	protected override getIncludes() {
		'''
			«super.getIncludes» 
			#include "rdtsc.h" 
		'''
	}

	protected override compileAction_beforeBody(Action action) {
		'''
			«super.compileAction_beforeBody(action)»
			// -- PROFILING: log variables
			unsigned volatile int __cycles_low_0, __cycles_high_0, __cycles_low_1, __cycles_high_1;
			
			// -- PROFILING: WARMUP
			asm volatile ("CPUID\n\t"
						"RDTSC\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t": "=r" (__cycles_high_0), "=r" (__cycles_low_0):: "%rax", "%rbx", "%rcx", "%rdx");
			asm volatile("RDTSCP\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t"
						"CPUID\n\t": "=r" (__cycles_high_1), "=r" (__cycles_low_1):: "%rax", "%rbx", "%rcx", "%rdx");
			asm volatile ("CPUID\n\t"
						"RDTSC\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t": "=r" (__cycles_high_0), "=r" (__cycles_low_0)::
						"%rax", "%rbx", "%rcx", "%rdx");
			asm volatile("RDTSCP\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t"
						"CPUID\n\t": "=r" (__cycles_high_1), "=r" (__cycles_low_1):: "%rax", "%rbx", "%rcx", "%rdx");
			
			// -- PROFILING: TICK
			asm volatile ("CPUID\n\t"
						"RDTSC\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t": "=r" (__cycles_high_0), "=r" (__cycles_low_0):: "%rax", "%rbx", "%rcx", "%rdx");
		'''
	}

	protected override compileAction_afterBody(Action action) {
		'''
			«super.compileAction_afterBody(action)»
			// -- PROFILING: TOCK
			asm volatile("RDTSCP\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t"
						"CPUID\n\t": "=r" (__cycles_high_1), "=r" (__cycles_low_1):: "%rax", "%rbx", "%rcx", "%rdx");
			
			 // -- PROFILING: CLOCKs evaluation				
			profilingData->addFiring("«instance.name»", "«action.name»", d_cycles(__cycles_low_0, __cycles_high_0, __cycles_low_1, __cycles_high_1));
		'''
	}
	
	
	override compileActionSelection(
		Actor actor
	) '''	
		// -- PROFILED SCHEDULER			
		bool «instance.simpleName»::action_selection(EStatus& status) {
			
			// -- PROFILING: WARMUP
			
			lastSelectedAction = "";
			
			asm volatile ("CPUID\n\t"
						"RDTSC\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t": "=r" (__scheduler_cycles_high_0), "=r" (__scheduler_cycles_low_0):: "%rax", "%rbx", "%rcx", "%rdx");
			asm volatile("RDTSCP\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t"
						"CPUID\n\t": "=r" (__scheduler_cycles_high_1), "=r" (__scheduler_cycles_low_1):: "%rax", "%rbx", "%rcx", "%rdx");
			asm volatile ("CPUID\n\t"
						"RDTSC\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t": "=r" (__scheduler_cycles_high_0), "=r" (__scheduler_cycles_low_0)::
						"%rax", "%rbx", "%rcx", "%rdx");
			asm volatile("RDTSCP\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t"
						"CPUID\n\t": "=r" (__scheduler_cycles_high_1), "=r" (__scheduler_cycles_low_1):: "%rax", "%rbx", "%rcx", "%rdx");
					
			// -- PROFILING: TICK
			asm volatile ("CPUID\n\t"
						"RDTSC\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t": "=r" (__scheduler_cycles_high_0), "=r" (__scheduler_cycles_low_0):: "%rax", "%rbx", "%rcx", "%rdx");
									
			«FOR port : actor.inputs»
				«IF instance.incomingPortMap.get(port) !== null»
					status_«port.name»_ = port_«port.name»->count(«instance.incomingPortMap.get(port).getAttribute("fifoId").objectValue»);
				«ENDIF»
			«ENDFOR»
			«FOR port : actor.outputs»
				«IF instance.outgoingPortMap.get(port) !== null»
					status_«port.name»_ = port_«port.name»->rooms();
				«ENDIF»
			«ENDFOR»
		
			bool res = true;
			executed_once = false;
			
			«IF !roundRobin»while (res) {«ENDIF»
				«IF actor.fsm !== null»
					res = false;
					«FOR action : actor.actionsOutsideFsm BEFORE "if" SEPARATOR "\nelse if"»«action.compileScheduler(null)»«ENDFOR»
					if(!res) {
						switch(state_) {
							«actor.fsm.compilerScheduler»
						}
					}
				«ELSE»
					res = false;
					«FOR action : actor.actions BEFORE "if" SEPARATOR "\nelse if"»«action.compileScheduler(null)»«ENDFOR»
				«ENDIF»
			«IF !roundRobin»}«ENDIF»
			
			return executed_once;
		}
		
	'''

	override compileScheduler(
		Action action,
		State state
	) '''
	(«FOR e : action.inputPattern.numTokensMap»«IF instance.incomingPortMap.get(e.key) !== null»status_«e.key.name»_ >= «e.value» && «ENDIF»«ENDFOR»«action.scheduler.name»()) {
		«IF !action.outputPattern.empty»
		if(«FOR e : action.outputPattern.numTokensMap SEPARATOR " &&" »«IF instance.outgoingPortMap.get(e.key) !== null»status_«e.key.name»_ >= «e.value» «ENDIF»«ENDFOR») {
			// -- PROFILING: TOCK
			asm volatile("RDTSCP\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t"
						"CPUID\n\t": "=r" (__scheduler_cycles_high_1), "=r" (__scheduler_cycles_low_1):: "%rax", "%rbx", "%rcx", "%rdx");
			profilingData->addScheduling("«instance.name»", lastSelectedAction, "«action.name»", d_cycles(__scheduler_cycles_low_0, __scheduler_cycles_high_0, __scheduler_cycles_low_1, __scheduler_cycles_high_1));
			«action.body.name»();
			lastSelectedAction = "«action.body.name»";
			asm volatile ("CPUID\n\t"
							"RDTSC\n\t"
							"mov %%edx, %0\n\t"
							"mov %%eax, %1\n\t": "=r" (__scheduler_cycles_high_0), "=r" (__scheduler_cycles_low_0):: "%rax", "%rbx", "%rcx", "%rdx");
			asm volatile("RDTSCP\n\t"
							"mov %%edx, %0\n\t"
							"mov %%eax, %1\n\t"
							"CPUID\n\t": "=r" (__scheduler_cycles_high_1), "=r" (__scheduler_cycles_low_1):: "%rax", "%rbx", "%rcx", "%rdx");
			asm volatile ("CPUID\n\t"
							"RDTSC\n\t"
							"mov %%edx, %0\n\t"
							"mov %%eax, %1\n\t": "=r" (__scheduler_cycles_high_0), "=r" (__scheduler_cycles_low_0)::
							"%rax", "%rbx", "%rcx", "%rdx");
			asm volatile("RDTSCP\n\t"
							"mov %%edx, %0\n\t"
							"mov %%eax, %1\n\t"
							"CPUID\n\t": "=r" (__scheduler_cycles_high_1), "=r" (__scheduler_cycles_low_1):: "%rax", "%rbx", "%rcx", "%rdx");
			asm volatile ("CPUID\n\t"
							"RDTSC\n\t"
							"mov %%edx, %0\n\t"
							"mov %%eax, %1\n\t": "=r" (__scheduler_cycles_high_0), "=r" (__scheduler_cycles_low_0):: "%rax", "%rbx", "%rcx", "%rdx");		
			res = true;
			executed_once = true;
			status = hasExecuted;
			«IF state !== null»state_ = state_«state.name»;«ENDIF»
		}
		«ELSE»
			// -- PROFILING: TOCK
			asm volatile("RDTSCP\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t"
						"CPUID\n\t": "=r" (__scheduler_cycles_high_1), "=r" (__scheduler_cycles_low_1):: "%rax", "%rbx", "%rcx", "%rdx");
			profilingData->addScheduling("«instance.name»", lastSelectedAction, "«action.name»", d_cycles(__scheduler_cycles_low_0, __scheduler_cycles_high_0, __scheduler_cycles_low_1, __scheduler_cycles_high_1));
			«action.body.name»();
			lastSelectedAction = "«action.body.name»";
			asm volatile ("CPUID\n\t"
						"RDTSC\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t": "=r" (__scheduler_cycles_high_0), "=r" (__scheduler_cycles_low_0):: "%rax", "%rbx", "%rcx", "%rdx");
			asm volatile("RDTSCP\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t"
						"CPUID\n\t": "=r" (__scheduler_cycles_high_1), "=r" (__scheduler_cycles_low_1):: "%rax", "%rbx", "%rcx", "%rdx");
			asm volatile ("CPUID\n\t"
						"RDTSC\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t": "=r" (__scheduler_cycles_high_0), "=r" (__scheduler_cycles_low_0)::
						"%rax", "%rbx", "%rcx", "%rdx");
			asm volatile("RDTSCP\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t"
						"CPUID\n\t": "=r" (__scheduler_cycles_high_1), "=r" (__scheduler_cycles_low_1):: "%rax", "%rbx", "%rcx", "%rdx");
			asm volatile ("CPUID\n\t"
						"RDTSC\n\t"
						"mov %%edx, %0\n\t"
						"mov %%eax, %1\n\t": "=r" (__scheduler_cycles_high_0), "=r" (__scheduler_cycles_low_0):: "%rax", "%rbx", "%rcx", "%rdx");		
			res = true;
			executed_once = true;
			status = hasExecuted;
			«IF state !== null»state_ = state_«state.name»;«ENDIF»
		«ENDIF»
	}'''
	

}
