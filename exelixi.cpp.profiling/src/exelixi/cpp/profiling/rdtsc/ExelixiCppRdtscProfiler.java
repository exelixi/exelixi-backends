/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.profiling.rdtsc;

import exelixi.cpp.profiling.common.ExelixiCppProfiler;
import exelixi.cpp.profiling.rdtsc.templates.actor.RdtscInstance;
import exelixi.cpp.profiling.rdtsc.templates.actor.RdtscInstanceHeader;
import exelixi.cpp.profiling.rdtsc.templates.cmake.RdtscCMakePrinter;
import exelixi.cpp.profiling.rdtsc.templates.lib.RdtscHeader;
import exelixi.cpp.profiling.rdtsc.templates.lib.RdtscSource;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.util.FilesManager;
import net.sf.orcc.util.Result;

/**
 * 
 * @author Endri Bezati
 * @author scb
 */
public class ExelixiCppRdtscProfiler extends ExelixiCppProfiler {

	@Override
	protected Result doAdditionalGeneration(Network network) {
		Result result = super.doAdditionalGeneration(network);

		RdtscHeader rdtscHederPrinter = new RdtscHeader();
		result.merge(FilesManager.writeFile(rdtscHederPrinter.getContent(), libIncludePath, "rdtsc.h"));

		RdtscSource rdtscSourcePrinter = new RdtscSource();
		result.merge(FilesManager.writeFile(rdtscSourcePrinter.getContent(), libSrcPath, "rdtsc.cpp"));
		return result;
	}

	@Override
	protected Result doGenerateInstance(Instance instance) {
		final Result result = Result.newInstance();

		RdtscInstanceHeader instancePrinterHeader = new RdtscInstanceHeader(instance, getOptions());
		instancePrinterHeader.setOptions(getOptions());
		result.merge(FilesManager.writeFile(instancePrinterHeader.getContent(), includePath, instance.getSimpleName() + ".h"));
			
		RdtscInstance actorPrinter = new RdtscInstance(instance, getOptions());
		actorPrinter.setOptions(getOptions());
		result.merge(FilesManager.writeFile(actorPrinter.getContent(), srcPath, instance.getSimpleName() + ".cpp"));
		return result;
	}

	@Override
	protected Result getCMakePrinters(Network network){
		final Result result = Result.newInstance();
		RdtscCMakePrinter cmakePrinter = new RdtscCMakePrinter(network, getOptions());
		result.merge(FilesManager.writeFile(cmakePrinter.getContentTop(), outputPath, "CMakeLists.txt"));
		result.merge(FilesManager.writeFile(cmakePrinter.getContentLib(), libPath, "CMakeLists.txt"));
		result.merge(FilesManager.writeFile(cmakePrinter.getContentSrc(), srcPath, "CMakeLists.txt"));
		return result;
	}

}
