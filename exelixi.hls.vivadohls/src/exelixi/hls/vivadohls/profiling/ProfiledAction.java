/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.profiling;

/**
 * 
 * @author Endri Bezati
 */
public class ProfiledAction {

	private String instanceName;

	private String moduleName;

	private int latencyMin;

	private int latencyMax;

	private int intervalMin;

	private int intervalMax;

	public ProfiledAction() {
	}

	public ProfiledAction(String instanceName, String moduleName, int latencyMin, int latencyMax, int intervalMin,
			int intervalMax) {

		this.instanceName = instanceName;
		this.moduleName = moduleName;
		this.latencyMax = latencyMax;
		this.intervalMin = intervalMin;
		this.intervalMax = intervalMax;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public int getLatencyMin() {
		return latencyMin;
	}

	public void setLatencyMin(int latencyMin) {
		this.latencyMin = latencyMin;
	}

	public int getLatencyMax() {
		return latencyMax;
	}

	public void setLatencyMax(int latencyMax) {
		this.latencyMax = latencyMax;
	}

	public int getIntervalMin() {
		return intervalMin;
	}

	public void setIntervalMin(int intervalMin) {
		this.intervalMin = intervalMin;
	}

	public int getIntervalMax() {
		return intervalMax;
	}

	public void setIntervalMax(int intervalMax) {
		this.intervalMax = intervalMax;
	}

	public boolean isUnknown() {
		return latencyMin == -1 || latencyMax == -1;
	}
	
	public boolean isStatic() {
		return latencyMin == latencyMax;
	}

	@Override
	public String toString() {
		return "Module Name: " + moduleName + "\n" + "Instance Name : " + instanceName + "\n" + "LatencyMin: "
				+ latencyMin + "\n" + "LatencyMax: " + latencyMax + "\n" + "IntervalMin: " + intervalMin + "\n"
				+ "IntervalMax: " + intervalMax;
	}

}
