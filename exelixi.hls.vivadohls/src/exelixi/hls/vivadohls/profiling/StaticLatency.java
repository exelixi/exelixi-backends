/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.profiling;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import net.sf.orcc.df.Instance;

/**
 * 
 * @author Endri Bezati
 */
public class StaticLatency {

	private ProfiledInstance profiledInstance;

	private File xmlFile;

	private Instance instance;

	public StaticLatency(Instance instance, String path) {
		this.instance = instance;
		xmlFile = new File(path + File.separator + ".autopilot" + File.separator + "db" + File.separator
				+ instance.getSimpleName() + ".verbose.rpt.xml");
		profiledInstance = new ProfiledInstance(instance);

		getVivadoHLSReport();
	}

	public ProfiledInstance getProfiledInstance() {
		return profiledInstance;
	}

	public boolean getVivadoHLSReport() {
		if (xmlFile.exists()) {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder;
			try {
				docBuilder = docBuilderFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(xmlFile);

				NodeList listOfSections = doc.getElementsByTagName("section");
				int totalSections = listOfSections.getLength();
				for (int i = 0; i < totalSections; i++) {
					Node sectionNode = listOfSections.item(i);
					Map<String, String> attributeMap = getAttributesAsString(sectionNode.getAttributes());
					if (attributeMap.containsKey("name")) {
						String name = attributeMap.get("name");
						if (name.equals("Performance Estimates")) {
							parsePerformanceEstimates(sectionNode);
						}
					}
				}

			} catch (ParserConfigurationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SAXException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;
		} else {
			return false;
		}
	}

	private void parsePerformanceEstimates(Node node) {
		NodeList listofItems = node.getChildNodes();
		int totalItems = listofItems.getLength();
		for (int i = 0; i < totalItems; i++) {
			Node itemNode = listofItems.item(i);
			if (itemNode.getAttributes() != null) {
				Map<String, String> attributeMap = getAttributesAsString(itemNode.getAttributes());
				String name = attributeMap.get("name");

				if (name.equals("Latency (clock cycles)")) {
					parseLatency(itemNode);
				}
			}
		}
	}

	private void parseLatency(Node node) {
		NodeList listOfSections = node.getChildNodes();
		int totalSections = listOfSections.getLength();
		for (int i = 0; i < totalSections; i++) {
			Node sectionNode = listOfSections.item(i);
			if (sectionNode.getAttributes() != null) {
				Map<String, String> attributeMap = getAttributesAsString(sectionNode.getAttributes());
				if (attributeMap.containsKey("name")) {
					String name = attributeMap.get("name");
					if (name.equals("")) {
						NodeList listOfItems = sectionNode.getChildNodes();
						int totalItems = listOfItems.getLength();
						for (int j = 0; j < totalItems; j++) {
							Node itemNode = listOfItems.item(j);
							if (itemNode.getAttributes() != null) {
								Map<String, String> itemAttributeMap = getAttributesAsString(itemNode.getAttributes());
								String itemName = itemAttributeMap.get("name");
								if (itemAttributeMap.containsKey("name")) {
									if (itemName.equals("Detail")) {
										parseLatencyTable(itemNode);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void parseLatencyTable(Node node) {
		NodeList listOfSections = node.getChildNodes();
		int totalSections = listOfSections.getLength();
		for (int i = 0; i < totalSections; i++) {
			Node sectionNode = listOfSections.item(i);
			if (sectionNode.getAttributes() != null) {
				Map<String, String> attributeMap = getAttributesAsString(sectionNode.getAttributes());
				if (attributeMap.containsKey("name")) {
					String name = attributeMap.get("name");
					if (name.equals("")) {
						NodeList listOfItems = sectionNode.getChildNodes();
						int totalItems = listOfItems.getLength();
						for (int j = 0; j < totalItems; j++) {
							Node itemNode = listOfItems.item(j);
							if (itemNode.getAttributes() != null) {
								Map<String, String> itemAttributeMap = getAttributesAsString(itemNode.getAttributes());
								String itemName = itemAttributeMap.get("name");
								if (itemAttributeMap.containsKey("name")) {
									if (itemName.equals("Instance")) {
										NodeList tableNodes = itemNode.getFirstChild().getChildNodes();
										int totalTableNodes = tableNodes.getLength();
										for (int k = 0; k < totalTableNodes; k++) {
											Node collumnNode = tableNodes.item(k);
											if (collumnNode.getAttributes() != null) {
												String nodeName = collumnNode.getNodeName();
												if (nodeName.equals("column")) {
													Map<String, String> collumnAttributeMap = getAttributesAsString(
															collumnNode.getAttributes());
													String textContext = collumnNode.getTextContent();
													String instanceName = collumnAttributeMap.get("name");

													String[] elements = textContext.split(", ");
													ProfiledAction profiledAction = new ProfiledAction();
													profiledAction.setInstanceName(instanceName);
													String actionName = elements[0]
															.replace(instance.getSimpleName() + "_", "");
													profiledAction.setModuleName(actionName);

													profiledAction.setLatencyMin(elements[1].equals("?") ? -1
															: Integer.parseInt(elements[1]));
													profiledAction.setLatencyMax(elements[2].equals("?") ? -1
															: Integer.parseInt(elements[2]));
													profiledAction.setIntervalMin(elements[3].equals("?") ? -1
															: Integer.parseInt(elements[3]));
													profiledAction.setIntervalMax(elements[4].equals("?") ? -1
															: Integer.parseInt(elements[4]));

													profiledInstance.setProfiledAction(actionName, profiledAction);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private Map<String, String> getAttributesAsString(NamedNodeMap attributes) {
		Map<String, String> attributeMap = new HashMap<String, String>();

		for (int j = 0; j < attributes.getLength(); j++) {
			String key = attributes.item(j).getNodeName();
			String value = attributes.item(j).getNodeValue();
			attributeMap.put(key, value);
		}
		return attributeMap;

	}
}
