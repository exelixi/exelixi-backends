/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.network

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import net.sf.orcc.df.Connection

class Writer extends HLSTemplate {

	Connection connection

	TypePrinterVisitor typePrinter

	new(Connection connection, TypePrinterVisitor typePrinter) {
		super(typePrinter)
		this.connection = connection
		this.typePrinter = typePrinter
	}

	def getContent() {
		'''
			«getFileHeader("Exilixi for Vivado HLS, IP reader Code Generation, Connection: " + getQueueName(connection))»
			#include <ap_axi_sdata.h>
			#include "hls_stream.h"
			
			template<int D>
			struct AxiWord {
				ap_int<D> data;
				ap_uint<1> last;
			};
			
			void «getQueueName(connection)»_writer(«typePrinter.channelClassName»< «connection.sourcePort.type.doSwitch» > &«getQueueName(connection)», hls::stream<AxiWord<32> > &«getQueueName(connection)»_axis){
			#pragma HLS STREAM variable=«getQueueName(connection)»
			#pragma HLS STREAM variable=«getQueueName(connection)»_axis»
				if(!«getQueueName(connection)».empty() && !«getQueueName(connection)»_axis.full()){
					int data = «getQueueName(connection)».read();
					AxiWord<32> axi_word = {data, 1};
					«getQueueName(connection)»_axis.write(axi_word);
				}
			}
		'''
	}

	private def getQueueName(Connection connection) {
		'''fifo_«connection.getAttribute("id").objectValue»'''
	}
}
