/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.network

import exelixi.hls.devices.XilinxDevice
import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.tclscripts.TCLTemplate
import exelixi.hls.util.MathUtil
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Set
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port
import net.sf.orcc.ir.Type

import static exelixi.hls.backend.Constants.ACTOR_ACTION_SELECTION_PEEKS
import static exelixi.hls.backend.Constants.BIT_ACCURATE_TYPES
import static exelixi.hls.backend.Constants.DEVICE
import static exelixi.hls.backend.Constants.NATIVE_IP_CORES
import static exelixi.hls.backend.Constants.NATIVE_IP_CORES_FOLDER
import static exelixi.hls.backend.Constants.RTL_PATH
import static exelixi.hls.backend.Constants.TB_RTL_PATH
import static exelixi.hls.backend.Constants.WCFG_PATH
import static exelixi.hls.backend.Constants.XDC_PATH
import static exelixi.hls.backend.Constants.ZYNQ_OPTION_AXI_LITE_CONTROL
import static net.sf.orcc.OrccLaunchConstants.DEFAULT_FIFO_SIZE
import static net.sf.orcc.OrccLaunchConstants.FIFO_SIZE

/**
 * A Vivado Block Design printer. Every actor on the network is an IP Core, 
 * the interconnection is done either through AXI Stream Data FIFO or by 
 * Vivado HLS FIFO.
 * 
 * @author Endri Bezati
 */
class VivadoNetworkBlockDesign extends TCLTemplate {

	Network network

	Map<String, Object> options

	Partitioner partitioner

	int fifoSize

	XilinxDevice xilinxDevice

	var Map<Connection, String> queueNames
	
	String hierarchyName
	
	boolean axiLiteControl
	
	String rtlPath
	
	String tbRtlPath
	
	String xdcPath

	String wcfgPath
	
	boolean idQueueName
	
	String nativeIPCoresFolder
	
	boolean useNativeIpCores
	
	boolean isBitAccurate
	
	new(Network network, Map<String, Object> options, Partitioner partitioner) {
		this.network = network
		this.options = options
		this.partitioner = partitioner
		this.axiLiteControl = options.getOrDefault(ZYNQ_OPTION_AXI_LITE_CONTROL, false) as Boolean
		this.rtlPath = options.get(RTL_PATH) as String
		this.tbRtlPath = options.get(TB_RTL_PATH) as String
		this.xdcPath = options.get(XDC_PATH) as String
		this.wcfgPath = options.get(WCFG_PATH) as String
		if (options.containsKey(FIFO_SIZE)) {
			fifoSize = options.get(FIFO_SIZE) as Integer
		} else {
			fifoSize = DEFAULT_FIFO_SIZE
		}
		idQueueName = true
		retrieveQueueNames()
		hierarchyName = network.simpleName
		this.useNativeIpCores = options.getOrDefault(NATIVE_IP_CORES, false) as Boolean
		if(useNativeIpCores){
			this.nativeIPCoresFolder = options.get(NATIVE_IP_CORES_FOLDER) as String
		}
		isBitAccurate = options.get(BIT_ACCURATE_TYPES) as Boolean
	}

	new(Network network, Map<String, Object> options) {
		this.network = network
		this.options = options
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		this.axiLiteControl = options.getOrDefault(ZYNQ_OPTION_AXI_LITE_CONTROL, false) as Boolean
		this.rtlPath = options.get(RTL_PATH) as String
		this.tbRtlPath = options.get(TB_RTL_PATH) as String
		this.xdcPath = options.get(XDC_PATH) as String
		this.wcfgPath = options.get(WCFG_PATH) as String
		if (options.containsKey(FIFO_SIZE)) {
			fifoSize = options.get(FIFO_SIZE) as Integer
		} else {
			fifoSize = DEFAULT_FIFO_SIZE
		}
		idQueueName = true
		retrieveQueueNames()
		hierarchyName = network.simpleName
		this.useNativeIpCores = options.getOrDefault(NATIVE_IP_CORES, false) as Boolean
		if(useNativeIpCores){
			this.nativeIPCoresFolder = options.get(NATIVE_IP_CORES_FOLDER) as String
		}
		if(options.containsKey(BIT_ACCURATE_TYPES)){
			isBitAccurate = options.get(BIT_ACCURATE_TYPES) as Boolean
		}else{
			isBitAccurate = false
		}
	}

	def getContent() {
			var List<Instance> instances = new ArrayList
			for(instance : network.children.filter(typeof(Instance))){
				instances.add(instance)
			}		
		'''
			«getFileHeader("Vivado Project TCL, Block Design")»
			create_project PL vivado_project -part «xilinxDevice.fpga»
			«IF !xilinxDevice.board.empty»set_property board_part «xilinxDevice.board» [current_project]«ENDIF»
			
			# -- Create Board Design
			create_bd_design "«network.simpleName»"
			
			# -- Add Vivado HLS IP Core
			set_property  ip_repo_paths { ip_cores «IF useNativeIpCores»«nativeIPCoresFolder» «ENDIF»} [current_project]
			update_ip_catalog
			
			«getCore(network.inputs, network.outputs, instances, network.connections)»
			
			# -- Regenerate Layout
			regenerate_bd_layout
			
			# -- Validate Desing
			validate_bd_design
			
			# -- Save the design
			save_bd_design
			
			# -- Create the wrapper
			make_wrapper -files [get_files vivado_project/PL.srcs/sources_1/bd/«network.simpleName»/«network.simpleName».bd] -top
			
			# -- Add FIFO RTL, Testbench, xdc constraints and ISIM wcfg
			set_property SOURCE_SET sources_1 [get_filesets sim_1]
			add_files -fileset sim_1 -norecurse {«TemplatesUtils.getPath(rtlPath)»/fifo.v}
			add_files -fileset sim_1 -norecurse {«TemplatesUtils.getPath(tbRtlPath)»/«network.simpleName»_tb.v}
			add_files -fileset sim_1 -norecurse {«TemplatesUtils.getPath(wcfgPath)»/«network.simpleName»_tb_behav.wcfg}
			add_files -fileset constrs_1 -norecurse {«TemplatesUtils.getPath(xdcPath)»/«network.simpleName».xdc}
			
			# -- Add the wrapper file and update sources
			add_files -norecurse vivado_project/PL.srcs/sources_1/bd/«network.simpleName»/hdl/«network.simpleName»_wrapper.v
			update_compile_order -fileset sources_1
			update_compile_order -fileset sim_1
		'''
	}
	
	
	def getCore(List<Port> inputs, List<Port> outputs, List<Instance> instances, List<Connection> connections){
		'''
			«createPorts(false, inputs, outputs)»
			
			«getHierarchy(inputs, outputs, instances, connections)»
			
			«connectHierarchy»
		'''
	}
	
	private def getHierarchy(List<Port> inputs, List<Port> outputs, List<Instance> instances, List<Connection> connections){
		'''
			create_bd_cell -type hier "«hierarchyName»" 
			
			«createPorts(true, inputs, outputs)»
			
			«getCores(instances, inputs, outputs)»
			
			«getConnections(inputs, outputs, instances, connections)»
		'''
	}
	
	private def connectHierarchy(){
		'''
			# -- Connect clock, reset and start
			«connectBlockDesign("ap_clk", hierarchyName, "ap_clk", false)»
			«connectBlockDesign("ap_rst_n", hierarchyName, "ap_rst_n", false)»
			«connectBlockDesign("start", hierarchyName, "start", false)»
			
			# -- Cores Handshake and hierarchy I/O
			«FOR port : network.inputs»
				«connectBlockDesign(TemplatesUtils.getNetworkPortName(port), hierarchyName, TemplatesUtils.getNetworkPortName(port), true)»
			«ENDFOR»
			
			«FOR port : network.outputs»
				«connectBlockDesign(TemplatesUtils.getNetworkPortName(port), hierarchyName, TemplatesUtils.getNetworkPortName(port), true)»
			«ENDFOR»
		'''
	}
	
	private def createInterfacePort(String name, String mode, String interfaceName, String version, Boolean useHierarchy){
		'''create_bd_intf_«IF useHierarchy»pin«ELSE»port«ENDIF» -mode «mode» -vlnv xilinx.com:interface:«interfaceName»:«version» «IF useHierarchy»«hierarchyName»/«ENDIF»«name»'''	
	}
	
	private def createPort(String name, String direction, String type, Boolean useHierarchy){
		'''create_bd_«IF useHierarchy»pin«ELSE»port«ENDIF» -dir «direction» -type «type» «IF useHierarchy»«hierarchyName»/«ENDIF»«name»'''
	}
	
	private def createPort(String name, String direction, Boolean useHierarchy){
		'''create_bd_«IF useHierarchy»pin«ELSE»port«ENDIF» -dir «direction» «IF useHierarchy»«hierarchyName»/«ENDIF»«name»'''
	}
	
	private def createPorts(Boolean useHierarchy, List<Port> inputs, List<Port> outputs) {
		'''
			«createPort("ap_rst_n", "I", "rst", useHierarchy)»
			«createPort("ap_clk", "I", "clk", useHierarchy)»
			«createPort("start", "I", useHierarchy)»
			
			
			«FOR port : inputs»
				«createInterfacePort(TemplatesUtils.getNetworkPortName(port),"Master","acc_fifo_read_rtl","1.0", useHierarchy)»
			«ENDFOR»
			
			«FOR port : outputs»
				«createInterfacePort(TemplatesUtils.getNetworkPortName(port),"Master","acc_fifo_write_rtl","1.0", useHierarchy)»
			«ENDFOR»
		'''
	}
	
	
	private def getCore(String ipName, String name, String family, String version, Boolean useHierarchy){
		'''create_bd_cell -type ip -vlnv xilinx.com:«family»:«ipName»:«version» «IF useHierarchy»«hierarchyName»/«ENDIF»«name»'''
	}


	private def getCores(List<Instance> instances, List<Port> inputs, List<Port> outputs) {
		'''
			«IF !inputs.empty»## -- Port Readers«ENDIF»
			«FOR port : inputs»
				«getCore("PortReader_"+ port.name, "PortReader_"+ port.name, "hls", "1.0", true)»
			«ENDFOR»
			
			## -- Instances Cores
			«FOR instance : instances»
				«getCore(TemplatesUtils.getInstanceName(instance), TemplatesUtils.getInstanceName(instance), "hls", "1.0", true)»
			«ENDFOR»
			
			«IF !outputs.empty»## -- Port Writer«ENDIF»
			«FOR port : outputs»
				«getCore("PortWriter_"+ port.name, "PortWriter_"+ port.name, "hls", "1.0", true)»
			«ENDFOR»
			
			## -- FIFO Queue Cores
			«FOR connection : network.connections»
				«getQueueCore(connection)»
			«ENDFOR»
		'''
	}
	
	private def getQueueCore(Connection connection) {
		val String name = queueNames.get(connection)
		val Integer data_width = if (connection.source instanceof Instance)
			connection.sourcePort.type.sizeInBits
		else
				(connection.source as Port).type.sizeInBits
		val Integer addr_width = MathUtil.log2_ceil(if(connection.size === null) fifoSize else connection.size)
		'''
			startgroup
			create_bd_cell -type ip -vlnv Exelixi:user:Exelixi_FIFO:1.0 «hierarchyName»/«name»
			set_property -dict [list CONFIG.MEM_STYLE {auto} CONFIG.DATA_WIDTH {«data_width»} CONFIG.ADDR_WIDTH {«addr_width»}] [get_bd_cells «hierarchyName»/«queueNames.get(connection)»]
			endgroup
		'''
	}

	private def getConnections(List<Port> inputs, List<Port> outputs, List<Instance> instances, List<Connection> connections) {
		'''
			## -- Connect Clocks
			«FOR port : inputs»
				«connectBlockDesignNetHierarchy("PortReader_" + port.name, "ap_clk", "ap_clk")»
			«ENDFOR»
			
			«FOR instance : instances»
				«connectBlockDesignNetHierarchy(TemplatesUtils.getInstanceName(instance), "ap_clk", "ap_clk")»
			«ENDFOR»
			
			«FOR port : outputs»
				«connectBlockDesignNetHierarchy("PortWriter_" + port.name,"ap_clk","ap_clk")»
			«ENDFOR»
			
			«FOR connection : connections»
				«connectBlockDesignNetHierarchy(queueNames.get(connection), "clk", "ap_clk")»
			«ENDFOR»
			
			## -- Connect Resets
			«FOR port : inputs»
				«connectBlockDesignNetHierarchy("PortReader_" + port.name,"ap_rst_n", "ap_rst_n")»
			«ENDFOR»
			
			«FOR instance : instances»
				«connectBlockDesignNetHierarchy(TemplatesUtils.getInstanceName(instance),"ap_rst_n", "ap_rst_n")»
			«ENDFOR»
			
			«FOR connection : connections»
				«connectBlockDesignNetHierarchy(queueNames.get(connection), "reset_n", "ap_rst_n")»
			«ENDFOR»
			
			«FOR port : outputs»
				«connectBlockDesignNetHierarchy("PortWriter_" + port.name,"ap_rst_n","ap_rst_n")»
			«ENDFOR»
			
			## -- Connect Start
			«FOR port : inputs»
				«connectBlockDesignNetHierarchy("PortReader_" + port.name, "ap_start", "start")»
			«ENDFOR»
			
			«FOR instance : instances»
				«connectBlockDesignNetHierarchy(TemplatesUtils.getInstanceName(instance), "ap_start", "start")»
			«ENDFOR»
			
			«FOR port : outputs»
				«connectBlockDesignNetHierarchy("PortWriter_" + port.name, "ap_start", "start")»
			«ENDFOR»
			
			«IF !inputs.empty» ## -- Connect Slave Ports to Port Reader«ENDIF»
			«FOR port : inputs»
				«connectBlockDesignInterfacePortNet(TemplatesUtils.getNetworkPortName(port), "PortReader_" + port.name, TemplatesUtils.getPortName(port, isBitAccurate))»
			«ENDFOR»
			
			«IF !outputs.empty» ## -- Connect Port Writer to Master Ports«ENDIF»
			«FOR port : outputs»
				«connectBlockDesignInterfaceNetPort("PortWriter_" + port.name, TemplatesUtils.getPortName(port, isBitAccurate), TemplatesUtils.getNetworkPortName(port))»
			«ENDFOR»
			
			«IF !inputs.empty»## -- Port Readers to Queues connections«ENDIF»
			«FOR port : inputs»
				«FOR connection : port.outgoing»
					«IF port.outgoing.size > 1»
						«connectBlockDesignInterfaceNet("PortReader_" + port.name, TemplatesUtils.getPortReaderNameIndex(port,port.outgoing.indexOf(connection),isBitAccurate ), queueNames.get(connection),"WRITE")»
					«ELSE»
						«connectBlockDesignInterfaceNet("PortReader_" + port.name, TemplatesUtils.getPortReaderName(port,isBitAccurate), queueNames.get(connection),"WRITE")»
					«ENDIF»
				«ENDFOR»
			«ENDFOR»
			
			## -- Instances connection
			«FOR instance : instances»
				«connectInstance(instance)»
			«ENDFOR»
			
			«IF !outputs.empty»## -- Queus to Port Writters connections«ENDIF»
			«FOR port : outputs»
				«connectBlockDesignInterfaceNet(queueNames.get(port.incoming.get(0)),"READ", "PortWriter_" + port.name, TemplatesUtils.getPortWritterName(port,isBitAccurate))»
			«ENDFOR»
		'''
	}

	private def connectInstance(Instance instance) {
		val Map<Port, Connection> incoming = instance.incomingPortMap
		val Map<Port, List<Connection>> outgoing = instance.outgoingPortMap
		val List<Port> inputs = instance.getActor.inputs
		val List<Port> outputs = instance.getActor.outputs
		val Set<Port> portsWithCount = TemplatesUtils.getPortsWithCount(instance)
		var Map<Port, List<Integer>> peekIndexes = new HashMap
		if(instance.getActor().hasAttribute(ACTOR_ACTION_SELECTION_PEEKS)){
			peekIndexes = instance.getActor().getValueAsObject(ACTOR_ACTION_SELECTION_PEEKS)
		}
		'''
			«FOR port : inputs»
				«connectBlockDesignInterfaceNet(queueNames.get(incoming.get(port)),"READ", TemplatesUtils.getInstanceName(instance), TemplatesUtils.getPortName(port,isBitAccurate))»
				«IF portsWithCount.contains(port)»
					«connectBlockDesignNet(queueNames.get(incoming.get(port)), "count", TemplatesUtils.getInstanceName(instance), port.name + "_count")»
				«ENDIF»
				«IF peekIndexes.containsKey(port)»
					«FOR i : peekIndexes.get(port)»
						«connectBlockDesignNet(queueNames.get(incoming.get(port)), "peek" + if (i > 0) "_" + i else "", TemplatesUtils.getInstanceName(instance), port.name + if (i > 0) "_" + i else "" + "_peek")»
					«ENDFOR»
				«ENDIF»
			«ENDFOR»
			
			«FOR port : outputs»
				«FOR connection: outgoing.get(port)»
					«IF outgoing.get(port).size > 1»
						«connectBlockDesignInterfaceNet(TemplatesUtils.getInstanceName(instance), TemplatesUtils.getPortNameIndex(port, outgoing.get(port).indexOf(connection),isBitAccurate), queueNames.get(connection),"WRITE")»
						«IF portsWithCount.contains(port)»
							«connectBlockDesignNet(queueNames.get(connection), "count", TemplatesUtils.getInstanceName(instance), port.name + "_" + outgoing.get(port).indexOf(connection) + "_count")»
							«connectBlockDesignNet(queueNames.get(connection), "size", TemplatesUtils.getInstanceName(instance), port.name + "_" + outgoing.get(port).indexOf(connection)  + "_size")»
						«ENDIF»
					«ELSE»
						«connectBlockDesignInterfaceNet(TemplatesUtils.getInstanceName(instance), TemplatesUtils.getPortName(port, isBitAccurate), queueNames.get(connection),"WRITE")»
						«IF portsWithCount.contains(port)»
							«connectBlockDesignNet(queueNames.get(connection), "count", TemplatesUtils.getInstanceName(instance), port.name + "_count")»
							«connectBlockDesignNet(queueNames.get(connection), "size", TemplatesUtils.getInstanceName(instance), port.name + "_size")»
						«ENDIF»
					«ENDIF»
				«ENDFOR»
			«ENDFOR»
		'''
	}

	// -- Connections methods
	private def connectBlockDesignInterfacePortNet(String sourcePort, String target, String targetPort) {
		'''connect_bd_intf_net [get_bd_intf_pins «hierarchyName»/«sourcePort»] [get_bd_intf_pins «hierarchyName»/«target»/«targetPort»]'''
	}

	private def connectBlockDesignInterfaceNetPort(String source, String sourcePort, String targetPort) {
		'''connect_bd_intf_net [get_bd_intf_pins «hierarchyName»/«source»/«sourcePort»] [get_bd_intf_pins «hierarchyName»/«targetPort»]'''
	}

	private def connectBlockDesignInterfaceNet(String source, String sourcePort, String target, String targetPort) {
		'''connect_bd_intf_net [get_bd_intf_pins «hierarchyName»/«source»/«sourcePort»] [get_bd_intf_pins «hierarchyName»/«target»/«targetPort»]'''
	}

	private def connectBlockDesignNetHierarchy(String source, String sourcePort, String net) {
		'''connect_bd_net [get_bd_pins /«hierarchyName»/«source»/«sourcePort»] [get_bd_pins «hierarchyName»/«net»]'''
	}
	
	private def connectBlockDesignNet(String source, String sourcePort,  String target, String targetPort) {
		'''connect_bd_net [get_bd_pins /«hierarchyName»/«source»/«sourcePort»] [get_bd_pins «hierarchyName»/«target»/«targetPort»]'''
	}
	
	private def connectBlockDesign(String sourcePort, String target, String targetPort, boolean isInterface) {
		'''connect_bd«IF isInterface»_intf«ENDIF»_net [get_bd«IF isInterface»_intf«ENDIF»_pins «sourcePort»] «IF isInterface»-boundary_type upper«ENDIF» [get_bd«IF isInterface»_intf«ENDIF»_pins «target»/«targetPort»]'''
	}
	
	// -- Helper Methods
	private def retrieveQueueNames() {
		queueNames = new HashMap<Connection, String>
		if(idQueueName){
			retrieveIdQueueNames()
		}else{
			for (connection : network.connections) {
				if (connection.source instanceof Port) {
					if (connection.target instanceof Instance) {
						queueNames.put(connection,
							"q_" + (connection.source as Port).name + "_" + (connection.target as Instance).name + "_" +
								connection.targetPort.name)
					}
				} else if (connection.source instanceof Instance) {
					if (connection.target instanceof Port) {
						queueNames.put(connection,
							"q_" + (connection.source as Instance).name + "_" + connection.sourcePort.name + "_" +
								(connection.target as Port).name)
					} else if (connection.target instanceof Instance) {
						queueNames.put(connection,
							"q_" + (connection.source as Instance).name + "_" + connection.sourcePort.name + "_" +
								(connection.target as Instance).name + "_" + connection.targetPort.name)
					}
	
				}
			}
		}
	}
	
	private def retrieveIdQueueNames() {
		queueNames = new HashMap<Connection, String>
		for (connection : network.connections) {
			queueNames.put(connection, "q_"+connection.getAttribute("id").objectValue)
		}
	}
	
	

	protected def int getNumBytes(Type type) {
		var int value = 1;
		if (type.isInt || type.isUint) {
			value = (Math.ceil(type.sizeInBits/8 as double)).intValue
		}else if(type.isFloat){
			value = 4;
		}
		return value;
	}
}
