/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.network

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port
import net.sf.orcc.graph.Edge
import net.sf.orcc.ir.Var

import static exelixi.hls.backend.Constants.INSTANCE_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.NETWORK_EXTERNAL_VARIABLES
import static net.sf.orcc.OrccLaunchConstants.*

/**
 * 
 * @author Endri Bezati
 */
class VivadoHLSNetworkCPP extends HLSTemplate {

	/** Network Inputs **/
	List<Port> inputs;

	/** Network Outputs **/
	List<Port> outputs;

	Network network;

 	Map<Connection, String> queueNames

	protected TypePrinterVisitor typePrinter

	new(Network network, TypePrinterVisitor typePrinter, Map<String, Object> options) {
		super(typePrinter)
		this.network = network
		this.typePrinter = typePrinter
		this.inputs = network.inputs
		this.outputs = network.outputs
		if (options.containsKey(FIFO_SIZE)) {
			fifoSize = options.get(FIFO_SIZE) as Integer
		} else {
			fifoSize = DEFAULT_FIFO_SIZE
		}
		retrieveQueueNames
	}

	def getContent() {
		var Map<Instance, List<Var>> instancesExternalVariablesMap = new HashMap;
		if (network.hasAttribute(NETWORK_EXTERNAL_VARIABLES)) {
			instancesExternalVariablesMap = network.getAttribute(NETWORK_EXTERNAL_VARIABLES).
				objectValue as Map<Instance, List<Var>>
		}

		'''
			«getFileHeader("Exilixi for Vivado HLS, Network Code Generation, Network: " + network.simpleName)»
			«headers»
			
			«streams»
			
			void «network.simpleName»(«FOR port : inputs SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDFOR»«IF !inputs.empty && !outputs.empty», «ENDIF»«FOR port : outputs SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDFOR»«IF !instancesExternalVariablesMap.empty»«IF !inputs.empty || !outputs.empty», «ENDIF»«getExternalVarsDeclaration(instancesExternalVariablesMap)»«ENDIF»){
			#pragma HLS INTERFACE ap_ctrl_hs port=return
			«IF !instancesExternalVariablesMap.empty»
				«FOR actor: instancesExternalVariablesMap.keySet»
					«FOR variable : instancesExternalVariablesMap.get(actor)»
						#pragma HLS INTERFACE m_axi depth=«getFlatListDepth(variable.type)» port=«variable.name»_«actor.simpleName» offset=slave bundle=«variable.name»_«actor.simpleName»
					«ENDFOR»
				«ENDFOR»
			«ENDIF»
			#pragma HLS DATAFLOW
			«streamSize»
				«portReaders»
				
				«instances»
				
				«portWriters»
				
				return;
			}
		'''
	}

	def getExternalVarsDeclaration(Map<Instance, List<Var>> actorsExternalVariablesMap) {
		'''«FOR instance : actorsExternalVariablesMap.keySet SEPARATOR ", "»«FOR variable : actorsExternalVariablesMap.get(instance) SEPARATOR ", "»volatile «variable.type.doSwitch» * «variable.name»_«instance.simpleName»«ENDFOR»«ENDFOR»'''
	}

	def getHeaders() {
		'''
			#include <«typePrinter.intIncludeHeaderFile»>
			#include "«typePrinter.channelIncludeHeaderFile»"
			«IF !network.inputs.empty»
				«FOR port : inputs»
					#include "PortReader_«port.name».h"
				«ENDFOR»
			«ENDIF»
			«FOR instance : network.children.filter(typeof(Instance))»
				#include "«instance.simpleName».h"
			«ENDFOR»
			«IF !network.outputs.empty»
				«FOR port : outputs»
					#include "PortWriter_«port.name».h"
				«ENDFOR»
			«ENDIF»
		'''
	}

	def getPortReaders() {
		'''
			«IF !inputs.empty»
				// -- Network Port Reader
				«FOR port : inputs»
					«getPortReader(port)»
				«ENDFOR»
			«ENDIF»
		'''
	}

	def getPortReader(Port port) {
		val List<Edge> outgoing = port.outgoing
		'''
			PortReader_«port.name»(«port.name», «FOR edge : outgoing SEPARATOR ", "»«queueNames.get(edge as Connection)»«ENDFOR»);
		'''
	}

	def getPortWriters() {
		'''
			«IF !outputs.empty»
				// -- Network Port Writer
				«FOR port : outputs»
					PortWriter_«port.name»(«queueNames.get(port.incoming.get(0) as Connection)», «port.name»);
				«ENDFOR»
			«ENDIF»
		'''
	}

	def getStreams() {
		'''
			// -- FIFO Queue Channels
			«FOR connection : network.connections»
				static hls::stream< «if (connection.source instanceof Instance) connection.sourcePort.type.doSwitch else (connection.source as Port).type.doSwitch» > «queueNames.get(connection)»;
			«ENDFOR»
		'''
	}

	def getStreamSize() {

		'''
			«FOR connection : network.connections»
				#pragma HLS STREAM variable=«queueNames.get(connection)» depth=«IF connection.size === null»«fifoSize»«ELSE»«connection.size»«ENDIF» dim=1
			«ENDFOR»
		'''

	}

	def getInstances() {
		'''
			// -- Instances
			«FOR instance : network.children.filter(typeof(Instance))»
				«getInstance(instance)»
			«ENDFOR»
		'''
	}

	def getInstance(Instance instance) {
		val Map<Port, Connection> incoming = instance.incomingPortMap
		val Map<Port, List<Connection>> outgoing = instance.outgoingPortMap
		val List<Port> inputs = instance.getActor.inputs
		val List<Port> outputs = instance.getActor.outputs

		var List<Var> externalVariables = new ArrayList<Var>
		if (instance.getActor.hasAttribute(INSTANCE_EXTERNAL_VARIABLES)) {
			externalVariables = instance.getActor.getAttribute(INSTANCE_EXTERNAL_VARIABLES).
				objectValue as ArrayList<Var>
		}

		'''
			«instance.simpleName»(«FOR port : inputs SEPARATOR ", "»«queueNames.get(incoming.get(port))»«ENDFOR»«IF !inputs.empty && !outputs.empty», «ENDIF»«FOR port : outputs SEPARATOR ", "»«FOR connection: outgoing.get(port) SEPARATOR ", "»«queueNames.get(connection)»«ENDFOR»«ENDFOR»«IF !externalVariables.empty»«IF !inputs.empty || !outputs.empty», «ENDIF»«FOR variable: externalVariables SEPARATOR ", "»«variable.name»_«instance.simpleName»«ENDFOR»«ENDIF»);
		'''
	}

	// -- Helper Methods
	def retrieveQueueNames() {
		queueNames = new HashMap<Connection, String>
		for (connection : network.connections) {
			if (connection.source instanceof Port) {
				if (connection.target instanceof Instance) {
					queueNames.put(connection,
						"q_" + (connection.source as Port).name + "_" + (connection.target as Instance).name + "_" +
							connection.targetPort.name)
				}
			} else if (connection.source instanceof Instance) {
				if (connection.target instanceof Port) {
					queueNames.put(connection,
						"q_" + (connection.source as Instance).name + "_" + connection.sourcePort.name + "_" +
							(connection.target as Port).name)
				} else if (connection.target instanceof Instance) {
					queueNames.put(connection,
						"q_" + (connection.source as Instance).name + "_" + connection.sourcePort.name + "_" +
							(connection.target as Instance).name + "_" + connection.targetPort.name)
				}

			}
		}
	}

}
