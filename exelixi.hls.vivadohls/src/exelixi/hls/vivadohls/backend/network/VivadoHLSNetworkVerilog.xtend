/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.network

import exelixi.hls.templates.network.AbstractNetworkPrinter
import exelixi.hls.util.MathUtil
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Set
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port
import net.sf.orcc.graph.Edge

import static exelixi.hls.backend.Constants.ACTOR_ACTION_SELECTION_PEEKS

/**
 * Verilog Network Printer for Vivado HLS
 * 
 * @author Endri Bezati
 */
class VivadoHLSNetworkVerilog extends AbstractNetworkPrinter {
	
	new(Network network, Map<String, Object> options) {
		super(network, options)
	}

	// ------------------------------------------------------------------------
	// -- Input / Output Signal
	override getSystemSignals() {
		'''
			input  wire ap_clk,
			input  wire ap_rst_n,
			input  wire ap_start,
			output wire ap_idle
		'''
	}

	override getPortDeclaration(Port port, boolean isInput) {
		'''
			«IF isInput»
				input  wire[«port.type.sizeInBits-1»:0] «port.name»_dout,
				input  wire «port.name»_empty_n,
				output wire «port.name»_read,
			«ELSE»
				output wire[«port.type.sizeInBits-1»:0] «port.name»_din,
				input  wire «port.name»_full_n,
				output wire «port.name»_write,
			«ENDIF»
		'''
	}

	// ------------------------------------------------------------------------
	// -- Parameters
	override getParameters(){
		'''
			«getQueueDepthParameters»
		'''
	}


	def getQueueDepthParameters(){
		'''
			«FOR connection : network.connections»
				parameter «(queueNames.get(connection)+"_ADDR_WIDTH").toUpperCase» = «getQueueAddrWidth(connection)»;
			«ENDFOR»
		'''
	}

	// ------------------------------------------------------------------------
	// -- Wires
	override getWires() {
		'''
			«fifoQueuesWires»
			
			«getApControlWires»
		'''
	}

	def getApControlWires(){
		'''
			«FOR port: network.inputs SEPARATOR "\n"»
				«getApControlSetOfWires("PortReader_" +port.name)»
			«ENDFOR»
			
			«FOR instance : network.children.filter(typeof(Instance)) SEPARATOR "\n"»
				«getApControlSetOfWires("id_" + instance.number + "_" + instance.simpleName)»
			«ENDFOR»
			
			«FOR port: network.outputs SEPARATOR "\n"»
				«getApControlSetOfWires("PortWriter_" +port.name)»
			«ENDFOR»
		'''
	}
	
	def getApControlSetOfWires(String name){
		'''
			wire «name»_ap_start;
			wire «name»_ap_done;
			wire «name»_ap_idle;
			wire «name»_ap_ready;
		'''
	}

	def getFifoQueuesWires(){
		'''
			«FOR connection : network.connections SEPARATOR "\n"»
				«getFifoQueueWire(connection)»
			«ENDFOR»
		'''
	}
	
	def getFifoQueueWire(Connection connection){
		var Integer dataWidth = if (connection.source instanceof Instance)
				connection.sourcePort.type.sizeInBits
			else
				(connection.source as Port).type.sizeInBits
		'''
			wire [«dataWidth - 1»:0] «queueNames.get(connection)»_din;
			wire «queueNames.get(connection)»_full_n;
			wire «queueNames.get(connection)»_write;
			
			wire [«dataWidth - 1»:0] «queueNames.get(connection)»_dout;
			wire «queueNames.get(connection)»_empty_n;
			wire «queueNames.get(connection)»_read;
			
			wire [«dataWidth - 1»:0] «queueNames.get(connection)»_peek;
			wire [31:0] «queueNames.get(connection)»_count;
			wire [31:0] «queueNames.get(connection)»_size;
		'''
	}


	// ------------------------------------------------------------------------
	// -- FIFO Queue
	override getQueue(Connection connection) {
		val String name = queueNames.get(connection)
		val Integer data_width = if (connection.source instanceof Instance)
			connection.sourcePort.type.sizeInBits
		else
				(connection.source as Port).type.sizeInBits
		'''
			FIFO #( 
				.MEM_STYLE("block"),
				.DATA_WIDTH(«data_width»),
				.ADDR_WIDTH(«(name + "_ADDR_WIDTH").toUpperCase»)
			) «name»(
				.clk(ap_clk),
				.reset_n(ap_rst_n),
				.if_full_n(«name»_full_n),
				.if_write(«name»_write),
				.if_din(«name»_din),
				
				.if_empty_n(«name»_empty_n),
				.if_read(«name»_read),
				.if_dout(«name»_dout),
				
				.peek(«name»_peek),
				.count(«name»_count),
				.size(«name»_size)
			);
		'''
	}



	// ------------------------------------------------------------------------
	// -- Port Reader/Writer
	override getPortReader(Port port) {
		val List<Edge> outgoing = port.outgoing
		'''
			assign PortReader_«port.name»_ap_start = «port.name»_empty_n;
			
			PortReader_«port.name» i_PortReader_«port.name»(
				.ap_clk(ap_clk),
				.ap_rst_n(ap_rst_n),
				.ap_start(PortReader_«port.name»_ap_start),
				.ap_done(PortReader_«port.name»_ap_done),
				.ap_idle(PortReader_«port.name»_ap_idle),
				.ap_ready(PortReader_«port.name»_ap_ready),
				.«port.name»«portExtension»_dout(«port.name»_dout),
				.«port.name»«portExtension»_empty_n(«port.name»_empty_n),
				.«port.name»«portExtension»_read(«port.name»_read),
				«IF outgoing.size > 1»
					«FOR int i : 0 .. outgoing.size - 1 SEPARATOR ","»
						.«port.name»_«i»_pr«portExtension»_din(«queueNames.get(outgoing.get(i) as Connection)»_din),
						.«port.name»_«i»_pr«portExtension»_full_n(«queueNames.get(outgoing.get(i) as Connection)»_full_n),
						.«port.name»_«i»_pr«portExtension»_write(«queueNames.get(outgoing.get(i) as Connection)»_write)
					«ENDFOR»
				«ELSE»
					.«port.name»_pr«portExtension»_din(«queueNames.get(outgoing.get(0) as Connection)»_din),
					.«port.name»_pr«portExtension»_full_n(«queueNames.get(outgoing.get(0) as Connection)»_full_n),
					.«port.name»_pr«portExtension»_write(«queueNames.get(outgoing.get(0) as Connection)»_write)
				«ENDIF»
			);
		'''
	}

	override getPortWriter(Port port) {
		'''
			assign PortWriter_«port.name»_ap_start = «queueNames.get(port.incoming.get(0) as Connection)»_empty_n;
			
			PortWriter_«port.name» i_PortWriter_«port.name»(
				.ap_clk(ap_clk),
				.ap_rst_n(ap_rst_n),
				.ap_start(PortWriter_«port.name»_ap_start),
				.ap_done(PortWriter_«port.name»_ap_done),
				.ap_idle(PortWriter_«port.name»_ap_idle),
				.ap_ready(PortWriter_«port.name»_ap_ready),
				.«port.name»_pw«portExtension»_dout(«queueNames.get(port.incoming.get(0) as Connection)»_dout),
				.«port.name»_pw«portExtension»_empty_n(«queueNames.get(port.incoming.get(0) as Connection)»_empty_n),
				.«port.name»_pw«portExtension»_read(«queueNames.get(port.incoming.get(0) as Connection)»_read),
				.«port.name»«portExtension»_din(«port.name»_din),
				.«port.name»«portExtension»_full_n(«port.name»_full_n),
				.«port.name»«portExtension»_write(«port.name»_write)
				
			);
		'''
	}


	// ------------------------------------------------------------------------
	// -- Instances
	override getInstance(Instance instance) {
		val Map<Port,Connection> incoming = instance.incomingPortMap
		val Map<Port,List<Connection>> outgoing = instance.outgoingPortMap
		val Set<Port> portsWithCount = TemplatesUtils.getPortsWithCount(instance)
		val List<Port> inputs = instance.getActor.inputs
		val List<Port> outputs = instance.getActor.outputs
		var Map<Port, List<Integer>> peekIndexes = new HashMap
		if(instance.getActor().hasAttribute(ACTOR_ACTION_SELECTION_PEEKS)){
			peekIndexes = instance.getActor().getValueAsObject(ACTOR_ACTION_SELECTION_PEEKS)
		}
		'''
			«IF enableProfiling»
				wire «instance.simpleName»_action_id_full_n;
				assign «instance.simpleName»_action_id_full_n = 1'b1;
			«ENDIF»
			assign id_«instance.number»_«instance.simpleName»_ap_start = «IF inputs.empty»1'b1«ELSE»«FOR port : inputs SEPARATOR " | "»«queueNames.get(incoming.get(port))»_empty_n«ENDFOR»«ENDIF»;
			
			id_«instance.number»_«instance.simpleName» i_«instance.simpleName»(
				.ap_clk(ap_clk),
				.ap_rst_n(ap_rst_n),
				.ap_start(id_«instance.number»_«instance.simpleName»_ap_start),
				.ap_done(id_«instance.number»_«instance.simpleName»_ap_done),
				.ap_idle(id_«instance.number»_«instance.simpleName»_ap_idle),
				.ap_ready(id_«instance.number»_«instance.simpleName»_ap_ready),
				«IF enableProfiling»
					.action_id«portExtension»_full_n(«instance.simpleName»_action_id_full_n)«IF !inputs.empty || !outputs.empty»,«ENDIF»
				«ENDIF»
				«IF outputs.empty»
					«FOR port : inputs SEPARATOR ","»
						«IF portsWithCount.contains(port)»
							.«port.name»_count(«queueNames.get(incoming.get(port))»_count),
						«ENDIF»
						«IF peekIndexes.containsKey(port)»
							«FOR i : peekIndexes.get(port)»
								.«port.name»_peek(«queueNames.get(incoming.get(port))»_peek),
							«ENDFOR»
						«ENDIF»
						.«port.name»«portExtension»_dout(«queueNames.get(incoming.get(port))»_dout),
						.«port.name»«portExtension»_empty_n(«queueNames.get(incoming.get(port))»_empty_n),
						.«port.name»«portExtension»_read(«queueNames.get(incoming.get(port))»_read)
					«ENDFOR»
				«ELSE»
					«FOR port : inputs »
						«IF portsWithCount.contains(port)»
							.«port.name»_count(«queueNames.get(incoming.get(port))»_count),
						«ENDIF»
						«IF peekIndexes.containsKey(port)»
							«FOR i : peekIndexes.get(port)»
								.«port.name»_peek(«queueNames.get(incoming.get(port))»_peek),
							«ENDFOR»
						«ENDIF»
						.«port.name»«portExtension»_dout(«queueNames.get(incoming.get(port))»_dout),
						.«port.name»«portExtension»_empty_n(«queueNames.get(incoming.get(port))»_empty_n),
						.«port.name»«portExtension»_read(«queueNames.get(incoming.get(port))»_read),
					«ENDFOR»
				«ENDIF»
				«FOR port : instance.getActor.outputs SEPARATOR ","»
					«IF outgoing.get(port).size > 1»
						«FOR int i : 0 .. outgoing.get(port).size - 1  SEPARATOR ", "»
							«IF portsWithCount.contains(port)»
								.«port.name»_«i»_count(«queueNames.get(outgoing.get(port).get(i))»_count),
								.«port.name»_«i»_size(«queueNames.get(outgoing.get(port).get(i))»_size),
							«ENDIF»
							.«port.name»_«i»«portExtension»_din(«queueNames.get(outgoing.get(port).get(i))»_din),
							.«port.name»_«i»«portExtension»_full_n(«queueNames.get(outgoing.get(port).get(i))»_full_n),
							.«port.name»_«i»«portExtension»_write(«queueNames.get(outgoing.get(port).get(i))»_write)
						«ENDFOR»
					«ELSE»
						«IF portsWithCount.contains(port)»
							.«port.name»_count(«queueNames.get(outgoing.get(port).get(0))»_count),
							.«port.name»_size(«queueNames.get(outgoing.get(port).get(0))»_size),
						«ENDIF»
						.«port.name»«portExtension»_din(«queueNames.get(outgoing.get(port).get(0))»_din),
						.«port.name»«portExtension»_full_n(«queueNames.get(outgoing.get(port).get(0))»_full_n),
						.«port.name»«portExtension»_write(«queueNames.get(outgoing.get(port).get(0))»_write)
					«ENDIF»
				«ENDFOR»
			);
			
		'''
	}
	
	// ------------------------------------------------------------------------
	// -- Idle
	
	override getIdle(){
		val instances = network.children.filter(typeof(Instance))
		val inputPorts = network.inputs
		val outputPorts = network.outputs
		//val connections =  network.connections
		
		val conditions = new ArrayList<String>
		
		for(instance : instances){
			conditions.add("id_"+ instance.number+"_"+instance.simpleName +"_ap_idle")
		}
		for (port : inputPorts){
			conditions.add("PortReader_"+port.name+"_ap_idle")
		}
		for (port : outputPorts){
			conditions.add("PortWriter_"+port.name+"_ap_idle")
		}
		/*
		for (connection : connections){
			conditions.add("~" + queueNames.get(connection)+"_empty_n")
		} 
		*/
		
		'''
			assign ap_idle = «FOR condition : conditions SEPARATOR " & "»«condition»«ENDFOR»;
		'''
	}
	
	
	// ------------------------------------------------------------------------
	// -- Helper definitions
	def getPortExtension(){
		'''«IF typeAccuracy»_V_V«ELSE»_V«ENDIF»'''
	}
	
	def Integer getQueueAddrWidth(Connection connection){
		val Integer addr_width = MathUtil.log2_ceil(if(connection.size === null) fifoSize else connection.size)
		return addr_width
	}

}
