/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend;

import static exelixi.hls.backend.Constants.AXI_STREAM_ACTOR_COMMUNICATION;
import static exelixi.hls.backend.Constants.BIT_ACCURATE_TYPES;
import static exelixi.hls.backend.Constants.CODE_GEN_PATH_HW;
import static exelixi.hls.backend.Constants.C_LM_LICENSE_FILE;
import static exelixi.hls.backend.Constants.C_VIVADO_HLS;
import static exelixi.hls.backend.Constants.C_XILINXD_LICENSE_FILE;
import static exelixi.hls.backend.Constants.DEVICE;
import static exelixi.hls.backend.Constants.EVERY_INSTANCE_AN_IP_CORE;
import static exelixi.hls.backend.Constants.FIFO_TRACE_PATH;
import static exelixi.hls.backend.Constants.FPGA_BOARD;
import static exelixi.hls.backend.Constants.LAUNCH_HLS;
import static exelixi.hls.backend.Constants.PROJECTS_PATH;
import static exelixi.hls.backend.Constants.RTL_PATH;
import static exelixi.hls.backend.Constants.TB_RTL_PATH;
import static exelixi.hls.backend.Constants.WCFG_PATH;
import static exelixi.hls.backend.Constants.XDC_PATH;
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION;
import static net.sf.orcc.backends.BackendsConstants.BXDF_FILE;
import static net.sf.orcc.backends.BackendsConstants.IMPORT_BXDF;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.common.io.Files;

import exelixi.hls.backend.XronosBackend;
import exelixi.hls.devices.XilinxDevice;
import exelixi.hls.partitioner.Partitioner;
import exelixi.hls.templates.types.TypePrinterVisitor;
import exelixi.hls.transformations.ActorAddFSM;
import exelixi.hls.transformations.ActorPeekComplexity;
import exelixi.hls.transformations.ConnectionLabeler;
import exelixi.hls.transformations.LoopLabeler;
import exelixi.hls.transformations.PortComplexity;
import exelixi.hls.transformations.UniquePortMemory;
import exelixi.hls.transformations.VarInitializer;
import exelixi.hls.vivadohls.backend.fifo.FifoPrinter;
import exelixi.hls.vivadohls.backend.instance.VivadoHLSInstance;
import exelixi.hls.vivadohls.backend.instance.VivadoHLSInstanceHeader;
import exelixi.hls.vivadohls.backend.network.VivadoHLSNetworkCPP;
import exelixi.hls.vivadohls.backend.network.VivadoHLSNetworkHeader;
import exelixi.hls.vivadohls.backend.network.VivadoHLSNetworkVerilog;
import exelixi.hls.vivadohls.backend.network.VivadoNetworkBlockDesign;
import exelixi.hls.vivadohls.backend.ports.PortReader;
import exelixi.hls.vivadohls.backend.ports.PortReaderHeader;
import exelixi.hls.vivadohls.backend.ports.PortWriter;
import exelixi.hls.vivadohls.backend.ports.PortWriterHeader;
import exelixi.hls.vivadohls.backend.scripts.FifoCoreTCLPrinter;
import exelixi.hls.vivadohls.backend.scripts.ModelsimTCLPrinter;
import exelixi.hls.vivadohls.backend.scripts.VivadoHLSProjectTCLPrinter;
import exelixi.hls.vivadohls.backend.scripts.VivadoProjectTCLPrinter;
import exelixi.hls.vivadohls.backend.scripts.VivadoXdcPrinter;
import exelixi.hls.vivadohls.backend.testbench.TestbechVerilog;
import exelixi.hls.vivadohls.backend.testbench.TestbenchCPP;
import exelixi.hls.vivadohls.backend.transformation.ExternalArray;
import exelixi.hls.vivadohls.backend.transformation.InstanceIndentifier;
import exelixi.hls.vivadohls.backend.transformation.PrintIO;
import exelixi.hls.vivadohls.backend.transformation.StateVariablesInGuards;
import exelixi.hls.vivadohls.backend.transformation.UniqueInstance;
import exelixi.hls.vivadohls.backend.type.VivadoHLSTypePrinter;
import exelixi.hls.vivadohls.backend.waveform.IsimWaveform;
import net.sf.orcc.backends.llvm.tta.transform.PrintRemoval;
import net.sf.orcc.backends.transform.DisconnectedOutputPortRemoval;
import net.sf.orcc.backends.util.Validator;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.transform.Instantiator;
import net.sf.orcc.df.transform.NetworkFlattener;
import net.sf.orcc.df.transform.TypeResizer;
import net.sf.orcc.df.transform.UnitImporter;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.df.util.NetworkValidator;
import net.sf.orcc.graph.Vertex;
import net.sf.orcc.ir.transform.DeadVariableRemoval;
import net.sf.orcc.ir.transform.RenameTransformation;
import net.sf.orcc.tools.mapping.XmlBufferSizeConfiguration;
import net.sf.orcc.util.FilesManager;
import net.sf.orcc.util.OrccLogger;
import net.sf.orcc.util.Result;
import net.sf.orcc.util.Void;

/**
 * A Vivado HLS backend
 * 
 * @author Endri Bezati
 */
public class VivadoHLSBackend extends XronosBackend {

	private TypePrinterVisitor typePrinter;

	private String simulationNetworkPath;

	private Partitioner partitioner;

	@Override
	protected void doInitializeOptions() {
		createFolder();

		// -- Options
		launchHLS = getOption(LAUNCH_HLS, false);
		getOptions().put(FIFO_TRACE_PATH, fifoTracePath);
		getOptions().put(PROJECTS_PATH, projectsPath);
		getOptions().put(RTL_PATH, rtlPath);
		getOptions().put(XDC_PATH, xdcPath);
		getOptions().put(TB_RTL_PATH, tbRtlPath);
		getOptions().put(WCFG_PATH, wcfgPath);
		getOptions().put(CODE_GEN_PATH_HW, "../../code-gen");
		String name = getOption(FPGA_BOARD, "ZC702 - Zynq 7");
		XilinxDevice xilinxDevice = new XilinxDevice(name);
		getOptions().put(DEVICE, xilinxDevice);
		getOptions().put(AXI_STREAM_ACTOR_COMMUNICATION, false);

		// -- Vivado HLS Type Printer
		typePrinter = new VivadoHLSTypePrinter(getOptions());

		networkTransfos.add(new Instantiator(true));
		networkTransfos.add(new NetworkFlattener());
		networkTransfos.add(new UniqueInstance());
		networkTransfos.add(new PrintIO());
		// networkTransfos.add(new ArgumentEvaluator());
		networkTransfos.add(new DisconnectedOutputPortRemoval());
		networkTransfos.add(new ConnectionLabeler());
		networkTransfos.add(new UnitImporter());
		networkTransfos.add(new DfVisitor<Void>(new DeadVariableRemoval()));
		networkTransfos.add(new ActorPeekComplexity());
		if (!getOption(BIT_ACCURATE_TYPES, true)) {
			networkTransfos.add(new TypeResizer(true, false, false, false));
		}
		networkTransfos.add(new ExternalArray(getOptions()));
		networkTransfos.add(new RenameTransformation(getDefaultRelacementMap(), false));
		networkTransfos.add(new InstanceIndentifier());

		childrenTransfos.add(new StateVariablesInGuards());
		childrenTransfos.add(new UniquePortMemory(fifoSize));

		childrenTransfos.add(new ActorAddFSM());
		childrenTransfos.add(new PortComplexity());
		childrenTransfos.add(new VarInitializer());
		childrenTransfos.add(new LoopLabeler());
		// childrenTransfos.add(new DfVisitor<Expression>(new
		// TacTransformation()));
		// childrenTransfos.add(new DfVisitor<Void>(new
		// MultiplierDirectiveAdder()));
		childrenTransfos.add(new PrintRemoval());
	}

	@Override
	protected void doValidate(Network network) {
		Validator.checkMinimalFifoSize(network, fifoSize);

		new NetworkValidator().doSwitch(network);
		network.computeTemplateMaps();
		partitioner = new Partitioner(network, mapping);
		// -- Check if partition is Empty
		if (partitioner.isMappingEmpty()) {
			// -- Fill it With ZYNQ_PL_PARTITION tag
			for (Vertex vertex : network.getChildren()) {
				if (vertex instanceof Instance) {
					Instance instance = (Instance) vertex;
					mapping.put(instance.getHierarchicalName(), ZYNQ_PL_PARTITION);
				}
			}
			// -- Re-run Partitioner
			partitioner = new Partitioner(network, mapping);
		}
	}

	@Override
	protected void beforeGeneration(Network network) {
		if (getOption(IMPORT_BXDF, false)) {
			File f = new File(getOption(BXDF_FILE, ""));
			new XmlBufferSizeConfiguration(true, false).load(f, network);
		}
	}

	@Override
	protected Result doGenerateNetwork(Network network) {
		final Result result = Result.newInstance();
		// -- Create Simulation Network Path
		simulationNetworkPath = simulationPath + File.separator + network.getSimpleName();
		createDirectory(simulationNetworkPath);

		boolean everyInstanceAnIpCore = getOption(EVERY_INSTANCE_AN_IP_CORE, true);
		if (!everyInstanceAnIpCore) {
			// -- CPP Network
			VivadoHLSNetworkCPP networkCPP = new VivadoHLSNetworkCPP(network, typePrinter, getOptions());
			result.merge(FilesManager.writeFile(networkCPP.getContent(), srcPath, network.getSimpleName() + ".cpp"));

			// -- CPP Network Header
			VivadoHLSNetworkHeader networkHeader = new VivadoHLSNetworkHeader(network, typePrinter);
			result.merge(
					FilesManager.writeFile(networkHeader.getContent(), includePath, network.getSimpleName() + ".h"));
		} else {
			// -- Create the Projects Path
			String networkProjectsPath = projectsPath + File.separator + network.getSimpleName();
			createDirectory(networkProjectsPath);

			// -- Exelixi FIFO IP Core
			boolean axiStreamCommunication = getOption(AXI_STREAM_ACTOR_COMMUNICATION, false);
			if (!axiStreamCommunication) {
				FifoCoreTCLPrinter fifoCoreTCLPrinter = new FifoCoreTCLPrinter(getOptions());
				result.merge(FilesManager.writeFile(fifoCoreTCLPrinter.getContent(), networkProjectsPath,
						"exelixi_fifo_core.tcl"));
			}

			// -- Vivado Block Design Network
			VivadoNetworkBlockDesign blockDesignNetwork = new VivadoNetworkBlockDesign(network, getOptions());
			result.merge(FilesManager.writeFile(blockDesignNetwork.getContent(), networkProjectsPath,
					network.getSimpleName() + "_vivado.tcl"));

		}
		// -- Verilog Network
		VivadoHLSNetworkVerilog vivadoHLSNetworkVerilog = new VivadoHLSNetworkVerilog(network, getOptions());
		result.merge(
				FilesManager.writeFile(vivadoHLSNetworkVerilog.getContent(), rtlPath, network.getSimpleName() + ".v"));
		result.merge(FilesManager.writeFile(vivadoHLSNetworkVerilog.getContent(), simulationNetworkPath,
				network.getSimpleName() + ".v"));

		return result;
	}

	@Override
	protected Result doGenerateInstance(Instance instance) {
		final Result result = Result.newInstance();
		if (!instance.getActor().isNative()) {
			String name = instance.getSimpleName();
			// -- Create folder for instance
			String projectInstancePath = projectsPath + File.separator + name;
			createDirectory(projectInstancePath);

			VivadoHLSProjectTCLPrinter projectPrinter = new VivadoHLSProjectTCLPrinter(instance, getOptions());
			result.merge(FilesManager.writeFile(projectPrinter.getContent(), projectInstancePath,
					instance.getSimpleName() + "_hls.tcl"));

			VivadoHLSInstance instancePrinter = new VivadoHLSInstance(instance, typePrinter, getOptions(), partitioner);
			Result instanceResult = FilesManager.writeFile(instancePrinter.getContent(), srcPath, name + ".cpp");
			if (!isCached(instanceResult)) {
				// -- Launch Vivado HLS
				launchHLS(name, "Instance");
			}

			copyVerilogFiles(name, simulationNetworkPath);
			result.merge(instanceResult);

			VivadoProjectTCLPrinter vivadoProjectPrinter = new VivadoProjectTCLPrinter(instance, getOptions());
			result.merge(FilesManager.writeFile(vivadoProjectPrinter.getContent(), projectInstancePath,
					instance.getSimpleName() + "_vivado.tcl"));

			VivadoHLSInstanceHeader headerPrinter = new VivadoHLSInstanceHeader(instance, typePrinter, partitioner,
					getOptions());
			result.merge(FilesManager.writeFile(headerPrinter.getContent(), includePath, name + ".h"));
		}
		return result;
	}

	@Override
	protected Result doAdditionalGeneration(Network network) {
		final Result result = Result.newInstance();
		// -- Port Readers
		for (Port port : network.getInputs()) {
			String name = "PortReader_" + port.getName();
			String projectInstancePath = projectsPath + File.separator + name;
			createDirectory(projectInstancePath);

			VivadoHLSProjectTCLPrinter projectPrinter = new VivadoHLSProjectTCLPrinter(name, getOptions());
			result.merge(FilesManager.writeFile(projectPrinter.getContent(), projectInstancePath, name + "_hls.tcl"));

			PortReader portReader = new PortReader(port, typePrinter, getOptions(), false);
			Result portReaderResult = FilesManager.writeFile(portReader.getContent(), srcPath, name + ".cpp");
			if (!isCached(portReaderResult)) {
				// -- Launch Vivado HLS
				launchHLS(name, "Port Reader");
			}
			result.merge(portReaderResult);

			copyVerilogFiles(name, simulationNetworkPath);

			PortReaderHeader portReaderHeader = new PortReaderHeader(port, typePrinter, false);
			Result portReaderResultHeader = FilesManager.writeFile(portReaderHeader.getContent(), includePath,
					name + ".h");
			result.merge(portReaderResultHeader);
		}

		// -- Port Writers
		for (Port port : network.getOutputs()) {
			String name = "PortWriter_" + port.getName();
			String projectInstancePath = projectsPath + File.separator + name;
			createDirectory(projectInstancePath);

			VivadoHLSProjectTCLPrinter projectPrinter = new VivadoHLSProjectTCLPrinter(name, getOptions());
			result.merge(FilesManager.writeFile(projectPrinter.getContent(), projectInstancePath, name + "_hls.tcl"));

			PortWriter portWriter = new PortWriter(port, typePrinter, getOptions(), false);
			Result portWriterResult = FilesManager.writeFile(portWriter.getContent(), srcPath, name + ".cpp");
			if (!isCached(portWriterResult)) {
				// -- Launch Vivado HLS
				launchHLS(name, "Port Writer");
			}
			result.merge(portWriterResult);

			copyVerilogFiles(name, simulationNetworkPath);
			PortWriterHeader portWriterHeader = new PortWriterHeader(port, typePrinter, false);
			Result portWriterResultHeader = FilesManager.writeFile(portWriterHeader.getContent(), includePath,
					name + ".h");
			result.merge(portWriterResultHeader);
		}
		boolean everyInstanceAnIpCore = getOption(EVERY_INSTANCE_AN_IP_CORE, true);
		if (!everyInstanceAnIpCore) {
			// -- Vivado HLS Project creation and synthesis script
			String networkProjectsPath = projectsPath + File.separator + network.getSimpleName();
			createDirectory(networkProjectsPath);

			VivadoHLSProjectTCLPrinter projectPrinter = new VivadoHLSProjectTCLPrinter(network, getOptions());
			result.merge(FilesManager.writeFile(projectPrinter.getContent(), networkProjectsPath,
					network.getSimpleName() + "_hls.tcl"));

			VivadoProjectTCLPrinter vivadoProjectPrinter = new VivadoProjectTCLPrinter(network, getOptions());
			result.merge(FilesManager.writeFile(vivadoProjectPrinter.getContent(), networkProjectsPath,
					network.getSimpleName() + "_vivado.tcl"));
		} else {
			// -- Create the Projects Path
			String networkProjectsPath = projectsPath + File.separator + network.getSimpleName();
			createDirectory(networkProjectsPath);
			String ipCorePath = networkProjectsPath + File.separator + "ip_cores";
			createDirectory(ipCorePath);

			VivadoHLSProjectTCLPrinter projectPrinter = new VivadoHLSProjectTCLPrinter(network, getOptions());
			result.merge(FilesManager.writeFile(projectPrinter.getContent(), networkProjectsPath,
					network.getSimpleName() + "_hls.tcl"));
		}
		// --Modelsim TCL script
		ModelsimTCLPrinter modelsimTCLPrinter = new ModelsimTCLPrinter(network, getOptions());
		result.merge(FilesManager.writeFile(modelsimTCLPrinter.getContent(), simulationNetworkPath,
				"sim_" + network.getSimpleName() + ".tcl"));

		// -- TestBench Printer CPP
		TestbenchCPP testbenchPrinter = new TestbenchCPP(network, typePrinter, getOptions());
		result.merge(
				FilesManager.writeFile(testbenchPrinter.getContent(), tbSrcPath, network.getSimpleName() + "_tb.cpp"));

		// -- Testbench Verilog
		TestbechVerilog testbenchVerilogPrinter = new TestbechVerilog(network, getOptions());
		result.merge(FilesManager.writeFile(testbenchVerilogPrinter.getContent(), tbRtlPath,
				network.getSimpleName() + "_tb.v"));

		// -- XDC File
		VivadoXdcPrinter xdcPrinter = new VivadoXdcPrinter(getOptions());
		result.merge(FilesManager.writeFile(xdcPrinter.getContent(), xdcPath, network.getSimpleName() + ".xdc"));

		// -- WCFG File
		IsimWaveform wcfgPrinter = new IsimWaveform(network, getOptions());
		result.merge(
				FilesManager.writeFile(wcfgPrinter.getContent(), wcfgPath, network.getSimpleName() + "_tb_behav.wcfg"));

		// -- Generate fifo
		FifoPrinter fifoPrinter = new FifoPrinter();
		result.merge(FilesManager.writeFile(fifoPrinter.getContent(), rtlPath, "fifo.v"));
		result.merge(FilesManager.writeFile(fifoPrinter.getContent(), simulationNetworkPath, "fifo.v"));

		return result;
	}

	@Override
	protected Result doAdditionalGeneration(Instance instance) {
		final Result result = Result.newInstance();

		// -- Testbench CPP
		TestbenchCPP testbenchPrinter = new TestbenchCPP(instance, typePrinter, getOptions());
		result.merge(
				FilesManager.writeFile(testbenchPrinter.getContent(), tbSrcPath, instance.getSimpleName() + "_tb.cpp"));

		// -- Simulation Path
		String simulationInstancePath = simulationPath + File.separator + instance.getSimpleName();
		createDirectory(simulationInstancePath);

		// -- Modelsim TCL script
		ModelsimTCLPrinter modelsimTCLPrinter = new ModelsimTCLPrinter(instance, getOptions());
		result.merge(FilesManager.writeFile(modelsimTCLPrinter.getContent(), simulationInstancePath,
				"sim_" + instance.getSimpleName() + ".tcl"));

		// -- Testbench Verilog
		TestbechVerilog testbenchVerilogPrinter = new TestbechVerilog(instance, getOptions());
		result.merge(FilesManager.writeFile(testbenchVerilogPrinter.getContent(), simulationInstancePath,
				instance.getSimpleName() + "_tb.v"));

		// -- Fifo for tesbench
		FifoPrinter fifoPrinter = new FifoPrinter();
		result.merge(FilesManager.writeFile(fifoPrinter.getContent(), simulationInstancePath, "fifo.v"));

		// -- Copy Generated Files
		copyVerilogFiles(instance.getSimpleName(), simulationInstancePath);
		copyVerilogFiles(instance.getSimpleName(), simulationNetworkPath);

		return result;
	}

	/**
	 * Launch Vivado HLS for a given Cpp file
	 * 
	 * @param name
	 * @param kind
	 * @return
	 */
	protected boolean launchHLS(String name, String kind) {
		if (launchHLS) {
			List<String> commands = new ArrayList<String>();

			// -- Vivado HLS executable
			commands.add(C_VIVADO_HLS);

			// -- -f Flag
			commands.add("-f");
			// -- TCL Script
			String tclScript = name + "_hls.tcl";
			commands.add(tclScript);

			ProcessBuilder builder = new ProcessBuilder(commands);

			// -- Working directory
			String directory = getProjectPath(name);
			builder.directory(new File(directory));

			// -- Environmental variables
			Map<String, String> env = builder.environment();
			if (C_XILINXD_LICENSE_FILE != null) {
				env.put("XILINXD_LICENSE_FILE", C_XILINXD_LICENSE_FILE);
			}

			if (C_XILINXD_LICENSE_FILE != null) {
				env.put("LM_LICENSE_FILE", C_LM_LICENSE_FILE);
			}

			try {
				Process p = builder.start();
				long t0 = System.currentTimeMillis();
				OrccLogger.traceln("Synthesizing " + kind + ": " + name);
				try {
					BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String line;
					while ((line = reader.readLine()) != null) {
						System.out.println(line);
					}
					p.waitFor();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				int rValue = p.exitValue();
				if (rValue == 0) {
					long t1 = System.currentTimeMillis();
					OrccLogger.traceln("\t - Synthesized in: " + (float) (t1 - t0) / 1000 + "s");
					// -- Copy the generated Verilog files
					String verilogPath = getSynthesizedVerilogPath(name);
					File verilogDir = new File(verilogPath);
					if (verilogDir.exists()) {
						for (File file : verilogDir.listFiles()) {
							File newFile = new File(rtlPath + File.separator + file.getName());
							Files.copy(file, newFile);
						}
					}
					return true;
				}
			} catch (IOException e) {
				OrccLogger.severeln("Error on Launching Vivado HLS: " + e.getMessage());
				e.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * Get the verilog synthesis folder name from the Vivado HLS solution
	 * 
	 * @param name
	 * @return
	 */
	private String getSynthesizedVerilogPath(String name) {
		return projectsPath + File.separator + name + File.separator + "hls_" + name + File.separator + name
				+ "_solution" + File.separator + "syn" + File.separator + "verilog";
	}

	private String getProjectPath(String name) {
		return projectsPath + File.separator + name;
	}

	/**
	 * Copy from a base name (e.g. actor name) to a destination folder
	 * 
	 * @param name
	 * @param destiantion
	 * @return
	 */
	private boolean copyVerilogFiles(String name, String destiantion) {
		String verilogPath = getSynthesizedVerilogPath(name);
		File verilogDir = new File(verilogPath);
		if (verilogDir.exists()) {
			for (File file : verilogDir.listFiles()) {
				if (file.getName().contains(name)) {
					File newFile = new File(destiantion + File.separator + file.getName());
					try {
						Files.copy(file, newFile);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return true;
	}

}
