/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


package exelixi.hls.vivadohls.backend.utils

import java.util.HashSet
import java.util.Set
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port

import static exelixi.hls.backend.Constants.ACTOR_COMPLEX_GUARDS

class TemplatesUtils {
	static def Set<Port> getPortsWithCount(Instance instance) {
		var Set<Port> ports = new HashSet<Port>
		val Actor actor = instance.getActor
		var boolean isActorComplex = false

		if (actor.hasAttribute(ACTOR_COMPLEX_GUARDS)) {
			isActorComplex = actor.getAttribute(ACTOR_COMPLEX_GUARDS).getObjectValue() as Boolean;
		}

		for (action : actor.actions) {
			if (!isActorComplex) {
				for (port : action.inputPattern.ports) {
					if (action.inputPattern.numTokensMap.get(port) > 1) {
						ports.add(port)
					}
				}
			}
			for (port : action.outputPattern.ports) {
				if (action.outputPattern.numTokensMap.get(port) > 1) {
					ports.add(port)
				}
			}
		}
		return ports
	}

	static def getPath(String path) {
		var String tmp = path
		// the vivado script does not like windows path!
		if (System.getProperty("os.name").toLowerCase().contains("windows")) {
			tmp = path.replaceAll("\\\\", "/");
		}

		return tmp;
	}

	static def String getInstanceName(Instance instance) {
		return if (!instance.getActor.isNative)
			"id_" + instance.number + "_" + instance.simpleName
		else
			instance.simpleName
	}

	static def String getNetworkPortName(Port port) {
		return "n_" + port.name
	}

	static def String getPortName(Port port, boolean isBitAccurate) {
		return port.name + if(isBitAccurate) "_V_V" else "_V"
	}

	static def String getPortNameIndex(Port port, Integer index, boolean isBitAccurate) {
		return port.name + "_" + index + if(isBitAccurate) "_V_V" else "_V"
	}

	static def String getPortReaderName(Port port, boolean isBitAccurate) {
		return port.name + "_pr" + if(isBitAccurate) "_V_V" else "_V"
	}

	static def String getPortReaderNameIndex(Port port, Integer index, boolean isBitAccurate) {
		return port.name + "_" + index + "_pr" + if(isBitAccurate) "_V_V" else "_V"
	}

	static def String getPortWritterName(Port port, boolean isBitAccurate) {
		return port.name + "_pw" + if(isBitAccurate) "_V_V" else "_V"
	}
}
