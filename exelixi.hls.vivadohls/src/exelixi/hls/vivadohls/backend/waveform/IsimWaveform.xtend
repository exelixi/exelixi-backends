/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.hls.vivadohls.backend.waveform

import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import java.util.Map
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port

import static exelixi.hls.backend.Constants.BIT_ACCURATE_TYPES

class IsimWaveform {

	Network network

	String name

	String dutHierarchy

	Integer groupCounter
	
	boolean typeAccuracy

	new(Network network, Map<String, Object> options) {
		this.network = network
		name = network.simpleName
		dutHierarchy = network.simpleName + "_tb" + "/dut/" + name + "_i" + "/" + name
		if(options.containsKey(BIT_ACCURATE_TYPES)){
			this.typeAccuracy = options.get(BIT_ACCURATE_TYPES) as Boolean
		}else{
			this.typeAccuracy = false
		}
		groupCounter = 0
	}

	def getContent() {
		'''
			<?xml version="1.0" encoding="UTF-8"?>
			<wave_config>
			«state»
			«dbRefList»
			«wvObjectSize(2)»
			«createGroupTB»
			«createGroupDUT»
			</wave_config>
		'''
	}

	private def getState() {
		'''
			<wave_state>
			</wave_state>
		'''
	}

	private def dbRefList() {
		'''
			<db_ref_list>
			     <db_ref path="«name»_tb_func_synth.wdb" id="1">
			        <top_modules>
			           <top_module name="«name»_tb" />
			           <top_module name="glbl" />
			        </top_modules>
			     </db_ref>
			  </db_ref_list>
		'''
	}

	private def wvObjectSize(int size) {
		'''<WVObjectSize size="«size»" />'''
	}

	private def wvObject(String type, String hierarchy, String name) {
		'''
			<wvobject type="«type»" fp_name="/«hierarchy»/«name»">
				<obj_property name="ElementShortName">«name»</obj_property>
				<obj_property name="ObjectShortName">«name»</obj_property>
			</wvobject>
		'''
	}
	
	private def wvObjectArrayPort(String hierarchy, Port port, boolean isInput, boolean hasExtension) {
		'''
			<wvobject type="array" fp_name="/«hierarchy»/«port.name»«getPortExtension(hasExtension)»«IF isInput»_dout«ELSE»_din«ENDIF»">
				<obj_property name="ElementShortName">«port.name»«getPortExtension(hasExtension)»«IF isInput»_dout«ELSE»_din«ENDIF»[«port.type.sizeInBits-1»:0]</obj_property>
				<obj_property name="ObjectShortName">«port.name»«getPortExtension(hasExtension)»«IF isInput»_dout«ELSE»_din«ENDIF»[«port.type.sizeInBits-1»:0]</obj_property>
				<obj_property name="Radix">«IF port.type.isUint»UNSIGNEDDECRADIX«ELSEIF port.type.isInt»SIGNEDDECRADIX«ELSE»UNSIGNEDDECRADIX«ENDIF»</obj_property>
			</wvobject>
		'''
	}
	
	private def wvObjectArrayPort(String hierarchy, Port port, String nameExtension, boolean isInput, boolean hasExtension) {
		'''
			<wvobject type="array" fp_name="/«hierarchy»/«port.name»_«nameExtension»«getPortExtension(hasExtension)»«IF isInput»_dout«ELSE»_din«ENDIF»">
				<obj_property name="ElementShortName">«port.name»_«nameExtension»«getPortExtension(hasExtension)»«IF isInput»_dout«ELSE»_din«ENDIF»[«port.type.sizeInBits-1»:0]</obj_property>
				<obj_property name="ObjectShortName">«port.name»_«nameExtension»«getPortExtension(hasExtension)»«IF isInput»_dout«ELSE»_din«ENDIF»[«port.type.sizeInBits-1»:0]</obj_property>
				<obj_property name="Radix">«IF port.type.isUint»UNSIGNEDDECRADIX«ELSEIF port.type.isInt»SIGNEDDECRADIX«ELSE»UNSIGNEDDECRADIX«ENDIF»</obj_property>
			</wvobject>
		'''
	}
	
	private def wvObjectArray(String hierarchy, String name, Integer nbBits) {
		'''
			<wvobject type="array" fp_name="/«hierarchy»/«name»">
				<obj_property name="ElementShortName">«name»[«nbBits»:0]</obj_property>
				<obj_property name="ObjectShortName">«name»[«nbBits»:0]</obj_property>
				<obj_property name="Radix">UNSIGNEDDECRADIX</obj_property>
			</wvobject>
		'''
	}

	///////////////////////////////////////////////////////////////////////////
	// -- TB

	private def createGroupTB() {
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">TB</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«createGroupCLK»
				«createGroupRESET»
				«createGrouprCONTROL»
				«createGroupInputOutputTB»
			</wvobject>
		'''
	}

	private def createGroupCLK() {
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">CLK</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«wvObject("other", name+"_tb", "cycle")»
				«wvObject("logic", name+"_tb", "clock")»
			</wvobject>
		'''
	}
	
	private def createGroupRESET(){
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">RESET</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«wvObject("logic", name+"_tb", "reset_n")»
			</wvobject>
		'''
	}
	
	private def createGrouprCONTROL(){
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">CONTORL</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«wvObject("logic", name+"_tb", "start")»
			</wvobject>
		'''
	}
	
	private def createGroupInputOutputTB() {
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">INPUTS</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«FOR input : network.inputs»
					«createGroupInputDUT(input)»
				«ENDFOR»
			</wvobject>
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">OUTPUTS</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«FOR output : network.outputs»
					«createGroupOutputDUT(output)»
				«ENDFOR»
			</wvobject>
		'''
	}
	
	
	private def createGroupInputDUT(Port port){
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">«port.name»</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«wvObjectArrayPort(name+"_tb", port, true, false)»
				«wvObject("logic", name + "_tb", port.name + "_read")»
				«wvObject("logic", name + "_tb", port.name + "_empty_n")»
			</wvobject>
		'''
	}
	
	private def createGroupOutputDUT(Port port){
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">«port.name»</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«wvObjectArray(name + "_tb", port.name +"_token_counter", 31)»
				«wvObjectArray(name + "_tb", port.name +"_exp_value", port.type.sizeInBits - 1)»
				«wvObjectArrayPort(name+"_tb", port, false, false)»
				«wvObject("logic", name + "_tb", port.name + "_write")»
				«wvObject("logic", name + "_tb", port.name + "_full_n")»
			</wvobject>
		'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// -- DUT
	
	private def createGroupDUT() {
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">DUT</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«createGroupPortReaders»
				«createGroupInstances»
				«createGroupPortWriters»
			</wvobject>
		'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// -- Group PortReader
	
	private def createGroupPortReaders(){
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">PortReaders</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«FOR port : network.inputs»
					«createGroupPortReader(port)»
				«ENDFOR»
			</wvobject>
		'''
	}
	
	private def createGroupPortReader(Port port){
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">«port.name»</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«getWvObjectClkReset("PortReader_" + port.name)»
				«getGroupApCtrl("PortReader_" + port.name)»
				<wvobject type="group" fp_name="«groupCounter++»">
					<obj_property name="label">INPUT</obj_property>
					<obj_property name="DisplayName">label</obj_property>
					«wvObjectArrayPort(dutHierarchy + "/" + "PortReader_" + port.name, port, true, true)»
					«wvObject("logic",dutHierarchy + "/" + "PortReader_" + port.name, port.name + getPortExtension(true) + "_read" )»
					«wvObject("logic",dutHierarchy + "/" + "PortReader_" + port.name, port.name + getPortExtension(true) + "_empty_n")»
				</wvobject>
				<wvobject type="group" fp_name="«groupCounter++»">
					<obj_property name="label">OUTPUT«IF port.outgoing.size > 1»S«ENDIF»</obj_property>
					<obj_property name="DisplayName">label</obj_property>
					«IF port.outgoing.size > 1»
						«FOR int i : 0 .. port.outgoing.size - 1»
							«wvObjectArrayPort(dutHierarchy + "/" + "PortReader_" + port.name, port, "pr" + i, false, true)»
							«wvObject("logic",dutHierarchy + "/" + "PortReader_" + port.name, port.name + "_pr_" + i + getPortExtension(true) + "_write" )»
							«wvObject("logic",dutHierarchy + "/" + "PortReader_" + port.name, port.name + "_pr_" + i + getPortExtension(true) + "_full_n")»
						«ENDFOR»
					«ELSE»
						«wvObjectArrayPort(dutHierarchy + "/" + "PortReader_" + port.name, port, "pr", false, true)»
						«wvObject("logic",dutHierarchy + "/" + "PortReader_" + port.name, port.name + "_pr" + getPortExtension(true) + "_write" )»
						«wvObject("logic",dutHierarchy + "/" + "PortReader_" + port.name, port.name + "_pr" + getPortExtension(true) + "_full_n")»
					«ENDIF»
				</wvobject>
			</wvobject>
		'''
	}
	
	
	
	
	///////////////////////////////////////////////////////////////////////////
	// -- Group Instance
	
	private def createGroupInstances(){
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">Instances</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«FOR instance : network.children.filter(typeof(Instance))»
					«createGroupInstance(instance)»
				«ENDFOR»
			</wvobject>
		'''
	}
	
	private def createGroupInstance(Instance instance){
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">«instance.simpleName»</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«getWvObjectClkReset(TemplatesUtils.getInstanceName(instance))»
				«getGroupApCtrl(TemplatesUtils.getInstanceName(instance))»
				<wvobject type="group" fp_name="«groupCounter++»">
					<obj_property name="label">INPUT«IF instance.getActor.inputs.size > 1»S«ENDIF»</obj_property>
					<obj_property name="DisplayName">label</obj_property>
					«FOR port : instance.getActor.inputs»
						«createGroupInstanceIO(instance, port, true)»
					«ENDFOR»
				</wvobject>
				<wvobject type="group" fp_name="«groupCounter++»">
					<obj_property name="label">OUTPUT«IF instance.getActor.outputs.size > 1»S«ENDIF»</obj_property>
					<obj_property name="DisplayName">label</obj_property>
					«FOR port : instance.getActor.outputs»
						«createGroupInstanceIO(instance, port, false)»
					«ENDFOR»
				</wvobject>
			</wvobject>
		'''
	}
	
	
	private def createGroupInstanceIO(Instance instance, Port port, boolean isInput){
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">«port.name»</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«IF isInput»
					«wvObjectArrayPort(dutHierarchy + "/" + TemplatesUtils.getInstanceName(instance), port, true, true)»
					«wvObject("logic",dutHierarchy, TemplatesUtils.getInstanceName(instance) + "/" + port.name + getPortExtension(true) + "_read")»
					«wvObject("logic",dutHierarchy, TemplatesUtils.getInstanceName(instance) + "/" + port.name + getPortExtension(true) + "_empty_n")»
				«ELSE»
					«IF instance.outgoingPortMap.get(port).size > 1»
						«FOR int i : 0 .. instance.outgoingPortMap.get(port).size - 1»
							«wvObjectArrayPort(dutHierarchy + "/" + TemplatesUtils.getInstanceName(instance), port, isInput, true)»
							«wvObject("logic",dutHierarchy, TemplatesUtils.getInstanceName(instance) + "/" + port.name + "_" + i + getPortExtension(true) + "_write" )»
							«wvObject("logic",dutHierarchy, TemplatesUtils.getInstanceName(instance) + "/" + port.name + "_" + i + getPortExtension(true) + "_full_n")»
						«ENDFOR»
					«ELSE»
						«wvObjectArrayPort(dutHierarchy + "/" + TemplatesUtils.getInstanceName(instance), port, isInput, true)»
						«wvObject("logic",dutHierarchy, TemplatesUtils.getInstanceName(instance) + "/" + port.name + getPortExtension(true) + "_write" )»
						«wvObject("logic",dutHierarchy, TemplatesUtils.getInstanceName(instance) + "/" + port.name + getPortExtension(true) + "_full_n" )»
					«ENDIF»
				«ENDIF»
			</wvobject>
		'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// -- Group Writer
	
	private def createGroupPortWriters(){
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">PortWriters</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«FOR port : network.outputs»
					«createGroupPortWriter(port)»
				«ENDFOR»
			</wvobject>
		'''
	}
	
	private def createGroupPortWriter(Port port){
		groupCounter++;
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">«port.name»</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				«getWvObjectClkReset("PortWriter_" + port.name)»
				«getGroupApCtrl("PortWriter_" + port.name)»
				<wvobject type="group" fp_name="«groupCounter++»">
					<obj_property name="label">INPUT</obj_property>
					<obj_property name="DisplayName">label</obj_property>
					«wvObjectArrayPort(dutHierarchy + "/" + "PortWriter_" + port.name, port, "pw", true, true)»
					«wvObject("logic",dutHierarchy + "/" + "PortWriter_" + port.name, port.name + "_pw" + getPortExtension(true) + "_read" )»
					«wvObject("logic",dutHierarchy + "/" + "PortWriter_" + port.name, port.name + "_pw" + getPortExtension(true) + "_empty_n")»
				</wvobject>
				<wvobject type="group" fp_name="«groupCounter++»">
					<obj_property name="label">OUTPUT</obj_property>
					<obj_property name="DisplayName">label</obj_property>
					«wvObjectArrayPort(dutHierarchy + "/" + "PortWriter_" + port.name, port, false, true)»
					«wvObject("logic",dutHierarchy + "/" + "PortWriter_" + port.name, port.name + getPortExtension(true) + "_write" )»
					«wvObject("logic",dutHierarchy + "/" + "PortWriter_" + port.name, port.name + getPortExtension(true) + "_full_n")»
				</wvobject>
			</wvobject>
		'''
	}
	

	// -- Helper Methods
	private def getPortExtension(boolean isInstance){
		'''«IF isInstance »«IF typeAccuracy»_V_V«ELSE»_V«ENDIF»«ENDIF»'''
	}
	
	private def getWvObjectClkReset(String name){
		'''
			<wvobject type="logic" fp_name="/«dutHierarchy»/«name»/ap_clk">
			   <obj_property name="ElementShortName">ap_clk</obj_property>
			   <obj_property name="ObjectShortName">ap_clk</obj_property>
			</wvobject>
			<wvobject type="logic" fp_name="/«dutHierarchy»/«name»/ap_rst_n">
			   <obj_property name="ElementShortName">ap_rst_n</obj_property>
			   <obj_property name="ObjectShortName">ap_rst_n</obj_property>
			</wvobject>
		'''
	}
	
	private def getGroupApCtrl(String name){
		groupCounter++
		'''
			<wvobject type="group" fp_name="«groupCounter»">
				<obj_property name="label">ap_ctrl</obj_property>
				<obj_property name="DisplayName">label</obj_property>
				<obj_property name="isExpanded"></obj_property>
				<wvobject type="logic" fp_name="/«dutHierarchy»/«name»/ap_start">
					<obj_property name="ElementShortName">ap_start</obj_property>
					<obj_property name="ObjectShortName">ap_start</obj_property>
				</wvobject>
				<wvobject type="logic" fp_name="/«dutHierarchy»/«name»/ap_done">
					<obj_property name="ElementShortName">ap_done</obj_property>
					<obj_property name="ObjectShortName">ap_done</obj_property>
				</wvobject>
				<wvobject type="logic" fp_name="/«dutHierarchy»/«name»/ap_idle">
					<obj_property name="ElementShortName">ap_idle</obj_property>
					<obj_property name="ObjectShortName">ap_idle</obj_property>
				</wvobject>
				<wvobject type="logic" fp_name="/«dutHierarchy»/«name»/ap_ready">
					<obj_property name="ElementShortName">ap_ready</obj_property>
					<obj_property name="ObjectShortName">ap_ready</obj_property>
				</wvobject>
			</wvobject>
		'''
	}

}
