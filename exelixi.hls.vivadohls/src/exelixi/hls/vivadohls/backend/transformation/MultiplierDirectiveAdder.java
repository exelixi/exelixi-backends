/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.transformation;

import java.util.ArrayList;
import java.util.List;

import net.sf.orcc.ir.ExprBinary;
import net.sf.orcc.ir.Expression;
import net.sf.orcc.ir.InstAssign;
import net.sf.orcc.ir.OpBinary;
import net.sf.orcc.ir.Procedure;
import net.sf.orcc.ir.util.AbstractIrVisitor;

/**
 * After three address code transformation add a directive for using a DSP
 * multiplier
 * 
 * @author Endri Bezati
 *
 */
public class MultiplierDirectiveAdder extends AbstractIrVisitor<Void> {

	private List<String> mulDirectives;

	public MultiplierDirectiveAdder() {
		super(true);
	}

	@Override
	public Void caseInstAssign(InstAssign assign) {
		Expression expr = assign.getValue();
		String variableName = assign.getTarget().getVariable().getName();
		if (expr.isExprBinary()) {
			ExprBinary exprBin = (ExprBinary) expr;
			if (exprBin.getOp() == OpBinary.TIMES) {
				if (!exprBin.getE1().isExprInt() && !exprBin.getE2().isExprInt()) {
					mulDirectives.add(createDirecrtive(variableName));
				}
			}
		}
		return null;
	}

	@Override
	public Void caseProcedure(Procedure procedure) {
		mulDirectives = new ArrayList<String>();
		super.caseProcedure(procedure);

		if (!mulDirectives.isEmpty()) {
			if (procedure.hasAttribute("directives")) {
				@SuppressWarnings("unchecked")
				List<String> directives = (List<String>) procedure.getAttribute("directive");
				directives.addAll(mulDirectives);
			} else {
				procedure.setAttribute("directives", mulDirectives);
			}
		}

		return null;
	}

	private String createDirecrtive(String variable) {
		return "#pragma HLS RESOURCE" + " variable=" + variable + " core=Mul";
	}

}
