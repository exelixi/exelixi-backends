/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.transformation;

import net.sf.orcc.df.Actor;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.util.DfVisitor;

public class PrintIO extends DfVisitor<Void> {

	@Override
	public Void caseActor(Actor actor) {
		
		System.out.println(actor.getSimpleName()+";;");
		
		
		for(int i=0; i < Math.max(actor.getInputs().size(), actor.getOutputs().size()); i++){
			String input = "";
			String output = "";
			
			if(actor.getInputs().size() > i){
				input = actor.getInputs().get(i).getName() + ";" + actor.getInputs().get(i).getType(); 
			}
			
			if(actor.getOutputs().size() > i){
				output = actor.getOutputs().get(i).getName() + ";" + actor.getOutputs().get(i).getType(); 
			}
			System.out.println(";"+input+";"+output+";");
		}
		
	
		System.out.println("");
		
		return null;
	}

	@Override
	public Void caseNetwork(Network network) {
		System.out.println(network.getSimpleName()+";;");
		
		for(int i=0; i < Math.max(network.getInputs().size(), network.getOutputs().size()); i++){
			String input = "";
			String output = "";
			
			if(network.getInputs().size() > i){
				input = network.getInputs().get(i).getName() + ";" + network.getInputs().get(i).getType(); 
			}
			
			if(network.getOutputs().size() > i){
				output = network.getOutputs().get(i).getName() + ";" + network.getOutputs().get(i).getType(); 
			}
			System.out.println(";"+input+";"+output+";");
		}
		
	
		System.out.println("");
		
		return super.caseNetwork(network);
	}
	
	
	
}
