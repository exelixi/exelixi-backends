/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


package exelixi.hls.vivadohls.backend.transformation;

import static exelixi.hls.backend.Constants.ACTION_EXTERNAL_VARIABLES;
import static exelixi.hls.backend.Constants.ACTOR_EXTERNAL_VARIABLES;
import static exelixi.hls.backend.Constants.AVAILABLE_DDR_SPACE;
import static exelixi.hls.backend.Constants.DEVICE;
import static exelixi.hls.backend.Constants.INSTANCE_EXTERNAL_VARIABLES;
import static exelixi.hls.backend.Constants.NETWORK_EXTERNAL_VARIABLES;
import static exelixi.hls.backend.Constants.PROCEDURE_EXTERNAL_VARIABLES;
import static exelixi.hls.backend.Constants.VARIABLE_BASE_ADDRESS;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import exelixi.hls.devices.XilinxDevice;
import net.sf.orcc.df.Action;
import net.sf.orcc.df.Actor;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.graph.Vertex;
import net.sf.orcc.ir.Def;
import net.sf.orcc.ir.ExprInt;
import net.sf.orcc.ir.Expression;
import net.sf.orcc.ir.InstLoad;
import net.sf.orcc.ir.InstStore;
import net.sf.orcc.ir.IrFactory;
import net.sf.orcc.ir.OpBinary;
import net.sf.orcc.ir.Procedure;
import net.sf.orcc.ir.Type;
import net.sf.orcc.ir.TypeList;
import net.sf.orcc.ir.Use;
import net.sf.orcc.ir.Var;
import net.sf.orcc.ir.util.AbstractIrVisitor;
import net.sf.orcc.ir.util.IrUtil;
import net.sf.orcc.util.util.EcoreHelper;

/**
 * 
 * @author Endri Bezati
 */
public class ExternalArray extends DfVisitor<Void> {

	private XilinxDevice device;

	/**
	 * List of actor External State Variables
	 */

	private List<Var> actorExternalVariables;

	private Map<Actor, List<Var>> actorsExternalVariablesMap;
	private Map<Instance, List<Var>> instancesExternalVariablesMap;

	private class InnerVisitor extends AbstractIrVisitor<Void> {

		private InnerVisitor() {
			super(true);
		}

		private void toOneDimIndex(List<Expression> indexes, Type listType) {

			List<Expression> restOfIndex = new ArrayList<Expression>(indexes);
			// Get the Dimension for the rest of Indexes
			List<Integer> listDim = listType.getDimensions();

			Expression restIndex = null;
			// Index in OpenForge is represented like a 32bit Integer
			Type exrpType = IrFactory.eINSTANCE.createTypeInt(32);
			restIndex = restOfIndex.get(0);
			for (int i = 1; i < listDim.size(); i++) {
				ExprInt exprDim = IrFactory.eINSTANCE.createExprInt(listDim.get(i));
				restIndex = IrFactory.eINSTANCE.createExprBinary(restIndex, OpBinary.TIMES, exprDim, exrpType);
				restIndex = IrFactory.eINSTANCE.createExprBinary(restIndex, OpBinary.PLUS, restOfIndex.get(i),
						exrpType);
			}
			IrUtil.delete(indexes);
			indexes.add(restIndex);

		}

		@Override
		public Void caseProcedure(Procedure procedure) {

			super.caseProcedure(procedure);

			return null;
		}

		@Override
		public Void caseInstLoad(InstLoad load) {
			List<Expression> indexes = load.getIndexes();
			Var source = load.getSource().getVariable();
			if (actorExternalVariables.contains(source)) {
				if (indexes.size() > 1) {
					toOneDimIndex(indexes, IrUtil.copy(load.getSource().getVariable().getType()));
				}

			}
			return null;
		}

		@Override
		public Void caseInstStore(InstStore store) {
			List<Expression> indexes = store.getIndexes();
			Var target = store.getTarget().getVariable();
			if (actorExternalVariables.contains(target)) {
				if (indexes.size() > 1) {
					toOneDimIndex(indexes, IrUtil.copy(store.getTarget().getVariable().getType()));
				}
			}
			return null;
		}

	}

	public ExternalArray(Map<String, Object> options) {
		device = (XilinxDevice) options.get(DEVICE);
	}

	@Override
	public Void caseActor(Actor actor) {
		actorExternalVariables = new ArrayList<Var>();
		for (Var var : actor.getStateVars()) {
			if (var.hasAttribute("external")) {
				actorExternalVariables.add(var);

				// -- Search for external variable definitions in actions
				for (Def def : var.getDefs()) {
					Action action = EcoreHelper.getContainerOfType(def, Action.class);
					Procedure procedure = EcoreHelper.getContainerOfType(def, Procedure.class);
					if (action != null) {
						if (action.hasAttribute(ACTION_EXTERNAL_VARIABLES)) {
							@SuppressWarnings("unchecked")
							List<Var> variables = (List<Var>) action.getAttribute(ACTION_EXTERNAL_VARIABLES)
									.getObjectValue();
							if (!variables.contains(var)) {
								variables.add(var);
							}
						} else {
							List<Var> variables = new ArrayList<Var>();
							variables.add(var);
							action.setAttribute(ACTION_EXTERNAL_VARIABLES, variables);
						}
					} else if (action == null && procedure != null) {
						if (procedure.hasAttribute(PROCEDURE_EXTERNAL_VARIABLES)) {
							@SuppressWarnings("unchecked")
							List<Var> variables = (List<Var>) procedure.getAttribute(PROCEDURE_EXTERNAL_VARIABLES)
									.getObjectValue();
							if (!variables.contains(var)) {
								variables.add(var);
							}
						} else {
							List<Var> variables = new ArrayList<Var>();
							variables.add(var);
							procedure.setAttribute(PROCEDURE_EXTERNAL_VARIABLES, variables);
						}
					}
				}

				// -- Search for external variable uses in actions
				for (Use use : var.getUses()) {
					Action action = EcoreHelper.getContainerOfType(use, Action.class);
					Procedure procedure = EcoreHelper.getContainerOfType(use, Procedure.class);
					if (action != null) {
						if (action.hasAttribute(ACTION_EXTERNAL_VARIABLES)) {
							@SuppressWarnings("unchecked")
							List<Var> variables = (List<Var>) action.getAttribute(ACTION_EXTERNAL_VARIABLES)
									.getObjectValue();
							if (!variables.contains(var)) {
								variables.add(var);
							}
						} else {
							List<Var> variables = new ArrayList<Var>();
							variables.add(var);
							action.setAttribute(ACTION_EXTERNAL_VARIABLES, variables);
						}
					} else if (action == null && procedure != null) {
						if (procedure.hasAttribute(PROCEDURE_EXTERNAL_VARIABLES)) {
							@SuppressWarnings("unchecked")
							List<Var> variables = (List<Var>) procedure.getAttribute(PROCEDURE_EXTERNAL_VARIABLES)
									.getObjectValue();
							if (!variables.contains(var)) {
								variables.add(var);
							}
						} else {
							List<Var> variables = new ArrayList<Var>();
							variables.add(var);
							procedure.setAttribute(PROCEDURE_EXTERNAL_VARIABLES, variables);
						}
					}
				}

			}
		}

		if (!actorExternalVariables.isEmpty()) {
			// -- Visit All loads and replace the indexes to a single dimension
			// one

			for (Procedure procedure : actor.getProcs()) {
				InnerVisitor innerVisitor = new InnerVisitor();
				innerVisitor.doSwitch(procedure);
			}

			for (Action action : actor.getActions()) {
				doSwitch(action);
			}
			actor.setAttribute(ACTOR_EXTERNAL_VARIABLES, actorExternalVariables);
			actorsExternalVariablesMap.put(actor, actorExternalVariables);
		}

		return null;
	}

	@Override
	public Void caseNetwork(Network network) {
		instancesExternalVariablesMap = new TreeMap<Instance, List<Var>>(new Comparator<Instance>() {
			@Override
			public int compare(Instance o1, Instance o2) {
				if (o1.getSimpleName().equals(o2.getSimpleName())) {
					return 0;
				}
				return o1.getSimpleName().compareTo(o2.getSimpleName());
			}
		});
		
		actorsExternalVariablesMap = new TreeMap<Actor, List<Var>>(new Comparator<Actor>() {
			@Override
			public int compare(Actor o1, Actor o2) {
				if (o1.getSimpleName().equals(o2.getSimpleName())) {
					return 0;
				}
				return o1.getSimpleName().compareTo(o2.getSimpleName());
			}
		});

		for (Vertex vertex : network.getVertices()) {
			if (!(vertex instanceof Port)) {
				doSwitch(vertex);
			}
		}

		// -- Calculate Base-addresses for External Arrays
		calculateBaseAddresses(network);

		if (!actorsExternalVariablesMap.isEmpty()) {
			network.setAttribute("networkActorsExternalVars", actorsExternalVariablesMap);
		}

		if (!instancesExternalVariablesMap.isEmpty()) {
			network.setAttribute(NETWORK_EXTERNAL_VARIABLES, instancesExternalVariablesMap);
		}

		return null;
	}

	private Void calculateBaseAddresses(Network network) {

		Integer ddrLength = device.getDDRLength();

		for (Actor actor : actorsExternalVariablesMap.keySet()) {
			for (Var variable : actorsExternalVariablesMap.get(actor)) {
				Type type = variable.getType();
				if (type.isList()) {
					TypeList typeList = (TypeList) type;
					Type innnerMostType = typeList.getInnermostType();
					Integer flastListDepth = getFlatListDepth(typeList);
					Integer requiredSizeInBytes = 0;
					if (innnerMostType.getSizeInBits() <= 8) {
						requiredSizeInBytes = flastListDepth;
					} else if (innnerMostType.getSizeInBits() <= 16) {
						requiredSizeInBytes = flastListDepth * 2;
					} else if (innnerMostType.getSizeInBits() <= 32) {
						requiredSizeInBytes = flastListDepth * 4;
					} else if (innnerMostType.getSizeInBits() <= 64) {
						requiredSizeInBytes = flastListDepth * 8;
					} else {
						// -- Unsupported size
					}

					ddrLength -= requiredSizeInBytes;
					Integer baseAddress = ddrLength;
					variable.setAttribute(VARIABLE_BASE_ADDRESS, baseAddress);
				}
			}
		}

		Integer freeAvailableSpaceLeft = ddrLength - 1;
		network.setAttribute(AVAILABLE_DDR_SPACE, freeAvailableSpaceLeft);
		return null;
	}

	private int getFlatListDepth(Type type) {
		int r = 1;
		if (type.isList()) {
			for (int dim : type.getDimensions()) {
				r = r * dim;
			}
		}
		return r;
	}

	@Override
	public Void caseInstance(Instance instance) {
		Actor actor = instance.getAdapter(Actor.class);
		if (actor != null) {
			doSwitch(actor);

			if (!actorExternalVariables.isEmpty()) {
				actor.setAttribute(INSTANCE_EXTERNAL_VARIABLES, actorExternalVariables);
				instancesExternalVariablesMap.put(instance, actorExternalVariables);
			}
		}
		return null;
	}

	@Override
	public Void caseAction(Action action) {
		InnerVisitor innerVisitor = new InnerVisitor();

		innerVisitor.doSwitch(action.getScheduler());

		innerVisitor.doSwitch(action.getBody());

		return null;
	}

}
