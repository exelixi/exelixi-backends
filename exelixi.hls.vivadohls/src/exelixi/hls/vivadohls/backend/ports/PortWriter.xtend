/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.hls.vivadohls.backend.ports

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.Map
import net.sf.orcc.df.Port

import static exelixi.hls.backend.Constants.AXI_STREAM_ACTOR_COMMUNICATION

/**
 * Vivado HLS Stream port Writer with fanout
 * @author Endri Bezati
 */
class PortWriter extends HLSTemplate {

	Port port;
	
	TypePrinterVisitor typePrinter
	
	Boolean axiStreamPort
	
	Boolean axiStreamActorCommunication

	new(Port port, TypePrinterVisitor typePrinter, Map<String, Object> options,  Boolean axiStreamPort) {
		super(typePrinter)
		this.typePrinter = typePrinter
		this.port = port
		this.axiStreamPort = axiStreamPort
		this.axiStreamActorCommunication = options.get(AXI_STREAM_ACTOR_COMMUNICATION) as Boolean
		
	}

	def getContent() {
		'''
			«getFileHeader("Exelixi for Vivado HLS,  Port Writer Code Generation, Output port: " + port.name)»
			«IF axiStreamPort»
				#include <ap_axi_sdata.h>
			«ELSE»
				#include <«typePrinter.intIncludeHeaderFile»>
			«ENDIF»
			#include <«typePrinter.channelIncludeHeaderFile»>
			
			void «IF axiStreamPort»AxiStream«ENDIF»PortWriter_«port.name»(«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»_pw, «typePrinter.channelClassName»< «IF axiStreamPort»ap_axiu<«port.type.sizeInBits»,4,5,5>«ELSE»«port.type.doSwitch» «ENDIF» > &«port.name»){
			#pragma HLS INTERFACE ap_ctrl_hs port=return
			«IF axiStreamActorCommunication»
				#pragma HLS INTERFACE axis port=«port.name»_pw
				#pragma HLS INTERFACE axis port=«port.name»
			«ENDIF»
				if(!«port.name»_pw.empty() && !«port.name».full()){
				«IF axiStreamPort»
					«port.type.doSwitch» data = «port.name»_pw.read();
					ap_axiu<«port.type.sizeInBits»,4,5,5> value;
					value.data = data;
					value.strb = -1;
					value.keep = 15; //e.strb;
					value.user = 0;
					value.last = 1;
					value.id = 0;
					value.dest = 0;
					«port.name».write(value);
				«ELSE»
					«port.name».write(«port.name»_pw.read());
				«ENDIF»
				}
				
				return;
			}
		'''
	}
}
