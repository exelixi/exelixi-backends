/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.hls.vivadohls.backend.ports

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.Map
import net.sf.orcc.df.Port

import static exelixi.hls.backend.Constants.AXI_STREAM_ACTOR_COMMUNICATION

/**
 * Vivado HLS Stream port Reader with fanout
 * @author Endri Bezati
 */
class PortReader extends HLSTemplate {

	Port port;

	TypePrinterVisitor typePrinter

	int fanoutNumber

	Boolean axiStreamPort

	Boolean axiStreamActorCommunication

	new(Port port, TypePrinterVisitor typePrinter, Map<String, Object> options, Boolean axiStreamPort) {
		super(typePrinter)
		this.typePrinter = typePrinter
		this.port = port
		this.axiStreamPort = axiStreamPort
		this.axiStreamActorCommunication = options.get(AXI_STREAM_ACTOR_COMMUNICATION) as Boolean

		fanoutNumber = port.outgoing.size
	}

	def getContent() {
		'''
			«getFileHeader("Exelixi for Vivado HLS,  Port Reader Code Generation, Input port: " + port.name)»
			«IF axiStreamPort»
				#include <ap_axi_sdata.h>
			«ELSE»
				#include <«typePrinter.intIncludeHeaderFile»>
			«ENDIF»
			#include <«typePrinter.channelIncludeHeaderFile»>
			
			void «IF axiStreamPort»AxiStream«ENDIF»PortReader_«port.name»(«typePrinter.channelClassName»< «IF axiStreamPort»ap_axiu<«port.type.sizeInBits»,4,5,5>«ELSE»«port.type.doSwitch»«ENDIF» > &«port.name», «IF fanoutNumber == 1»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»_pr«ELSE»«FOR int i : 0 .. fanoutNumber-1 SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»_«i»_pr«ENDFOR»«ENDIF»){
			#pragma HLS INTERFACE ap_ctrl_hs port=return
			«IF axiStreamActorCommunication»
				#pragma HLS INTERFACE axis port=«port.name»
				«IF fanoutNumber > 1»
					«FOR int i : 0 .. fanoutNumber - 1»
						#pragma HLS INTERFACE axis port=«port.name»_«i»_pr
					«ENDFOR»
				«ELSE»
					#pragma HLS INTERFACE axis port=«port.name»_pr
				«ENDIF»
			«ENDIF»
			
				if(!«port.name».empty() && «IF fanoutNumber > 1»«FOR int i : 0 .. fanoutNumber -1 SEPARATOR " && "»!«port.name»_«i»_pr.full()«ENDFOR»«ELSE»!«port.name»_pr.full()«ENDIF»){
				«IF axiStreamPort»
				ap_axiu<«port.type.sizeInBits»,4,5,5> axi_value = «port.name».read();
				«port.type.doSwitch» value = axi_value.data;
				«ELSE»
				«port.type.doSwitch» value = «port.name».read();
				«ENDIF»
				«IF fanoutNumber > 1»
				«FOR int i : 0 .. fanoutNumber - 1»
					«port.name»_«i»_pr.write(value);
				«ENDFOR»
				«ELSE»
				«port.name»_pr.write(value);
				«ENDIF»
				}
				
				return;
			}
		'''
	}
}
