/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.hls.vivadohls.backend.scripts

import exelixi.hls.templates.tclscripts.TCLTemplate
import java.util.Map

import static exelixi.hls.backend.Constants.CLOCK_PERIOD
import static exelixi.hls.backend.Constants.CLOCK_PERIOD_VALUE
import static exelixi.hls.backend.Constants.EXELIXI_CLK_NAME

class VivadoXdcPrinter extends TCLTemplate {
	
	float clockPeriod
	
	new(Map<String, Object> options){
		if(options.containsKey(CLOCK_PERIOD)){
			if(options.get(CLOCK_PERIOD) as Boolean){
				try{
					this.clockPeriod = Float.parseFloat(options.get(CLOCK_PERIOD_VALUE) as String)	
				}catch(NumberFormatException e){
					this.clockPeriod = 10
				}	
			}
		}else{
			this.clockPeriod = 10
		}
		
	}
	
	def getContent(){
		'''
			# -- Clock Contraints
			create_clock -name «EXELIXI_CLK_NAME» -period «clockPeriod» [get_ports «EXELIXI_CLK_NAME»]
		'''
	}
}
