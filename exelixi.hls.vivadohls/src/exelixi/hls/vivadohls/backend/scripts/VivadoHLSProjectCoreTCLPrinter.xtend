/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.scripts

import exelixi.hls.devices.XilinxDevice
import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.tclscripts.TCLTemplate
import java.util.ArrayList
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network

import static exelixi.hls.backend.Constants.CLOCK_PERIOD
import static exelixi.hls.backend.Constants.CLOCK_PERIOD_VALUE
import static exelixi.hls.backend.Constants.CODE_GEN_PATH_HW
import static exelixi.hls.backend.Constants.DEVICE
import static exelixi.hls.backend.Constants.EVERY_INSTANCE_AN_IP_CORE
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION

/**
 * A Vivado HLS TCL Printer for Synthesis
 * 
 * @author Endri Bezati
 */
class VivadoHLSProjectCoreTCLPrinter extends TCLTemplate {

	protected String name

	protected float clockPeriod

	protected Network network

	protected XilinxDevice xilinxDevice

	protected String partialPath

	protected List<Instance> instances

	protected Boolean everyInstanceAnIPCore

	protected Partitioner partitioner
	
	protected Map<String, Object> options

	new(Network network, Map<String, Object> options, Partitioner partitioner) {
		this.name = network.simpleName + "_pl"
		this.network = network
		this.partitioner = partitioner
		this.instances = partitioner.getInstances(ZYNQ_PL_PARTITION)
		this.everyInstanceAnIPCore = options.get(EVERY_INSTANCE_AN_IP_CORE) as Boolean
		this.clockPeriod = 10
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		this.partialPath = options.get(CODE_GEN_PATH_HW) as String
		this.options = options 
		if (options.get(CLOCK_PERIOD) as Boolean) {
			try {
				clockPeriod = Float.parseFloat(options.get(CLOCK_PERIOD_VALUE) as String)
			} catch (NumberFormatException e) {
				clockPeriod = 10
			}
		}
	}

	def getContent() {
		'''
			«getFileHeader("Vivado HLS Project TCL")»
			«IF !everyInstanceAnIPCore»
				«singleIpCore»
			«ELSE»
				«multipleIpCores»
			«ENDIF»
		'''
	}

	protected def singleIpCore() {
		'''
			set src_path "«partialPath»/src"
			set rtl_path "../«partialPath»/rtl"
			set tbsrc_path "«partialPath»/src-tb"
			set header_path "«partialPath»/include"
			
			## -- Create Project
			open_project hls_«name»
			
			## -- Add file
			add_files $src_path/«name».cpp -cflags "-I$header_path"
			
			
			## -- Add file
			«FOR instance : instances»
				add_files $src_path/«instance.simpleName».cpp -cflags "-I$header_path"
			«ENDFOR»
			
			## -- Add TestBench file
			add_files -tb $tbsrc_path/«name»_tb.cpp -cflags "-I$header_path"
			
			## -- Set Top Level
			set_top «name»
			
			## -- Create Solution
			open_solution -reset «name»_solution
			
			## -- Define Xilinx Technology
			set_part {«xilinxDevice.fpga»} «IF xilinxDevice.isZynqUltrascale»-tool vivado«ENDIF»
			
			## -- Define Clock period
			create_clock -period «clockPeriod» -name clk
			
			
			## -- Run Synthesis
			csynth_design
			
			## -- Export Design
			export_design -format ip_catalog
			
			## -- Copy RTL files
			set rtl_files [glob -dir ./«name»_solution/syn/verilog *]
			foreach f $rtl_files {file copy $f $rtl_path}
			
			exit
			## EOF
		'''
	}

	protected def multipleIpCores() {
		val Map<Connection, List<Connection>> bcConnections = partitioner.getConnectionToConnectionBd(ZYNQ_PS_PARTITION,
			ZYNQ_PL_PARTITION);
		var List<String> fanoutFifoNames = new ArrayList
		for (Connection connection : bcConnections.keySet()) {
			if (bcConnections.get(connection).size() > 1) {
				val String name = "fanout_fifo_bcId_" + connection.getAttribute("idNoBcast").getObjectValue()
				fanoutFifoNames.add(name)
			}
		}
		'''
			set src_path "../«partialPath»/src"
			set rtl_path "../«partialPath»/rtl"
			set tbsrc_path "../«partialPath»/src-tb"
			set header_path "../«partialPath»/include"
			
			cd ip_cores
			
			«IF !fanoutFifoNames.empty»
				## ----------------------------------------------------------------------------
				## -- Fanout Cores
				«FOR fanoutFifo : fanoutFifoNames»
					## -- Create Project
					open_project hls_«fanoutFifo» 
					
					## -- Add file
					add_files $src_path/«fanoutFifo».cpp
					
					## -- Set Top Level
					set_top «fanoutFifo»
					
					## -- Create Solution
					open_solution -reset «fanoutFifo»_solution
					
					## -- Define Xilinx Technology
					set_part {«xilinxDevice.fpga»} «IF xilinxDevice.isZynqUltrascale»-tool vivado«ENDIF»
					
					## -- Define Clock period
					create_clock -period «clockPeriod» -name clk
					
					## -- Config RTL
					config_rtl -reset_level low
					
					## -- Run Synthesis
					csynth_design
						
					## -- Export Design
					export_design -format ip_catalog
					
					## -- Copy RTL files
					set rtl_files [glob -dir ./«fanoutFifo»_solution/syn/verilog *]
					foreach f $rtl_files {file copy $f $rtl_path}
				«ENDFOR»
			«ENDIF»
			
			## ----------------------------------------------------------------------------
			## -- Instances
			«FOR instance : instances»
				«IF !instance.getActor.isNative»
					## -- Create Project
					open_project hls_«instance.simpleName» 
					
					## -- Add file
					add_files $src_path/«instance.simpleName».cpp -cflags "-I$header_path"
					
					## -- Add TestBench files directory
					add_files -tb ../../../fifo-traces
					
					## -- Add TestBench source file
					add_files -tb $tbsrc_path/«instance.simpleName»_tb.cpp -cflags "-I$header_path"
					
					## -- Set Top Level
					set_top «instance.simpleName»
					
					## -- Create Solution
					open_solution -reset «instance.simpleName»_solution
					
					## -- Define Xilinx Technology
					set_part {«xilinxDevice.fpga»} «IF xilinxDevice.isZynqUltrascale»-tool vivado«ENDIF»
					
					## -- Define Clock period
					create_clock -period «clockPeriod» -name clk
					
					## -- Config RTL
					config_rtl -reset_level low
					
					## -- Run Synthesis
					csynth_design
						
					## -- Export Design
					export_design -format ip_catalog
					
					## -- Copy RTL files
					set rtl_files_«instance.simpleName» [glob -dir ./«instance.simpleName»_solution/syn/verilog *]
					foreach f $rtl_files_«instance.simpleName» {file copy $f $rtl_path}
				«ENDIF»
			«ENDFOR»
			
			cd ..
			
			exit
			## EOF
		'''
	}
	
}
