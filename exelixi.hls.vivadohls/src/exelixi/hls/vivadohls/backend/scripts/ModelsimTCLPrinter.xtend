/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.scripts

import exelixi.hls.templates.tclscripts.TCLTemplate
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port

import static exelixi.hls.backend.Constants.PROFILE_DATAFLOW_NETWORK

/**
 * 
 * @author Endri Bezati
 */
 
class ModelsimTCLPrinter extends TCLTemplate {
	
	String name
	
	Network network
	
	Instance instance
	
	List<Port> inputs
	
	List<Port> outputs
	
	Map<Port, List<Connection>> outgoingPortMap
	
	boolean enableProfiling
	
	new (Instance instance, Map<String, Object> options){
		this.instance = instance
		this.name = instance.simpleName
		this.inputs = instance.getActor.inputs
		this.outputs = instance.getActor.outputs
		this.outgoingPortMap = instance.outgoingPortMap
		if(options.containsKey(PROFILE_DATAFLOW_NETWORK)){
			this.enableProfiling = options.get(PROFILE_DATAFLOW_NETWORK) as Boolean
		}else{
			this.enableProfiling = false
		}
	}
	
	new (Network network, Map<String, Object> options){
		this.network = network
		this.name = network.simpleName
		this.inputs = network.inputs
		this.outputs = network.outputs
		if(options.containsKey(PROFILE_DATAFLOW_NETWORK)){
			this.enableProfiling = options.get(PROFILE_DATAFLOW_NETWORK) as Boolean
		}else{
			this.enableProfiling = false
		}
	}
	
	def getContent(){
		'''
			«getFileHeader("ModelSim TCL")»
			
			«compileWorkLibrary»
			
			«startSimulation»
			
			«waveSignals»
		'''
	}
	
	
	def getCompileWorkLibrary(){
		'''
			## -- Create the work design library
			if {[file exist work_«name»]} {vdel -all -lib work_«name»}
			vlib work_«name»
			#vmap work_«name» work_«name»
			
			## -- Compile Verilog Files
			vlog -work work_«name» *.v
		'''
	}
	
	def getStartSimulation(){
		'''
			## -- Start VSIM
			vsim -novopt -t ns work_«name».«name»_tb
		'''
	}
	
	def getWaveSignals(){
		'''
			## -- Wave signals
			
			add wave -noupdate -divider -height 20 "CLK & RESET"
			add wave -position end sim:/«name»_tb/clock
			add wave -position end sim:/«name»_tb/reset
			
			«IF !inputs.empty»
				add wave -noupdate -divider -height 20 "Inputs"
				«FOR port : inputs»
					add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»«portExtension»_dout;
					add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»«portExtension»_empty_n;
					add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»«portExtension»_read;
				«ENDFOR»
			«ENDIF»
			«IF !outputs.empty»
				add wave -noupdate -divider -height 20 "Outputs"
				«FOR port : outputs»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»_«i»«portExtension»_din;
								add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»_«i»«portExtension»_full_n;
								add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»_«i»«portExtension»_write;
							«ENDFOR»
						«ELSE»
							add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»«portExtension»_din;
							add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»«portExtension»_full_n;
							add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»«portExtension»_write;
						«ENDIF»
					«ELSE»
						add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»«portExtension»_din;
						add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»«portExtension»_full_n;
						add wave -position end sim:/«name»_tb/«IF instance !== null»dut/«ENDIF»«port.name»«portExtension»_write;
					«ENDIF»
				«ENDFOR»
				
			«ENDIF»
			
			
			«IF network !== null»
				«FOR instance : network.children.filter(typeof(Instance)) SEPARATOR "\n"»
					add wave -noupdate -divider -height 20 "i_«instance.simpleName»"
					add wave -noupdate -divider -height 20
					«FOR port : instance.getActor.inputs»
						add wave -noupdate -divider -height 20 "I - «port.name»"
						add wave -position end sim:/«name»_tb/dut/i_«instance.simpleName»/«port.name»_V_dout;
						add wave -position end sim:/«name»_tb/dut/i_«instance.simpleName»/«port.name»_V_empty_n;
						add wave -position end sim:/«name»_tb/dut/i_«instance.simpleName»/«port.name»_V_read;
					«ENDFOR»
					«FOR port : instance.getActor.outputs»
						«IF instance.outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. instance.outgoingPortMap.get(port).size - 1»
								add wave -noupdate -divider -height 20 "O - «port.name»_«i»"
								add wave -position end sim:/«name»_tb/dut/i_«instance.simpleName»/«port.name»_«i»_V_din;
								add wave -position end sim:/«name»_tb/dut/i_«instance.simpleName»/«port.name»_«i»_V_full_n;
								add wave -position end sim:/«name»_tb/dut/i_«instance.simpleName»/«port.name»_«i»_V_write;
							«ENDFOR»
						«ELSE»
							add wave -noupdate -divider -height 20 "O - «port.name»"
							add wave -position end sim:/«name»_tb/dut/i_«instance.simpleName»/«port.name»_V_din;
							add wave -position end sim:/«name»_tb/dut/i_«instance.simpleName»/«port.name»_V_full_n;
							add wave -position end sim:/«name»_tb/dut/i_«instance.simpleName»/«port.name»_V_write;
						«ENDIF»
						«IF enableProfiling»
							add wave -noupdate -divider -height 20 "Action ID"
							add wave -position end  sim:/«name»_tb/«instance.simpleName»_action_id
						«ENDIF»
					«ENDFOR»
					add wave -noupdate -divider -height 20
				«ENDFOR»
			«ELSE»
				«IF enableProfiling»
					add wave -noupdate -divider -height 20 "Action ID"
					add wave -position end  sim:/«name»_tb/«name»_action_id
				«ENDIF»
			«ENDIF»
			
			## Change radix to decimal
			radix -decimal
		'''
	}
	
	def getPortExtension(){
		'''«IF instance !== null»_V«ENDIF»'''
	}
	
}
