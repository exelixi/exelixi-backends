/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.scripts

import exelixi.hls.devices.XilinxDevice
import exelixi.hls.templates.tclscripts.TCLTemplate
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import java.util.Map
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network

import static exelixi.hls.backend.Constants.CLOCK_PERIOD
import static exelixi.hls.backend.Constants.CLOCK_PERIOD_VALUE
import static exelixi.hls.backend.Constants.CODE_GEN_PATH_HW
import static exelixi.hls.backend.Constants.DEVICE
import static exelixi.hls.backend.Constants.EVERY_INSTANCE_AN_IP_CORE

/**
 * A Vivado HLS TCL Printer for Synthesis
 * 
 * @author Endri Bezati
 */
class VivadoHLSProjectTCLPrinter extends TCLTemplate {

	String name

	float clockPeriod

	Network network

	Instance instance

	XilinxDevice xilinxDevice

	String partialPath

	boolean everyInstanceAnIpCore

	new(String name, Map<String, Object> options) {
		this.name = name
		this.clockPeriod = 10
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		this.partialPath = options.get(CODE_GEN_PATH_HW) as String
		if (options.containsKey(EVERY_INSTANCE_AN_IP_CORE)) {
			this.everyInstanceAnIpCore = options.get(EVERY_INSTANCE_AN_IP_CORE) as Boolean
		} else {
			this.everyInstanceAnIpCore = true
		}
		if (options.containsKey(CLOCK_PERIOD)) {
			if (options.get(CLOCK_PERIOD) as Boolean) {
				try {
					clockPeriod = Float.parseFloat(options.get(CLOCK_PERIOD_VALUE) as String)
				} catch (NumberFormatException e) {
					clockPeriod = 10
				}
			}
		} else {
			clockPeriod = 10
		}
	}

	new(Instance instance, Map<String, Object> options) {
		this.name = instance.simpleName
		this.instance = instance
		this.clockPeriod = 10
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		this.partialPath = options.get(CODE_GEN_PATH_HW) as String
		if (options.containsKey(EVERY_INSTANCE_AN_IP_CORE)) {
			this.everyInstanceAnIpCore = options.get(EVERY_INSTANCE_AN_IP_CORE) as Boolean
		} else {
			this.everyInstanceAnIpCore = true
		}

		if (options.containsKey(CLOCK_PERIOD)) {
			if (options.get(CLOCK_PERIOD) as Boolean) {
				try {
					clockPeriod = Float.parseFloat(options.get(CLOCK_PERIOD_VALUE) as String)
				} catch (NumberFormatException e) {
					clockPeriod = 10
				}
			}
		} else {
			clockPeriod = 10
		}
	}

	new(Network network, Map<String, Object> options) {
		this.name = network.simpleName
		this.network = network
		this.clockPeriod = 10
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		this.partialPath = options.get(CODE_GEN_PATH_HW) as String
		if (options.containsKey(EVERY_INSTANCE_AN_IP_CORE)) {
			this.everyInstanceAnIpCore = options.get(EVERY_INSTANCE_AN_IP_CORE) as Boolean
		} else {
			this.everyInstanceAnIpCore = true
		}
		if (options.containsKey(CLOCK_PERIOD)) {
			if (options.get(CLOCK_PERIOD) as Boolean) {
				try {
					clockPeriod = Float.parseFloat(options.get(CLOCK_PERIOD_VALUE) as String)
				} catch (NumberFormatException e) {
					clockPeriod = 10
				}
			}
		} else {
			clockPeriod = 10
		}

	}

	def getContent() {
		'''
			«IF network!== null && everyInstanceAnIpCore»
				«IpCores»
			«ELSE»
				«instanceAndNetwork»
			«ENDIF»
		'''
	}

	private def instanceAndNetwork() {
		'''
			«getFileHeader("Vivado HLS Project TCL")»
			set src_path "«partialPath»/src"
			set rtl_path "../«partialPath»/rtl"
			set tbsrc_path "«partialPath»/src-tb"
			set header_path "«partialPath»/include"
			
			## -- Create Project
			open_project hls_«name»
			
			## -- Add file
			add_files $src_path/«name».cpp -cflags "-I$header_path"
			
			«IF network !== null»
				«IF !network.inputs.empty»
					«FOR port : network.inputs»
						add_files $src_path/PortReader_«port.name».cpp -cflags "-I$header_path"
					«ENDFOR»
				«ENDIF»
				«IF !network.outputs.empty»
					«FOR port : network.outputs»
						add_files $src_path/PortWriter_«port.name».cpp -cflags "-I$header_path"
					«ENDFOR»
				«ENDIF»
				«FOR instance : network.children.filter(typeof(Instance))»
					add_files $src_path/«instance.simpleName».cpp -cflags "-I$header_path"
				«ENDFOR»			
			«ENDIF»
			
			## -- Add TestBench files directory
			add_files -tb ../../fifo-traces
			
			## -- Add TestBench source file
			add_files -tb $tbsrc_path/«name»_tb.cpp -cflags "-I$header_path"
			
			## -- Set Top Level
			«IF instance === null»
				set_top «name»
			«ELSE»
				set_top «TemplatesUtils.getInstanceName(instance)»
			«ENDIF»
			
			## -- Create Solution
			open_solution -reset «name»_solution
			
			## -- Define Xilinx Technology
			set_part {«xilinxDevice.fpga»} «IF xilinxDevice.isZynqUltrascale»-tool vivado«ENDIF»
			
			## -- Define Clock period
			create_clock -period «clockPeriod» -name clk
			
			«IF network === null»
				## -- Config RTL
				config_rtl -reset_level low
				config_compile -name_max_length 512 -pipeline_loops 256
			«ENDIF»
			
			## -- Run Synthesis
			csynth_design
			
			## -- Copy RTL files
			set rtl_files_«name» [glob -dir ./hls_«name»/«name»_solution/syn/verilog *]
			foreach f $rtl_files_«name» {file copy $f $rtl_path}
			
			exit
			## EOF
		'''
	}

	private def IpCores() {
		'''
			«getFileHeader("Vivado HLS Project TCL")»
			set src_path "../../../code-gen/src"
			set rtl_path "../«partialPath»/rtl"
			set tbsrc_path "../../../code-gen/src-tb"
			set header_path "../../../code-gen/include"
			
			cd ip_cores
			«IF !network.inputs.empty»## -- Port Readers«ENDIF»
			«FOR port : network.inputs»
				«byNameScript("PortReader_"+port.name)»
			«ENDFOR»
			
			## -- Instances
			«FOR instance : network.children.filter(typeof(Instance))»
				«IF !instance.getActor.isNative»
					## -- Create Project
					open_project hls_«instance.simpleName» 
					
					## -- Add file
					add_files $src_path/«instance.simpleName».cpp -cflags "-I$header_path"
					
					## -- Add TestBench files directory
					add_files -tb ../../../fifo-traces
					
					## -- Add TestBench source file
					add_files -tb $tbsrc_path/«instance.simpleName»_tb.cpp -cflags "-I$header_path"
					
					## -- Set Top Level
					set_top «TemplatesUtils.getInstanceName(instance)»
					
					## -- Create Solution
					open_solution -reset «instance.simpleName»_solution
					
					## -- Define Xilinx Technology
					set_part {«xilinxDevice.fpga»} «IF xilinxDevice.isZynqUltrascale»-tool vivado«ENDIF»
					
					## -- Define Clock period
					create_clock -period «clockPeriod» -name clk
					
					## -- Config RTL
					config_rtl -reset_level low
					config_compile -name_max_length 512 -pipeline_loops 256
					
					## -- Run Synthesis
					csynth_design
						
					## -- Export Design
					export_design -format ip_catalog
					
					## -- Copy RTL files
					set rtl_files_«instance.simpleName» [glob -dir ./hls_«instance.simpleName»/«instance.simpleName»_solution/syn/verilog *]
					foreach f $rtl_files_«instance.simpleName» {file copy $f $rtl_path}
					
				«ENDIF»
			«ENDFOR»
			
			«IF !network.outputs.empty»## -- Port Writer«ENDIF»
			«FOR port : network.outputs»
				«byNameScript("PortWriter_"+port.name)»
			«ENDFOR»
			
			cd ..
			
			exit
			## EOF
		'''
	}

	private def byNameScript(String name) {
		'''
			## -- Create Project
			open_project hls_«name»
			
			## -- Add file
			add_files $src_path/«name».cpp -cflags "-I$header_path"
			
			## -- Set Top Level
			set_top «name»
						
			## -- Create Solution
			open_solution -reset «name»_solution
			
			## -- Define Xilinx Technology
			set_part {«xilinxDevice.fpga»} «IF xilinxDevice.isZynqUltrascale»-tool vivado«ENDIF»
			
			## -- Define Clock period
			create_clock -period «clockPeriod» -name clk
			
			## -- Config RTL
			config_rtl -reset_level low
			
			## -- Run Synthesis
			csynth_design
				
			## -- Export Design
			export_design -format ip_catalog
			
			## -- Copy RTL files
			set rtl_files_«name» [glob -dir ./hls_«name»/«name»_solution/syn/verilog *]
			foreach f $rtl_files_«name» {file copy $f $rtl_path}
		'''
	}

}
