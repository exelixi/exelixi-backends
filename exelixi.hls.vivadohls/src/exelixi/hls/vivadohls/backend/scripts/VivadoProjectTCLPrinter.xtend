/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.scripts

import exelixi.hls.devices.XilinxDevice
import exelixi.hls.templates.tclscripts.TCLTemplate
import java.io.File
import java.util.ArrayList
import java.util.List
import java.util.Map
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network

import static exelixi.hls.backend.Constants.DEVICE
import static exelixi.hls.backend.Constants.PROJECTS_PATH
import static exelixi.hls.backend.Constants.RTL_PATH

/**
 * 
 * @author Endri Bezati
 */
class VivadoProjectTCLPrinter extends TCLTemplate {

	Network network

	Instance instance

	String name

	String rtlPath

	String projectsPath

	XilinxDevice xilinxDevice

	new(Network network, Map<String, Object> options) {
		this.network = network
		this.name = network.simpleName
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		this.rtlPath = options.get(RTL_PATH) as String
		this.projectsPath = options.get(PROJECTS_PATH) as String
	}

	new(Instance instance, Map<String, Object> options) {
		this.instance = instance
		this.name = instance.simpleName
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		this.projectsPath = options.get(PROJECTS_PATH) as String
	}

	def getContent() {
		'''
			«getFileHeader("Vivado Project TCL")»
			create_project «name» vivado_«name» -part «xilinxDevice.fpga»
			set_property board_part «xilinxDevice.board» [current_project]
			«IF network !== null»«getVerilogFiles(rtlPath)»«ELSE»«getVerilogFiles(getSynthesizedVerilogPath(name))»«ENDIF»
			add_files -fileset sim_1 -norecurse ../../simulation/«name»/«name»_tb.v
			update_compile_order -fileset sources_1
			update_compile_order -fileset sources_1
			update_compile_order -fileset sim_1
		'''
	}

	def getVerilogFiles(String path) {
		val File rtlDir = new File(path);

		var List<String> verilogFiles = new ArrayList();
		if (rtlDir.exists) {
			for (File file : rtlDir.listFiles()) {
				verilogFiles.add(file.absolutePath);
			}
		}
		'''
			«FOR verilogFile : verilogFiles»
				add_files -norecurse «verilogFile»
			«ENDFOR»
		'''
	}

	def getSynthesizedVerilogPath(String name) {
		return projectsPath + File.separator + name + File.separator + "hls_" + name + File.separator + name +
			"_solution" + File.separator + "syn" + File.separator + "verilog";
	}

}
