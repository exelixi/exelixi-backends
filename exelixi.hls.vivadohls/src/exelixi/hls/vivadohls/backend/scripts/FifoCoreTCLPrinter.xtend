/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.hls.vivadohls.backend.scripts

import exelixi.hls.devices.XilinxDevice
import exelixi.hls.templates.tclscripts.TCLTemplate
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import java.util.Map

import static exelixi.hls.backend.Constants.DEVICE
import static exelixi.hls.backend.Constants.PROJECTS_PATH
import static exelixi.hls.backend.Constants.RTL_PATH

class FifoCoreTCLPrinter extends TCLTemplate {
	
	XilinxDevice xilinxDevice
	
	String rtlPath
	
	String projectsPath

	new(Map<String, Object> options) {
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		this.rtlPath = options.get(RTL_PATH) as String
		this.projectsPath = options.get(PROJECTS_PATH) as String
	}
	
	new(Map<String, Object> options, String rtlPath) {
		this.xilinxDevice = options.get(DEVICE) as XilinxDevice
		this.rtlPath = rtlPath
		this.projectsPath = options.get(PROJECTS_PATH) as String
	}

	def getContent() {
		'''
			«getFileHeader("Vivado Project TCL for Exelixi FIFO IP Core")»
			# -- Create Project
			create_project exelixi_fifo ip_cores/exelixi_fifo  -part «xilinxDevice.fpga»
			
			# -- Add fifo.v rtl 
			add_files -norecurse «TemplatesUtils.getPath(rtlPath)»/fifo.v
			import_files -force -norecurse
			update_compile_order -fileset sources_1
			update_compile_order -fileset sim_1
			
			# -- Package the project
			ipx::package_project 
			
			# -- Vendor Information
			set_property vendor Exelixi [ipx::current_core]
			set_property name {Exelixi FIFO} [ipx::current_core]
			set_property name Exelixi_FIFO [ipx::current_core]
			set_property display_name {Exelixi FIFO} [ipx::current_core]
			set_property description {A FIFO for Exelxi Dataflow} [ipx::current_core]
			set_property vendor_display_name Exelixi [ipx::current_core]
			set_property taxonomy {/UserIP} [ipx::current_core]
			
			# -- Core Compatibility
			set_property supported_families {virtex7 Production qvirtex7 Production kintex7 Production kintex7l Production qkintex7 Production qkintex7l Production artix7 Production artix7l Production aartix7 Production qartix7 Production zynq Production qzynq Production azynq Production spartan7 Production virtexu Production zynquplus Production virtexuplus Production kintexu Production} [ipx::current_core]
			
			# -- Configure Parameters
			set_property widget {comboBox} [ipgui::get_guiparamspec -name "MEM_STYLE" -component [ipx::current_core] ]
			set_property value distributed [ipx::get_user_parameters MEM_STYLE -of_objects [ipx::current_core]]
			set_property value distributed [ipx::get_hdl_parameters MEM_STYLE -of_objects [ipx::current_core]]
			set_property value auto [ipx::get_user_parameters MEM_STYLE -of_objects [ipx::current_core]]
			set_property value auto [ipx::get_hdl_parameters MEM_STYLE -of_objects [ipx::current_core]]
			set_property value_validation_type list [ipx::get_user_parameters MEM_STYLE -of_objects [ipx::current_core]]
			set_property value_validation_list {auto distributed block} [ipx::get_user_parameters MEM_STYLE -of_objects [ipx::current_core]]
			
			# -- WRITE ACC Interface
			ipx::add_bus_interface WRITE [ipx::current_core]
			set_property abstraction_type_vlnv xilinx.com:interface:acc_fifo_write_rtl:1.0 [ipx::get_bus_interfaces WRITE -of_objects [ipx::current_core]]
			set_property bus_type_vlnv xilinx.com:interface:acc_fifo_write:1.0 [ipx::get_bus_interfaces WRITE -of_objects [ipx::current_core]]
			set_property display_name WRITE [ipx::get_bus_interfaces WRITE -of_objects [ipx::current_core]]
			set_property description {FIFO WRITE} [ipx::get_bus_interfaces WRITE -of_objects [ipx::current_core]]
			ipx::add_port_map WR_DATA [ipx::get_bus_interfaces WRITE -of_objects [ipx::current_core]]
			set_property physical_name if_din [ipx::get_port_maps WR_DATA -of_objects [ipx::get_bus_interfaces WRITE -of_objects [ipx::current_core]]]
			ipx::add_port_map WR_EN [ipx::get_bus_interfaces WRITE -of_objects [ipx::current_core]]
			set_property physical_name if_write [ipx::get_port_maps WR_EN -of_objects [ipx::get_bus_interfaces WRITE -of_objects [ipx::current_core]]]
			ipx::add_port_map FULL_N [ipx::get_bus_interfaces WRITE -of_objects [ipx::current_core]]
			set_property physical_name if_full_n [ipx::get_port_maps FULL_N -of_objects [ipx::get_bus_interfaces WRITE -of_objects [ipx::current_core]]]
			
			# -- READ ACC Interface
			ipx::add_bus_interface READ [ipx::current_core]
			set_property abstraction_type_vlnv xilinx.com:interface:acc_fifo_read_rtl:1.0 [ipx::get_bus_interfaces READ -of_objects [ipx::current_core]]
			set_property bus_type_vlnv xilinx.com:interface:acc_fifo_read:1.0 [ipx::get_bus_interfaces READ -of_objects [ipx::current_core]]
			set_property display_name READ [ipx::get_bus_interfaces READ -of_objects [ipx::current_core]]
			set_property description {FIFO READ} [ipx::get_bus_interfaces READ -of_objects [ipx::current_core]]
			ipx::add_port_map RD_DATA [ipx::get_bus_interfaces READ -of_objects [ipx::current_core]]
			set_property physical_name if_dout [ipx::get_port_maps RD_DATA -of_objects [ipx::get_bus_interfaces READ -of_objects [ipx::current_core]]]
			ipx::add_port_map RD_EN [ipx::get_bus_interfaces READ -of_objects [ipx::current_core]]
			set_property physical_name if_read [ipx::get_port_maps RD_EN -of_objects [ipx::get_bus_interfaces READ -of_objects [ipx::current_core]]]
			ipx::add_port_map EMPTY_N [ipx::get_bus_interfaces READ -of_objects [ipx::current_core]]
			set_property physical_name if_empty_n [ipx::get_port_maps EMPTY_N -of_objects [ipx::get_bus_interfaces READ -of_objects [ipx::current_core]]]
			
			set_property previous_version_for_upgrade user.org:user:FIFO:1.0 [ipx::current_core]
			set_property core_revision 1 [ipx::current_core]
			ipx::create_xgui_files [ipx::current_core]
			ipx::update_checksums [ipx::current_core]
			ipx::save_core [ipx::current_core]
			
			close_project
			quit
		'''
	}
}
