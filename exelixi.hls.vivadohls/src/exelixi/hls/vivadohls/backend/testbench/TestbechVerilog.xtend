/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.testbench

import java.io.File
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port

import static exelixi.hls.backend.Constants.BIT_ACCURATE_TYPES
import static exelixi.hls.backend.Constants.CLOCK_PERIOD
import static exelixi.hls.backend.Constants.CLOCK_PERIOD_VALUE
import static exelixi.hls.backend.Constants.FIFO_TRACE_PATH
import static exelixi.hls.backend.Constants.PROFILE_DATAFLOW_NETWORK

/**
 * 
 * @author Endri Bezati
 */
class TestbechVerilog {

	Network network
	
	Instance instance
	
	String name
	
	List<Port> inputs
	
	List<Port> outputs

	boolean typeAccuracy
	
	String fifoTracePath
	
	float clockPeriod
	
	Map<Port, List<Connection>> outgoingPortMap
	
	boolean enableProfiling

	new(Instance instance, Map<String, Object> options) {
		this.instance = instance;
		this.name = instance.simpleName
		this.inputs = instance.getActor.inputs
		this.outputs = instance.getActor.outputs
		if(options.containsKey(PROFILE_DATAFLOW_NETWORK)){
			this.enableProfiling = options.get(PROFILE_DATAFLOW_NETWORK) as Boolean
		}else{
			this.enableProfiling = false
		}
		
		if(options.containsKey(BIT_ACCURATE_TYPES)){
			this.typeAccuracy = options.get(BIT_ACCURATE_TYPES) as Boolean
		}else{
			this.typeAccuracy = false
		}
		
		if(options.containsKey(FIFO_TRACE_PATH)){
			this.fifoTracePath = options.get(FIFO_TRACE_PATH) as String
		}else{
			this.fifoTracePath = ""
		}
		
		this.outgoingPortMap = instance.outgoingPortMap
		if(options.containsKey(CLOCK_PERIOD)){
			if(options.get(CLOCK_PERIOD) as Boolean){
				try{
					clockPeriod = Float.parseFloat(options.get(CLOCK_PERIOD_VALUE) as String)	
				}catch(NumberFormatException e){
					clockPeriod = 10
				}
			}	
		}else{
			clockPeriod = 10
		}
	}
	
	new(Network network,Map<String, Object> options){
		this.network = network
		this.name = network.simpleName
		this.inputs = network.inputs
		this.outputs = network.outputs
		if(options.containsKey(PROFILE_DATAFLOW_NETWORK)){
			this.enableProfiling = options.get(PROFILE_DATAFLOW_NETWORK) as Boolean
		}else{
			this.enableProfiling = false
		}
		if(options.containsKey(BIT_ACCURATE_TYPES)){
			this.typeAccuracy = options.get(BIT_ACCURATE_TYPES) as Boolean
		}else{
			this.typeAccuracy = false
		}
		
		if(options.containsKey(FIFO_TRACE_PATH)){
			this.fifoTracePath = options.get(FIFO_TRACE_PATH) as String
		}else{
			this.fifoTracePath = ""
		}
		
		if(options.containsKey(CLOCK_PERIOD)){
			if(options.get(CLOCK_PERIOD) as Boolean){
				try{
					clockPeriod = Float.parseFloat(options.get(CLOCK_PERIOD_VALUE) as String)	
				}catch(NumberFormatException e){
					clockPeriod = 10
				}
			}	
		}else{
			clockPeriod = 10
		}
	}
	
	def getContent() {
		'''
			«getFileHeader("Verilog Testbench for" + if(network !== null) "Actor Composition :" else "Instance :" + name)»
			
			`define NULL 0
			`timescale 1 ns / 1 ps
			
			module «name»_tb();
			
				// -- CLK, reset_n and clock cycle
				reg clock;
				reg reset_n;
				reg start;
				parameter cycle = «clockPeriod»;
				
				«fileIntegers»
			
				«instanceIORegWire»
			
				«initial»
			
				«clockGeneration»
			
				«FOR port : inputs SEPARATOR "\n"»
					«getInputVectorReader(port)»
				«ENDFOR»
				
				«FOR port : outputs SEPARATOR "\n"»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								«getCompareWithGoldenReference(port,i)»
							«ENDFOR»
						«ELSE»
							«getCompareWithGoldenReference(port)»
						«ENDIF»
					«ELSE»
						«getCompareWithGoldenReference(port)»
					«ENDIF»
				«ENDFOR»
			
				«inputFifos»
			
				«IF enableProfiling»
					«profiledWires»
				«ENDIF»
				
				«getDut»
				
				«endofSimulation»
			
			endmodule
		'''
	}

	def getFileIntegers() {
		'''
			«IF !inputs.empty || !outputs.empty»// -- File Integers«ENDIF»
			«FOR port : inputs»
				integer «port.name»_data_file;
				integer «port.name»_scan_file;
			«ENDFOR»
			
			«IF instance !== null»
				«FOR port : outputs»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								integer «port.name»_«i»_data_file;
								integer «port.name»_«i»_scan_file;
							«ENDFOR»
						«ELSE»
							integer «port.name»_data_file;
							integer «port.name»_scan_file;
						«ENDIF»
					«ELSE»
						integer «port.name»_data_file;
						integer «port.name»_scan_file;
					«ENDIF»
				«ENDFOR»
			«ELSE»
				«FOR port : outputs»
					integer «port.name»_data_file;
					integer «port.name»_scan_file;
				«ENDFOR»
			«ENDIF»
		'''
	}

	def getInstanceIORegWire() {
		'''
			«IF !inputs.empty»
				// State Machine states
				parameter READ = 1'b0;
				parameter SEND = 1'b1;
				
				// -- Input Port Reg, wires and state for reading
				«FOR port : inputs»
					reg «IF port.type.isInt»signed«ENDIF» «IF port.type.sizeInBits > 1»[«port.type.sizeInBits - 1»:0]«ENDIF» q_«port.name»_din;
					reg «IF port.type.isInt»signed«ENDIF» «IF port.type.sizeInBits > 1»[«port.type.sizeInBits - 1»:0]«ENDIF» q_«port.name»_din_tmp;
					reg q_«port.name»_write;
					wire q_«port.name»_full_n;

					reg «port.name»_read_reg;
					
					wire «IF port.type.isInt»signed«ENDIF» «IF port.type.sizeInBits > 1»[«port.type.sizeInBits - 1»:0]«ENDIF» «port.name»«portExtension»_dout;
					wire «port.name»«portExtension»_empty_n;
					wire «port.name»«portExtension»_read;
				«ENDFOR»
			«ENDIF»
			«IF !outputs.empty»
				// -- Output Port Reg and wires
				«FOR port : outputs»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								wire «IF port.type.isInt»signed«ENDIF» «IF port.type.sizeInBits > 1»[«port.type.sizeInBits - 1»:0]«ENDIF» «port.name»_«i»«portExtension»_din;
								reg «port.name»_«i»«portExtension»_full_n;
								wire «port.name»_«i»«portExtension»_write;
								
								reg «IF port.type.isInt»signed«ENDIF» «IF port.type.sizeInBits > 1»[«port.type.sizeInBits - 1»:0]«ENDIF» «port.name»_«i»_exp_value;
								reg «port.name»_«i»_end_of_file;
								reg [31:0] «port.name»_«i»_token_counter;
							«ENDFOR»
						«ELSE»
							wire «IF port.type.isInt»signed«ENDIF» «IF port.type.sizeInBits > 1»[«port.type.sizeInBits - 1»:0]«ENDIF» «port.name»«portExtension»_din;
							reg «port.name»«portExtension»_full_n;
							wire «port.name»«portExtension»_write;
							
							reg «IF port.type.isInt»signed«ENDIF» «IF port.type.sizeInBits > 1»[«port.type.sizeInBits - 1»:0]«ENDIF» «port.name»_exp_value;
							reg «port.name»_end_of_file;
							reg [31:0] «port.name»_token_counter;
						«ENDIF»
					«ELSE»
						wire «IF port.type.isInt»signed«ENDIF» «IF port.type.sizeInBits > 1»[«port.type.sizeInBits - 1»:0]«ENDIF» «port.name»«portExtension»_din;
						reg «port.name»«portExtension»_full_n;
						wire «port.name»«portExtension»_write;
						
						reg «IF port.type.isInt»signed«ENDIF» «IF port.type.sizeInBits > 1»[«port.type.sizeInBits - 1»:0]«ENDIF» «port.name»_exp_value;
						reg «port.name»_end_of_file;
						reg [31:0] «port.name»_token_counter;
					«ENDIF»
				«ENDFOR»
			«ENDIF»
		'''
	}

	def getInitial() {
		'''
			initial begin
				$display("Testbench for «IF network !== null»Actor Composition :«ELSE»Actor: «ENDIF» «name»" );
				clock = 1'b0;
				reset_n = 1'b0;
				start = 1'b0;
			
				«IF !inputs.empty»// -- Initialize read_reg for Inputs«ENDIF»
				«FOR port : inputs»
					«port.name»_read_reg = 1'b0;
					q_«port.name»_din = 1'b0;
					q_«port.name»_din_tmp = 1'b0;
					q_«port.name»_write = 1'b0;
				«ENDFOR»
				
				«IF !outputs.empty»// -- Initialize Output Fifo Fullness«ENDIF»
				«FOR port : outputs»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								«port.name»_«i»«portExtension»_full_n = 1'b1;
							«ENDFOR»
						«ELSE»
							«port.name»«portExtension»_full_n = 1'b1;
						«ENDIF»
					«ELSE»
						«port.name»«portExtension»_full_n = 1'b1;
					«ENDIF»
				«ENDFOR»
			
				«IF !outputs.empty»// -- Initialize End of File for Outputs«ENDIF»
				«FOR port : outputs»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								«port.name»_«i»_end_of_file = 1'b0;
							«ENDFOR»
						«ELSE»
							«port.name»_end_of_file = 1'b0;
						«ENDIF»
					«ELSE»
						«port.name»_end_of_file = 1'b0;
					«ENDIF»
				«ENDFOR»
			
				«IF !outputs.empty»// -- Initialize output token Counters«ENDIF»
				«FOR port : outputs»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								«port.name»_«i»_token_counter = 0;
							«ENDFOR»
						«ELSE»
							«port.name»_token_counter = 0;
						«ENDIF»
					«ELSE»
						«port.name»_token_counter = 0;
					«ENDIF»
				«ENDFOR»
			
				«FOR port : inputs»
					// -- Open Input vector for port: «port.name»
					«port.name»_data_file = $fopen("«IF instance !== null»«instancePortFilePath(instance,port)»«ELSE»«fifoTracePath»«File.separator»«port.name».txt«ENDIF»", "r");
					if(«port.name»_data_file == `NULL) begin
						$display("Error: File «name»_«port.name».txt does not exist");
						$finish;
					end 
				«ENDFOR»
			
				«FOR port : outputs»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								// -- Open golden reference for port: «port.name»_«i»
								«port.name»_«i»_data_file = $fopen("«IF instance !== null»«instancePortFilePath(instance,port)»«ELSE»«fifoTracePath»«File.separator»«port.name».txt«ENDIF»", "r");
								if(«port.name»_«i»_data_file == `NULL) begin
									$display("Error: File «name»_«i»_«port.name».txt does not exist");
									$finish;
								end
							«ENDFOR»
						«ELSE»
							// -- Open golden reference for port: «port.name»
							«port.name»_data_file = $fopen("«IF instance !== null»«instancePortFilePath(instance,port)»«ELSE»«fifoTracePath»«File.separator»«port.name».txt«ENDIF»", "r");
							if(«port.name»_data_file == `NULL) begin
								$display("Error: File «name»_«port.name».txt does not exist");
								$finish;
							end
						«ENDIF»
					«ELSE»
						// -- Open golden reference for port: «port.name»
						«port.name»_data_file = $fopen("«IF instance !== null»«instancePortFilePath(instance,port)»«ELSE»«fifoTracePath»«File.separator»«port.name».txt«ENDIF»", "r");
						if(«port.name»_data_file == `NULL) begin
							$display("Error: File «name»_«port.name».txt does not exist");
							$finish;
						end
					«ENDIF»
				«ENDFOR»
				#55 reset_n = 1'b1;
				#10 start = 1'b1;
			end
		'''
	}

	def getClockGeneration() {
		'''
			// -- Clock Generation
			always #(cycle / 2) clock = !clock;
		'''
	}

	def getInputVectorReader(Port port) {
		'''
			// -- Read input token vector for Port: «port.name»
			always @(posedge clock) begin
				if(reset_n == 1'b1) begin
					if(q_«port.name»_full_n) begin
						if(!$feof(«port.name»_data_file)) begin
							«port.name»_scan_file = $fscanf(«port.name»_data_file, "%d\n", q_«port.name»_din_tmp);
							q_«port.name»_din <= q_«port.name»_din_tmp;
							q_«port.name»_write <= 1'b1;
						end else begin
							q_«port.name»_write <= 1'b0;
						end
					end
				end
			end
		'''
	}

	def getCompareWithGoldenReference(Port port) {
		'''
			// -- Compare «port.name» output tokesn with a given golden reference
			always @(posedge clock) begin
				if(!$feof(«port.name»_data_file)) begin
					if(«port.name»«portExtension»_write) begin
						«port.name»_scan_file = $fscanf(«port.name»_data_file, "%d\n", «port.name»_exp_value);
						if( «port.name»«portExtension»_din != «port.name»_exp_value) begin
							$display("Time: %0d ns, Port «port.name»: Error !!! Expected value does not match golden reference, Token Counter: %0d", $time, «port.name»_token_counter); 
							$display("\tGot      : %0d", «port.name»«portExtension»_din);
							$display("\tExpected : %0d", «port.name»_exp_value);
							$finish;
						end else begin
							$display("Time: %0d ns, Port «port.name» : Expected value on Port «port.name», matches golden reference, Token Counter: %0d", $time, «port.name»_token_counter); 
							$display("\tGot      : %0d", «port.name»«portExtension»_din);
							$display("\tExpected : %0d", «port.name»_exp_value);
						end
						«port.name»_token_counter <= «port.name»_token_counter + 1;	
					end
				end else begin
					«port.name»_end_of_file <= 1'b1;
				end
			end
		'''
	}
	
	def getCompareWithGoldenReference(Port port, int i) {
		'''
			// -- Compare «port.name»_«i» output tokesn with a given golden reference
			always @(posedge clock) begin
				if(!$feof(«port.name»_«i»_data_file)) begin
					if(«port.name»_«i»«portExtension»_write) begin
						«port.name»_«i»_scan_file = $fscanf(«port.name»_«i»_data_file, "%d\n", «port.name»_«i»_exp_value);
						if( «port.name»_«i»«portExtension»_din != «port.name»_«i»_exp_value) begin
							$display("Time: %0d ns, Port «port.name»_«i»: Error !!! Expected value does not match golden reference, Token Counter: %0d", $time, «port.name»_«i»_token_counter); 
							$display("\tGot      : %0d", «port.name»_«i»«portExtension»_din);
							$display("\tExpected : %0d", «port.name»_«i»_exp_value);
							$finish;
						end else begin
							$display("Time: %0d ns, Port «port.name»_«i» : Expected value on Port «port.name»_«i», matches golden reference, Token Counter: %0d", $time, «port.name»_«i»_token_counter); 
							$display("\tGot      : %0d", «port.name»_«i»«portExtension»_din);
							$display("\tExpected : %0d", «port.name»_«i»_exp_value);
						end
						«port.name»_«i»_token_counter <= «port.name»_«i»_token_counter + 1;	
					end
				end else begin
					«port.name»_«i»_end_of_file <= 1'b1;
				end
			end
		'''
	}
	
	

	def getEndofSimulation() {
		'''
			«IF enableProfiling»
				integer xml_report_data_file;
			«ENDIF»
			
			«IF !outputs.empty»
				// -- End Of Simulation
				always @(posedge clock) begin
					if(«FOR port : outputs SEPARATOR " && "»«IF instance !== null»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR " && "»«port.name»_«i»_end_of_file«ENDFOR»«ELSE»«port.name»_end_of_file«ENDIF»«ELSE»«port.name»_end_of_file«ENDIF»«ENDFOR») begin
						$display("Simulation has terminated !");
						«IF enableProfiling»
							xml_report_data_file = $fopen("«name»_dynamic_weights.xml", "w");
							$fdisplay(xml_report_data_file,"<actors>");
							«IF instance !== null»
								$fdisplay(xml_report_data_file,"\t<actor id=\"«instance.simpleName»\">");
								$fdisplay(xml_report_data_file,"\t\t<actions>");
								«FOR action : instance.getActor.actions»
									$fdisplay(xml_report_data_file,"\t\t\t<action id=\"«action.name»\" clockcycles-min=\"%0d\" clockcycles=\"%0d\" clockcycles-max=\"%0d\"/>", «instance.simpleName»_«action.name»_clk_counter_min, «instance.simpleName»_«action.name»_clk_counter_acc/«instance.simpleName»_«action.name»_clk_counter_nbrexec, «instance.simpleName»_«action.name»_clk_counter_max);
								«ENDFOR»
								$fdisplay(xml_report_data_file,"\t\t</actions>");
								$fdisplay(xml_report_data_file,"\t</actor>");
							«ELSE»
								«FOR instance : network.children.filter(typeof(Instance))»
									$fdisplay(xml_report_data_file,"\t<actor id=\"«instance.simpleName»\">");
									$fdisplay(xml_report_data_file,"\t\t<actions>");
									«FOR action : instance.getActor.actions»
										$fdisplay(xml_report_data_file,"\t\t\t<action id=\"«action.name»\" clockcycles-min=\"%0d\" clockcycles=\"%0d\" clockcycles-max=\"%0d\"/>",  «instance.simpleName»_«action.name»_clk_counter_min, «instance.simpleName»_«action.name»_clk_counter_acc/«instance.simpleName»_«action.name»_clk_counter_nbrexec, «instance.simpleName»_«action.name»_clk_counter_max);
									«ENDFOR»
									$fdisplay(xml_report_data_file,"\t\t</actions>");
									$fdisplay(xml_report_data_file,"\t</actor>");
								«ENDFOR»
							«ENDIF»
							$fdisplay(xml_report_data_file,"</actors>");
						«ENDIF»
						$finish;
					end
				end
			«ENDIF»
		'''
	}
	
	
	def getInputFifos(){
		'''
			«FOR port : inputs»
				FIFO #( 
					.MEM_STYLE("block"),
					.DATA_WIDTH(«port.type.sizeInBits»),
					.ADDR_WIDTH(9)
				) «port.name»_queue(
					.clk(clock),
					.reset_n(reset_n),
					.if_full_n(q_«port.name»_full_n),
					.if_write(q_«port.name»_write),
					.if_din(q_«port.name»_din),
					
					.if_empty_n(«port.name»«portExtension»_empty_n),
					.if_read(«port.name»«portExtension»_read),
					.if_dout(«port.name»«portExtension»_dout)
				);
			«ENDFOR»
		'''
	}
	
	def getDut(){
		'''
			«IF network !== null»
				«name»_wrapper dut(
					«FOR port : inputs»
						.n_«port.name»_rd_data(«port.name»_dout),
						.n_«port.name»_empty_n(«port.name»_empty_n),
						.n_«port.name»_rd_en(«port.name»_read),
					«ENDFOR»
					«FOR port : outputs»
						.n_«port.name»_wr_data(«port.name»_din),
						.n_«port.name»_full_n(«port.name»_full_n),
						.n_«port.name»_wr_en(«port.name»_write),
					«ENDFOR»
					.start(start),
					.ap_clk(clock),
					.ap_rst_n(reset_n)
				);
			«ELSE»
				«IF enableProfiling»
					wire «instance.name»_action_id_full_n;
					assign «instance.name»_action_id_full_n = 1'b1;
				«ENDIF»
				
				«name»_wrapper dut(
					.start(start),
					.ap_clk(clock),
					.ap_rst(reset_n),
					«IF enableProfiling»
						.action_id_V_din(«instance.simpleName»_action_id),
						.action_id_V_full_n(«instance.name»_action_id_full_n),
						.action_id_V_write(«instance.simpleName»_action_id_write),
					«ENDIF»
					«FOR port : inputs»
						.«port.name»«portExtension»_rd_data(«port.name»«portExtension»_dout),
						.«port.name»«portExtension»_empty_n(«port.name»«portExtension»_empty_n),
						.«port.name»«portExtension»_rd_en(«port.name»«portExtension»_read),
					«ENDFOR»
					«FOR port : outputs SEPARATOR  ","»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR  ","»
								.«port.name»_«i»«portExtension»_wr_data(«port.name»_«i»«portExtension»_din),
								.«port.name»_«i»«portExtension»_full_n(«port.name»_«i»«portExtension»_full_n),
								.«port.name»_«i»«portExtension»_wr_en(«port.name»_«i»«portExtension»_write)
							«ENDFOR»
						«ELSE»
							.«port.name»«portExtension»_wr_data(«port.name»«portExtension»_din),
							.«port.name»«portExtension»_full_n(«port.name»«portExtension»_full_n),
							.«port.name»«portExtension»_wr_en(«port.name»«portExtension»_write)
						«ENDIF»
					«ENDFOR»
				);
			«ENDIF»
		'''
	}
	
	def profiledWires(){
		'''
			«IF instance !== null»
				«profileInstance(instance)»
			«ELSE»
				«FOR instance : network.children.filter(typeof(Instance))»
					«profileInstance(instance)»
				«ENDFOR»
			«ENDIF»
		'''
	}
	
	def profileInstance(Instance instance){
		'''
			wire [31:0] «instance.simpleName»_action_id = dut.«IF network !== null»i_«instance.simpleName».«ENDIF»action_id_V_din;
			wire «instance.simpleName»_action_id_write = dut.«IF network !== null»i_«instance.simpleName».«ENDIF»action_id_V_write;
			
			reg [31:0] «instance.simpleName»_clk_counter;
			
			«FOR action : instance.getActor.actions»
				reg [31:0] «instance.simpleName»_«action.name»_clk_counter_nbrexec;
				reg [31:0] «instance.simpleName»_«action.name»_clk_counter_min;
				reg [31:0] «instance.simpleName»_«action.name»_clk_counter_max;
				reg [31:0] «instance.simpleName»_«action.name»_clk_counter_acc;
			«ENDFOR»
			
			initial begin
				«instance.simpleName»_clk_counter = 0;
				«FOR action : instance.getActor.actions»
					«instance.simpleName»_«action.name»_clk_counter_nbrexec = 0;
					«instance.simpleName»_«action.name»_clk_counter_acc = 0;
					«instance.simpleName»_«action.name»_clk_counter_min = 0;
					«instance.simpleName»_«action.name»_clk_counter_max = 0;
				«ENDFOR»
			end
			
			always@(posedge clock) begin
				if (reset_n == 1'b0) begin
					«FOR action : instance.getActor.actions»
						«instance.simpleName»_«action.name»_clk_counter_nbrexec = 0;
						«instance.simpleName»_«action.name»_clk_counter_acc = 0;
						«instance.simpleName»_«action.name»_clk_counter_min = 0;
						«instance.simpleName»_«action.name»_clk_counter_max = 0;
					«ENDFOR»
				end else begin
					if («instance.simpleName»_action_id_write == 1'b1) begin
						«FOR action : instance.getActor.actions»
							«IF instance.getActor.actions.indexOf(action) == 0 »
								if(«instance.simpleName»_action_id == «instance.getActor.actions.indexOf(action)») begin
									«instance.simpleName»_«action.name»_clk_counter_nbrexec = «instance.simpleName»_«action.name»_clk_counter_nbrexec + 1;
									«instance.simpleName»_«action.name»_clk_counter_acc = «instance.simpleName»_«action.name»_clk_counter_acc + «instance.simpleName»_clk_counter;
									if(«instance.simpleName»_clk_counter < «instance.simpleName»_«action.name»_clk_counter_min) begin
										«instance.simpleName»_«action.name»_clk_counter_min = «instance.simpleName»_clk_counter;
									end
									if(«instance.simpleName»_clk_counter > «instance.simpleName»_«action.name»_clk_counter_max) begin
										«instance.simpleName»_«action.name»_clk_counter_max = «instance.simpleName»_clk_counter;
									end
								end
							«ELSE»
								else if(«instance.simpleName»_action_id == «instance.getActor.actions.indexOf(action)») begin
									«instance.simpleName»_«action.name»_clk_counter_nbrexec = «instance.simpleName»_«action.name»_clk_counter_nbrexec + 1;
									«instance.simpleName»_«action.name»_clk_counter_acc = «instance.simpleName»_«action.name»_clk_counter_acc + «instance.simpleName»_clk_counter;
									if(«instance.simpleName»_clk_counter < «instance.simpleName»_«action.name»_clk_counter_min) begin
										«instance.simpleName»_«action.name»_clk_counter_min = «instance.simpleName»_clk_counter;
									end
									if(«instance.simpleName»_clk_counter > «instance.simpleName»_«action.name»_clk_counter_max) begin
										«instance.simpleName»_«action.name»_clk_counter_max = «instance.simpleName»_clk_counter;
									end
								end
							«ENDIF»
						«ENDFOR»
						«instance.simpleName»_clk_counter = 0;
					end else begin
						«instance.simpleName»_clk_counter = «instance.simpleName»_clk_counter + 1;
					end
				end
			end
		'''
	}
	
	// -- Helper definitions
	def getFileHeader(String comment) {
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- Exelixi Design Systems 
			// -- Copyright (C) 2016 Exelixi. All rights reserved.
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- «comment»
			// ----------------------------------------------------------------------------
		'''
	}
	
	def getPortExtension(){
		'''«IF instance !== null»«IF typeAccuracy»_V_V«ELSE»_V«ENDIF»«ENDIF»'''
	}
	
	// -- Helper Methods
	private def String instancePath(Instance instance) {
		var dir = fifoTracePath + File.separator;
		for (String sp : instance.hierarchicalId.subList(1, instance.hierarchicalId.size)) {
			dir += (sp + File.separator);
		}
		return dir;
	}

	private def String instancePortFilePath(Instance instance, Port port) {
		return instancePath(instance) + port.name + ".txt";
	}

}
