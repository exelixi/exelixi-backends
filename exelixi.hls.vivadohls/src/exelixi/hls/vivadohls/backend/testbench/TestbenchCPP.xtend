/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.hls.vivadohls.backend.testbench

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import java.util.ArrayList
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Set
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port
import net.sf.orcc.ir.Var

import static exelixi.hls.backend.Constants.AXI_STREAM_ACTOR_COMMUNICATION
import static exelixi.hls.backend.Constants.FIFO_TRACE_PATH
import static exelixi.hls.backend.Constants.INSTANCE_EXTERNAL_VARIABLES

/**
 * 
 * @author Endri Bezati
 */
class TestbenchCPP extends HLSTemplate {

	Instance instance

	Network network

	String name

	TypePrinterVisitor typePrinter

	List<Port> inputs

	List<Port> outputs

	Map<Port, List<Connection>> outgoingPortMap

	String fifoTracePath
	
	boolean axiStreamActorCommunication
	
	List<String> parameters
	
	List<Var> externalVariables

	new(Instance instance, TypePrinterVisitor typePrinter, Map<String, Object> options) {
		super(typePrinter)
		this.name = instance.simpleName
		this.instance = instance
		this.typePrinter = typePrinter
		this.inputs = instance.getActor.inputs
		this.outputs = instance.getActor.outputs
		this.outgoingPortMap = instance.outgoingPortMap
		this.fifoTracePath = options.get(FIFO_TRACE_PATH) as String
		this.parameters = new ArrayList<String>
		this.axiStreamActorCommunication = options.getOrDefault(AXI_STREAM_ACTOR_COMMUNICATION, false) as Boolean
		if (instance.getActor.hasAttribute(INSTANCE_EXTERNAL_VARIABLES)) {
			externalVariables = instance.getActor.getAttribute(INSTANCE_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
		}else{
			externalVariables = new ArrayList<Var>
		}
		getParameters()
	}

	new(Network network, TypePrinterVisitor typePrinter, Map<String, Object> options) {
		super(typePrinter)
		this.name = network.simpleName
		this.network = network
		this.typePrinter = typePrinter
		this.inputs = network.inputs
		this.outputs = network.outputs
		this.fifoTracePath = options.get(FIFO_TRACE_PATH) as String
		this.parameters = new ArrayList<String>
		externalVariables = new ArrayList<Var>
		getParameters()
	}

	def getParameters(){
		var Set<Port> portsWithCount = new HashSet
		
		if(network === null){
			portsWithCount = TemplatesUtils.getPortsWithCount(instance)
		}
		
		// -- Input parameters
		for (port : inputs) {
			val String value = port.name
			parameters.add(value)
			if(network === null){
				if (axiStreamActorCommunication) {
					if(portsWithCount.contains(port)){
						val String count = port.name + "_count"
						parameters.add(count)
					}
				}
			}
		}

		// -- Output Parameters
		for (port : outputs) {
			if(network === null){
				if (outgoingPortMap.get(port).size > 1) {
					for (connection : outgoingPortMap.get(port)) {
						val String value = port.name + "_" + outgoingPortMap.get(port).indexOf(connection)
						parameters.add(value)
						if (axiStreamActorCommunication) {
							if(portsWithCount.contains(port)){
								val String count = port.name + "_" + outgoingPortMap.get(port).indexOf(connection) + "_count"
								parameters.add(count)
							}
						}
					}
				} else {
					val String value = port.name
					parameters.add(value)
					if (axiStreamActorCommunication) {
						if(portsWithCount.contains(port)){
							val String count = port.name + "_count"
							parameters.add(count)
						}
					}
				}
			}else{
				val String value = port.name
				parameters.add(value)
			}
		}

		// -- External Variables
		for (variable : externalVariables) {
			val String value = variable.name
			parameters.add(value)
		}
	}


	def getContent() {
		'''
			«getFileHeader("Exilixi for Vivado HLS, C++ Testbench Code Generation")»
			
			«headers»
			
			«IF !externalVariables.empty»«externalMemories»«ENDIF»
			
			int main(int argc, char *argv[]){
				«fileStreams»
				
				«channels»
				
				«fillInputStreams»
				
				«fillOutputQueues»
				
				«initTokensCounters»
				
				bool end_of_execution = false;
				
				«IF network === null»«getAXIStreamCount»«ENDIF»
				
				while(!end_of_execution){
					// execute the network under test
					«name»(«FOR parameter : parameters SEPARATOR ", "»«parameter»«ENDFOR»);
				
					«compareGoldenOutput»
				
					«computeEndOfExecution»
				}
				
				std::cout << "Passed testbench for «name» !" << std::endl;
				
				«FOR port : outputs»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								std::cout <<  "Port «port.name»_«i» : " << «port.name»_«i»_token_counter << " produced." << std::endl;
							«ENDFOR»
						«ELSE»
							std::cout <<  "Port «port.name» : " << «port.name»_token_counter << " produced." << std::endl;
						«ENDIF»
					«ELSE»
						std::cout <<  "Port «port.name» : " << «port.name»_token_counter << " produced." << std::endl;
					«ENDIF»
				«ENDFOR»
				
				return 0;
			}
		'''
	}
	
	def getExternalMemories(){
		'''
			«FOR variable : externalVariables»
				volatile «variable.type.doSwitch» «variable.name»[«getFlatListDepth(variable.type)»];
			«ENDFOR»
		'''
	}

	def getAXIStreamCount(){
		var Set<Port> portsWithCount = TemplatesUtils.getPortsWithCount(instance)
		var List<String> counts = new ArrayList
		
		for (port : instance.getActor.inputs) {
			if (axiStreamActorCommunication) {
				if(portsWithCount.contains(port)){
					val String count = "int " + port.name + "_count"
					counts.add(count)
				}
			}
		}
		
		
		for (port : instance.getActor.outputs) {
			if (outgoingPortMap.get(port).size > 1) {
				for (connection : outgoingPortMap.get(port)) {
					if (axiStreamActorCommunication) {
						if(portsWithCount.contains(port)){
							val String count = "int " + port.name + "_" + outgoingPortMap.get(port).indexOf(connection) +
								"_count"
							counts.add(count)
						}
					}
				}
			} else {
				if (axiStreamActorCommunication) {
					if(portsWithCount.contains(port)){
						val String count = "int " + port.name + "_count"
						counts.add(count)
					}
				}
			}
		}

		'''
			«FOR count : counts»
				«count»;
			«ENDFOR»
		'''
	}

	def initTokensCounters(){
		'''
			«FOR port : outputs»
				«IF instance !== null»
					«IF outgoingPortMap.get(port).size > 1»
						«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
							int «port.name»_«i»_token_counter = 0;
						«ENDFOR»
					«ELSE»
						int «port.name»_token_counter = 0;
					«ENDIF»
				«ELSE»
					int «port.name»_token_counter = 0;
				«ENDIF»
			«ENDFOR»
		'''
	}

	def getComputeEndOfExecution() {
		'''
			«IF !outputs.empty»
				end_of_execution = «FOR port : outputs SEPARATOR " && "»«IF instance !== null»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR " && "»qref_«port.name»_«i».empty()«ENDFOR»«ELSE»qref_«port.name».empty()«ENDIF»«ELSE»qref_«port.name».empty()«ENDIF»«ENDFOR»;
			«ELSEIF !inputs.empty»
				end_of_execution = «FOR port : inputs SEPARATOR " && "»«port.name».empty()«ENDFOR»;
			«ELSE»
				end_of_execution = true;
			«ENDIF»
		'''
	}

	def getHeaders() {
		'''
			#include <fstream>
			#include <sstream>
			#include <string>
			#include <iostream>
			#include "«typePrinter.intIncludeHeaderFile»"
			#include "«typePrinter.channelIncludeHeaderFile»"
			
			#include "«name».h"
		'''
	}

	def getFileStreams() {
		'''
			«IF !inputs.empty && !outputs.empty»// -- File Streams«ENDIF»
			«IF !inputs.empty»// -- Actor Input Vector of Tokens«ENDIF»
			«FOR port : inputs»
				std::ifstream «port.name»_file("«portFilePath(instance,port)»");
				if(«port.name»_file.fail()){
					std::cout <<"«port.name» input file not found!" << std::endl;
					return 1;
				}
			«ENDFOR»
			
			«IF !outputs.empty»// -- Actor Output Vector of Tokens, golden reference«ENDIF»
			«FOR port : outputs»
				«IF instance !== null»
					«IF outgoingPortMap.get(port).size > 1»
						«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
							std::ifstream «port.name»_«i»_file("«portFilePath(instance,port)»");
							if(«port.name»_«i»_file.fail()){
								std::cout <<"«port.name» reference file not found!" << std::endl;
								return 1;
							}
						«ENDFOR»
					«ELSE»
						std::ifstream «port.name»_file("«portFilePath(instance,port)»");
						if(«port.name»_file.fail()){
							std::cout <<"«port.name» reference file not found!" << std::endl;
							return 1;
						}
					«ENDIF»
				«ELSE»
					std::ifstream «port.name»_file("«portFilePath(instance,port)»");
					if(«port.name»_file.fail()){
						std::cout <<"«port.name» reference file not found!" << std::endl;
						return 1;
					}
				«ENDIF»
			«ENDFOR»
		'''
	}

	def getFillOutputQueues() {
		'''
			«FOR port : outputs SEPARATOR "\n"»
				«IF instance !== null»
					«IF outgoingPortMap.get(port).size > 1»
						«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
							// -- Store «port.name»_«i» token to the reference queue
							std::string «port.name»_«i»_line;
							while(std::getline(«port.name»_«i»_file, «port.name»_«i»_line)){
								if(!«port.name»_«i»_line.empty()){
									std::istringstream iss(«port.name»_«i»_line);
									int «port.name»_«i»_tmp;
									iss >> «port.name»_«i»_tmp;
									qref_«port.name»_«i».push((«port.type.doSwitch») «port.name»_«i»_tmp);
								}
							}
						«ENDFOR»
					«ELSE»
						// -- Store «port.name» token to the reference queue
						std::string «port.name»_line;
						while(std::getline(«port.name»_file, «port.name»_line)){
							if(!«port.name»_line.empty()){
								std::istringstream iss(«port.name»_line);
								int «port.name»_tmp;
								iss >> «port.name»_tmp;
								qref_«port.name».push((«port.type.doSwitch») «port.name»_tmp);
							}
						}
					«ENDIF»
				«ELSE»
					// -- Store «port.name» token to the reference queue
					std::string «port.name»_line;
					while(std::getline(«port.name»_file, «port.name»_line)){
						if(!«port.name»_line.empty()){
							std::istringstream iss(«port.name»_line);
							int «port.name»_tmp;
							iss >> «port.name»_tmp;
							qref_«port.name».push((«port.type.doSwitch») «port.name»_tmp);
						}
					}
				«ENDIF»
			«ENDFOR»
		'''

	}

	def getChannels() {
		'''
			«IF !inputs.empty»// -- Input Channels«ENDIF»
			«FOR port : inputs»
				hls::stream< «port.type.doSwitch» > «port.name»;
			«ENDFOR»
			
			«IF !outputs.empty»// -- Output Channels and refernce queues«ENDIF»
			«FOR port : outputs»
				«IF instance !== null»
					«IF outgoingPortMap.get(port).size > 1»
						«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
							hls::stream< «port.type.doSwitch» > «port.name»_«i»;
							std::queue< «port.type.doSwitch» > qref_«port.name»_«i»;
						«ENDFOR»
					«ELSE»
						hls::stream< «port.type.doSwitch» > «port.name»;
						std::queue< «port.type.doSwitch» > qref_«port.name»;
					«ENDIF»
				«ELSE»
					hls::stream< «port.type.doSwitch» > «port.name»;
					std::queue< «port.type.doSwitch» > qref_«port.name»;
				«ENDIF»
			«ENDFOR»
		'''
	}

	def fillInputStreams() {
		'''
			«FOR port : inputs SEPARATOR "\n"»
				// -- Write «port.name» token to the stream
				std::string «port.name»_line;
				while(std::getline(«port.name»_file, «port.name»_line)){
					std::istringstream iss(«port.name»_line);
					int «port.name»_tmp;
					iss >> «port.name»_tmp;
					«port.name».write((«port.type.doSwitch») «port.name»_tmp);
				}
			«ENDFOR»
		'''
	}

	def compareGoldenOutput() {
		'''
			«FOR port : outputs»
				«IF instance !== null»
					«IF outgoingPortMap.get(port).size > 1»
						«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
							if(!«port.name»_«i».empty() && !qref_«port.name»_«i».empty()){
								«port.type.doSwitch» got_value = «port.name»_«i».read();
								«port.type.doSwitch» ref_value = qref_«port.name»_«i».front();
								qref_«port.name»_«i».pop();
								if (got_value != ref_value) {
									std::cout << "Port «port.name»_«i»: Error !!! Expected value does not match golden reference, Token Counter: " << «port.name»_«i»_token_counter << std::endl;
									std::cout << "Expected: " <<  ref_value << std::endl;
									std::cout << "Got: " << got_value << std::endl;
									return 1;
								}
							
								«port.name»_«i»_token_counter++;
							
							}
						«ENDFOR»
					«ELSE»
						if(!«port.name».empty() && !qref_«port.name».empty()){
							«port.type.doSwitch» got_value = «port.name».read();
							«port.type.doSwitch» ref_value = qref_«port.name».front();
							qref_«port.name».pop();
							if (got_value != ref_value) {
								std::cout << "Port «port.name»: Error !!! Expected value does not match golden reference, Token Counter: " << «port.name»_token_counter << std::endl;
								std::cout << "Expected: " <<  ref_value << std::endl;
								std::cout << "Got: " << got_value << std::endl;
								return 1;
							}
							
							«port.name»_token_counter++;
							
						}
					«ENDIF»
				«ELSE»
					if(!«port.name».empty() && !qref_«port.name».empty()){
						«port.type.doSwitch» got_value = «port.name».read();
						«port.type.doSwitch» ref_value = qref_«port.name».front();
						qref_«port.name».pop();
						if (got_value != ref_value) {
							std::cout << "Port «port.name»: Error !!! Expected value does not match golden reference, Token Counter: " << «port.name»_token_counter << std::endl;
							std::cout << "Expected: " <<  ref_value << std::endl;
							std::cout << "Got: " << got_value << std::endl;
							return 1;
						}
						
						«port.name»_token_counter++;
						
					}
				«ENDIF»
			«ENDFOR»
		'''
	}

	private def String portFilePath(Instance instance, Port port) {
		var dir = "fifo-traces/";
		if (instance !== null) {
			for (String sp : instance.hierarchicalId.subList(1, instance.hierarchicalId.size)) {
				dir += (sp + "/");
			}
		}
		return dir + port.name + ".txt";
	}

}
