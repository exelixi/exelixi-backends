/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.hls.vivadohls.backend.fifo

/**
 * A Synchronous FIFO Queue
 * 
 * @author Xilinx Inc.
 */
class FifoPrinter {

	def getContent() {
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- FIFO Queue from Xilinx
			// -- Modified to include count output, removed clock enables in write & read
			// -- Copyright (C) 2015 Xilinx Inc
			// ----------------------------------------------------------------------------
			`timescale 1ns/1ps
			
			module FIFO
			#(parameter
			    MEM_STYLE  = "auto",
			    DATA_WIDTH = 32,
			    ADDR_WIDTH = 9
			)
			(
			    // system signal
			    input  wire                  clk,
			    input  wire                  reset_n,
			
			    // write
			    output wire                  if_full_n,
			    input  wire                  if_write,
			    input  wire [DATA_WIDTH-1:0] if_din,
			
			    // read
			    output wire                  if_empty_n,
			    input  wire                  if_read,
			    output wire [DATA_WIDTH-1:0] if_dout,
			
			    // peek
			    output wire [DATA_WIDTH-1:0] peek,
			
			    // size
			    output wire [31:0] size,
			
			    // count
			    output reg [31:0] count 
			);
			
			localparam DEPTH = 1 << ADDR_WIDTH;
			
			//------------------------Local signal-------------------
			(* ram_style = MEM_STYLE *)
			reg  [DATA_WIDTH-1:0] mem[0:DEPTH-1];
			reg  [DATA_WIDTH-1:0] q_buf = 1'b0;
			reg  [ADDR_WIDTH-1:0] waddr = 1'b0;
			reg  [ADDR_WIDTH-1:0] raddr = 1'b0;
			wire [ADDR_WIDTH-1:0] wnext;
			wire [ADDR_WIDTH-1:0] rnext;
			wire                  push;
			wire                  pop;
			reg  [ADDR_WIDTH-1:0] usedw = 1'b0;
			reg                   full_n = 1'b1;
			reg                   empty_n = 1'b0;
			reg  [DATA_WIDTH-1:0] q_tmp = 1'b0;
			reg                   show_ahead = 1'b0;
			reg  [DATA_WIDTH-1:0] dout_buf = 1'b0;
			reg                   dout_valid = 1'b0;
			
			//------------------------Body---------------------------
			assign if_full_n  = full_n;
			assign if_empty_n = dout_valid;
			assign if_dout    = dout_buf;
			assign push       = full_n & if_write;
			assign pop        = empty_n & (~dout_valid | if_read);
			assign wnext      = !push                ? waddr :
			                    (waddr == DEPTH - 1) ? 1'b0  :
			                    waddr + 1'b1;
			assign rnext      = !pop                 ? raddr :
			                    (raddr == DEPTH - 1) ? 1'b0  :
			                    raddr + 1'b1;
			assign peek       = dout_buf;
			assign size       = DEPTH;
			
			
			// waddr
			always @(posedge clk) begin
			    if (reset_n == 1'b0)
			        waddr <= 1'b0;
			    else
			        waddr <= wnext;
			end
			
			// raddr
			always @(posedge clk) begin
			    if (reset_n == 1'b0)
			        raddr <= 1'b0;
			    else
			        raddr <= rnext;
			end
			
			// usedw
			always @(posedge clk) begin
			    if (reset_n == 1'b0)
			        usedw <= 1'b0;
			    else if (push & ~pop)
			        usedw <= usedw + 1'b1;
			    else if (~push & pop)
			        usedw <= usedw - 1'b1;
			end
			
			// count
			always @(posedge clk) begin
			    if (reset_n == 1'b0)
			        count <= 1'b0;
			    else if((if_full_n & if_write) & (if_empty_n & if_read))
			    	count <= count;
			    else if (if_full_n & if_write)
			        count <= count + 1'b1;
			    else if (if_empty_n & if_read)
			        count <= count - 1'b1;
			    else
			    	count <= count;
			end
			
			// full_n
			always @(posedge clk) begin
			    if (reset_n == 1'b0)
			        full_n <= 1'b1;
			    else if (push & ~pop)
			        full_n <= (usedw != DEPTH - 1);
			    else if (~push & pop)
			        full_n <= 1'b1;
			end
			
			// empty_n
			always @(posedge clk) begin
			    if (reset_n == 1'b0)
			        empty_n <= 1'b0;
			    else if (push & ~pop)
			        empty_n <= 1'b1;
			    else if (~push & pop)
			        empty_n <= (usedw != 1'b1);
			end
			
			// mem
			always @(posedge clk) begin
			    if (push)
			        mem[waddr] <= if_din;
			end
			
			// q_buf
			always @(posedge clk) begin
			    q_buf <= mem[rnext];
			end
			
			// q_tmp
			always @(posedge clk) begin
			    if (reset_n == 1'b0)
			        q_tmp <= 1'b0;
			    else if (push)
			        q_tmp <= if_din;
			end
			
			// show_ahead
			always @(posedge clk) begin
			    if (reset_n == 1'b0)
			        show_ahead <= 1'b0;
			    else if (push && usedw == pop)
			        show_ahead <= 1'b1;
			    else
			        show_ahead <= 1'b0;
			end
			
			// dout_buf
			always @(posedge clk) begin
			    if (reset_n == 1'b0)
			        dout_buf <= 1'b0;
			    else if (pop)
			        dout_buf <= show_ahead? q_tmp : q_buf;
			end
			
			// dout_valid
			always @(posedge clk) begin
			    if (reset_n == 1'b0)
			        dout_valid <= 1'b0;
			    else if (pop)
			        dout_valid <= 1'b1;
			    else if (if_read)
			        dout_valid <= 1'b0;
			end
			
			endmodule
		'''
	}
}
