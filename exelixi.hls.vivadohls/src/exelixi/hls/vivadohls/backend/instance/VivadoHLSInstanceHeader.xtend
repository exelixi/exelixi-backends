/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.instance

import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import java.util.ArrayList
import java.util.List
import java.util.Map
import java.util.Set
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.graph.Edge
import net.sf.orcc.ir.Var
import org.eclipse.emf.common.util.EList

import static exelixi.hls.backend.Constants.AXI_STREAM_ACTOR_COMMUNICATION
import static exelixi.hls.backend.Constants.INSTANCE_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION

/**
 * 
 * @author Endri Bezati
 */
class VivadoHLSInstanceHeader extends HLSTemplate {

	Instance instance

	protected Actor actor

	String name

	List<Port> inputs

	List<Port> outputs

	TypePrinterVisitor typePrinter

	Partitioner partitioner

	List<Connection> plToPsFifos

	List<Connection> psToPlFifos

	EList<Edge> incoming

	Map<Port, List<Connection>> outgoingPortMap

	boolean axiStreamActorCommunication

	new(Instance instance, TypePrinterVisitor typePrinter, Partitioner partitioner, Map<String, Object> options) {
		super(typePrinter)
		this.typePrinter = typePrinter
		this.instance = instance;
		this.actor = instance.getActor
		this.name = instance.simpleName
		this.inputs = instance.getActor.inputs
		this.outputs = instance.getActor.outputs
		this.axiStreamActorCommunication = options.getOrDefault(AXI_STREAM_ACTOR_COMMUNICATION, false) as Boolean
		this.partitioner = partitioner
		this.plToPsFifos = partitioner.getConnections(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION)
		this.psToPlFifos = partitioner.getConnections(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION)
		this.incoming = instance.incoming
		this.outgoingPortMap = instance.outgoingPortMap
	}

	def getContent() {
		var List<Var> externalVariables = new ArrayList<Var>
		if (instance.getActor.hasAttribute(INSTANCE_EXTERNAL_VARIABLES)) {
			externalVariables = instance.getActor.getAttribute(INSTANCE_EXTERNAL_VARIABLES).
				objectValue as ArrayList<Var>
		}

		var List<String> parameters = new ArrayList<String>

		val Set<Port> portsWithCount = TemplatesUtils.getPortsWithCount(instance);
		// -- Input parameters
		for (port : actor.inputs) {
			val String value = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" + " &" + port.name
			parameters.add(value)
			if (axiStreamActorCommunication) {
				if (portsWithCount.contains(port)) {
					val String count = "int " + port.name + "_count"
					parameters.add(count)
				}
			}
		}

		// -- Output Parameters
		for (port : actor.outputs) {
			if (outgoingPortMap.get(port).size > 1) {
				for (connection : outgoingPortMap.get(port)) {
					val String value = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" + " &" +
						port.name + "_" + outgoingPortMap.get(port).indexOf(connection)
					parameters.add(value)
					if (axiStreamActorCommunication) {
						if (portsWithCount.contains(port)) {
							val String count = "int " + port.name + "_" +
								outgoingPortMap.get(port).indexOf(connection) + "_count"
							parameters.add(count)
						}
					}
				}
			} else {
				val String value = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" + " &" + port.name
				parameters.add(value)
				if (axiStreamActorCommunication) {
					if (portsWithCount.contains(port)) {
						val String count = "int " + port.name + "_count"
						parameters.add(count)
					}
				}
			}
		}

		// -- External Variables
		for (variable : externalVariables) {
			val String value = "volatile " + variable.type.doSwitch + " * " + variable.name
			parameters.add(value)
		}

		'''
			«getFileHeader("Vivado HLS Actor Header Code Generation, Instance: " + instance.label)»
			#include <«typePrinter.intIncludeHeaderFile»>
			#include <«typePrinter.channelIncludeHeaderFile»>
			
			void «TemplatesUtils.getInstanceName(instance)»(«FOR parameter : parameters SEPARATOR ", "»«parameter»«ENDFOR»);
		'''
	}

}
