/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.instance

import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.instance.AbstractInstancePrinter
import exelixi.hls.templates.instance.actionselection.ActionSelection
import exelixi.hls.templates.types.TypePrinterVisitor
import exelixi.hls.vivadohls.backend.instance.actionselection.ActionSelectionComplex
import exelixi.hls.vivadohls.backend.instance.actionselection.ActionSelectionGeneric
import java.util.ArrayList
import java.util.List
import java.util.Map
import net.sf.orcc.df.Action
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.graph.Edge
import net.sf.orcc.ir.InstCall
import net.sf.orcc.ir.Procedure
import net.sf.orcc.ir.TypeList
import net.sf.orcc.ir.Var
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.EMap

import static exelixi.hls.backend.Constants.ACTION_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ACTION_SCHEDULER_PEEKS
import static exelixi.hls.backend.Constants.AXI_STREAM_ACTOR_COMMUNICATION
import static exelixi.hls.backend.Constants.PROCEDURE_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION
import static net.sf.orcc.OrccLaunchConstants.*

/**
 * 
 * @author Endri Bezati
 */
class VivadoHLSInstance extends AbstractInstancePrinter {

	protected TypePrinterVisitor typePrinter

	protected Partitioner partitioner

	protected List<Connection> plToPsFifos

	protected List<Connection> psToPlFifos

	protected EList<Edge> incoming

	protected Map<Port, List<Connection>> outgoingPortMap

	protected Map<String, Object> options

	protected boolean axiStreamActorCommunication;

	new(Instance instance, TypePrinterVisitor typePrinter, Map<String, Object> options, Partitioner partitioner) {
		super(instance, typePrinter, options)
		this.options = options
		this.typePrinter = typePrinter
		this.partitioner = partitioner
		if (options.containsKey(FIFO_SIZE)) {
			fifoSize = options.get(FIFO_SIZE) as Integer
		} else {
			fifoSize = DEFAULT_FIFO_SIZE
		}
		this.incoming = instance.incoming
		this.outgoingPortMap = instance.outgoingPortMap

		this.axiStreamActorCommunication = options.getOrDefault(AXI_STREAM_ACTOR_COMMUNICATION, false) as Boolean

		this.plToPsFifos = partitioner.getConnections(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION)
		this.psToPlFifos = partitioner.getConnections(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION)
	}

	override getContent() {
		var ActionSelection actionSelection = getActionSelection
		'''
			«getFileHeader("Exilixi for Vivado HLS, Instance Code Generation, Instance: " + instance.label)»
			«headers»
			«instanceParameters»
			«stateVariables»
			«IF !actor.procs.empty»«funcitonsProcedures»«ENDIF»
			«actions»
			«actionSelection.actionSelection»
		'''
	}

	def ActionSelection getActionSelection() {
		if (isActorComplex) {
			return new ActionSelectionComplex(instance, typePrinter, partitioner, options)
		} else {
			return new ActionSelectionGeneric(instance, typePrinter, partitioner, options)
		}
	}

	override getHeaders() {
		'''
			#include <«typePrinter.intIncludeHeaderFile»>
			#include <«typePrinter.channelIncludeHeaderFile»>
			//#include <«name».h>
		'''
	}

	// -- Variables
	override getStateVariables() {
		'''
			«IF !actor.stateVars.empty || actor.fsm.states.size > 1 || isActorComplex »
				// ----------------------------------------------------------------------------
				// -- State Variables
			«ENDIF»
			««« 
			«FOR variable : actor.stateVars»
				«IF !variable.hasAttribute("xronos_ddr")»
					«variable.declare»
				«ENDIF»
			«ENDFOR»
			««« 
			«IF actor.fsm.states.size > 1 || isActorComplex»
				// -- Action Selection States
				enum state_t_«instance.simpleName» {«FOR state : actor.fsm.states SEPARATOR ", "»s_«instance.simpleName»_«state.label»«ENDFOR»};
				
				static state_t_«instance.simpleName» state =  s_«instance.simpleName»_«actor.fsm.initialState.name»;
			«ENDIF»
			««« 
			«IF isActorComplex»
				«IF !actor.inputs.empty»// -- Input Ports indexes«ENDIF»
				«FOR port : actor.inputs»
					«IF actor.getStateVar("p_" + port.name) !== null»
						static int p_«port.name»_token_index = 0;
						static int p_«port.name»_head = 0;
						const  int p_«port.name»_size = «(actor.getStateVar("p_" + port.name).type as TypeList).size»;
					«ENDIF»
				«ENDFOR»
			«ENDIF»
		'''
	}

	override print(Action action) {
		var externalVariables = new ArrayList<Var>
		if (action.hasAttribute(ACTION_EXTERNAL_VARIABLES)) {
			externalVariables = action.getAttribute(ACTION_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
		}

		val output = '''
			«printActionScheduler(action)»
			«IF axiStreamActorCommunication && !isActorComplex»
				static void «action.body.name»(«FOR port : action.inputPattern.ports SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDFOR»«IF !action.inputPattern.empty && !action.outputPattern.empty» , «ENDIF»«FOR port : action.outputPattern.ports SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR connection : outgoingPortMap.get(port) SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»_«outgoingPortMap.get(port).indexOf(connection)»«ENDFOR»«ELSE»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDIF»«ENDFOR»«IF !externalVariables.empty»«IF !action.inputPattern.ports.empty || !action.outputPattern.ports.empty», «ENDIF»«getExternalVarsDeclaration(externalVariables)»«ENDIF») {
			«ELSE»
				«IF isActorComplex»
				static void «action.body.name»(«FOR port : action.outputPattern.ports SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR connection : outgoingPortMap.get(port) SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»_«outgoingPortMap.get(port).indexOf(connection)»«ENDFOR»«ELSE»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDIF»«ENDFOR»«IF !externalVariables.empty»«IF !action.outputPattern.ports.empty», «ENDIF»«getExternalVarsDeclaration(externalVariables)»«ENDIF»){
				«ELSE»
					static void «action.body.name»(«FOR port : action.inputPattern.ports SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDFOR»«IF !action.inputPattern.empty && !action.outputPattern.empty» , «ENDIF»«FOR port : action.outputPattern.ports SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR connection : outgoingPortMap.get(port) SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»_«outgoingPortMap.get(port).indexOf(connection)»«ENDFOR»«ELSE»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDIF»«ENDFOR»«IF !externalVariables.empty»«IF !action.inputPattern.ports.empty || !action.outputPattern.ports.empty», «ENDIF»«getExternalVarsDeclaration(externalVariables)»«ENDIF») {
				«ENDIF»
			«ENDIF»
			«printActionBody(action)»
			}
		'''
		return output
	}

	protected def getExternalVarsDeclaration(List<Var> externalVars) {
		'''«FOR variable : externalVars SEPARATOR ", "»volatile «variable.type.doSwitch» * «variable.name»«ENDFOR»'''
	}

	override printActionScheduler(Action action) {
		var List<String> parameters = new ArrayList
		if(action.scheduler.hasAttribute(ACTION_SCHEDULER_PEEKS)){
			val Map<Port, List<Integer>> indexes = action.scheduler.getValueAsObject(ACTION_SCHEDULER_PEEKS)
			
			for(Port port : indexes.keySet){
				for(Integer index : indexes.get(port)){
					val String value = doSwitch(port.type) + " " + port.name + "_peek" + if (index > 1) "_" + index else ""
					parameters.add(value)
				}
			}
		}
		'''
			static «action.scheduler.returnType.doSwitch» «action.scheduler.name»(«FOR parameter : parameters SEPARATOR ", "»«parameter»«ENDFOR») {
			#pragma HLS INLINE	
				«FOR variable : action.scheduler.locals»
					«variable.declare»;
				«ENDFOR»
			
				«FOR block : action.scheduler.blocks»
					«block.doSwitch»
				«ENDFOR»
			}
			
		'''
	}

	override printActionBody(Action action) {
		var List<String> directives = new ArrayList
		if (action.body.hasAttribute("directives")) {
			directives = action.body.getAttribute("directives").objectValue as List<String>
		}
		'''
			#pragma HLS INLINE «IF !inlineActions»off«ENDIF»
			«FOR directive : directives»
				«directive»
			«ENDFOR»
				«super.printActionBody(action)»
		'''
	}

	override protected print(Procedure proc) {
		var List<String> directives = new ArrayList
		if (proc.hasAttribute("directives")) {
			directives = proc.getAttribute("directives").objectValue as List<String>
		}
		var externalVariables = new ArrayList<Var>
		if (proc.hasAttribute(PROCEDURE_EXTERNAL_VARIABLES)) {
			externalVariables = proc.getAttribute(PROCEDURE_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
		}
		'''
			static «proc.returnType.doSwitch» «proc.name»(«proc.parameters.join(", ")[declare]»«IF !externalVariables.empty»«IF !proc.parameters.empty», «ENDIF»«FOR extVars: externalVariables SEPARATOR ", "»volatile «extVars.type.doSwitch» *«extVars.name»«ENDFOR»«ENDIF») {
			#pragma HLS INLINE
			«FOR directive : directives»
				«directive»
			«ENDFOR»
				«FOR variable : proc.locals»
				«variable.declare»;
				«ENDFOR»
			
				«FOR block : proc.blocks»
					«block.doSwitch»
				«ENDFOR»
			}
		'''
	}

	// -- AXI Readers Writers
	override getRead(Port port, Var target, Var source) {
		'''
			«target.name» = «source.name».read();
		'''
	}

	override getWrite(Port port, EMap<Port, Var> portToVarMap, String index) {
		'''
			«port.name».write(p_«portToVarMap.get(port).name»[«index»]);
		'''
	}

	override getWriteWithBroadcast(Port port, EMap<Port, Var> portToVarMap, String index, int broadcastIndex) {
		'''
			«port.name»_«broadcastIndex».write(p_«portToVarMap.get(port).name»[«index»]);
		'''
	}

	override caseInstCall(InstCall call) {
		val Procedure procedure = call.procedure
		var externalVariables = new ArrayList<Var>
		if (procedure.hasAttribute(PROCEDURE_EXTERNAL_VARIABLES)) {
			externalVariables = procedure.getAttribute(PROCEDURE_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
		}
		'''
			«IF call.print»
				#ifndef __SYNTHESIS__
					std::cout << «FOR arg : call.arguments SEPARATOR " << "»(«arg.coutArg»)«ENDFOR»;
				#endif
			«ELSE»
				«IF call.target !== null»«call.target.variable.name» = «ENDIF»«call.procedure.name»(«IF !call.procedure.parameters.empty»«FOR j : 0 .. call.procedure.parameters.size - 1 SEPARATOR ", "»«printWithCast(call.procedure.parameters.get(j), call.arguments.get(j) )»«ENDFOR»«ENDIF»«IF !externalVariables.empty»«IF !call.procedure.parameters.empty», «ENDIF»«FOR variable : externalVariables SEPARATOR ", "»«variable.name»«ENDFOR»«ENDIF»);
			«ENDIF»
		'''
	}

	override getFuncitonsProcedures() {
		'''
			
			// ----------------------------------------------------------------------------
			// -- Function(s) / Procedure(s)
			«FOR proc : actor.procs»
				«getFunctionsProceduresPrototype(proc)»
			«ENDFOR»
			
			«FOR proc : actor.procs.filter[!native]»
				«proc.print»
			«ENDFOR»
		'''
	}

	def getFunctionsProceduresPrototype(Procedure proc) {
		var externalVariables = new ArrayList<Var>
		if (proc.hasAttribute(PROCEDURE_EXTERNAL_VARIABLES)) {
			externalVariables = proc.getAttribute(PROCEDURE_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
		}
		'''
			«IF proc.native»extern«ELSE»static«ENDIF» «proc.returnType.doSwitch» «proc.name»(«proc.parameters.join(", ",[variable.declare])»«IF !externalVariables.empty»«IF !proc.parameters.empty», «ENDIF»«FOR extVars: externalVariables SEPARATOR ", "»volatile «extVars.type.doSwitch» *«extVars.name»«ENDFOR»«ENDIF»);
		'''
	}

}
