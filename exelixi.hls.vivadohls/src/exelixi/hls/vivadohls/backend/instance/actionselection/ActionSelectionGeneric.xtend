/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.instance.actionselection

import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.instance.actionselection.ActionSelection
import exelixi.hls.templates.types.TypePrinterVisitor
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Set
import net.sf.orcc.df.Action
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.df.State
import net.sf.orcc.df.Transition
import net.sf.orcc.graph.Edge
import net.sf.orcc.ir.Var
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.EMap

import static exelixi.hls.backend.Constants.ACTION_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ACTION_SCHEDULER_PEEKS
import static exelixi.hls.backend.Constants.ACTOR_ACTION_SELECTION_PEEKS
import static exelixi.hls.backend.Constants.ACTOR_COMPLEX_GUARDS
import static exelixi.hls.backend.Constants.ACTOR_GURAD_STATE_VARIABLES
import static exelixi.hls.backend.Constants.EVERY_INSTANCE_AN_IP_CORE
import static exelixi.hls.backend.Constants.INSTANCE_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.PROFILE_DATAFLOW_NETWORK
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION
import static net.sf.orcc.OrccLaunchConstants.DEFAULT_FIFO_SIZE
import static net.sf.orcc.OrccLaunchConstants.FIFO_SIZE

/**
 * 
 * @author Endri Bezati
 */
class ActionSelectionGeneric extends HLSTemplate implements ActionSelection {

	protected Instance instance

	protected Actor actor

	protected EList<Edge> incoming

	protected Map<Port, List<Connection>> outgoingPortMap

	protected TypePrinterVisitor typePrinter

	protected Partitioner partitioner

	protected List<Connection> plToPsFifos;

	protected List<Connection> psToPlFifos;

	protected boolean enableProfiling

	// protected boolean axiLiteControl
	protected boolean everyInstanceAnIPCore

	protected boolean isActorComplex

	protected List<Var> externalVariables

	new(Instance instance, TypePrinterVisitor typePrinter, Partitioner partitioner, Map<String, Object> options) {
		super(typePrinter)
		this.typePrinter = typePrinter
		this.instance = instance
		this.actor = instance.getActor
		if (options.containsKey(FIFO_SIZE)) {
			fifoSize = options.get(FIFO_SIZE) as Integer
		} else {
			fifoSize = DEFAULT_FIFO_SIZE
		}

		this.partitioner = partitioner
		this.plToPsFifos = partitioner.getConnections(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION)
		this.psToPlFifos = partitioner.getConnections(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION)
		this.incoming = instance.incoming
		this.outgoingPortMap = instance.outgoingPortMap
		if (options.containsKey(PROFILE_DATAFLOW_NETWORK)) {
			this.enableProfiling = options.get(PROFILE_DATAFLOW_NETWORK) as Boolean
		} else {
			this.enableProfiling = false
		}

		if (options.containsKey(EVERY_INSTANCE_AN_IP_CORE)) {
			this.everyInstanceAnIPCore = options.get(EVERY_INSTANCE_AN_IP_CORE) as Boolean
		} else {
			this.everyInstanceAnIPCore = true
		}

		if (actor.hasAttribute(ACTOR_COMPLEX_GUARDS)) {
			isActorComplex = actor.getAttribute(ACTOR_COMPLEX_GUARDS).getObjectValue() as Boolean;
		}

		if (instance.getActor.hasAttribute(INSTANCE_EXTERNAL_VARIABLES)) {
			externalVariables = instance.getActor.getAttribute(INSTANCE_EXTERNAL_VARIABLES).
				objectValue as ArrayList<Var>
		} else {
			externalVariables = new ArrayList
		}

	}

	// -- Action Selection
	override getActionSelection() {
		var List<String> parameters = getActionSelectionParameters()
		'''
			void «TemplatesUtils.getInstanceName(instance)»(«FOR parameter : parameters SEPARATOR ", "»«parameter»«ENDFOR»){
				«getActionSelectionPragmas»
				«IF actor.fsm.states.size > 1»
					switch (state) {
						«FOR state : actor.fsm.states SEPARATOR "\n"»
							«getStateContent(state)»
						«ENDFOR»
					
					default:
						state = s_«instance.simpleName»_«actor.fsm.initialState.label»;
						break;
					}
				«ELSE»
					«IF isActorComplex»
						«getComplexStateReading(actor.fsm.initialState)»
					«ENDIF»
					«FOR edge : actor.fsm.initialState.outgoing SEPARATOR " else"»
						«getTransitionContentNoFSM(edge as Transition)»
					}«ENDFOR»
				«ENDIF»
				
				return;
			}
		'''
	}

	def List<String> getActionSelectionParameters() {
		var List<String> parameters = new ArrayList

		val Set<Port> portsWithCount = TemplatesUtils.getPortsWithCount(instance);
		// -- Input parameters
		for (port : actor.inputs) {
			val String value = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" + " &" + port.name
			parameters.add(value)
			if (portsWithCount.contains(port)) {
				val String count = "int " + port.name + "_count"
				parameters.add(count)
			}
		}

		// -- Peeks
		if (actor.hasAttribute(ACTOR_ACTION_SELECTION_PEEKS)) {
			val Map<Port, List<Integer>> indexes = actor.getValueAsObject(ACTOR_ACTION_SELECTION_PEEKS)

			for (Port port : indexes.keySet) {
				for (Integer index : indexes.get(port)) {
					val String value = doSwitch(port.type) + " " + port.name + "_peek" +
						if(index > 1) "_" + index else ""
					if (!parameters.contains(value))
						parameters.add(value)
				}
			}
		}

		// -- Output Parameters
		for (port : actor.outputs) {
			if (outgoingPortMap.get(port).size > 1) {
				for (connection : outgoingPortMap.get(port)) {
					val String value = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" + " &" +
						port.name + "_" + outgoingPortMap.get(port).indexOf(connection)
					parameters.add(value)
					if (portsWithCount.contains(port)) {
						val String count = "int " + port.name + "_" + outgoingPortMap.get(port).indexOf(connection) +
							"_count"
						parameters.add(count)
						val String size = "int " + port.name + "_" + outgoingPortMap.get(port).indexOf(connection) +
							"_size"
						parameters.add(size)
					}

				}
			} else {
				val String value = typePrinter.channelClassName + "< " + port.type.doSwitch + " >" + " &" + port.name
				parameters.add(value)
				if (portsWithCount.contains(port)) {
					val String count = "int " + port.name + "_count"
					parameters.add(count)
					val String size = "int " + port.name + "_size"
					parameters.add(size)
				}

			}
		}

		// -- External Variables
		for (variable : externalVariables) {
			val String value = "volatile " + variable.type.doSwitch + " * " + variable.name
			parameters.add(value)
		}

		// -- Profiling
		if (enableProfiling) {
			val String value = typePrinter.channelClassName + "< int > &action_id";
			parameters.add(value)
		}

		return parameters
	}

	def getActionSelectionPragmas() {
		'''
			#pragma HLS INTERFACE ap_ctrl_hs port=return
			«IF actor.hasAttribute(ACTOR_GURAD_STATE_VARIABLES)»
				«FOR variable : actor.getAttribute(ACTOR_GURAD_STATE_VARIABLES).getObjectValue() as List<Var>»
					«IF variable.isAssignable»
						#pragma HLS RESET variable=«variable.name»
					«ENDIF»
				«ENDFOR»
				«IF actor.fsm.states.size > 1»
					#pragma HLS RESET variable=state
				«ENDIF»
			«ENDIF»
			«FOR variable : externalVariables»
				#pragma HLS INTERFACE m_axi depth=«getFlatListDepth(variable.type)» port=«variable.name» offset=off name=id_«instance.number»_«variable.name»
			«ENDFOR»
		'''
	}

	def getComplexStateReading(State state) {
		var Map<Port, Integer> maxPortTokens = new HashMap
		var Boolean actionsHaveInputPrts = false
		for (edge : state.outgoing) {
			var action = (edge as Transition).action
			if (!action.inputPattern.ports.empty) {
				var EMap<Port, Integer> inputNumTokens = action.inputPattern.numTokensMap
				for (port : inputNumTokens.keySet) {
					if (maxPortTokens.containsKey(port)) {
						var oldValue = maxPortTokens.get(port)
						if (oldValue < inputNumTokens.get(port)) {
							maxPortTokens.put(port, inputNumTokens.get(port))
						}
					} else {
						maxPortTokens.put(port, inputNumTokens.get(port))
					}
				}
				actionsHaveInputPrts = true;
			}
		}
		'''
			«FOR port : maxPortTokens.keySet»
				«IF maxPortTokens.get(port) == 1»
					if(!«port.name».empty() && p_«port.name»_token_index < «maxPortTokens.get(port)»){
						p_«port.name»[(p_«port.name»_head + p_«port.name»_token_index ) & (p_«port.name»_size - 1)] = «port.name».read();
						p_«port.name»_token_index++;
					}
				«ELSE»
					while(!«port.name».empty() && p_«port.name»_token_index < «maxPortTokens.get(port)»){
						p_«port.name»[(p_«port.name»_head + p_«port.name»_token_index ) & (p_«port.name»_size - 1)] = «port.name».read();
						p_«port.name»_token_index++;
					}
				«ENDIF»
			«ENDFOR»
		'''
	}

	def getStateContent(State state) {
		var Map<Port, Integer> maxPortTokens = new HashMap
		var Boolean actionsHaveInputPrts = false
		for (edge : state.outgoing) {
			var action = (edge as Transition).action
			if (!action.inputPattern.ports.empty) {
				var EMap<Port, Integer> inputNumTokens = action.inputPattern.numTokensMap
				for (port : inputNumTokens.keySet) {
					if (maxPortTokens.containsKey(port)) {
						var oldValue = maxPortTokens.get(port)
						if (oldValue < inputNumTokens.get(port)) {
							maxPortTokens.put(port, inputNumTokens.get(port))
						}
					} else {
						maxPortTokens.put(port, inputNumTokens.get(port))
					}
				}
				actionsHaveInputPrts = true;
			}
		}
		'''
			case(s_«instance.simpleName»_«state.label»):
				«IF isActorComplex»
					«getComplexStateReading(state)»
				«ENDIF»
				«FOR edge : state.outgoing SEPARATOR " else"»
					«getTransitionContent(edge as Transition)»
				}«ENDFOR»
			break;
		'''
	}

	def getTransitionContent(Transition transition) {
		var action = transition.action
		var tState = transition.target
		val List<String> conditions = new ArrayList<String>

		var List<String> schedulerArguments = new ArrayList<String>
		if (!isActorComplex) {
			if (action.scheduler.hasAttribute(ACTION_SCHEDULER_PEEKS)) {
				var Map<Port, List<Integer>> schedulerPeeks = action.scheduler.getValueAsObject(ACTION_SCHEDULER_PEEKS)
				for (Port port : schedulerPeeks.keySet) {
					for (Integer index : schedulerPeeks.get(port)) {
						val String schedulerArgument = port.name + "_peek" + if(index > 1) "_" + index else ""
						if (!schedulerArguments.contains(schedulerArgument))
							schedulerArguments.add(schedulerArgument)
					}
				}
			}
		}

		// -- Scheduling condition
		val String scheduling = action.scheduler.name + "(" + getActionSchedulerArguments(schedulerArguments) + ")"
		conditions.add(scheduling)

		// -- Input port(s) condition 
		conditions.addAll(getTransitionInputConditions(action))

		// -- Output port condition
		val List<String> outputConditions = getTransitionOutputConditions(action)

		// -- Transition Arguments
		val List<String> transitionArguments = getTransitionArguments(action)

		'''
			if(«FOR condition : conditions SEPARATOR " && "»«condition»«ENDFOR»){
				«IF !action.outputPattern.isEmpty»
					if(«FOR condition : outputConditions SEPARATOR " && "»«condition»«ENDFOR»){
						// -- Call Action
						«action.body.name»(«FOR argument : transitionArguments SEPARATOR ", "»«argument»«ENDFOR»);
						state = s_«instance.simpleName»_«tState.label»;
					}
				«ELSE»
					// -- Call Action
					«action.body.name»(«FOR argument : transitionArguments SEPARATOR ", "»«argument»«ENDFOR»);
					state = s_«instance.simpleName»_«tState.label»;
				«ENDIF»
				«IF enableProfiling»
					action_id.write(«instance.getActor.actions.indexOf(action)»);
				«ENDIF»
		'''
	}

	def String getActionSchedulerArguments(List<String> arguments) {
		return '''«FOR arg : arguments SEPARATOR ", "»«arg»«ENDFOR»'''
	}

	def getTransitionContentNoFSM(Transition transition) {
		var action = transition.action

		val List<String> conditions = new ArrayList<String>

		var List<String> schedulerArguments = new ArrayList<String>
		if (!isActorComplex) {
			if (action.scheduler.hasAttribute(ACTION_SCHEDULER_PEEKS)) {
				var Map<Port, List<Integer>> schedulerPeeks = action.scheduler.getValueAsObject(ACTION_SCHEDULER_PEEKS)
				for (Port port : schedulerPeeks.keySet) {
					for (Integer index : schedulerPeeks.get(port)) {
						val String schedulerArgument = port.name + "_peek" + if(index > 1) "_" + index else ""
						schedulerArguments.add(schedulerArgument)
					}
				}
			}
		}

		// -- Scheduling condition
		val String scheduling = action.scheduler.name + "(" + getActionSchedulerArguments(schedulerArguments) + ")"
		conditions.add(scheduling)

		// -- Input port(s) condition 
		conditions.addAll(getTransitionInputConditions(action))

		// -- Output port condition
		val List<String> outputConditions = getTransitionOutputConditions(action)

		// -- Transition Arguments
		val List<String> transitionArguments = getTransitionArguments(action)

		'''
			if(«FOR condition : conditions SEPARATOR " && "»«condition»«ENDFOR»){
				«IF !action.outputPattern.isEmpty»
					if(«FOR condition : outputConditions SEPARATOR " && "»«condition»«ENDFOR»){
						// -- Call Action
						«action.body.name»(«FOR argument : transitionArguments SEPARATOR ", "»«argument»«ENDFOR»);
					}
				«ELSE»
					// -- Call Action
					«action.body.name»(«FOR argument : transitionArguments SEPARATOR ", "»«argument»«ENDFOR»);
				«ENDIF»
				«IF enableProfiling»
					action_id.write(«instance.getActor.actions.indexOf(action)»);
				«ENDIF»
		'''
	}

	def List<String> getTransitionInputConditions(Action action) {
		val List<String> conditions = new ArrayList<String>
		for (port : action.inputPattern.ports) {
			var String inputPortCondition = ""

			if (isActorComplex) {
				inputPortCondition = "(" + "p_" + port.name + "_token_index" + " >= " +
					action.inputPattern.numTokensMap.get(port) + ")"
			} else {
				if (action.inputPattern.numTokensMap.get(port) == 1) {
					inputPortCondition = "!" + port.name + ".empty()"
				} else {
					inputPortCondition = "(" + "!" + port.name + ".empty()" + " && " + port.name + "_count" + " >= " +
						(action.inputPattern.numTokensMap.get(port) - 1) + ")"
				}
			}
			conditions.add(inputPortCondition)
		}
		return conditions
	}

	def List<String> getTransitionOutputConditions(Action action) {
		val List<String> conditions = new ArrayList<String>
		for (port : action.outputPattern.ports) {
			if (outgoingPortMap.get(port).size > 1) {
				for (connection : outgoingPortMap.get(port)) {
					if (action.outputPattern.numTokensMap.get(port) == 1) {
						val String outputPortCondition = "!" + port.name + "_" +
							outgoingPortMap.get(port).indexOf(connection) + ".full()"
						conditions.add(outputPortCondition)
					} else {
						val String outputPortCondition = "(" + "!" + port.name + "_" +
							outgoingPortMap.get(port).indexOf(connection) + ".full()" + " && " + "(" + port.name + "_" +
							outgoingPortMap.get(port).indexOf(connection) + "_size" + " - " + port.name + "_" +
							outgoingPortMap.get(port).indexOf(connection) + "_count" + " >= " +
							action.outputPattern.numTokensMap.get(port) + ")" + ")"
						conditions.add(outputPortCondition)
					}
				}
			} else {
				if (action.outputPattern.numTokensMap.get(port) == 1) {
					val String outputPortCondition = "!" + port.name + ".full()"
					conditions.add(outputPortCondition)
				} else {
					val String outputPortCondition = "(" + "!" + port.name + ".full()" + " && " + "(" + port.name +
						"_size" + " - " + port.name + "_count" + " >= " + action.outputPattern.numTokensMap.get(port) +
						")" + ")"
					conditions.add(outputPortCondition)
				}
			}
		}
		return conditions
	}

	def List<String> getTransitionArguments(Action action) {
		val List<String> transitionArguments = new ArrayList<String>
		var externalVariables = new ArrayList<Var>
		if (action.hasAttribute(ACTION_EXTERNAL_VARIABLES)) {
			externalVariables = action.getAttribute(ACTION_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
		}
		// -- Inputs
		if (!isActorComplex) {
			for (port : action.inputPattern.ports) {
				transitionArguments.add(port.name)
			}
		}

		// -- Outputs
		for (port : action.outputPattern.ports) {
			if (outgoingPortMap.get(port).size > 1) {
				for (i : 0 .. outgoingPortMap.get(port).size - 1) {
					transitionArguments.add(port.name + "_" + i)
				}
			} else {
				transitionArguments.add(port.name)
			}
		}

		// -- External variables
		for (variable : externalVariables) {
			transitionArguments.add(variable.name)
		}

		return transitionArguments
	}

}
