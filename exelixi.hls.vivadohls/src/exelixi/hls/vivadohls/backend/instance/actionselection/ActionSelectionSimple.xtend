/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.instance.actionselection

import exelixi.hls.partitioner.Partitioner
import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.instance.actionselection.ActionSelection
import exelixi.hls.templates.types.TypePrinterVisitor
import exelixi.hls.vivadohls.backend.utils.TemplatesUtils
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.df.State
import net.sf.orcc.df.Transition
import net.sf.orcc.graph.Edge
import net.sf.orcc.ir.Var
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.EMap

import static exelixi.hls.backend.Constants.ACTION_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.ACTOR_GURAD_STATE_VARIABLES
import static exelixi.hls.backend.Constants.AXI_STREAM_ACTOR_COMMUNICATION
import static exelixi.hls.backend.Constants.EVERY_INSTANCE_AN_IP_CORE
import static exelixi.hls.backend.Constants.INSTANCE_EXTERNAL_VARIABLES
import static exelixi.hls.backend.Constants.PROFILE_DATAFLOW_NETWORK
import static exelixi.hls.backend.Constants.ZYNQ_PL_PARTITION
import static exelixi.hls.backend.Constants.ZYNQ_PS_PARTITION

/**
 * 
 * @author Endri Bezati
 */
class ActionSelectionSimple extends HLSTemplate implements ActionSelection {

	protected Instance instance

	protected Actor actor

	protected EList<Edge> incoming

	protected Map<Port,List<Connection>> outgoingPortMap

	private TypePrinterVisitor typePrinter
	
	private Partitioner partitioner
	
	private List<Connection> plToPsFifos;
	
	private List<Connection> psToPlFifos;
	
	private boolean enableProfiling
	
	private boolean axiStreamActorCommunication
	
	private boolean everyInstanceAnIPCore

	new(Instance instance, TypePrinterVisitor typePrinter, Partitioner partitioner, Map<String, Object> options) {
		super(typePrinter)
		this.typePrinter = typePrinter
		this.instance = instance
		this.actor = instance.getActor
		this.partitioner = partitioner
		this.plToPsFifos = partitioner.getConnections(ZYNQ_PL_PARTITION, ZYNQ_PS_PARTITION)
		this.psToPlFifos = partitioner.getConnections(ZYNQ_PS_PARTITION, ZYNQ_PL_PARTITION)
		this.incoming = instance.incoming
		this.outgoingPortMap = instance.outgoingPortMap
		this.enableProfiling = options.get(PROFILE_DATAFLOW_NETWORK) as Boolean
		// FIXME SCB: solve bug with not selected widget and remove default value
		this.axiStreamActorCommunication = options.getOrDefault(AXI_STREAM_ACTOR_COMMUNICATION, false) as Boolean
		this.everyInstanceAnIPCore = options.get(EVERY_INSTANCE_AN_IP_CORE) as Boolean
	}

	// -- Action Selection
	override getActionSelection() {
		var List<Var> externalVariables = new ArrayList<Var>
			if(instance.getActor.hasAttribute(INSTANCE_EXTERNAL_VARIABLES)){
			externalVariables = instance.getActor.getAttribute(INSTANCE_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
		}
		'''
			void «TemplatesUtils.getInstanceName(instance)»(«FOR port : actor.inputs SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDFOR»«IF !actor.inputs.empty && !actor.outputs.empty», «ENDIF»«FOR port : actor.outputs SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR connection : outgoingPortMap.get(port) SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»_«outgoingPortMap.get(port).indexOf(connection)»«ENDFOR»«ELSE»«typePrinter.channelClassName»<«port.type.doSwitch» > &«port.name»«ENDIF»«ENDFOR»«IF enableProfiling» ,«typePrinter.channelClassName»< int > action_id«ENDIF»«IF !externalVariables.empty»«IF !actor.inputs.empty || !actor.outputs.empty», «ENDIF»«FOR variable : externalVariables SEPARATOR ", "»volatile «variable.type.doSwitch» * «variable.name»«ENDFOR»«ENDIF»){				
			#pragma HLS INTERFACE ap_ctrl_hs port=return
			«IF actor.hasAttribute(ACTOR_GURAD_STATE_VARIABLES)»
				«FOR variable : actor.getAttribute(ACTOR_GURAD_STATE_VARIABLES).getObjectValue() as List<Var>»
					#pragma HLS RESET variable=«variable.name»
				«ENDFOR»
				«IF actor.fsm.states.size > 1»
					#pragma HLS RESET variable=state
				«ENDIF»
			«ENDIF»
			«IF axiStreamActorCommunication»
				«FOR port : actor.inputs»
					#pragma HLS INTERFACE axis port=«port.name»
				«ENDFOR»
				«FOR port : actor.outputs »
					«IF outgoingPortMap.get(port).size > 1»
						«FOR connection : outgoingPortMap.get(port)»
							#pragma HLS INTERFACE axis port=«port.name»_«outgoingPortMap.get(port).indexOf(connection)»
						«ENDFOR»
					«ELSE»
						#pragma HLS INTERFACE axis port=«port.name»
					«ENDIF»
				«ENDFOR»
				«FOR variable : externalVariables»
					#pragma HLS INTERFACE m_axi depth=«getFlatListDepth(variable.type)» port=«variable.name» offset=off name=id_«instance.number»_«variable.name»
				«ENDFOR»
			«ENDIF»
			«FOR variable : externalVariables»
				#pragma HLS INTERFACE m_axi depth=«getFlatListDepth(variable.type)» port=«variable.name» offset=off name=id_«instance.number»_«variable.name»
			«ENDFOR»
				«IF actor.fsm.states.size > 1»
				switch (state) {
					«FOR state : actor.fsm.states SEPARATOR "\n"»
						«getStateContent(state)»
					«ENDFOR»
				
				default:
					state = s_«instance.simpleName»_«actor.fsm.initialState.label»;
					break;
				}
				«ELSE»
				«FOR edge : actor.fsm.initialState.outgoing SEPARATOR " else"»
					«getTransitionContentNoFSM(edge as Transition)»
				}«ENDFOR»
				«ENDIF»
				
				return;
			}
		'''
	}

	def getStateContent(State state) {
		var Map<Port, Integer> maxPortTokens = new HashMap
		var Boolean actionsHaveInputPrts = false
		for (edge : state.outgoing) {
			var action = (edge as Transition).action
			if (!action.inputPattern.ports.empty) {
				var EMap<Port, Integer> inputNumTokens = action.inputPattern.numTokensMap
				for (port : inputNumTokens.keySet) {
					if (maxPortTokens.containsKey(port)) {
						var oldValue = maxPortTokens.get(port)
						if (oldValue < inputNumTokens.get(port)) {
							maxPortTokens.put(port, inputNumTokens.get(port))
						}
					} else {
						maxPortTokens.put(port, inputNumTokens.get(port))
					}
				}
				actionsHaveInputPrts = true;
			}
		}
		'''
			case(s_«instance.simpleName»_«state.label»):
				«FOR edge : state.outgoing SEPARATOR " else"»
					«getTransitionContent(edge as Transition)»
				}«ENDFOR»
			break;
		'''
	}

	def getTransitionContent(Transition transition) {
		var action = transition.action
		var tState = transition.target
		var externalVariables = new ArrayList<Var>
		if(action.hasAttribute(ACTION_EXTERNAL_VARIABLES)){
			externalVariables = action.getAttribute(ACTION_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
		}
		
		// -- Output port condition
		val List<String> outputConditions = new ArrayList<String>
		for (port : action.outputPattern.ports) {
			if (outgoingPortMap.get(port).size > 1) {
				for(connection : outgoingPortMap.get(port)){
					val String outputPortCondition = "!" + port.name+ "_" + outgoingPortMap.get(port).indexOf(connection) + ".full()"
					outputConditions.add(outputPortCondition)
				}
			} else {
				val String outputPortCondition = "!" + port.name + ".full()"
				outputConditions.add(outputPortCondition)
			}
		}
		
		'''
			if(«action.scheduler.name»()«IF !action.inputPattern.ports.empty» && «FOR port : action.inputPattern.ports SEPARATOR " && "»!«port.name».empty() «ENDFOR»«ENDIF»){
				«IF !action.outputPattern.isEmpty»
					if(«FOR condition : outputConditions SEPARATOR " && "»«condition»«ENDFOR»){
						// -- Call Action
					«action.body.name»(«FOR port : action.inputPattern.ports SEPARATOR ", "»«port.name»«ENDFOR»«IF !action.inputPattern.empty && !action.outputPattern.empty», «ENDIF»«FOR port : action.outputPattern.ports SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR ", "»«port.name»_«i»«ENDFOR»«ELSE»«port.name»«ENDIF»«ENDFOR»«IF !externalVariables.empty»«IF !action.inputPattern.ports.empty || !action.outputPattern.ports.empty », «ENDIF»«getExternalVarsAguments(externalVariables)»«ENDIF»);
					}
				«ELSE»
					// -- Call Action
				«action.body.name»(«FOR port : action.inputPattern.ports SEPARATOR ", "»«port.name»«ENDFOR»«IF !action.inputPattern.empty && !action.outputPattern.empty», «ENDIF»«FOR port : action.outputPattern.ports SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR ", "»«port.name»_«i»«ENDFOR»«ELSE»«port.name»«ENDIF»«ENDFOR»«IF !externalVariables.empty»«IF !action.inputPattern.ports.empty || !action.outputPattern.ports.empty », «ENDIF»«getExternalVarsAguments(externalVariables)»«ENDIF»);
				«ENDIF»
				«IF enableProfiling»
				action_id.write(«instance.getActor.actions.indexOf(action)»);
				«ENDIF»
				state = s_«instance.simpleName»_«tState.label»;
		'''
	}

	def getTransitionContentNoFSM(Transition transition) {
		var action = transition.action
		var externalVariables = new ArrayList<Var>
		if(action.hasAttribute(ACTION_EXTERNAL_VARIABLES)){
			externalVariables = action.getAttribute(ACTION_EXTERNAL_VARIABLES).objectValue as ArrayList<Var>
		}
		
		// -- Output port condition
		val List<String> outputConditions = new ArrayList<String>
		for (port : action.outputPattern.ports) {
			if (outgoingPortMap.get(port).size > 1) {
				for(connection : outgoingPortMap.get(port)){
					val String outputPortCondition = "!" + port.name+ "_" + outgoingPortMap.get(port).indexOf(connection) + ".full()"
					outputConditions.add(outputPortCondition)
				}
			} else {
				val String outputPortCondition = "!" + port.name + ".full()"
				outputConditions.add(outputPortCondition)
			}
		}
		
		'''
			if(«action.scheduler.name»()«IF !action.inputPattern.ports.empty» && «FOR port : action.inputPattern.ports SEPARATOR " && "»!«port.name».empty() «ENDFOR»«ENDIF»){
				«IF !action.outputPattern.isEmpty»
					if(«FOR condition : outputConditions SEPARATOR " && "»«condition»«ENDFOR»){
						// -- Call Action
					«action.body.name»(«FOR port : action.inputPattern.ports SEPARATOR ", "»«port.name»«ENDFOR»«IF !action.inputPattern.empty && !action.outputPattern.empty», «ENDIF»«FOR port : action.outputPattern.ports SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR ", "»«port.name»_«i»«ENDFOR»«ELSE»«port.name»«ENDIF»«ENDFOR»«IF !externalVariables.empty»«IF !action.inputPattern.ports.empty || !action.outputPattern.ports.empty », «ENDIF»«getExternalVarsAguments(externalVariables)»«ENDIF»);
					}
				«ELSE»
					// -- Call Action
				«action.body.name»(«FOR port : action.inputPattern.ports SEPARATOR ", "»«port.name»«ENDFOR»«IF !action.inputPattern.empty && !action.outputPattern.empty», «ENDIF»«FOR port : action.outputPattern.ports SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR ", "»«port.name»_«i»«ENDFOR»«ELSE»«port.name»«ENDIF»«ENDFOR»«IF !externalVariables.empty»«IF !action.inputPattern.ports.empty || !action.outputPattern.ports.empty », «ENDIF»«getExternalVarsAguments(externalVariables)»«ENDIF»);
				«ENDIF»
				«IF enableProfiling»
				action_id.write(«instance.getActor.actions.indexOf(action)»);
				«ENDIF»
		'''
	}
	
	def getExternalVarsAguments(List<Var> externalVars){
		'''«FOR variable : externalVars SEPARATOR ", "»«variable.name»«ENDFOR»'''
	}
	
}
