/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.backend.type

import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.Map
import net.sf.orcc.df.Port
import net.sf.orcc.ir.TypeBool
import net.sf.orcc.ir.TypeFloat
import net.sf.orcc.ir.TypeInt
import net.sf.orcc.ir.TypeUint

import static exelixi.hls.backend.Constants.BIT_ACCURATE_TYPES

/**
 * 
 * @author Endri Bezati
 */
class VivadoHLSTypePrinter extends TypePrinterVisitor {

	boolean typeAccuracy

	new(Map<String, Object> options) {
		if (options.containsKey(BIT_ACCURATE_TYPES)) {
			this.typeAccuracy = options.get(BIT_ACCURATE_TYPES) as Boolean
		} else {
			this.typeAccuracy = false;
		}
	}

	override getChannelClassName() {
		'''hls::stream'''
	}

	override getIntClassName() {
		'''ap'''
	}

	override getIntIncludeHeaderFile() {
		'''ap_int.h'''
	}

	override getChannelIncludeHeaderFile() {
		'''hls_stream.h'''
	}

	override caseTypeBool(TypeBool type) '''bool'''

	override caseTypeInt(
		TypeInt type) '''«IF typeAccuracy»ap_int<«type.sizeInBits»>«ELSE»«getIntSize(type.sizeInBits)»«ENDIF»'''

	override caseTypeUint(
		TypeUint type
	) '''«IF typeAccuracy»ap_uint<«type.sizeInBits»>«ELSE»unsigned «getIntSize(type.sizeInBits)»«ENDIF»'''

	override caseTypeFloat(TypeFloat type) '''float'''

	override getEmptyN(Port port) {
		'''!«port.name».empty()'''
	}

	override getFullN(Port port) {
		''''''
	}

}
