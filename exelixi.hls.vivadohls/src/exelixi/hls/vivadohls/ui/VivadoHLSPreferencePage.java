/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls.ui;

import static exelixi.hls.backend.Constants.P_LM_LICENSE_FILE;
import static exelixi.hls.backend.Constants.P_VIVADO;
import static exelixi.hls.backend.Constants.P_VIVADO_HLS;
import static exelixi.hls.backend.Constants.P_XILINXD_LICENSE_FILE;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

import exelixi.hls.vivadohls.Activator;

/**
 * 
 * @author Endri Bezati
 */
public class VivadoHLSPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	public VivadoHLSPreferencePage() {
		super(GRID);
		IPreferenceStore store = new ScopedPreferenceStore(InstanceScope.INSTANCE, Activator.PLUGIN_ID);
		setPreferenceStore(store);
		setDescription("General Setting for Exelixi Vivado HLS");

	}

	@Override
	protected void createFieldEditors() {
		Composite composite = getFieldEditorParent();

		StringFieldEditor fieldXilinx = new StringFieldEditor(P_XILINXD_LICENSE_FILE, "XILINXD_LICENSE_FILE",
				composite);
		addField(fieldXilinx);

		StringFieldEditor fieldModelsim = new StringFieldEditor(P_LM_LICENSE_FILE, "LM_LICENSE_FILE", composite);
		addField(fieldModelsim);

		addField(new FileFieldEditor(P_VIVADO, "Path of Vivado executable:", composite));
		addField(new FileFieldEditor(P_VIVADO_HLS, "Path of Vivado HLS executable:", composite));

	}

	@Override
	public void init(IWorkbench workbench) {
	}

}
