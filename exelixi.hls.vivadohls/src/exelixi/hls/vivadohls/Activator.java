/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.vivadohls;

import static exelixi.hls.backend.Constants.C_LM_LICENSE_FILE;
import static exelixi.hls.backend.Constants.C_VIVADO;
import static exelixi.hls.backend.Constants.C_VIVADO_HLS;
import static exelixi.hls.backend.Constants.C_XILINXD_LICENSE_FILE;
import static exelixi.hls.backend.Constants.P_LM_LICENSE_FILE;
import static exelixi.hls.backend.Constants.P_VIVADO;
import static exelixi.hls.backend.Constants.P_VIVADO_HLS;
import static exelixi.hls.backend.Constants.P_XILINXD_LICENSE_FILE;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "exelixi.hls.vivadohls"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		loadRegisterPrefereces();
		registerRegisterPrefereces();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	private void loadRegisterPrefereces() {
		IPreferenceStore store = getDefault().getPreferenceStore();
		if (store.contains(P_LM_LICENSE_FILE)) {
			C_LM_LICENSE_FILE = store.getString(P_LM_LICENSE_FILE);
		}

		if (store.contains(P_XILINXD_LICENSE_FILE)) {
			C_XILINXD_LICENSE_FILE = store.getString(P_XILINXD_LICENSE_FILE);
		}

		if (store.contains(P_VIVADO)) {
			C_VIVADO = store.getString(P_VIVADO);
		}

		if (store.contains(P_VIVADO_HLS)) {
			C_VIVADO_HLS = store.getString(P_VIVADO_HLS);
		}

	}

	private void registerRegisterPrefereces() {
		IPreferenceStore store = getDefault().getPreferenceStore();
		
		getDefault().getPreferenceStore().addPropertyChangeListener(new IPropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent event) {
				String property = event.getProperty();
				
				if (property.equals(P_LM_LICENSE_FILE)){
					C_LM_LICENSE_FILE = store.getString(property);
				} else if (property.equals(P_XILINXD_LICENSE_FILE)){
					C_XILINXD_LICENSE_FILE = store.getString(property);
				} else if (property.equals(P_VIVADO)){
					C_VIVADO = store.getString(property);
				} else if (property.equals(P_VIVADO_HLS)){
					C_VIVADO_HLS = store.getString(property);
				} 
			}
		});

	}
	
}
