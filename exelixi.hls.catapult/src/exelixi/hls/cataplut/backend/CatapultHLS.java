/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.cataplut.backend;

import static exelixi.hls.backend.Constants.BIT_ACCURATE_TYPES;
import static exelixi.hls.backend.Constants.FIFO_TRACE_PATH;
import static exelixi.hls.backend.Constants.RTL_PATH;

import java.io.File;

import exelixi.hls.backend.XronosBackend;
import exelixi.hls.cataplut.backend.templates.instance.CatapultInstance;
import exelixi.hls.cataplut.backend.templates.network.CatapultNetworkCPP;
import exelixi.hls.cataplut.backend.templates.network.CatapultNetworkHeader;
import exelixi.hls.cataplut.backend.templates.scripts.CatapultShell;
import exelixi.hls.cataplut.backend.templates.scverify.SCVerifyTestbench;
import exelixi.hls.cataplut.backend.templates.type.CatapultTypePrinter;
import exelixi.hls.templates.instance.InstanceHeaderPrinter;
import exelixi.hls.templates.ports.PortReader;
import exelixi.hls.templates.ports.PortReaderHeader;
import exelixi.hls.templates.ports.PortWriter;
import exelixi.hls.templates.ports.PortWriterHeader;
import exelixi.hls.templates.types.TypePrinterVisitor;
import exelixi.hls.transformations.ActorAddFSM;
import exelixi.hls.transformations.ActorPeekComplexity;
import exelixi.hls.transformations.ConnectionLabeler;
import exelixi.hls.transformations.LoopLabeler;
import exelixi.hls.transformations.UniquePortMemory;
import exelixi.hls.transformations.VarInitializer;
import net.sf.orcc.backends.llvm.tta.transform.PrintRemoval;
import net.sf.orcc.backends.transform.DisconnectedOutputPortRemoval;
import net.sf.orcc.backends.util.Validator;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.transform.Instantiator;
import net.sf.orcc.df.transform.NetworkFlattener;
import net.sf.orcc.df.transform.TypeResizer;
import net.sf.orcc.df.transform.UnitImporter;
import net.sf.orcc.df.util.NetworkValidator;
import net.sf.orcc.ir.transform.RenameTransformation;
import net.sf.orcc.util.FilesManager;
import net.sf.orcc.util.Result;

/**
 * 
 * @author Endri Bezati
 */
public class CatapultHLS extends XronosBackend {

	private TypePrinterVisitor typePrinter;

	/** Projects path, a folder that contains the Catapult Shell TCL scripts **/
	private String projectsPath;

	private String fifoTracePath;

	private String codeGenerationPath;

	@Override
	protected void doInitializeOptions() {
		createFolder();

		// -- Catapult Type Printer
		typePrinter = new CatapultTypePrinter(getOptions());

		networkTransfos.add(new Instantiator(false));
		networkTransfos.add(new NetworkFlattener());
		// networkTransfos.add(new ArgumentEvaluator());
		networkTransfos.add(new DisconnectedOutputPortRemoval());
		networkTransfos.add(new ConnectionLabeler());
		networkTransfos.add(new UnitImporter());
		if (!getOption(BIT_ACCURATE_TYPES, true)) {
			networkTransfos.add(new TypeResizer(true, false, false, false));
		}

		childrenTransfos.add(new RenameTransformation(getDefaultRelacementMap()));
		childrenTransfos.add(new ActorPeekComplexity());
		childrenTransfos.add(new UniquePortMemory(fifoSize));
		childrenTransfos.add(new ActorAddFSM());
		childrenTransfos.add(new VarInitializer());
		childrenTransfos.add(new LoopLabeler());
		childrenTransfos.add(new PrintRemoval());

	}

	/**
	 * Create the needed folders for the backend
	 */
	@Override
	protected void createFolder() {
		// -- RTL Path
		rtlPath = outputPath + File.separator + "rtl";
		File rtlDir = new File(rtlPath);
		if (!rtlDir.exists()) {
			rtlDir.mkdir();
		}
		// -- Add RTL Path option
		getOptions().put(RTL_PATH, rtlPath);

		// -- Script Path
		projectsPath = outputPath + File.separator + "projects";
		File projectsDir = new File(projectsPath);
		if (!projectsDir.exists()) {
			projectsDir.mkdir();
		}

		// -- Fifo Trace Folder
		fifoTracePath = outputPath + File.separator + "fifo-traces";
		File fifoTraceDir = new File(fifoTracePath);
		if (!fifoTraceDir.exists()) {
			fifoTraceDir.mkdir();
		}

		// -- Code Generation path
		codeGenerationPath = outputPath + File.separator + "code-gen";
		File codeGenDir = new File(codeGenerationPath);
		if (!codeGenDir.exists()) {
			codeGenDir.mkdir();
		}

		// -- Source path
		srcPath = codeGenerationPath + File.separator + "src";
		File srcDir = new File(srcPath);
		if (!srcDir.exists()) {
			srcDir.mkdir();
		}

		// -- Source path
		tbSrcPath = codeGenerationPath + File.separator + "src-tb";
		File tbSrcDir = new File(tbSrcPath);
		if (!tbSrcDir.exists()) {
			tbSrcDir.mkdir();
		}

		// -- Include path
		includePath = codeGenerationPath + File.separator + "include";
		File includeDir = new File(includePath);
		if (!includeDir.exists()) {
			includeDir.mkdir();
		}

		getOptions().put(FIFO_TRACE_PATH, fifoTracePath);
	}

	@Override
	protected void doValidate(Network network) {
		Validator.checkMinimalFifoSize(network, fifoSize);

		new NetworkValidator().doSwitch(network);
	}

	@Override
	protected Result doGenerateInstance(Instance instance) {
		final Result result = Result.newInstance();

		String name = instance.getName();

		CatapultInstance instancePrinter = new CatapultInstance(instance, typePrinter, getOptions());
		Result instanceResult = FilesManager.writeFile(instancePrinter.getContent(), srcPath, name + ".cpp");
		
		InstanceHeaderPrinter headerPrinter = new InstanceHeaderPrinter(instance, typePrinter);
		result.merge(FilesManager.writeFile(headerPrinter.getContent(), includePath, name + ".h"));

		
		result.merge(instanceResult);
		return result;
	}

	@Override
	protected Result doAdditionalGeneration(Instance instance) {
		final Result result = Result.newInstance();

		// -- Create folder for instance
		String instancePath = projectsPath + File.separator + instance.getSimpleName();
		File instanceDir = new File(instancePath);
		if (!instanceDir.exists()) {
			instanceDir.mkdir();
		}

		SCVerifyTestbench instanceTestbench = new SCVerifyTestbench(instance, typePrinter, getOptions());
		result.merge(FilesManager.writeFile(instanceTestbench.getContent(), tbSrcPath,
				instance.getSimpleName() + "_tb.cpp"));

		CatapultShell catapultShellPrinter = new CatapultShell(instance, getOptions());
		result.merge(FilesManager.writeFile(catapultShellPrinter.getContent(), instancePath,
				instance.getSimpleName() + ".tcl"));
		return result;
	}

	@Override
	protected Result doAdditionalGeneration(Network network) {
		final Result result = Result.newInstance();

		// -- Port Readers
		for (Port port : network.getInputs()) {
			String name = "PortReader_" + port.getName();

			PortReader portReader = new PortReader(port, typePrinter);
			Result portReaderResult = FilesManager.writeFile(portReader.getContent(), srcPath, name + ".cpp");
			result.merge(portReaderResult);

			PortReaderHeader portReaderHeader = new PortReaderHeader(port, typePrinter);
			Result portReaderResultHeader = FilesManager.writeFile(portReaderHeader.getContent(), includePath,
					name + ".h");
			result.merge(portReaderResultHeader);
		}

		// -- Port Writers
		for (Port port : network.getOutputs()) {
			String name = "PortWriter_" + port.getName();

			PortWriter portWriter = new PortWriter(port, typePrinter);
			Result portWriterResult = FilesManager.writeFile(portWriter.getContent(), srcPath, name + ".cpp");
			result.merge(portWriterResult);

			PortWriterHeader portWriterHeader = new PortWriterHeader(port, typePrinter);
			Result portWriterResultHeader = FilesManager.writeFile(portWriterHeader.getContent(), includePath,
					name + ".h");
			result.merge(portWriterResultHeader);
		}

		// -- Catapult Shell
		String networkPath = projectsPath + File.separator + network.getSimpleName();
		File networkDir = new File(networkPath);
		if (!networkDir.exists()) {
			networkDir.mkdir();
		}

		CatapultShell catapultShellPrinter = new CatapultShell(network, getOptions());
		result.merge(FilesManager.writeFile(catapultShellPrinter.getContent(), networkPath,
				network.getSimpleName() + ".tcl"));

		// -- SCVerify
		SCVerifyTestbench instanceTestbench = new SCVerifyTestbench(network, typePrinter, getOptions());
		result.merge(
				FilesManager.writeFile(instanceTestbench.getContent(), tbSrcPath, network.getSimpleName() + "_tb.cpp"));
		return result;
	}

	@Override
	protected Result doGenerateNetwork(Network network) {
		final Result result = Result.newInstance();

		CatapultNetworkCPP networkPrinterCPP = new CatapultNetworkCPP(network, typePrinter);
		result.merge(FilesManager.writeFile(networkPrinterCPP.getContent(), srcPath, network.getSimpleName() + ".cpp"));

		CatapultNetworkHeader networkPrinterHeader = new CatapultNetworkHeader(network, typePrinter);
		result.merge(
				FilesManager.writeFile(networkPrinterHeader.getContent(), includePath, network.getSimpleName() + ".h"));

		return result;
	}

}
