/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.cataplut.backend.templates.instance.actionselection

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.instance.actionselection.ActionSelection
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.HashMap
import java.util.List
import java.util.Map
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.df.State
import net.sf.orcc.df.Transition
import org.eclipse.emf.common.util.EMap

/**
 * 
 * @author Endri Bezati
 */
class ActionSelectionGeneric extends HLSTemplate implements ActionSelection {

	protected Instance instance

	protected Actor actor

	protected var Map<Port, List<String>> fanoutPortConenction

	protected Map<Port, List<Connection>> outgoingPortMap

	TypePrinterVisitor typePrinter

	new(Instance instance, TypePrinterVisitor typePrinter) {
		super(typePrinter)
		this.typePrinter = typePrinter
		this.instance = instance
		this.actor = instance.getActor
		this.fanoutPortConenction = fanoutPortConenction
		this.outgoingPortMap = instance.outgoingPortMap
	}

	// -- Action Selection
	override getActionSelection() {
		'''
			#pragma hls_design
			void «instance.simpleName»(«FOR port : actor.inputs SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDFOR»«IF !actor.inputs.empty», «ENDIF»«FOR port : actor.outputs SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»_«i»«ENDFOR»«ELSE»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDIF»«ENDFOR»){
			
				switch (state) {
					«FOR state : actor.fsm.states SEPARATOR "\n"»
						«getStateContent(state)»
					«ENDFOR»
			
				default:
					state = s_«instance.simpleName»_«actor.fsm.initialState.label»;
					break;
				}
			}
		'''
	}

	def getStateContent(State state) {
		var Map<Port, Integer> maxPortTokens = new HashMap
		var Boolean actionsHaveInputPrts = false
		for (edge : state.outgoing) {
			var action = (edge as Transition).action
			if (!action.inputPattern.ports.empty) {
				var EMap<Port, Integer> inputNumTokens = action.inputPattern.numTokensMap
				for (port : inputNumTokens.keySet) {
					if (maxPortTokens.containsKey(port)) {
						var oldValue = maxPortTokens.get(port)
						if (oldValue < inputNumTokens.get(port)) {
							maxPortTokens.put(port, inputNumTokens.get(port))
						}
					} else {
						maxPortTokens.put(port, inputNumTokens.get(port))
					}
				}
				actionsHaveInputPrts = true;
			}
		}
		'''
			case(s_«instance.simpleName»_«state.label»):
				«FOR port : maxPortTokens.keySet»
					«IF maxPortTokens.get(port) == 1»
						if(«port.name».size() > 0 && p_«port.name»_token_index < «maxPortTokens.get(port)»){
							p_«port.name»[(p_«port.name»_head + p_«port.name»_token_index ) & (p_«port.name»_size - 1)] = «port.name».read();
							p_«port.name»_token_index++;
						}
					«ELSE»
						while(«port.name».size() > 0 && p_«port.name»_token_index < «maxPortTokens.get(port)»){
							p_«port.name»[(p_«port.name»_head + p_«port.name»_token_index ) & (p_«port.name»_size - 1)] = «port.name».read();
							p_«port.name»_token_index++;
						}
					«ENDIF»
				«ENDFOR»
				«FOR edge : state.outgoing SEPARATOR " else"»
					«getTransitionContent(edge as Transition)»
				}«ENDFOR»
			break;
		'''
	}

	def getTransitionContent(Transition transition) {
		var action = transition.action
		var tState = transition.target
		var EMap<Port, Integer> inputNumTokens = action.inputPattern.numTokensMap
		'''
			if(«action.scheduler.name»()«IF !inputNumTokens.empty» && «FOR port : inputNumTokens.keySet SEPARATOR " && "»p_«port.name»_token_index >= «inputNumTokens.get(port)»«ENDFOR»«ENDIF»){
				// -- Call Action
				«action.body.name»(«FOR port : action.outputPattern.ports SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR ", "»«port.name»_«i»«ENDFOR»«ELSE»«port.name»«ENDIF»«ENDFOR»);
				state = s_«instance.simpleName»_«tState.label»;
		'''
	}

}
