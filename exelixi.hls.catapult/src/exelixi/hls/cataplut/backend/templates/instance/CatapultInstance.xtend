/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.cataplut.backend.templates.instance

import exelixi.hls.cataplut.backend.templates.instance.actionselection.ActionSelectionGeneric
import exelixi.hls.cataplut.backend.templates.instance.actionselection.ActionSelectionSimple
import exelixi.hls.templates.instance.AbstractInstancePrinter
import exelixi.hls.templates.instance.actionselection.ActionSelection
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.Map
import net.sf.orcc.df.Instance

/**
 * 
 * @author Endri Bezati
 */
class CatapultInstance extends AbstractInstancePrinter {

	TypePrinterVisitor typePrinter

	new(Instance instance, TypePrinterVisitor typePrinter, Map<String, Object> options) {
		super(instance, typePrinter,options)
		this.typePrinter = typePrinter
	}

	override getContent() {

		var ActionSelection actionSelection
		if (isActorComplex) {
			actionSelection = new ActionSelectionGeneric(instance, typePrinter)

		} else {
			actionSelection = new ActionSelectionSimple(instance, typePrinter)
		}
		'''
			«getFileHeader("Exilixi for Catapult, Instance Code Generation, Instance: " + instance.label)»
			«headers»
			«instanceParameters»
			«stateVariables»
			«IF !actor.procs.empty»«funcitonsProcedures»«ENDIF»
			«actions»
			«actionSelection.actionSelection»
		'''

	}

}
