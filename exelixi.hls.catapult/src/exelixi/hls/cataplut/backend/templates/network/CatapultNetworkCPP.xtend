/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.cataplut.backend.templates.network

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.HashMap
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port
import net.sf.orcc.graph.Edge

/**
 * 
 * @author Endri Bezati
 */
class CatapultNetworkCPP extends HLSTemplate {

	/** Network Inputs **/
	List<Port> inputs;

	/** Network Outputs **/
	List<Port> outputs;

	Network network;

	var Map<Connection, String> queueNames

	protected TypePrinterVisitor typePrinter

	new(Network network, TypePrinterVisitor typePrinter) {
		super(typePrinter)
		this.network = network
		this.typePrinter = typePrinter
		this.inputs = network.inputs
		this.outputs = network.outputs;
		retrieveQueueNames
	}

	def getContent() {
		'''
			«getFileHeader("Exilixi for Catapult, Network Code Generation, Network: " + network.simpleName)»
			«headers»
			#pragma hls_design top
			void «network.simpleName»(«FOR port : inputs SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDFOR»«IF !inputs.empty», «ENDIF»«FOR port : outputs SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDFOR»){
				«channels»
				«portReaders»
				«instances»
				«portWriters»
			} 
		'''
	}

	def getHeaders() {
		'''
			#include "«typePrinter.intIncludeHeaderFile»"
			#include "«typePrinter.channelIncludeHeaderFile»"
			«IF !network.inputs.empty»
				«FOR port : inputs»
					#include "../../code-gen/include/PortReader_«port.name».h"
				«ENDFOR»
			«ENDIF»
			«FOR instance : network.children.filter(typeof(Instance))»
				#include "../../code-gen/include/«instance.simpleName».h"
			«ENDFOR»
			«IF !network.outputs.empty»
				«FOR port : outputs»
					#include "../../code-gen/include/PortWriter_«port.name».h"
				«ENDFOR»
			«ENDIF»
		'''
	}

	def getPortReaders() {
		'''
			«IF !inputs.empty»
				// -- Network Port Reader
				«FOR port : inputs»
					«getPortReader(port)»
				«ENDFOR»
			«ENDIF»
		'''
	}
	
	def getPortReader(Port port){
		val List<Edge> outgoing = port.outgoing
		'''
			PortReader_«port.name»(«port.name», «FOR edge : outgoing SEPARATOR ", "»«queueNames.get(edge as Connection)»«ENDFOR»);
		'''
	}

	def getPortWriters() {
		'''
			«IF !outputs.empty»
				// -- Network Port Writer
				«FOR port : outputs»
					PortWriter_«port.name»(«queueNames.get(port.incoming.get(0) as Connection)», «port.name»);
				«ENDFOR»
			«ENDIF»
		'''
	}

	def getChannels() {
		'''
			// -- FIFO Queue Channels
			«FOR connection : network.connections»
				static ac_channel<«if (connection.source instanceof Instance) connection.sourcePort.type.doSwitch else (connection.source as Port).type.doSwitch»> «queueNames.get(connection)»;
			«ENDFOR»
		'''
	}


	def getInstances() {
		'''
			// -- Instances
			«FOR instance : network.children.filter(typeof(Instance))»
				«getInstance(instance)»
			«ENDFOR»
		'''
	}

	def getInstance(Instance instance){
		val Map<Port,Connection> incoming = instance.incomingPortMap
		val Map<Port,List<Connection>> outgoing = instance.outgoingPortMap
		val List<Port> inputs = instance.getActor.inputs
		val List<Port> outputs = instance.getActor.outputs
		'''
			«instance.simpleName»(«FOR port : inputs SEPARATOR ", "»«queueNames.get(incoming.get(port))»«ENDFOR»«IF !inputs.empty && !outputs.empty», «ENDIF»«FOR port : outputs SEPARATOR ", "»«FOR connection: outgoing.get(port) SEPARATOR ", "»«queueNames.get(connection)»«ENDFOR»«ENDFOR»);
		'''
	}
	
	// -- Helper Methods
	def retrieveQueueNames() {
		queueNames = new HashMap<Connection, String>
		for (connection : network.connections) {
			if (connection.source instanceof Port) {
				if (connection.target instanceof Instance) {
					queueNames.put(connection,
						"q_" + (connection.source as Port).name + "_" + (connection.target as Instance).name + "_" +
							connection.targetPort.name)
				}
			} else if (connection.source instanceof Instance) {
				if (connection.target instanceof Port) {
					queueNames.put(connection,
						"q_" + (connection.source as Instance).name + "_" + connection.sourcePort.name + "_" +
							(connection.target as Port).name)
				} else if (connection.target instanceof Instance) {
					queueNames.put(connection,
						"q_" + (connection.source as Instance).name + "_" + connection.sourcePort.name + "_" +
							(connection.target as Instance).name + "_" + connection.targetPort.name)
				}

			}
		}
	}

}
