/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.cataplut.backend.templates.scripts

import static exelixi.hls.backend.Constants.CLOCK_PERIOD;
import static exelixi.hls.backend.Constants.CLOCK_PERIOD_VALUE;

import static net.sf.orcc.OrccLaunchConstants.*
import exelixi.hls.templates.tclscripts.TCLTemplate
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Connection
import java.util.Map
import java.util.HashMap
import net.sf.orcc.df.Port

/**
 * 
 * @author Endri Bezati
 */
class CatapultShell extends TCLTemplate {

	private Instance instance

	private Network network

	private String name
	
	private var Map<Connection, String> queueNames

	private double clockPeriod
	
	private int fifoSize
	
	new(Instance instance, Map<String, Object> options) {
		this.name = instance.simpleName
		this.instance = instance
		clockPeriod = 10.0
		if(options.get(CLOCK_PERIOD) as Boolean){
			try{
				clockPeriod = Float.parseFloat(options.get(CLOCK_PERIOD_VALUE) as String)	
			}catch(NumberFormatException e){
				clockPeriod = 10.0
			}	
		}
	}

	new(Network network, Map<String, Object> options) {
		this.name = network.simpleName
		this.network = network
		clockPeriod = 10.0
		if(options.get(CLOCK_PERIOD) as Boolean){
			try{
				clockPeriod = Float.parseFloat(options.get(CLOCK_PERIOD_VALUE) as String)	
			}catch(NumberFormatException e){
				clockPeriod = 10.0
			}	
		}
		if (options.containsKey(FIFO_SIZE)) {
			fifoSize = options.get(FIFO_SIZE) as Integer
		} else {
			fifoSize = DEFAULT_FIFO_SIZE
		}
		retrieveQueueNames
	}

	def getContent() {
		'''
			«getFileHeader("Catpult Shell TCL Script")»
			
			project new
			flow package require /SCVerify
			«IF instance !== null»
				solution file add ../../code-gen/src/«instance.simpleName».cpp -type C++
				solution file add ../../code-gen/include/«instance.simpleName».h -type CHEADER
				solution file add ../../code-gen/src-tb/«instance.simpleName»_tb.cpp -type C++ -exclude true
			«ELSE»
				«IF !network.inputs.empty»
					«FOR port : network.inputs»
						solution file add "../../code-gen/src/PortReader_«port.name».cpp" -type C++
						solution file add "../../code-gen/include/PortReader_«port.name».h" -type CHEADER
					«ENDFOR»
				«ENDIF»
				«IF !network.outputs.empty»
					«FOR port : network.outputs»
						solution file add "../../code-gen/src/PortWriter_«port.name».cpp" -type C++
						solution file add "../../code-gen/include/PortWriter_«port.name».h" -type CHEADER
					«ENDFOR»
				«ENDIF»
				«FOR instance : network.children.filter(typeof(Instance))»
					solution file add ../../code-gen/src/«instance.simpleName».cpp -type C++
					solution file add ../../code-gen/include/«instance.simpleName».h -type CHEADER 
				«ENDFOR»
				solution file add ../../code-gen/src/«network.simpleName».cpp -type C++
				solution file add ../../code-gen/src-tb/«network.simpleName»_tb.cpp -type C++ -exclude true
			«ENDIF»
			directive set -DESIGN_GOAL latency
			directive set -OLD_SCHED false
			directive set -SPECULATE true
			directive set -MERGEABLE true
			directive set -REGISTER_THRESHOLD 256
			directive set -MEM_MAP_THRESHOLD 32
			directive set -FSM_ENCODING none
			directive set -REG_MAX_FANOUT 0
			directive set -NO_X_ASSIGNMENTS true
			directive set -SAFE_FSM false
			directive set -ASSIGN_OVERHEAD 0
			directive set -UNROLL no
			directive set -IO_MODE super
			directive set -REGISTER_IDLE_SIGNAL false
			directive set -IDLE_SIGNAL {}
			directive set -TRANSACTION_DONE_SIGNAL true
			directive set -DONE_FLAG {}
			directive set -START_FLAG {}
			directive set -BLOCK_SYNC none
			directive set -TRANSACTION_SYNC ready
			directive set -DATA_SYNC none
			directive set -RESET_CLEARS_ALL_REGS true
			directive set -CLOCK_OVERHEAD 20.000000
			directive set -OPT_CONST_MULTS -1
			directive set -CHARACTERIZE_ROM false
			directive set -PROTOTYPE_ROM true
			directive set -ROM_THRESHOLD 64
			directive set -CLUSTER_INPUT_WIDTH_THRESHOLD 0
			directive set -CLUSTER_OPT_CONSTANT_INPUTS true
			directive set -CLUSTER_RTL_SYN false
			directive set -CLUSTER_FAST_MODE false
			directive set -CLUSTER_TYPE combinational
			directive set -COMPGRADE fast
			directive set -PIPELINE_RAMP_UP true
			go analyze
			solution library add mgc_Xilinx-VIRTEX-7-1_beh_xst -- -rtlsyntool Xilinx -manufacturer Xilinx -family VIRTEX-7 -speed -1 -part 7V1500TFLG1761
			solution library add Xilinx_accel
			solution library add Xilinx_RAMS
			go compile
			directive set -CLOCKS {clk {-CLOCK_PERIOD «clockPeriod» -CLOCK_EDGE rising -CLOCK_UNCERTAINTY 0.0 -CLOCK_HIGH_TIME «clockPeriod/2» -RESET_SYNC_NAME rst -RESET_ASYNC_NAME arst_n -RESET_KIND sync -RESET_SYNC_ACTIVE high -RESET_ASYNC_ACTIVE low -ENABLE_NAME {} -ENABLE_ACTIVE high}}
			directive set -CLOCK_NAME clk
			go assembly
			go libraries
			«IF network !== null»
				«FOR connection : network.connections»
					directive set /«network.simpleName»/«queueNames.get(connection)»:cns -FIFO_DEPTH «IF connection.size === null»«fifoSize»«ELSE»«connection.size»«ENDIF» 
				«ENDFOR»
			«ENDIF»
			go architect
			go extract
			
		'''
	}
	
	// -- Helper Methods
	def retrieveQueueNames() {
		queueNames = new HashMap<Connection, String>
		for (connection : network.connections) {
			if (connection.source instanceof Port) {
				if (connection.target instanceof Instance) {
					queueNames.put(connection,
						"q_" + (connection.source as Port).name + "_" + (connection.target as Instance).name + "_" +
							connection.targetPort.name)
				}
			} else if (connection.source instanceof Instance) {
				if (connection.target instanceof Port) {
					queueNames.put(connection,
						"q_" + (connection.source as Instance).name + "_" + connection.sourcePort.name + "_" +
							(connection.target as Port).name)
				} else if (connection.target instanceof Instance) {
					queueNames.put(connection,
						"q_" + (connection.source as Instance).name + "_" + connection.sourcePort.name + "_" +
							(connection.target as Instance).name + "_" + connection.targetPort.name)
				}

			}
		}
	}
	
}
