/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.hls.cataplut.backend.templates.scverify

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import java.io.File
import java.util.List
import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port

import static exelixi.hls.backend.Constants.FIFO_TRACE_PATH

/**
 * 
 * @author Endri Bezati
 */
class SCVerifyTestbench extends HLSTemplate {

	Instance instance

	Network network

	String name

	TypePrinterVisitor typePrinter

	List<Port> inputs

	List<Port> outputs

	Map<Port, List<Connection>> outgoingPortMap

	String fifoTracePath

	new(Instance instance, TypePrinterVisitor typePrinter, Map<String, Object> options) {
		super(typePrinter)
		this.name = instance.simpleName
		this.instance = instance
		this.typePrinter = typePrinter
		this.inputs = instance.getActor.inputs
		this.outputs = instance.getActor.outputs
		this.outgoingPortMap = instance.outgoingPortMap
		this.fifoTracePath = options.get(FIFO_TRACE_PATH) as String

	}

	new(Network network, TypePrinterVisitor typePrinter, Map<String, Object> options) {
		super(typePrinter)
		this.name = network.simpleName
		this.network = network
		this.typePrinter = typePrinter
		this.inputs = network.inputs
		this.outputs = network.outputs
		this.fifoTracePath = options.get(FIFO_TRACE_PATH) as String

	}

	def getContent() {
		'''
			«getFileHeader("Exelixi for Catapult, SCVerify Testbench Code Generation")»
			
			«headers»
			
			CCS_MAIN(int argc, char *argv[]){
				«fileStreams»
				
				«channels»
				
				«fillInputStreams»
				
				
				«FOR port : outputs»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								int «port.name»_«i»_token_counter = 0;
							«ENDFOR»
						«ELSE»
							int «port.name»_token_counter = 0;
						«ENDIF»
					«ELSE»
						int «port.name»_token_counter = 0;
					«ENDIF»
				«ENDFOR»
				
				«FOR port : outputs»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								bool end_of_file_n_«port.name»_«i» = false;
							«ENDFOR»
						«ELSE»
							bool end_of_file_n_«port.name» = false;
						«ENDIF»
					«ELSE»
						bool end_of_file_n_«port.name» = false;
					«ENDIF»
				«ENDFOR»
				
				bool end_of_files = false;
				
				while(«IF !inputs.empty»(«FOR port : inputs SEPARATOR " && "»!«port.name».empty()«ENDFOR»)«ENDIF»«IF !outputs.empty» || !end_of_files«ENDIF»){
					CCS_DESIGN(«name»)(«FOR port : inputs SEPARATOR ", "»«port.name»«ENDFOR»«IF !inputs.empty && !outputs.empty», «ENDIF»«FOR port : outputs SEPARATOR ", "»«IF instance !== null»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR ", "»«port.name»_«i»«ENDFOR»«ELSE»«port.name»«ENDIF»«ELSE»«port.name»«ENDIF»«ENDFOR»);
				
					«compareGoldenOutput»
				
				}
				
				std::cout << "Passed testbench for «name» !" << std::endl;
				
				«FOR port : outputs»
					«IF instance !== null»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
								std::cout <<  "Port «port.name»_«i» : " << «port.name»_«i»_token_counter << " produced." << std::endl;
							«ENDFOR»
						«ELSE»
							std::cout <<  "Port «port.name» : " << «port.name»_token_counter << " produced." << std::endl;
						«ENDIF»
					«ELSE»
						std::cout <<  "Port «port.name» : " << «port.name»_token_counter << " produced." << std::endl;
					«ENDIF»
				«ENDFOR»
				
				CCS_RETURN(0);
			}
		'''
	}

	def getHeaders() {
		'''
			#include <fstream>
			#include <sstream>
			#include <string>
			#include <iostream>
			
			#include <mc_scverify.h>
			
			#include "../../code-gen/include/«name».h"
		'''
	}

	def getFileStreams() {
		'''
			// - File Streams
			«IF !inputs.empty»// -- Actor Input Vector of Tokens«ENDIF»
			«FOR port : inputs»
				std::ifstream «port.name»_file("«IF instance !== null»«instancePortFilePath(instance,port)»«ELSE»«fifoTracePath»«File.separator»«port.name».txt«ENDIF»");
				if(«port.name»_file == NULL){
					std::cout <<"«port.name» input file not found!" << std::endl;
					return 1;
				}
			«ENDFOR»
			
			«IF !outputs.empty»// -- Actor Output Vector of Tokens, golden reference«ENDIF»
			«FOR port : outputs»
				«IF instance !== null»
					«IF outgoingPortMap.get(port).size > 1»
						«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
							std::ifstream «port.name»_«i»_file("«IF instance !== null»«instancePortFilePath(instance,port)»«ELSE»«fifoTracePath»«File.separator»«port.name».txt«ENDIF»");
							if(«port.name»_«i»_file == NULL){
								std::cout <<"«port.name» reference file not found!" << std::endl;
								CCS_RETURN(1);
							}
						«ENDFOR»
					«ELSE»
						std::ifstream «port.name»_file("«IF instance !== null»«instancePortFilePath(instance,port)»«ELSE»«fifoTracePath»«File.separator»«port.name».txt«ENDIF»");
						if(«port.name»_file == NULL){
							std::cout <<"«port.name» reference file not found!" << std::endl;
							CCS_RETURN(1);
						}
					«ENDIF»
				«ELSE»
					std::ifstream «port.name»_file("«IF instance !== null»«instancePortFilePath(instance,port)»«ELSE»«fifoTracePath»«File.separator»«port.name».txt«ENDIF»");
					if(«port.name»_file == NULL){
						std::cout <<"«port.name» reference file not found!" << std::endl;
						CCS_RETURN(1);
					}
				«ENDIF»
			«ENDFOR»
		'''
	}

	def getChannels() {
		'''
			// -- Channels
			«FOR port : inputs»
				static ac_channel< «port.type.doSwitch» > «port.name»;
			«ENDFOR»
			
			«FOR port : outputs»
				«IF instance !== null»
					«IF outgoingPortMap.get(port).size > 1»
						«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
							static ac_channel< «port.type.doSwitch» > «port.name»_«i»;
						«ENDFOR»
					«ELSE»
						static ac_channel< «port.type.doSwitch» > «port.name»;
					«ENDIF»
				«ELSE»
					static ac_channel< «port.type.doSwitch» > «port.name»;
				«ENDIF»
			«ENDFOR»
		'''
	}

	def fillInputStreams() {
		'''
			«FOR port : inputs»
				// -- Write «port.name» token to the stream
				std::string «port.name»_line;
				while(std::getline(«port.name»_file, «port.name»_line)){
					std::istringstream iss(«port.name»_line);
					int «port.name»_tmp;
					iss >> «port.name»_tmp;
					«port.name».write((«port.type.doSwitch») «port.name»_tmp);
				}
			«ENDFOR»
		'''
	}

	def compareGoldenOutput() {
		'''
			«FOR port : outputs»
				«IF instance !== null»
					«IF outgoingPortMap.get(port).size > 1»
						«FOR int i : 0 .. outgoingPortMap.get(port).size - 1»
							if(!«port.name»_«i».empty()){
								std::string «port.name»_«i»_line;
								end_of_file_n_«port.name»_«i» = std::getline(«port.name»_«i»_file, «port.name»_«i»_line);
								if(end_of_file_n_«port.name»_«i»){
									«port.type.doSwitch» «port.name»_«i»_exp_value = «port.name»_«i».read();
									int «port.name»_«i»_golden_reference;
									
									std::istringstream iss(«port.name»_«i»_line);
									iss >> «port.name»_«i»_golden_reference;
									
									if ((«port.type.doSwitch») «port.name»_«i»_golden_reference != «port.name»_«i»_exp_value) {
										std::cout << "Port «port.name»_«i»: Error !!! Expected value does not match golden reference, Token Counter: " << «port.name»_«i»_token_counter << std::endl;
										std::cout << "Expected: " <<  «port.name»_«i»_golden_reference << std::endl;
										std::cout << "Got: " << «port.name»_«i»_exp_value << std::endl;
										return 1;
									}
									«port.name»_«i»_token_counter++;
								}
							}
						«ENDFOR»
					«ELSE»
						if(!«port.name».empty()){
							std::string «port.name»_line;
							end_of_file_n_«port.name» = std::getline(«port.name»_file, «port.name»_line);
							if(end_of_file_n_«port.name»){
								«port.type.doSwitch» «port.name»_exp_value = «port.name».read();
								int «port.name»_golden_reference;
								
								std::istringstream iss(«port.name»_line);
								iss >> «port.name»_golden_reference;
								
								if ((«port.type.doSwitch») «port.name»_golden_reference != «port.name»_exp_value) {
									std::cout << "Port «port.name»: Error !!! Expected value does not match golden reference, Token Counter: " << «port.name»_token_counter << std::endl;
									std::cout << "Expected: " <<  «port.name»_golden_reference << std::endl;
									std::cout << "Got: " << «port.name»_exp_value << std::endl;
									return 1;
								}
								«port.name»_token_counter++;
							}
						}
					«ENDIF»
				«ELSE»
					if(!«port.name».empty()){
						std::string «port.name»_line;
						end_of_file_n_«port.name» = std::getline(«port.name»_file, «port.name»_line);
						if(end_of_file_n_«port.name»){
							«port.type.doSwitch» «port.name»_exp_value = «port.name».read();
							int «port.name»_golden_reference;
							
							std::istringstream iss(«port.name»_line);
							iss >> «port.name»_golden_reference;
							
							if ((«port.type.doSwitch») «port.name»_golden_reference != «port.name»_exp_value) {
								std::cout << "Port «port.name»: Error !!! Expected value does not match golden reference, Token Counter: " << «port.name»_token_counter << std::endl;
								std::cout << "Expected: " <<  «port.name»_golden_reference << std::endl;
								std::cout << "Got: " << «port.name»_exp_value << std::endl;
								return 1;
							}
							«port.name»_token_counter++;
							}
						}
				«ENDIF»
			«ENDFOR»
			
			if(«FOR port : outputs SEPARATOR " && "»«IF instance !== null»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR " && "»end_of_file_n_«port.name»_«i» == false«ENDFOR»«ELSE»end_of_file_n_«port.name» == false«ENDIF»«ELSE»end_of_file_n_«port.name» == false«ENDIF»«ENDFOR»)
				end_of_files = true;
			
			
		'''
	}

	// -- Helper Methods
	private def String instancePath(Instance instance) {
		var dir = fifoTracePath + File.separator;
		for (String sp : instance.hierarchicalId.subList(1, instance.hierarchicalId.size)) {
			dir += (sp + File.separator);
		}
		return dir;
	}

	private def String instancePortFilePath(Instance instance, Port port) {
		return instancePath(instance) + port.name + ".txt";
	}

}
