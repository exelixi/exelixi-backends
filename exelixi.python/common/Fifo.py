import sys 

class Fifo():
    
    def __init__(self, size=512, readers=1):
        self.readers = readers
        self.size = size
        self.buffer = [None] * size
        self.wr_ptr = 0
        self.rd_ptr = [0] * readers
        
    def rooms(self):
        max_rooms = sys.maxint
        for i in range(self.readers):
            rooms = (self.size + self.rd_ptr[i] - self.wr_ptr - 1) % self.size
            if(rooms < max_rooms): max_rooms = rooms
        return max_rooms
    
    def count(self, reader=0):
        return (self.size + self.wr_ptr - self.rd_ptr[reader]) % self.size
     
    def peek(self, reader=0): 
        return self.buffer[self.rd_ptr[reader] % self.size]
    
    def peek_n(self, elements, reader=0):
        out = [None] * elements
        for i in range(elements):
            out[i] = self.buffer[(self.rd_ptr[reader] + i) % self.size]
        return out
    
    def write(self, data):
        self.buffer[self.wr_ptr] = data
        self.wr_ptr = (self.wr_ptr + 1) % self.size
            
    def write_n(self, data, elements):
        self.buffer[self.wr_ptr] = data     
        for i in range(elements):
            self.buffer[(self.wr_ptr + i) % self.size] = data[i]
        self.wr_ptr = (self.wr_ptr + elements) % self.size
    
    def read(self, reader=0):
        out = self.peek(reader)
        self.rd_ptr[reader] = (self.rd_ptr[reader] + 1) % self.size
        return out
        
    def read_n(self, elements, reader=0):
        out = self.peek_n(elements, reader)
        self.rd_ptr[reader] = (self.rd_ptr[reader] + elements) % self.size
        return out
