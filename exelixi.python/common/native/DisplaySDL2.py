import numpy
import pygame
from Options import Options
from pygame.locals import QUIT, KEYDOWN, K_ESCAPE
from operator import concat
from Framerate import fpsPrintInit

class Globals():
    init = True
    lastWidth = 0
    lastHeight = 0
    
def displayYUV_init():
    if (Globals.init) :
        Globals.init = False;
        if (pygame.init() < 0) :
            print("Video initialization failed")
            exit(-1)
        fpsPrintInit()
        
def displayYUV_getNbFrames():
    return Options.nbFrames

def display_close():
    pygame.quit()
    
def displayYUV_getFlags():
    return not Options.disable_display

def displayYUV_setSize(width, height):
    global window
    window = pygame.display.set_mode([width,height])
    pygame.display.set_caption("Display")
    
    global overlay
    overlay = pygame.Overlay(pygame.YV12_OVERLAY, [width,height])
    
    if window == None or overlay == None:
        print ("Error opening the display windows")  

def displayYUV_displayPicture(pictureBufferY, pictureBufferU, pictureBufferV, pictureWidth, pictureHeight):
    if ((pictureHeight != Globals.lastHeight) or (pictureWidth != Globals.lastWidth)):
        displayYUV_setSize(pictureWidth, pictureHeight)
        Globals.lastHeight = pictureHeight
        Globals.lastWidth = pictureWidth
    
    size = pictureWidth * pictureHeight;
    
    global overlay
    y = reduce(concat, numpy.char.mod('%c', pictureBufferY[0:size]))
    u = reduce(concat, numpy.char.mod('%c', pictureBufferU[0:(size/4)]))
    v = reduce(concat, numpy.char.mod('%c', pictureBufferV[0:(size/4)]))           
    overlay.display((y, u, v))
    
    events = pygame.event.get()
    for e in events:
        if e.type == QUIT or (e.type == KEYDOWN and e.key == K_ESCAPE):
            pygame.quit()
            exit(0)
    