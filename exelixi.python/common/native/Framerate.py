import time

def Globals():
    startTime = 0.0
    relativeStartTime = 0.0
    lastNumPic = 0
    numPicturesDecoded = 0
    numAlreadyDecoded = 0
    partialNumPicturesDecoded = 0

def print_fps_avg():
    endTime = time.clock()
    decodingTime = (endTime - Globals.startTime) #/ CLOCKS_PER_SEC;
    framerate = Globals.numPicturesDecoded / decodingTime;

    print("{} images in {} seconds: {:0.2f} FPS".format(Globals.numPicturesDecoded, decodingTime, framerate))

def fpsPrintInit():
    Globals.startTime = time.clock();
    Globals.numPicturesDecoded = 0;
    Globals.partialNumPicturesDecoded = 0;
    Globals.lastNumPic = 0;
    Globals.relativeStartTime = Globals.startTime;

def fpsPrintNewPicDecoded():
    Globals.numPicturesDecoded += 1
    Globals.partialNumPicturesDecoded += 1
    endTime = time.clock()

    relativeTime = (endTime - Globals.relativeStartTime) #/ CLOCKS_PER_SEC;

    if(relativeTime >= 5):
        framerate = (Globals.numPicturesDecoded - Globals.lastNumPic) / relativeTime;
        print("{:0.2f} images/sec".format(framerate))
        Globals.relativeStartTime = endTime;
        Globals.lastNumPic = Globals.numPicturesDecoded;
    

