import argparse

class Options():
    mapping_file = ""
    input_file = ""
    write_file = ""
    nbLoops = -1
    nbFrames = -1
    disable_display = False
    
    def __init__(self):
        pass
        
    def parse(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("-m", help="Actor mapping file", default="")
        parser.add_argument("-i", help="Input file path", default="")
        parser.add_argument("-l", help="Input file reading loops number", type=int, default=-1)
        parser.add_argument("-f", help="Number of frame to display before quitting the application", type=int, default=-1)
        parser.add_argument("-w", help="Output file path", default="")
        parser.add_argument("-n", help="Disable display", default=False, action="store_true")
        args = parser.parse_args()
            
        Options.mapping_file = args.m
        Options.input_file = args.i
        Options.nbLoops = args.l
        Options.nbFrames = args.f
        Options.write_file = args.w
        Options.disable_display = args.n