'''
Created on May 24, 2017

@author: scb
'''
from Options import Options
import os.path 

def source_init():
    if not os.path.isfile(Options.input_file):
        print("[ERROR] no input file provided. Run the application with -h to see the help")
        exit(-1)

    global __SOURCE_FILE 
    __SOURCE_FILE = open(Options.input_file, "rb")
    global __SOURCE_LOOPS 
    __SOURCE_LOOPS = Options.nbLoops
    

def source_readNBytes(data_to_append, num_bytes):
    for i in range(num_bytes):
        data_to_append[i] = bytes2int(__SOURCE_FILE.read(1))

def source_sizeOfFile():
    fileSize = os.path.getsize(Options.input_file)
    #print("file size: {}".format(fileSize))
    return fileSize


def source_isMaxLoopsReached():
    global __SOURCE_LOOPS 
    return __SOURCE_LOOPS == 0


def source_decrementNbLoops():
    global __SOURCE_LOOPS
    #print(__SOURCE_LOOPS) 
    __SOURCE_LOOPS -= 1

def source_rewind():
    global __SOURCE_FILE 
    __SOURCE_FILE.close()
    __SOURCE_FILE = open(Options.input_file, "rb")

def bytes2int(byte_value):
    return int(byte_value.encode('hex'), 16)

def source_exit(value):
    pass
