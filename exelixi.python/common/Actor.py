from abc import ABCMeta, abstractmethod
             
class Actor:
             
    __metaclass__ = ABCMeta
            
    @abstractmethod
    def execute(self):
        return False
    
    @abstractmethod
    def initialize(self):
        pass
                                
    @abstractmethod
    def name(self):
        pass