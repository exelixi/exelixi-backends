/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Simone Casale Brunet
 * web   : http://gramm.epfl.ch
 * email : simone.casalebrunet@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.python;

import static exelixi.python.Constants.CREATE_PYDEV_PROJECT;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import exelixi.python.templates.EclipseProjectPrinter;
import exelixi.python.templates.InstancePrinter;
import exelixi.python.templates.NetworkPrinter;
import exelixi.python.transformations.VarInitializer;
import exelixi.python.util.Common;
import net.sf.orcc.backends.AbstractBackend;
import net.sf.orcc.backends.transform.DisconnectedOutputPortRemoval;
import net.sf.orcc.backends.util.Validator;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.transform.Instantiator;
import net.sf.orcc.df.transform.NetworkFlattener;
import net.sf.orcc.df.transform.TypeResizer;
import net.sf.orcc.df.transform.UnitImporter;
/* 
 * XRONOS-EXELIXI
 * 
 * Copyright (C) 2011-2016 EPFL SCI STI MM
 *
 * This file is part of XRONOS-EXELIXI.
 *
 * XRONOS-EXELIXI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XRONOS-EXELIXI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XRONOS-EXELIXI. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Additional permission under GNU GPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or combining it
 * with Eclipse (or a modified version of Eclipse or an Eclipse plugin or 
 * an Eclipse library), containing parts covered by the terms of the 
 * Eclipse Public License (EPL), the licensors of this Program grant you 
 * additional permission to convey the resulting work.  Corresponding Source 
 * for a non-source form of such a combination shall include the source code 
 * for the parts of Eclipse libraries used as well as that of the covered work.
 * 
 */
import net.sf.orcc.df.util.NetworkValidator;
import net.sf.orcc.ir.transform.RenameTransformation;
import net.sf.orcc.util.FilesManager;
import net.sf.orcc.util.Result;

/**
 * 
 * @author Simone Casale-Brunet
 *
 */
public class ExelixiPython extends AbstractBackend {

	protected String nativePath;

	@Override
	protected void doInitializeOptions() {
		
		Map<String, String> replacementMap = new HashMap<String, String>();
		replacementMap.put("list", "__list");
		replacementMap.put("len", "__len");
		replacementMap.put("sum", "__sum");

		networkTransfos.add(new Instantiator(false));
		networkTransfos.add(new NetworkFlattener());
		networkTransfos.add(new UnitImporter());
		networkTransfos.add(new DisconnectedOutputPortRemoval());

		childrenTransfos.add(new VarInitializer());
		childrenTransfos.add(new TypeResizer(true, false, false, false));
		childrenTransfos.add(new RenameTransformation(replacementMap));

		// Create Folders
		createFolders();
	}

	protected void createFolders() {
		nativePath = outputPath + File.separator + "native";
		File nativeDir = new File(nativePath);
		if (!nativeDir.exists()) {
			nativeDir.mkdir();
		}
	}

	@Override
	protected void doValidate(Network network) {
		Validator.checkMinimalFifoSize(network, fifoSize);
		new NetworkValidator().doSwitch(network);
		network.computeTemplateMaps();
	}

	@Override
	protected Result doGenerateNetwork(Network network) {
		final Result result = Result.newInstance();
		result.merge(FilesManager.writeFile(new NetworkPrinter(network, getOptions()).getContent(), outputPath, network.getSimpleName() + ".py"));
		return result;
	}

	@Override
	protected Result doAdditionalGeneration(Network network) {		
		final Result result = Result.newInstance();
		result.merge(Common.copyResource("__init__.py", new File(outputPath, "__init__.py")));
		result.merge(Common.copyResource("Actor.py", new File(outputPath, "Actor.py")));
		result.merge(Common.copyResource("Fifo.py", new File(outputPath, "Fifo.py")));
		
		result.merge(Common.copyNatives(nativePath));
		
		if (getOption(CREATE_PYDEV_PROJECT, true)) {
			result.merge(FilesManager.writeFile(new EclipseProjectPrinter(network).getContent(), outputPath, ".project"));
			result.merge(Common.copyResource("pydevproject", new File(outputPath, ".pydevproject")));
		}
		return result;
	}

	@Override
	protected Result doGenerateInstance(Instance instance) {
		final Result result = Result.newInstance();

		result.merge(FilesManager.writeFile(new InstancePrinter(instance, getOptions()).getContent(), outputPath, instance.getSimpleName() + ".py"));

		return result;
	}

}
