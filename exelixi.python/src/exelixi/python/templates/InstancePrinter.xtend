/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Simone Casale Brunet
 * web   : http://gramm.epfl.ch
 * email : simone.casalebrunet@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.python.templates

import java.util.List
import java.util.Map
import net.sf.orcc.df.Action
import net.sf.orcc.df.Actor
import net.sf.orcc.df.FSM
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.df.State
import net.sf.orcc.df.Transition
import net.sf.orcc.ir.ArgByRef
import net.sf.orcc.ir.ArgByVal
import net.sf.orcc.ir.BlockBasic
import net.sf.orcc.ir.BlockIf
import net.sf.orcc.ir.BlockWhile
import net.sf.orcc.ir.InstAssign
import net.sf.orcc.ir.InstCall
import net.sf.orcc.ir.InstLoad
import net.sf.orcc.ir.InstReturn
import net.sf.orcc.ir.InstStore
import net.sf.orcc.ir.Procedure

import static exelixi.python.util.NetworkUtils.getPorts
import net.sf.orcc.ir.ExprVar

/**
 * 
 * @author Simone Casale-Brunet
 *
 */
class InstancePrinter  extends ExprAndTypePrinter{
	
	private final Instance instance;
	private final Actor actor;
	private final List<Port> ports;
	
	new(Instance instance, Map<String, Object> options) {
		this.instance = instance;
		this.actor = instance.getActor
		this.ports = getPorts(instance)
	}

	def getContent() {
		'''
			«"'''"»
			Source code for «actor.fileName»
			«"'''"»
			from Actor import Actor
			«IF !actor.procs.filter(p | p.native && !"print".equals(p.name)).isEmpty()»
				from native.Options import *
				from native.CompareYUV import *
				from native.DisplaySDL2 import *
				from native.Framerate import *
				from native.Source import *
			«ENDIF»
			
			«IF actor.fsm !== null»
			class State():
				«var states = actor.fsm.states»
				«FOR state : states SEPARATOR "\n"»«state.name» = «states.indexOf(state)»«ENDFOR»
			«ENDIF»
			
			class «instance.simpleName»(Actor):
			
				def __init__(self«FOR param : actor.parameters», «param.name»«ENDFOR»«FOR port : ports», port_«port.name»«ENDFOR»):
					«FOR param : actor.parameters»
						self.«param.name» = «param.name»
					«ENDFOR»
					«FOR port : ports»
						self.port_«port.name» = port_«port.name»
						self.status_«port.name»_ = 0
					«ENDFOR»
					«FOR variable : actor.stateVars»
						self.«variable.varDeclWithInit»
					«ENDFOR»
					«IF actor.fsm !== null»
						self.state_ = State.«actor.fsm.initialState.name»
					«ENDIF»
					«IF actor.parameters.isEmpty && ports.isEmpty && actor.fsm == null && actor.stateVars.isEmpty»
						pass
					«ENDIF»

				def name(self):
					return "«instance.simpleName»"
					
				«compileInitialActions»
				
				«compileScheduler»
			
				«actor.actions.map[compileAction].join»
				
				«actor.procs.filter(p | !p.native).map[compileProcedure].join»
		'''
	}
	
	private def compileInitialActions(){
		'''
			def initialize(self):
				«IF actor.initializes.empty»
					pass
				«ELSE»
					«FOR action : actor.initializes»
						self.«action.name»()
					«ENDFOR»
			«ENDIF»
			
			«actor.initializes.map[compileAction].join»
		'''
	}
	
	private def compileAction(Action action){
		'''
			def «action.scheduler.returnType.doSwitch» «action.scheduler.name»(self):
				«getPeekAddresses(action)»
				«action.scheduler.doSwitch»
			
			def «action.body.name»(self):
				«IF !action.inputPattern.ports.empty»«getReadAddresses(action)»«ENDIF»
				«IF !action.outputPattern.ports.empty»«getWriteAddresses(action)»«ENDIF»
				«action.body.doSwitch»
				«IF !action.inputPattern.ports.empty»«getReadAdvance(action)»«ENDIF»
				«IF !action.outputPattern.ports.empty»«getWriteAdvance(action)»«ENDIF»
				pass
			
		'''
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// Action Selection
	
	private def compileScheduler(){
		'''
		def execute(self):
			«getActionSelectionCount(actor)»
			«getActionSelectionRooms(actor)»
			execute = True
			executed_once = False
			while execute:
				«IF actor.fsm !== null»
					execute = False;
					«FOR action : actor.actionsOutsideFsm»
						«IF action == actor.actionsOutsideFsm.get(0)»if«ELSE»elif«ENDIF»«action.compileScheduler(null)»
					«ENDFOR»
					«actor.fsm.compilerScheduler»
				«ELSE»
					execute = False;
					«FOR action : actor.actions»
						«IF action == actor.actions.get(0)»if«ELSE»elif«ENDIF»«action.compileScheduler(null)»
					«ENDFOR»
				«ENDIF»
			return executed_once
		'''
	}
	
	def compileScheduler(
		Action action,
		State state
	) '''
	(«FOR e : action.inputPattern.numTokensMap»«IF instance.incomingPortMap.get(e.key) !== null»self.status_«e.key.name»_ >= «e.value» and «ENDIF»«ENDFOR»self.«action.scheduler.name»()):
		«IF !action.outputPattern.empty»
		if(«FOR e : action.outputPattern.numTokensMap SEPARATOR " and " »«IF instance.outgoingPortMap.get(e.key) !== null»self.status_«e.key.name»_ >= «e.value» «ENDIF»«ENDFOR»):
			self.«action.body.name»()
			execute = True;
			executed_once = True;
			«IF state !== null»self.state_ = State.«state.name»«ENDIF»
		«ELSE»
			self.«action.body.name»()
			execute = True
			executed_once = True
			«IF state !== null»self.state_ = State.«state.name»«ENDIF»
		«ENDIF»
	'''

	def compilerScheduler(FSM fsm) '''
		«FOR state : fsm.states»
			«IF state == fsm.states.get(0)»if«ELSE»elif«ENDIF» self.state_ == State.«state.name»:
				«FOR edge : state.outgoing»
					«IF edge == state.outgoing.get(0)»if «ELSE»elif «ENDIF»«(edge as Transition).action.compileScheduler(edge.target as State)»
				«ENDFOR»
		«ENDFOR»
	'''
	
	def getActionSelectionCount(Actor actor){
		'''
			«FOR port : instance.incomingPortMap.keySet»
				«IF instance.incomingPortMap.get(port) !== null»
					self.status_«port.name»_ = self.port_«port.name».count(«instance.incomingPortMap.get(port).getAttribute("fifoId").objectValue»)
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	def getActionSelectionRooms(Actor actor){
		'''
			«FOR port : instance.outgoingPortMap.keySet»
				«IF instance.outgoingPortMap.get(port) !== null»
					self.status_«port.name»_ = self.port_«port.name».rooms()
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Procedures / Functions
	
	
	def compileProcedure(Procedure proc) {
		'''
			def «proc.name»(self«FOR param : proc.parameters», «param.variable.name»«ENDFOR»):
				«proc.doSwitch»
				pass
				
		'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Ports handling
	
	def getReadAddresses(Action action){
		'''
			«FOR e : action.inputPattern.numTokensMap»
				«var name = e.key.name»
				«var tokens = e.value»
				«IF instance.incomingPortMap.get(e.key) !== null»
					«name» = self.port_«name».read_n(«tokens», «instance.incomingPortMap.get(e.key).getAttribute("fifoId").objectValue»)
				«ELSE»
					«e.key.name» = [None] * «e.value»
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	
	def getPeekAddresses(Action action){
		'''
			«FOR e : action.peekPattern.numTokensMap»
				«e.key.name» = self.port_«e.key.name».peek_n(«e.value», «instance.incomingPortMap.get(e.key).getAttribute("fifoId").objectValue»);
			«ENDFOR»
		'''
	}
	
	def getWriteAddresses(Action action){
		'''
			«FOR port : action.outputPattern.ports»
				«IF instance.outgoingPortMap.get(port) !== null»
					«var tokens = action.outputPattern.numTokensMap.get(port)»
					«port.name» = [None]«IF tokens > 1» * «tokens»«ENDIF»
				«ELSE»
					// TODO «port.type.doSwitch» «port.name»«port.type»«FOR dim:port.type.dimensions»[«dim»]«ENDFOR»;
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	def getReadAdvance(Action action){
		'''
			«FOR e : action.inputPattern.numTokensMap»
				«IF instance.incomingPortMap.get(e.key) !== null»
					self.status_«e.key.name»_ -= «e.value»;
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	def getWriteAdvance(Action action){
		'''
			«FOR e : action.outputPattern.numTokensMap»
				«IF instance.outgoingPortMap.get(e.key) !== null»
				«var name = e.key.name»
				«var tokens = e.value»
				self.port_«name».write_n(«name», «tokens»);
				self.status_«name»_ -= «tokens»;
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Visitor Members
	
	// -- Instructions
	
	override caseInstAssign(InstAssign inst) '''
		«inst.target.variable.name» = «inst.value.doSwitch»
	'''

	override caseInstCall(InstCall inst) {
		if (inst.print) {
			'''
				print ("«FOR arg : inst.arguments SEPARATOR " "»{}«ENDFOR»".format(«FOR arg : inst.arguments SEPARATOR ", "»«arg.compileArgCout»«ENDFOR»))
			'''
		} else {
			'''
				«IF inst.target !== null»«inst.target.variable.name» = «ENDIF»«IF !inst.procedure.isNative»self.«ENDIF»«inst.procedure.name»(«FOR arg : inst.getArguments SEPARATOR ", "»«arg.compileArg»«ENDFOR»);
			'''
		}
	}
	
	override caseInstLoad(
		InstLoad inst
	) 
	'''
		«inst.target.variable.name» = «IF !inst.source.variable.local»self.«ENDIF»«inst.source.variable.name»«FOR index : inst.indexes»[«index.doSwitch»]«ENDFOR»;
	'''

	override caseInstReturn(
		InstReturn inst
	) '''
		«IF inst.value !== null»return «inst.value.doSwitch»«ENDIF»
	'''

	override caseInstStore(
		InstStore inst
	) '''
		«IF !inst.target.variable.local»self.«ENDIF»«inst.target.variable.name»«FOR index : inst.indexes»[«index.doSwitch»]«ENDFOR» = «inst.value.doSwitch»;
	'''
	
	// -- Blocks
	
	override caseBlockBasic(BlockBasic node) '''
		«FOR inst : node.instructions»«inst.doSwitch»«ENDFOR»
	'''

	override caseBlockIf(BlockIf node) '''
		if(«node.condition.doSwitch»):
			«IF node.thenBlocks.isEmpty»
				pass
			«ELSE»
				«FOR then : node.thenBlocks»«then.doSwitch»«ENDFOR»
			«ENDIF»	
		«IF !node.elseBlocks.empty»else:
			«IF node.elseBlocks.empty»
				pass
			«ELSE»
				«FOR els : node.elseBlocks»«els.doSwitch»«ENDFOR»
			«ENDIF»
		«ENDIF»
		«node.joinBlock.doSwitch»
	'''

	override caseBlockWhile(BlockWhile node) '''
		while(«node.condition.doSwitch»):
			«FOR whil : node.blocks»«whil.doSwitch»«ENDFOR»

		«node.joinBlock.doSwitch»
	'''

	// -- Procedures
	
	override caseProcedure(Procedure procedure) {
		'''
			«FOR variable : procedure.locals.filter[initialized || !type.dimensions.empty]»
				«variable.varDeclWithInit»
			«ENDFOR»
			«FOR node : procedure.blocks»
				«node.doSwitch»
			«ENDFOR»
		'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Arguments
	
	def dispatch compileArg(ArgByRef arg) {
		'''«IF arg.use.variable.global»self.«ENDIF»«arg.use.variable.doSwitch»«FOR index : arg.indexes»[«index.doSwitch»]«ENDFOR»'''
	}

	def dispatch compileArg(ArgByVal arg) {
		'''«IF arg.isArgByValGlobal»self.«ENDIF»«arg.value.doSwitch»'''
	}
	
	def isArgByValGlobal(ArgByVal arg){
		if(arg.value.exprVar){
			return ((arg.value as ExprVar).use.variable.global)
		}else{
			return false;
		}
	}
	
	def dispatch compileArgCout(ArgByRef arg) {
		'''«arg.use.variable.doSwitch»«FOR index : arg.indexes»[«index.doSwitch»]«ENDFOR»'''
	}
	
	def dispatch compileArgCout(ArgByVal arg) {		
		'''«arg.value.doSwitch»'''
	}
	
}
