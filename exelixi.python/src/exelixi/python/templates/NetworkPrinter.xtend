/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Simone Casale Brunet
 * web   : http://gramm.epfl.ch
 * email : simone.casalebrunet@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.python.templates

import java.util.Map
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network

import static exelixi.python.util.NetworkUtils.getConnections
import static net.sf.orcc.OrccLaunchConstants.*

/**
 * 
 * @author Simone Casale-Brunet
 *
 */
class NetworkPrinter extends ExprAndTypePrinter{
	
	private final Network network;
	
	new(Network network, Map<String, Object> options) {
		this.network = network;
		fifoSize = options.getOrDefault(FIFO_SIZE, DEFAULT_FIFO_SIZE) as Integer;
	}

	def getContent() {
		'''
			from Fifo import Fifo
			«FOR instance : network.children.filter(typeof(Instance))»
				from «instance.simpleName» import «instance.simpleName»
			«ENDFOR»
			from native.Options import *
			
			class «network.simpleName»():
			
				def __init__(self):
					
					self.fifos = []
					«FOR instance : network.children.filter(typeof(Instance))»
						«FOR edges : instance.outgoingPortMap.values»
							«val conn = edges.get(0)»
							self.fifo_«conn.getAttribute("idNoBcast").objectValue» = Fifo(«conn.computeSize», «edges.size»)
							self.fifos.append(self.fifo_«conn.getAttribute("idNoBcast").objectValue»)
						«ENDFOR»
			  		«ENDFOR»	  		
					«FOR instance : network.children.filter(typeof(Instance))»
						«instance.instantiateInstance»
					«ENDFOR»
					self.actors = []
					«FOR instance : network.children.filter(typeof(Instance))»
						self.actors.append(self.«instance.simpleName»)
					«ENDFOR»
					
				def execute(self):
						for actor in self.actors:
								actor.initialize()	
						execute = True
						while execute:
							execute = False
							for actor in self.actors:
								execute |= actor.execute()		
			
			
			if __name__ == "__main__":
				op = Options()
				op.parse()
				net = «network.simpleName»()
				net.execute()
		'''
	}
	
	private def int computeSize(Connection connection){
		if(connection.size !== null){
			return connection.size;
		}else{
			return fifoSize;
		}
	}
	
	private def instantiateInstance(Instance instance){
	val connections = getConnections(instance)
	val parameters = instance.getActor.parameters
	'''self.«instance.simpleName» = «instance.simpleName»(«FOR parameter : parameters SEPARATOR ", "»«IF instance.getArgument(parameter.name) !== null»«instance.getArgument(parameter.name).value.doSwitch»«ELSE»«parameter.initialValue.doSwitch»«ENDIF»«ENDFOR»«IF !parameters.empty && !connections.empty», «ENDIF»«FOR conn : connections SEPARATOR ", "»self.fifo_«conn.getAttribute("idNoBcast").objectValue»«ENDFOR»)'''
	}
}
