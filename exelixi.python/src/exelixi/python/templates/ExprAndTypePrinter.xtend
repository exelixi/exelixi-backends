/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Simone Casale Brunet
 * web   : http://gramm.epfl.ch
 * email : simone.casalebrunet@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.python.templates

import net.sf.orcc.backends.CommonPrinter
import net.sf.orcc.ir.ExprBinary
import net.sf.orcc.ir.ExprBool
import net.sf.orcc.ir.ExprFloat
import net.sf.orcc.ir.ExprInt
import net.sf.orcc.ir.ExprList
import net.sf.orcc.ir.ExprString
import net.sf.orcc.ir.ExprUnary
import net.sf.orcc.ir.ExprVar
import net.sf.orcc.ir.Expression
import net.sf.orcc.ir.OpBinary
import net.sf.orcc.ir.OpUnary
import net.sf.orcc.ir.TypeBool
import net.sf.orcc.ir.TypeFloat
import net.sf.orcc.ir.TypeInt
import net.sf.orcc.ir.TypeList
import net.sf.orcc.ir.TypeString
import net.sf.orcc.ir.TypeUint
import net.sf.orcc.ir.TypeVoid
import net.sf.orcc.ir.Var
import net.sf.orcc.util.util.EcoreHelper
import net.sf.orcc.ir.Type

/**
 * 
 * @author Simone Casale Brunet
 */
class ExprAndTypePrinter extends CommonPrinter {

	override caseExprBinary(ExprBinary expr) {
		val op = expr.op
		val container = EcoreHelper.getContainerOfType(expr, typeof(Expression))
		var nextPrec = if (op == OpBinary::SHIFT_LEFT || op == OpBinary::SHIFT_RIGHT) {
				// special case, for shifts always put parentheses because compilers
				// often issue warnings
				Integer::MIN_VALUE;
			} else {
				op.precedence;
			}

		val resultingExpr = '''«expr.e1.printExpr(nextPrec, 0)» «op.stringRepresentation» «expr.e2.printExpr(nextPrec, 1)»'''

		if (op.needsParentheses(precedence, branch) || (container !== null && op.logical)) {
			'''(«resultingExpr»)'''
		} else {
			resultingExpr
		}
	}

	override caseExprBool(ExprBool expr) '''«IF expr.value»True«ELSE»False«ENDIF»'''

	override caseExprFloat(ExprFloat expr) '''«expr.value»'''

	override caseExprInt(ExprInt expr) '''«expr.value»'''

	override caseExprList(ExprList expr) '''[«FOR value : expr.value SEPARATOR ","» «value.doSwitch»«ENDFOR»]'''

	override caseExprString(ExprString expr) '''"«expr.value»"'''

	override caseExprUnary(ExprUnary expr) '''«expr.op.stringRepresentation»(«expr.expr.doSwitch»)'''

	override caseExprVar(ExprVar expr) '''«expr.use.variable.name»'''

	override caseTypeBool(TypeBool type) ''''''

	override caseTypeFloat(TypeFloat type) ''''''

	override caseTypeInt(TypeInt type) ''''''

	override caseTypeList(TypeList type) {
		type.type.doSwitch
	}

	override caseExpression(Expression expression) {
		if (expression != null) {
			'''«expression.doSwitch»'''
		} else {
			'''None'''
		}
	}

	override caseTypeString(TypeString type) ''''''

	override caseTypeUint(TypeUint type) ''''''

	override caseTypeVoid(TypeVoid type) ''''''

	override protected stringRepresentation(OpBinary op) {
		switch (op) {
			case (DIV_INT):
				return "/"
			case (LOGIC_AND):
				return "and"
			case (LOGIC_OR):
				return "or"
			default:
				return super.stringRepresentation(op)
		}
	}

	override protected stringRepresentation(OpUnary op) {
		switch (op) {
			case (LOGIC_NOT):
				return "not"
			default:
				return super.stringRepresentation(op)
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// Variables
	def protected varDeclWithInit(
		Var v) {
		'''«v.name»«IF v.initialized» = «v.initialValue.caseExpression»«ELSEIF !v.type.dimensions.empty» = «FOR dim :  v.type.dimensions»[ «ENDFOR»«varDefaultValue(v.type)»«FOR dim :  v.type.dimensions.reverse» for _ in range(«dim»)] «ENDFOR»«ENDIF»'''
	}

	def protected varDefaultValue(Type t) {
		var type = t
		while (true) {
			if (type == null) {
				return "None";
			} else if (type instanceof TypeInt || type instanceof TypeUint) {
				return "0";
			} else if (type instanceof TypeFloat) {
				return "0.0";
			} else if (type instanceof TypeBool) {
				return "False";
			} else if (type instanceof TypeString) {
				return "\"\""
			} else if (type instanceof TypeList) {
				type = type.innermostType;
			} else {
				return "None"
			}
		}
	}

}
