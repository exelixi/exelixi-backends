/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Simone Casale Brunet
 * web   : http://gramm.epfl.ch
 * email : simone.casalebrunet@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.python.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import net.sf.orcc.OrccRuntimeException;
import net.sf.orcc.util.Result;

/**
 * 
 * @author Simone Casale-Brunet
 *
 */
public class Common {

	public static Result copyNatives(String nativePath) {

		String[] __all__ = { "__init__.py", "CompareYUV.py", "DisplaySDL2.py", "Framerate.py", "Options.py",
				"Source.py" };

		for (String fileName : __all__) {
			File destination = new File(nativePath, fileName);
			copyResource("native/" + fileName, destination);
		}

		return Result.newOkInstance();
	}

	public static Result copyResource(String resource, File destination) {
		try {
			InputStream is = new Common().getClass().getClassLoader().getResource("/common/" + resource).openStream();
			byte[] buffer = new byte[is.available()];
			is.read(buffer);

			OutputStream outStream = new FileOutputStream(destination);
			outStream.write(buffer);

			is.close();
			outStream.close();
		} catch (Exception e) {
			throw new OrccRuntimeException("Error exporting \"" + resource + "\"", e);
		}

		return Result.newOkInstance();
	}

}
