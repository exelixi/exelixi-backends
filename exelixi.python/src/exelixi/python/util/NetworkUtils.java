/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Simone Casale Brunet
 * web   : http://gramm.epfl.ch
 * email : simone.casalebrunet@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.python.util;

import java.util.ArrayList;
import java.util.List;

import net.sf.orcc.df.Connection;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Port;

/**
 * 
 * @author Simone Casale-Brunet
 *
 */
public class NetworkUtils {

	public static List<Port> getPorts(Instance instance) {
		List<Port> ports = new ArrayList<Port>();
		for(Port port : instance.getActor().getInputs()){
			if(instance.getIncomingPortMap().containsKey(port)){
				ports.add(port);
			}else{
				System.out.println("IN not connected "+port.getName());
			}
		}
		//ports.addAll(instance.getActor().getInputs());
		for(Port port : instance.getActor().getOutputs()){
			if(instance.getOutgoingPortMap().containsKey(port) && !instance.getOutgoingPortMap().get(port).isEmpty()){
				ports.add(port);
			}else{
				System.out.println("OUT not connected "+port.getName());
			}
		}
		//ports.addAll(instance.getActor().getOutputs());
		return ports;
	}

	public static List<Connection> getConnections(Instance instance) {
		List<Connection> connections = new ArrayList<>();

		for (Port port : instance.getActor().getInputs()) {
			Connection conn = instance.getIncomingPortMap().get(port);
			if (conn != null)
				connections.add(conn);
		}

		for (Port port : instance.getActor().getOutputs()) {
			Connection conn = instance.getOutgoingPortMap().get(port).get(0);
			if (conn != null)
				connections.add(conn);
		}

		return connections;
	}

}
