/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.backend.constants;

/**
 * 
 * @author Endri Bezati
 */
public class Constants {

	/** Backend Options **/
	
	public static String STATIC_PARTITIONING = "exelixi.cpp.options.staticPartitioning";
	
	public static String GENERATE_FIFO_TRACES = "exelixi.cpp.options.generateFifoTraces";
	
	public static String GENERATE_FIFO_TRACES_FOLDER = "exelixi.cpp.options.generateFifoTracesFolder";
	
	public static String LINK_NATIVE_LIBRARY = "exelixi.cpp.options.linkNativeLibrary";
	
	public static String LINK_NATIVE_LIBRARY_FOLDER = "exelixi.cpp.options.linkNativeLibraryFolder";

	public static String LINK_NATIVE_LIBRARY_HEADERS = "exelixi.cpp.options.linkNativeLibraryHeaders";
	
	public static String MAINTAIN_HIERARCHY = "exelixi.cpp.options.maintainHierarchy";
	
	public static String SCHEDULING_MISS_COUNTER = "exelixi.cpp.options.scheduling.missCounter"; 
	
	public static String CODESIGN_MODE = "exelixi.codesign";
	
	public static String PARTITIONED_NETWORK = "exelixi.codesign.partitionedNetwork";
	
}
