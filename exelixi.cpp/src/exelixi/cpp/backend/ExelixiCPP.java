/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.backend;

import static exelixi.cpp.backend.constants.Constants.GENERATE_FIFO_TRACES;
import static exelixi.cpp.backend.constants.Constants.GENERATE_FIFO_TRACES_FOLDER;
import static exelixi.cpp.backend.constants.Constants.MAINTAIN_HIERARCHY;
import static exelixi.cpp.backend.constants.Constants.STATIC_PARTITIONING;
import static exelixi.cpp.backend.constants.Constants.SCHEDULING_MISS_COUNTER;
import static net.sf.orcc.OrccLaunchConstants.MAPPING;
import static net.sf.orcc.backends.BackendsConstants.BXDF_FILE;
import static net.sf.orcc.backends.BackendsConstants.IMPORT_BXDF;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import exelixi.core.backends.ExelixiBackend;
import exelixi.core.backends.templates.ExelixiPrinter;
import exelixi.core.backends.templates.exelixirunime.ActorHeader;
import exelixi.core.backends.templates.exelixirunime.FifoHeader;
import exelixi.core.backends.templates.exelixirunime.FifoTraceHeader;
import exelixi.core.backends.templates.exelixirunime.FileReadHeader;
import exelixi.core.backends.templates.exelixirunime.FileReadSource;
import exelixi.core.backends.templates.exelixirunime.FileUtilsHeader;
import exelixi.core.backends.templates.exelixirunime.FileUtilsSource;
import exelixi.core.backends.templates.exelixirunime.FileWriteHeader;
import exelixi.core.backends.templates.exelixirunime.FileWriteSource;
import exelixi.core.backends.templates.exelixirunime.GetOptHeader;
import exelixi.core.backends.templates.exelixirunime.GetOptSource;
import exelixi.core.backends.templates.exelixirunime.MappingParserHeader;
import exelixi.core.backends.templates.exelixirunime.MappingParserSource;
import exelixi.core.backends.templates.exelixirunime.NativeHeader;
import exelixi.core.backends.templates.exelixirunime.SchedulingMissLogger;
import exelixi.core.backends.templates.exelixirunime.SdlDisplayHeader;
import exelixi.core.backends.templates.exelixirunime.SdlDisplaySource;
import exelixi.core.backends.templates.exelixirunime.TinyXML2Header;
import exelixi.core.backends.templates.exelixirunime.TinyXML2Source;
import exelixi.cpp.backend.templates.actor.InstanceHeaderPrinter;
import exelixi.cpp.backend.templates.actor.InstanceSourcePrinter;
import exelixi.cpp.backend.templates.cmake.CMakePrinter;
import exelixi.cpp.backend.templates.cmake.CmakeLibPrinter;
import exelixi.cpp.backend.templates.network.CPP11Main;
import exelixi.cpp.backend.templates.network.NetworkHeaderPrinter;
import exelixi.cpp.backend.templates.network.NetworkSourcePrinter;
import exelixi.cpp.backend.transformations.ConnectionReaders;
import exelixi.cpp.backend.transformations.ConnectionsThresholds;
import exelixi.cpp.backend.transformations.Partitioner;
import exelixi.cpp.backend.transformations.SharedVariableDetection;
import exelixi.cpp.backend.transformations.UniqueInstance;
import exelixi.cpp.backend.transformations.VarInitializer;
import net.sf.orcc.backends.transform.DisconnectedOutputPortRemoval;
import net.sf.orcc.backends.util.Mapping;
import net.sf.orcc.backends.util.Validator;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.transform.Instantiator;
import net.sf.orcc.df.transform.NetworkFlattener;
import net.sf.orcc.df.transform.TypeResizer;
import net.sf.orcc.df.transform.UnitImporter;
import net.sf.orcc.df.util.NetworkValidator;
import net.sf.orcc.ir.transform.RenameTransformation;
import net.sf.orcc.tools.mapping.XmlBufferSizeConfiguration;
import net.sf.orcc.util.FilesManager;
import net.sf.orcc.util.OrccLogger;
import net.sf.orcc.util.Result;

/**
 * A CPP11 backend
 * 
 * @author Endri Bezati
 *
 */
public class ExelixiCPP extends ExelixiBackend {

	protected String srcPath;

	protected String includePath;

	protected String libPath;

	protected String libSrcPath;

	protected String libIncludePath;

	protected Map<String, String> mapping;

	protected boolean fifoTrace = false;

	protected boolean maintainHierarchy;

	@Override
	protected void doInitializeOptions() {
		// -- Replacement Map
		Map<String, String> replacementMap = new HashMap<String, String>();
		replacementMap.put("abs", "abs_");
		replacementMap.put("getw", "getw_");
		replacementMap.put("index", "index_");
		replacementMap.put("max", "max_");
		replacementMap.put("min", "min_");
		replacementMap.put("select", "select_");
		replacementMap.put("bitand", "bitand_");
		replacementMap.put("bitor", "bitor_");
		replacementMap.put("not", "not_");
		replacementMap.put("and", "and_");
		replacementMap.put("OUT", "OUT_");
		replacementMap.put("IN", "IN_");
		replacementMap.put("DEBUG", "DEBUG_");
		replacementMap.put("INT_MIN", "INT_MIN_");
		replacementMap.put("INT_MAX", "INT_MAX_");
		replacementMap.put("SHORT_MIN", "SHORT_MIN_");
		replacementMap.put("SHORT_MAX", "SHORT_MAX_");

		maintainHierarchy = getOption(MAINTAIN_HIERARCHY, false);

		if (!maintainHierarchy) {
			networkTransfos.add(new NetworkFlattener());
			networkTransfos.add(new Instantiator(true));
			networkTransfos.add(new UniqueInstance());
		}

		networkTransfos.add(new ConnectionReaders());
		networkTransfos.add(new UnitImporter());
		networkTransfos.add(new DisconnectedOutputPortRemoval());
		networkTransfos.add(new SharedVariableDetection());
		networkTransfos.add(new ConnectionsThresholds());

		mapping = getOption(MAPPING, new HashMap<String, String>());

		if (getOption(STATIC_PARTITIONING, false)) {
			networkTransfos.add(new Partitioner(mapping));
		}
		childrenTransfos.add(new VarInitializer());
		childrenTransfos.add(new TypeResizer(true, false, false, false));
		childrenTransfos.add(new RenameTransformation(replacementMap));

		fifoTrace = getOption(GENERATE_FIFO_TRACES, false);
	}

	@Override
	protected void createDirectories() {
		if (codesignMode) {
			// -- Source path
			srcPath = outputPath + File.separator + "src";
			createDirectory(srcPath);
			// -- Include path
			includePath = outputPath + File.separator + "include";
			createDirectory(includePath);
		} else {
			// -- Source path
			srcPath = outputPath + File.separator + "src";
			createDirectory(srcPath);

			// -- Include path
			includePath = srcPath + File.separator + "include";
			createDirectory(includePath);

			// -- Library path
			libPath = outputPath + File.separator + "lib";
			createDirectory(libPath);

			// -- Library Src path
			libSrcPath = libPath + File.separator + "src";
			createDirectory(libSrcPath);

			// -- Library Include path
			libIncludePath = libPath + File.separator + "include";
			createDirectory(libIncludePath);

			// -- Bin directory path
			String binPath = outputPath + File.separator + "bin";
			createDirectory(binPath);

			// -- Build directory path
			String buildPath = outputPath + File.separator + "build";
			createDirectory(buildPath);
		}
	}

	@Override
	protected void doValidate(Network network) {
		Validator.checkMinimalFifoSize(network, fifoSize);

		new NetworkValidator().doSwitch(network);
	}

	@Override
	protected Result doGenerateNetwork(Network network) {
		network.computeTemplateMaps();
		final Result result = Result.newInstance();
		if (codesignMode) {
			// -- Header
			NetworkHeaderPrinter networkHeaderPrinter = new NetworkHeaderPrinter(network);
			result.merge(FilesManager.writeFile(networkHeaderPrinter.getContent(), includePath, networkHeaderPrinter.getFileName()));
			
			// -- Source
			NetworkSourcePrinter networkSourcePrinter = new NetworkSourcePrinter(network);
			result.merge(FilesManager.writeFile(networkSourcePrinter.getContent(), srcPath, networkSourcePrinter.getFileName()));
		} else {
			CPP11Main printer = new CPP11Main(network, getOptions());
			printer.setOptions(getOptions());
			result.merge(FilesManager.writeFile(printer.getContent(), srcPath, network.getSimpleName() + ".cpp"));
		}
		return result;
	}

	@Override
	protected void beforeTransformations(Network network) {
		new Instantiator(false).doSwitch(network);

		if (fifoTrace) {
			String fifoTracePath = getOption(GENERATE_FIFO_TRACES_FOLDER, "");
			FifoTraceHeader fifoTracePrinter = new FifoTraceHeader(network, fifoTracePath);
			FilesManager.writeFile(fifoTracePrinter.getContent(), libIncludePath, "fifoTrace.h");
		}
	}

	@Override
	protected void beforeGeneration(Network network) {
		// if required, load the buffer size from the mapping file
		if (getOption(IMPORT_BXDF, false)) {
			File f = new File(getOption(BXDF_FILE, ""));
			new XmlBufferSizeConfiguration(true, true).load(f, network);
		}

		if (network.getVertex(network.getSimpleName()) != null) {
			final StringBuilder warnMsg = new StringBuilder();
			warnMsg.append('"').append(network.getSimpleName()).append('"');
			warnMsg.append(" is the name of both the network you want to generate");
			warnMsg.append(" and a vertex in this network.").append('\n');
			warnMsg.append("The 2 entities will be generated");
			warnMsg.append(" in the same file. Please rename one of these elements to prevent");
			warnMsg.append(" unwanted overwriting.");
			OrccLogger.warnln(warnMsg.toString());
		}
	}

	@Override
	protected Result doAdditionalGeneration(Network network) {
		final Result result = Result.newInstance();
		// -- CMake
		result.merge(getCMakePrinters(network));

		if (!codesignMode) {
			final Mapping mapper = new Mapping(network, mapping);
			result.merge(FilesManager.writeFile(mapper.getContentFile(), srcPath, network.getSimpleName() + ".xcf"));

			// -- Exelixi Runtime Headers
			List<ExelixiPrinter> headerPrinters = new ArrayList<ExelixiPrinter>();
			headerPrinters.add(
					new ActorHeader(getOption(GENERATE_FIFO_TRACES, false), getOption(SCHEDULING_MISS_COUNTER, false)));
			headerPrinters.add(new FifoHeader());
			headerPrinters.add(new GetOptHeader());
			headerPrinters.add(new TinyXML2Header());
			headerPrinters.add(new SchedulingMissLogger());
			headerPrinters.add(new MappingParserHeader());
			headerPrinters.add(new FileReadHeader());
			headerPrinters.add(new FileWriteHeader());
			headerPrinters.add(new FileUtilsHeader());
			headerPrinters.add(new NativeHeader());
			headerPrinters.add(new SdlDisplayHeader());

			// -- Print
			headerPrinters.stream().forEach(
					(p -> result.merge(FilesManager.writeFile(p.getContent(), libIncludePath, p.getFileName()))));

			// -- Exelixi Runtime Sources
			List<ExelixiPrinter> srcPrinters = new ArrayList<ExelixiPrinter>();
			srcPrinters.add(new GetOptSource());
			srcPrinters.add(new TinyXML2Source());
			srcPrinters.add(new MappingParserSource());
			srcPrinters.add(new SdlDisplaySource());
			srcPrinters.add(new FileReadSource());
			srcPrinters.add(new FileWriteSource());
			srcPrinters.add(new FileUtilsSource());

			// -- Print
			srcPrinters.stream()
					.forEach((p -> result.merge(FilesManager.writeFile(p.getContent(), libSrcPath, p.getFileName()))));
		}
		return result;
	}

	protected Result getCMakePrinters(Network network) {
		final Result result = Result.newInstance();
		if (codesignMode) {
			CmakeLibPrinter cmakeLibPrinter = new CmakeLibPrinter(network);
			result.merge(
					FilesManager.writeFile(cmakeLibPrinter.getContent(), outputPath, cmakeLibPrinter.getFileName()));
		} else {
			CMakePrinter cmakePrinter = new CMakePrinter(network, getOptions());
			result.merge(FilesManager.writeFile(cmakePrinter.getContentTop(), outputPath, "CMakeLists.txt"));
			result.merge(FilesManager.writeFile(cmakePrinter.getContentLib(), libPath, "CMakeLists.txt"));
			result.merge(FilesManager.writeFile(cmakePrinter.getContentSrc(), srcPath, "CMakeLists.txt"));
		}
		return result;
	}

	@Override
	protected Result doGenerateInstance(Instance instance) {
		final Result result = Result.newInstance();
		// -- Header File
		InstanceHeaderPrinter instancePrinterHeader = new InstanceHeaderPrinter(instance, getOptions());
		instancePrinterHeader.setOptions(getOptions());
		result.merge(FilesManager.writeFile(instancePrinterHeader.getContent(), includePath,
				instance.getSimpleName() + ".h"));

		// -- Source File
		InstanceSourcePrinter instancePrinterSource = new InstanceSourcePrinter(instance, getOptions());
		instancePrinterSource.setOptions(getOptions());
		result.merge(
				FilesManager.writeFile(instancePrinterSource.getContent(), srcPath, instance.getSimpleName() + ".cpp"));

		return result;
	}
}
