/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.backend.templates.actor

import exelixi.cpp.backend.templates.common.ExprAndTypePrinter
import java.text.SimpleDateFormat
import java.util.Date
import java.util.HashSet
import java.util.List
import java.util.Map
import net.sf.orcc.df.Action
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.graph.Edge
import net.sf.orcc.ir.Procedure
import net.sf.orcc.ir.Var
import org.eclipse.emf.common.util.EList

import static exelixi.cpp.backend.constants.Constants.GENERATE_FIFO_TRACES

import static exelixi.cpp.backend.constants.Constants.*

/**
 * 
 * @author Endri Bezati
 */
class InstanceHeaderPrinter extends ExprAndTypePrinter {

	protected Actor actor

	protected Instance instance

	protected boolean roundRobin = false

	protected String instanceName

	protected boolean fifoTrace = false

	protected EList<Edge> incoming

	protected Map<Port, List<Connection>> outgoingPortMap;

	private boolean linkNativeLib;

	private String linkNativeLibHeaders;

	new(Instance instance, Map<String, Object> options) {
		this.instance = instance
		this.actor = instance.getActor
		this.instanceName = instance.simpleName
		this.incoming = instance.incoming
		this.outgoingPortMap = instance.outgoingPortMap

		this.fifoTrace = options.getOrDefault(GENERATE_FIFO_TRACES, false) as Boolean

		linkNativeLib = options.getOrDefault(LINK_NATIVE_LIBRARY, false) as Boolean;
		linkNativeLibHeaders = options.getOrDefault(LINK_NATIVE_LIBRARY_HEADERS, "") as String;
	}

	// -- Get Content For each Top Level
	protected final def getFileHeader() {
		var dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		var date = new Date();
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- Date: «dateFormat.format(date)»
			// ----------------------------------------------------------------------------
		'''
	}

	def getContent() {
		'''
			#ifndef __«instanceName.toUpperCase»_H__
			#define __«instanceName.toUpperCase»_H__
			«getFileHeader»
			«getIncludes»
			«IF !actor.procs.filter(p | p.native && !"print".equals(p.name)).isEmpty()»
				#include "native.h"
			«ENDIF»
			
			«IF linkNativeLib && linkNativeLibHeaders != ""» «nativeLibHeaders» «ENDIF»
			
			«IF actor.hasAttribute("actor_shared_variables")»
				// -- Shared Variables
				«FOR v : actor.getAttribute("actor_shared_variables").objectValue as HashSet<Var>»
					extern «v.type.doSwitch» «v.name»«FOR dim : v.type.dimensions»[«dim»]«ENDFOR»;
				«ENDFOR»
			«ENDIF»
			
			class «instanceName»: public Actor
			{
			public:
				«publicMembers»
				«instanceName»(«FOR param : instance.getActor.parameters SEPARATOR ", "»«param.type.doSwitch»«FOR dim : param.type.dimensions»[«dim»]«ENDFOR» «param.name»«ENDFOR»)
				«FOR param : instance.getActor.parameters BEFORE ":" SEPARATOR "\n,"»«param.name»(«param.name»)«ENDFOR»
				{
				«IF actor.fsm !== null»state_ = state_«actor.fsm.initialState.name»;«ENDIF»
				}
			
				«getPorts»
				«getObjects»
				
				std::string name(){return "«instance.name»";}
				
				void initialize();
			
				bool action_selection(EStatus& status);
				
				«IF fifoTrace»void enable_fifo_traces();«ENDIF»
				
				void getState();
				
			private:
				«privateMembers»
				// -- Procedures Functions
				«actor.procs.filter(p | !p.native).map[compileProcedure].join»
				// -- Actions initialize
				«actor.initializes.map[compileAction].join»
				// -- Actions
				«actor.actions.map[compileAction].join»
				
				«FOR param : actor.parameters SEPARATOR "\n"»«param.varDecl»;«ENDFOR»
				«FOR variable : actor.stateVars SEPARATOR "\n"»«IF !variable.hasAttribute("shared")»«variable.varDecl»«IF variable.initialValue !==null» = «variable.initialValue.doSwitch»«ENDIF»;«ENDIF»«ENDFOR»
				«FOR conn : incoming SEPARATOR "\n"»«(conn as Connection).targetPort.getInputStatus(conn as Connection)»«ENDFOR»
				«FOR port : outgoingPortMap.keySet SEPARATOR "\n"»«port.getOutputStatus(outgoingPortMap.get(port))»«ENDFOR»
				«IF actor.fsm !== null»
				enum states {
					«FOR state : actor.fsm.states SEPARATOR ","»
						state_«state.name»
					«ENDFOR»
				} state_;
				«ENDIF»
				«IF fifoTrace»bool _enable_fifo_traces;«ENDIF»
			};
			#endif // __«instanceName.toUpperCase»_H__
		'''
	}

	def getNativeLibHeaders() {
		'''
			// -- Native lib headers
			«FOR header : linkNativeLibHeaders.split(";")»
				#include "«header.trim()»"
			«ENDFOR»
			
		'''
	}

	protected def getPrivateMembers() {
		'''
		'''
	}

	protected def getPublicMembers() {
		'''
		'''
	}

	def getInputStatus(Port port, Connection connection) {
		'''int status_«port.name»_;'''
	}

	def getOutputStatus(Port port, List<Connection> connections) {
		'''int status_«port.name»_;'''
	}

	protected def getObjects() {
		'''
			«IF fifoTrace»
				FifoTrace* fifoTrace; 
			«ENDIF»
		'''
	}

	protected def getIncludes() {
		'''
			#include <stdint.h>
			#include <iostream>
			#include "actor.h"
			#include "fifo.h"
			«IF fifoTrace»
				#include "fifoTrace.h"
			«ENDIF»
		'''
	}

	def getPorts() {
		val connectedOutput = [Port port|instance.outgoingPortMap.get(port) !== null]
		'''
			«FOR port : instance.incomingPortMap.keySet»
				«IF instance.incomingPortMap.get(port) !== null»
					«port.compilePort()»
				«ENDIF»
			«ENDFOR»			
			
			«FOR port : instance.outgoingPortMap.keySet.filter(connectedOutput)»
				«IF instance.outgoingPortMap.get(port) !== null»
					«port.compilePort()»
				«ENDIF»
			«ENDFOR»
		'''
	}

	def compilePort(
		Port port
	) '''
		Fifo<«port.type.doSwitch»>* port_«port.name»;
	'''

	def compileProcedure(Procedure proc) {
		'''
			«proc.returnType.doSwitch» «proc.name» («FOR param : proc.parameters SEPARATOR ", "»«param.variable.varDecl»«ENDFOR»);
		'''
	}

	def compileAction(Action action) '''
		«action.scheduler.returnType.doSwitch» «action.scheduler.name»();
		
		«action.body.returnType.doSwitch» «action.body.name»();
	'''

}
