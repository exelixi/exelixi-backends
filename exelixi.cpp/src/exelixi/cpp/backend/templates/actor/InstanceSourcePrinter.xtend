/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.backend.templates.actor

import static exelixi.cpp.backend.constants.Constants.GENERATE_FIFO_TRACES
import static exelixi.cpp.backend.constants.Constants.SCHEDULING_MISS_COUNTER

import exelixi.cpp.backend.templates.common.ExprAndTypePrinter
import java.util.List
import java.util.Map
import net.sf.orcc.df.Action
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Connection
import net.sf.orcc.df.FSM
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Pattern
import net.sf.orcc.df.Port
import net.sf.orcc.df.State
import net.sf.orcc.df.Transition
import net.sf.orcc.graph.Edge
import net.sf.orcc.ir.ArgByRef
import net.sf.orcc.ir.ArgByVal
import net.sf.orcc.ir.BlockBasic
import net.sf.orcc.ir.BlockIf
import net.sf.orcc.ir.BlockWhile
import net.sf.orcc.ir.ExprVar
import net.sf.orcc.ir.InstAssign
import net.sf.orcc.ir.InstCall
import net.sf.orcc.ir.InstLoad
import net.sf.orcc.ir.InstReturn
import net.sf.orcc.ir.InstStore
import net.sf.orcc.ir.Procedure
import org.eclipse.emf.common.util.EList

import static net.sf.orcc.util.OrccAttributes.DIRECTIVE_OPTIMIZE_C

class InstanceSourcePrinter extends ExprAndTypePrinter {

	/**
	 * The Actor of the instance to be generated
	 */
	protected Actor actor

	/**
	 * The instance to be generated
	 */
	protected Instance instance

	/**
	 * The Actor Scheduling, round robin or non-preemptive
	 */
	protected boolean roundRobin = false

	/**
	 * The instance name 
	 */
	protected String instanceName

	/**
	 * Enable fifo traces, by default no
	 */
	protected boolean fifoTrace = false

	/**
	 * The list of incoming connections to the instance
	 */
	private EList<Edge> incoming
	
	/**
	 * Log scheduling miss
	 */
	protected boolean logSchedulingMiss;

	/**
	 * The Map of ports and their outgoing  connections
	 */
	private Map<Port, List<Connection>> outgoingPortMap;

	new(Instance instance, Map<String, Object> options) {
		this.instance = instance
		this.actor = instance.getActor
		this.instanceName = instance.simpleName
		this.incoming = instance.incoming
		this.outgoingPortMap = instance.outgoingPortMap

		this.fifoTrace = options.getOrDefault(GENERATE_FIFO_TRACES, false) as Boolean
		this.logSchedulingMiss = options.getOrDefault(SCHEDULING_MISS_COUNTER, false) as Boolean
	}

	
	def getContent(){
		'''
			«classHeader»
			
			«IF !actor.procs.empty»
			// -- Procedures / Functions
			«actor.procs.filter(p | !p.native).map[compileProcedure].join»
			«ENDIF»
			
			«IF !actor.initializes.empty»
			// -- Intialize Actions
			«actor.initializes.map[compileAction].join»
			«ENDIF»
			«compileInitialActions»
			
			«IF fifoTrace»
			void «instance.simpleName»::enable_fifo_traces(){
				this->_enable_fifo_traces = true;
			}
			«ENDIF»
			
			// -- Actions
			«actor.actions.map[compileAction].join»
			
			// -- Action Selection
			«compileActionSelection(actor)»
			
			// -- Final State
			«finalState»
		'''
	}

	///////////////////////////////////////////////////////////////////////////
	// Class header
	
	def getClassHeader(){
		'''
			#include "«instance.simpleName».h"
			«includes»
		'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Includes
	
	protected def getIncludes(){
		''''''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Arguments
	
	def dispatch compileArg(ArgByRef arg) {
		'''&«arg.use.variable.doSwitch»«FOR index : arg.indexes»[«index.doSwitch»]«ENDFOR»'''
	}

	def dispatch compileArg(ArgByVal arg) {
		arg.value.doSwitch
	}
	
	def dispatch compileArgCout(ArgByRef arg) {
		'''&«arg.use.variable.doSwitch»«FOR index : arg.indexes»[«index.doSwitch»]«ENDFOR»'''
	}
	
	def dispatch compileArgCout(ArgByVal arg) {		
		'''«IF arg.value.exprVar»«IF (arg.value as ExprVar).type.uint || (arg.value as ExprVar).type.int»static_cast<int>(«arg.value.doSwitch»)«ELSE»«arg.value.doSwitch»«ENDIF»«ELSE»«arg.value.doSwitch»«ENDIF»'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Procedures / Functions
	
	
	def compileProcedure(Procedure proc) {
		'''
			«proc.returnType.doSwitch» «instance.simpleName»::«proc.name» («FOR param : proc.parameters SEPARATOR ", "»«param.variable.varDecl»«ENDFOR»){
				«proc.doSwitch»
			}
		'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Ports handling
	
	def getReadAddresses(Action action){
		'''
			«FOR e : action.inputPattern.numTokensMap»
				«IF instance.incomingPortMap.get(e.key) !== null»
					«e.key.type.doSwitch»* «e.key.name» = port_«e.key.name»->read_address(«instance.incomingPortMap.get(e.key).getAttribute("fifoId").objectValue»«IF e.value > 1», «e.value»«ENDIF»);
				«ELSE»
					«e.key.type.doSwitch» «e.key.name»[«e.value»];
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	
	def getPeekAddresses(Action action){
		'''
			«FOR e : action.peekPattern.numTokensMap»
				«e.key.type.doSwitch»* «e.key.name» = port_«e.key.name»->read_address(«instance.incomingPortMap.get(e.key).getAttribute("fifoId").objectValue»«IF e.value > 1», «e.value»«ENDIF»);
			«ENDFOR»
		'''
	}
	
	def getWriteAddresses(Action action){
		'''
			«FOR port : action.outputPattern.ports»
				«IF instance.outgoingPortMap.get(port) !== null»
					«port.type.doSwitch»* «port.name» = port_«port.name»->write_address();
				«ELSE»
					«port.type.doSwitch» «port.name»«port.type»«FOR dim:port.type.dimensions»[«dim»]«ENDFOR»;
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	def getReadAdvance(Action action){
		'''
			«FOR e : action.inputPattern.numTokensMap»
				«IF instance.incomingPortMap.get(e.key) !== null»
					port_«e.key.name»->read_advance(«instance.incomingPortMap.get(e.key).getAttribute("fifoId").objectValue»«IF e.value > 1», «e.value»«ENDIF»);
					status_«e.key.name»_ -= «e.value»;
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	def getWriteAdvance(Action action){
		'''
			«FOR e : action.outputPattern.numTokensMap»
				«IF instance.outgoingPortMap.get(e.key) !== null»
					port_«e.key.name»->write_advance(«IF e.value > 1»«e.value»«ENDIF»);
					status_«e.key.name»_ -= «e.value»;
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Actions
	
	
	def compileInitialActions(){
		'''
			void «instance.simpleName»::initialize() {
				«FOR action : actor.initializes»
					«action.name»();
				«ENDFOR»
			}
		'''
	}
	
	def compileAction(Action action) '''
		«action.scheduler.returnType.doSwitch» «instance.simpleName»::«action.scheduler.name»() {
			«getPeekAddresses(action)»
			«action.scheduler.doSwitch»
		}
		
		«action.body.returnType.doSwitch» «instance.simpleName»::«action.body.name»() {
			«IF !action.inputPattern.ports.empty»«getReadAddresses(action)»«ENDIF»
			«IF !action.outputPattern.ports.empty»«getWriteAddresses(action)»«ENDIF»
			«writeTraces(action.inputPattern)»
			«compileAction_beforeBody(action)»
			«action.body.doSwitch»
			«compileAction_afterBody(action)»
			«writeTraces(action.outputPattern)»
			
			«IF !action.inputPattern.ports.empty»«getReadAdvance(action)»«ENDIF»
			«IF !action.outputPattern.ports.empty»«getWriteAdvance(action)»«ENDIF»
		}
		
	'''
	
	///////////////////////////////////////////////////////////////////////////
	// Action Selection
	
	def compileActionSelection(Actor actor)
	'''	
		bool «instance.simpleName»::action_selection(EStatus& status) {	
			«getActionSelectionCount(actor)»
			«getActionSelectionRooms(actor)»
		
			bool res = true;
			executed_once = false;
			
			«IF !roundRobin»while (res) {«ENDIF»
				«IF actor.fsm !== null»
					res = false;
					«FOR action : actor.actionsOutsideFsm BEFORE "if" SEPARATOR "\nelse if"»«action.compileScheduler(null)»«ENDFOR»
					if(!res) {
						switch(state_) {
							«actor.fsm.compilerScheduler»
						}
					}
				«ELSE»
					res = false;
					«FOR action : actor.actions BEFORE "if" SEPARATOR "\nelse if"»«action.compileScheduler(null)»«ENDFOR»
				«ENDIF»
			«IF !roundRobin»}«ENDIF»
			
			«IF logSchedulingMiss»
			scheduling_miss += !executed_once;
			«ENDIF»
			
			return executed_once;
		}
	'''
 

	def getActionSelectionCount(Actor actor){
		'''
			«FOR port : instance.incomingPortMap.keySet»
				«IF instance.incomingPortMap.get(port) !== null»
					status_«port.name»_ = port_«port.name»->count(«instance.incomingPortMap.get(port).getAttribute("fifoId").objectValue»);
				«ENDIF»
			«ENDFOR»
		'''
	}
	
	def getActionSelectionRooms(Actor actor){
		'''
			«FOR port : instance.outgoingPortMap.keySet»
				«IF instance.outgoingPortMap.get(port) !== null»
					status_«port.name»_ = port_«port.name»->rooms();
				«ENDIF»
			«ENDFOR»
		'''
	}

	def compileScheduler(
		Action action,
		State state
	) '''
	(«FOR e : action.inputPattern.numTokensMap»«IF instance.incomingPortMap.get(e.key) !== null»status_«e.key.name»_ >= «e.value» && «ENDIF»«ENDFOR»«action.scheduler.name»()) {
		«IF !action.outputPattern.empty»
		if(«FOR e : action.outputPattern.numTokensMap SEPARATOR " &&" »«IF instance.outgoingPortMap.get(e.key) !== null»status_«e.key.name»_ >= «e.value» «ENDIF»«ENDFOR») {
			«action.body.name»();
			res = true;
			executed_once = true;
			status = hasExecuted;
			«IF state !== null»state_ = state_«state.name»;«ENDIF»
		}
		«ELSE»
			«action.body.name»();
			res = true;
			executed_once = true;
			status = hasExecuted;
			«IF state !== null»state_ = state_«state.name»;«ENDIF»
		«ENDIF»
	}'''

	def compilerScheduler(FSM fsm) '''
		«FOR state : fsm.states»
			case state_«state.name»:
				«FOR edge : state.outgoing BEFORE "if" SEPARATOR "\nelse if"»«(edge as Transition).action.compileScheduler(edge.target as State)»«ENDFOR»
				break;
		«ENDFOR»
	'''
	
	///////////////////////////////////////////////////////////////////////////
	// Visitor Members
	
	// -- Instructions
	
	override caseInstAssign(InstAssign inst) '''
		«inst.target.variable.name» = «inst.value.doSwitch»;
	'''

	override caseInstCall(InstCall inst) {
		if (inst.print) {
			'''
				std::cout << «FOR arg : inst.arguments SEPARATOR " << "»(«arg.compileArgCout»)«ENDFOR»;
			'''
		} else {
			'''
				«IF inst.target !== null»«inst.target.variable.name» = «ENDIF»«inst.procedure.name»(«FOR arg : inst.getArguments SEPARATOR ", "»«arg.compileArg»«ENDFOR»);
			'''
		}
	}
	
	override caseInstLoad(
		InstLoad inst
	) '''
		«inst.target.variable.name» = «inst.source.variable.name»«FOR index : inst.indexes»[«index.doSwitch»]«ENDFOR»;
	'''

	override caseInstReturn(
		InstReturn inst
	) '''
		«IF inst.value !== null»return «inst.value.doSwitch»;«ENDIF»
	'''

	override caseInstStore(
		InstStore inst
	) '''
		«inst.target.variable.name»«FOR index : inst.indexes»[«index.doSwitch»]«ENDFOR» = «inst.value.doSwitch»;
	'''
	
	// -- Blocks
	
	override caseBlockBasic(BlockBasic node) '''
		«FOR inst : node.instructions»«inst.doSwitch»«ENDFOR»
	'''

	override caseBlockIf(BlockIf node) '''
		if(«node.condition.doSwitch») {
			«FOR then : node.thenBlocks»«then.doSwitch»«ENDFOR»	
		}«IF !node.elseBlocks.empty» else {
			«FOR els : node.elseBlocks»«els.doSwitch»«ENDFOR»
		}«ENDIF»
		«node.joinBlock.doSwitch»
	'''

	override caseBlockWhile(BlockWhile node) '''
		while(«node.condition.doSwitch») {
			«FOR whil : node.blocks»«whil.doSwitch»«ENDFOR»
		}
		«node.joinBlock.doSwitch»
	'''

	// -- Procedures
	
	override caseProcedure(Procedure procedure) {
		val isOptimizable = procedure.hasAttribute(DIRECTIVE_OPTIMIZE_C);
		val optCond = procedure.getAttribute(DIRECTIVE_OPTIMIZE_C)?.getValueAsString("condition")
		val optName = procedure.getAttribute(DIRECTIVE_OPTIMIZE_C)?.getValueAsString("name")
		'''
			«IF isOptimizable»
				#if «optCond»
				«optName»(«procedure.parameters.join(", ")[variable.name]»);
				#else
			«ENDIF»
			«FOR variable : procedure.locals SEPARATOR "\n"»«variable.varDeclWithInit»;«ENDFOR»
			«FOR node : procedure.blocks»«node.doSwitch»«ENDFOR»
			«IF isOptimizable»
				#endif // «optCond»
			«ENDIF»
		'''
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// Helper Methods
	
	protected def compileAction_beforeBody(Action action) {
	}

	protected def compileAction_afterBody(Action action) {
	}
	
	def private writeTraces(Pattern pattern) {
		if(!fifoTrace || pattern.ports.empty) return ''''''
		'''
			
		if(_enable_fifo_traces)
		{// FIFO traces
			«FOR port : pattern.ports»	
				for (int i = 0; i < «pattern.getNumTokens(port)»; i++) {
					fprintf(fifoTrace->«instance.simpleName»_«port.name», "%«port.type.printfFormat»\n",«port.name»[i]);
				}
			«ENDFOR»
		}
		'''
	}
	
	def private getFinalState(){
		'''
			void «instance.simpleName»::getState() {
				std::cout << "Actor: «actor.simpleName»";
				«IF actor.hasFsm»
					std::cout <<  ": last-state=";
					switch(state_) {
					«FOR state : actor.fsm.states»
						case state_«state.name»:
							std::cout <<  "«state.name»";
							break;
					«ENDFOR»
					}
				«ELSE»
					std::cout << "has no state";
				«ENDIF»
				
				«IF logSchedulingMiss»
				std::cout << " , scheduling-miss="<< scheduling_miss;
				«ENDIF»
				
				std::cout << std::endl;
			}
		'''
	}
	
}
