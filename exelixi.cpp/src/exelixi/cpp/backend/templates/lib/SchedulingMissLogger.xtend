/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.cpp.backend.templates.lib

class SchedulingMissLogger {
	
	def getContent(){
		'''
		/******************************************************************************
		 *  _____          _ _      _
		 * | ____|_  _____| (_)_  _(_)
		 * |  _| \ \/ / _ \ | \ \/ / |
		 * | |___ >  <  __/ | |>  <| |
		 * |_____/_/\_\___|_|_/_/\_\_|
		 *
		 ******************************************************************************/
		#ifndef __EXELIXI_SW_SCHEDULING_MISS_LOGGER_H__
		#define __EXELIXI_SW_SCHEDULING_MISS_LOGGER_H__
		
		#include <stdlib.h>
		#include <inttypes.h>
		#include <stdint.h>
		#include <sys/types.h>
		#include <sys/stat.h>
		#include <vector>
		#include <string>
		#include <map>
		#include "tinyxml2.h"
		
		using namespace std;
		using namespace tinyxml2;
		
		class SchedulingMissLogger {
		public:
		
			SchedulingMissLogger(string networkName) {
				this->networkName = networkName;
			}
		
			void addSchedulingMiss(string actor, long value) {
				missMap[actor] = value;
			}
		
			void write() {
				// create XML file
				FILE* file = fopen("scheduling_miss.xml", "w");
				XMLPrinter sprinter(file);
				sprinter.OpenElement("scheduling-miss");
				sprinter.PushAttribute("network", networkName.c_str());
		
				char buffer[256];
				for (auto const &ent1 : missMap) {
					sprinter.OpenElement("actor");
					sprinter.PushAttribute("id", ent1.first.c_str());
					sprintf(buffer, "%ld", ent1.second);
					sprinter.PushAttribute("miss", buffer);
					sprinter.CloseElement();
				}
		
				sprinter.CloseElement();
				fclose(file);
			}
		
		private:
		
			string networkName;
			map<string, long> missMap;
		};
		
		#endif // __EXELIXI_SCHEDULING_MISS_LOGGER_H__
		
		'''
	}
	
}