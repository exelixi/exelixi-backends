/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.backend.templates.lib

import java.util.ArrayList
import java.util.HashSet
import java.util.List
import java.util.Set
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.df.Port
import net.sf.orcc.graph.Graph
import net.sf.orcc.graph.Vertex

/**
 * 
 * @author Endri Bezati
 */
class FifoTraceHeader {

	private String path;
	private Network network;
	private List<Instance> instances = new ArrayList;
	private List<Network> subNetworks = new ArrayList;

	new(Network network, String path) {
		this.network = network;
		this.path = path.replaceAll("\\\\", "/");

		addNetworks(network);
		addInstances(network);
	}

	def getContent() {
		'''
			/******************************************************************************
			 *  _____          _ _      _ 
			 * | ____|_  _____| (_)_  _(_)
			 * |  _| \ \/ / _ \ | \ \/ / |
			 * | |___ >  <  __/ | |>  <| |
			 * |_____/_/\_\___|_|_/_/\_\_|
			 *
			 ******************************************************************************/
			 
			#ifndef __EXELIXI_SW_FIFO_TRACE_H__
			#define __EXELIXI_SW_FIFO_TRACE_H__
			
			#include <stdio.h>
			#include "actor.h"
			#include <string>
			#include <vector>
			#include <map>
			#include <set>
			
			#ifdef _WIN32
			#define WIN32_LEAN_AND_MEAN
			#include <windows.h>
			#include <direct.h>
			#endif
			
			class FifoTrace
			{
				public:
				
				FifoTrace(const std::set<std::string> instances) {
					this->instances = instances;
					«FOR instance : instances»  
						«FOR p : getAllPorts(instance)»
							«instancePortFileId(instance, p)» = NULL;
						«ENDFOR»
					«ENDFOR»
				}
				
				void enableTraces(std::vector<Actor*> actors) {
					for (auto instance : actors) {
						instance->enable_fifo_traces();
					}
				};
				
				void open(){
					«FOR instance : instances»
						auto «flatenInstanceName(instance).toLowerCase»_search = this->instances.find("«flatenInstanceName(instance)»");
						if («flatenInstanceName(instance).toLowerCase»_search != this->instances.end()) {
							// create the directory for «instance.simpleName» 
							create_directory("«instancePath(instance)»");
							«FOR p : getAllPorts(instance)»
								«instancePortFileId(instance, p)» = fopen("«instancePortFilePath(instance, p)»","w");
							«ENDFOR»
						}
					«ENDFOR»
				}
				
				#ifdef _WIN32 
				int mkpath(std::string s)
				{
					size_t pre = 0, pos;
					std::string dir;
					int mdret;
				
					if (s[s.size() - 1] != '/') {
						// force trailing / so we can handle everything in loop
						s += '/';
					}
				
					while ((pos = s.find_first_of('/', pre)) != std::string::npos) {
						dir = s.substr(0, pos++);
						pre = pos;
						if (dir.size() == 0) continue; // if leading / first time is 0 length
						if ((mdret = _mkdir(dir.c_str())) && errno != EEXIST) {
							return mdret;
						}
					}
					return mdret;
				}
				#endif

				void close(){
					«FOR instance : instances»  
						auto «flatenInstanceName(instance).toLowerCase»_search = this->instances.find("«flatenInstanceName(instance)»");
						if («flatenInstanceName(instance).toLowerCase»_search != this->instances.end()) {
							«FOR p : getAllPorts(instance)»
								fclose(«instancePortFileId(instance, p)»);
							«ENDFOR»
						}
					«ENDFOR»
					
					// copy network ports files
					«FOR network : subNetworks.reverse»
						«FOR ine : network.inputs»
							«IF !ine.connecting.isEmpty»
								«val con = ine.connecting.get(0) as Connection»
								«val tgt = con.target»
								«IF tgt instanceof Instance»
									copy_file("«instancePortFilePath(tgt as Instance, con.targetPort)»","«networkPortFilePath(network, ine)»");
								«ELSEIF tgt instanceof Network»
									copy_file("«networkPortFilePath(tgt as Network, con.targetPort)»","«networkPortFilePath(network, ine)»");
								«ENDIF»	
							«ENDIF»
						«ENDFOR»
						«FOR oute : network.outputs»
							«IF !oute.connecting.isEmpty»
								«val con = oute.connecting.get(0) as Connection»
								«val src = con.source»
								«IF src instanceof Instance»
									copy_file("«instancePortFilePath(src as Instance, con.sourcePort)»","«networkPortFilePath(network, oute)»");
								«ELSEIF src instanceof Network»
									copy_file("«networkPortFilePath(src as Network, con.sourcePort)»","«networkPortFilePath(network, oute)»");	
								«ENDIF»	
							«ENDIF»
						«ENDFOR»
					«ENDFOR»
				}
				
				«FOR instance : instances» 
					«FOR p : getAllPorts(instance)»
						FILE *«instancePortFileId(instance, p)»;
					«ENDFOR»
				«ENDFOR»
				
				private:
				
				const std::set<std::string> instances;
				
				int copy_file(const std::string src, const std::string tgt){
					  FILE *p,*q;
					  char ch;
					
					  p=fopen(src.c_str(),"r");
					  if(p==NULL){
					      printf("cannot open %s",src.c_str());
					      return 1;
					  }
					
					  q=fopen(tgt.c_str(),"w");
					  if(q==NULL){
					      printf("cannot open %s",tgt.c_str());
					      return 1;
					  }
					  
					  while((ch=getc(p))!=EOF){
					      putc(ch,q);
					  }
					      
					  fclose(p);
					  fclose(q);
					  
					 return 0;
				}
				void create_directory(const std::string path) {
				#ifdef _WIN32 
					mkpath(path);
				#else
					std::string cmd = "mkdir -p " + path;
					system(cmd.c_str());
				#endif
				}
			};
			
			#endif // __EXELIXI_SW_FIFO_TRACE_H__
		'''
	}

	def private void addInstances(Vertex v) {
		if (v instanceof Instance) {
			instances.add(v as Instance);
		} else if (v instanceof Network) {
			for (sv : v.vertices) {
				addInstances(sv);
			}
		}
	}

	def private void addNetworks(Vertex v) {
		if (v instanceof Network) {
			subNetworks.add(v as Network);
			for (sv : v.vertices) {
				addNetworks(sv);
			}
		}
	}

	private def String instancePortFileId(Instance instance, Port port) {
		var String name = "";
		for (s : instance.hierarchicalId.subList(1, instance.hierarchicalId.size)) {
			name += (s + "_");
		}
		return name + port.name;
	}
	
	private def String flatenInstanceName(Instance instance) {
		var String name = "";
		val List<String> substrings = instance.hierarchicalId.subList(1, instance.hierarchicalId.size) 
		
		for (i : 0 .. substrings.size - 1) {
			if(i == substrings.size - 1){
				name += substrings.get(i)
			}else{
				name += substrings.get(i) + "_"
			}
		}
		
		return name;
	}
	
	

	private def String instancePortFilePath(Instance instance, Port port) {
		return instancePath(instance) + port.name + ".txt";
	}

	private def String networkPortFilePath(Network network, Port port) {
		return networkPath(network) + port.name + ".txt";
	}

	private def Set<Port> getAllPorts(Instance instance) {
		var Set<Port> ports = new HashSet;
		ports.addAll(instance.getActor.inputs);
		ports.addAll(instance.getActor.outputs);
		return ports;

	}

	private def String networkPath(Network network) {
		var dir = path + "/";
		for (Graph sp : network.hierarchy.subList(1, network.hierarchy.size)) {
			dir += ((sp as Network).name + "/");
		}
		dir += network.name + "/";
		return dir;
	}

	private def String instancePath(Instance instance) {
		var dir = path + "/";
		for (String sp : instance.hierarchicalId.subList(1, instance.hierarchicalId.size)) {
			dir += (sp + "/");
		}
		return dir;
	}

}
