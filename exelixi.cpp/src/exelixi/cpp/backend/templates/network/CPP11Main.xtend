/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.cpp.backend.templates.network

import static exelixi.cpp.backend.constants.Constants.SCHEDULING_MISS_COUNTER
import exelixi.cpp.backend.templates.common.ExprAndTypePrinter
import java.text.SimpleDateFormat
import java.util.Date
import java.util.HashMap
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Set
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Network
import net.sf.orcc.ir.Var
import net.sf.orcc.util.FilesManager

import static exelixi.cpp.backend.constants.Constants.*
import net.sf.orcc.graph.visit.ReversePostOrder

/**
 *  A CPP 11 backend for Orcc 
 * @author Endri Bezati
 */
class CPP11Main extends ExprAndTypePrinter {

	private Set<Network> partitions;

	protected Network network

	private Map<String, Set<Actor>> networkPartition;

	private boolean definedMapping

	private boolean lockFreeFifo;

	private boolean noTimer;

	private boolean fifoTrace;
	
	private boolean logSchedulingMiss;
	
	private boolean linkNativeLib;
	
	private String linkNativeLibHeaders;

	new(Set<Network> partitions) {
		this.partitions = partitions;
	}

	new(Network network, Map<String, Object> options) {
		this.network = network;
		definedMapping = false;
		lockFreeFifo = false;
		noTimer = false;

		fifoTrace = options.getOrDefault(GENERATE_FIFO_TRACES, false) as Boolean;
		linkNativeLib = options.getOrDefault(LINK_NATIVE_LIBRARY, false) as Boolean;
		linkNativeLibHeaders =	options.getOrDefault(LINK_NATIVE_LIBRARY_HEADERS,"") as String;
		logSchedulingMiss = options.getOrDefault(SCHEDULING_MISS_COUNTER, false) as Boolean
	}

	def printMainMultiNetwork(String targetFolder) {
		val content = getFileContentMultiNetwork
		FilesManager.writeFile(content, targetFolder, "main.cpp")
	}

	def printMain(String targetFolder) {
		val content = getContent
		FilesManager.writeFile(content, targetFolder, "main.cpp")
	}

	def getFileContentMultiNetwork() {
		'''
			«getFileHeader("Exelixi CPP Code Generation")»
			
			«getHeaders»
			
			«getChrono»
			
			«getPartitionsMultiNetwork»
			
			«getMainMultiNetwork»
		'''
	}

	def getContent() {
		'''
			«getFileHeader("Exelixi CPP Code Generation")»
			
			«getHeaders»
			
			«getChrono»
			
			«getActorsAndFifos»
			
			«IF definedMapping»
				«getFixedPartitions»
			«ELSE»
				«getPartitions»
			«ENDIF»
			«getMain»
		'''
	}

	def getFileHeader(String comment) {
		var dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		var date = new Date();
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- Exelixi Design Systems 
			// -- All rights reserved, 2015 
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- «comment»
			// -- Date: «dateFormat.format(date)»
			// ----------------------------------------------------------------------------
		'''
	}

	def getHeaders() {
		'''
			// -- STD includes
			#include <stdint.h>
			#include <thread>
			#include <chrono>
			#include <map>
			#include <set>
			#include <vector>
			#include <iostream>
			#include <string>
			#include <functional>
			
			// -- Backend includes
			#include "mapping_parser.h"
			#include "get_opt.h"
			#include "actor.h"
			#include "fifo.h"
			«IF logSchedulingMiss»#include "scheduling_miss_logger.h"«ENDIF»
			«IF linkNativeLib && linkNativeLibHeaders != ""» «nativeLibHeaders» «ENDIF»
			
			// -- Actors Headers
			«FOR instance : network.children.filter(typeof(Instance))»
				#include "«instance.simpleName».h"
			«ENDFOR»
			
			«IF fifoTrace»
				#include "fifoTrace.h"
			«ENDIF»
		'''
	}
	
	def getNativeLibHeaders() {
		'''
			// -- Native lib headers
			«FOR header : linkNativeLibHeaders.split(";")»
				#include "«header.trim()»"
			«ENDFOR»
			
		'''
	}

	def getChrono() {
		'''
			// -- Chrono
			using Clock = std::chrono::high_resolution_clock;
			using Ms = std::chrono::milliseconds;
			template<class Duration>
			using TimePoint = std::chrono::time_point<Clock, Duration>;
		'''
	}

	def getPartitionsMultiNetwork() {
		'''
			«FOR partition : partitions SEPARATOR "\n"»
				// -- Partition «partition.simpleName»
				// -- Actors
				«FOR instance : partition.children.filter(typeof(Instance))»
					«instance.simpleName» *act_«instance.simpleName»;
				«ENDFOR»
			«ENDFOR»
			
			// -- Partitions Prototype
			«FOR partition : partitions»
				void «partition.simpleName»();
			«ENDFOR»
			
			// -- Partitions
			«FOR partition : partitions SEPARATOR "\n"»
				void «partition.simpleName»(){
					// -- Run 
					EStatus status = None;
					Clock::time_point _start = Clock::now();
					bool stop = false;
					while(1){
					//do{
						status = None;
						«FOR instance : partition.children.filter(typeof(Instance))»
							act_«instance.simpleName»->action_selection(status);
						«ENDFOR»
						if(status == None){
							stop = TimePoint<Ms>(std::chrono::duration_cast<Ms>(Clock::now() - _start)) > TimePoint<Ms>(Ms(1000));
							if(stop){
								std::cout << "Time out occurred on partition «partition.simpleName» !" << std::endl;
							}
						}else{
							_start = Clock::now();
						}
					} //while (!stop);
				}
			«ENDFOR»
		'''
	}

	def getActorsAndFifos() {
		'''
			«FOR instance : network.children.filter(typeof(Instance))»
				«instance.simpleName» *act_«instance.simpleName»;
			«ENDFOR»
			
			// -- FIFOs
			«IF lockFreeFifo»
				«FOR conn : network.connections»
					Fifo<«conn.sourcePort.type.doSwitch»> *fifo_«conn.getAttribute("fifoName").stringValue»;
				«ENDFOR»
			«ELSE»
				«FOR instance : network.children.filter(typeof(Instance))»
					«FOR edges : instance.outgoingPortMap.values»
						Fifo<«edges.get(0).sourcePort.type.doSwitch»> *fifo_«edges.get(0).getAttribute("idNoBcast").objectValue»;
					«ENDFOR»
				«ENDFOR»
			«ENDIF»
			
			«IF fifoTrace»
				// -- FIFO TRACE
				FifoTrace *fifoTrace;
			«ENDIF»
		'''
	}

	// TODO: Fix actor mapping
	def getFixedPartitions() {
		networkPartition = new HashMap<String, Set<Actor>>()
		for (Actor actor : network.children.filter(typeof(Actor))) {
			if (actor.hasAttribute("partition")) {
				var String partition = actor.getAttribute("partition").getStringValue() as String
				if (networkPartition.containsKey(partition)) {
					var Set<Actor> actors = networkPartition.get(partition)
					actors.add(actor)
				} else {
					var Set<Actor> actors = new HashSet<Actor>
					actors.add(actor)
					networkPartition.put(partition, actors)
				}
			}
		}

		'''
			// -- Partitions Prototype
			«FOR partition : networkPartition.keySet»
				void «partition»();
			«ENDFOR»
			
			// -- Partitions
			«FOR partition : networkPartition.keySet SEPARATOR "\n"»
				void «partition»(){
					// -- Run 
					EStatus status = None;
					«IF !noTimer»
						Clock::time_point _start = Clock::now();
					«ENDIF»
					bool stop = false;
					
					«FOR actor : networkPartition.get(partition)»
						act_«actor.name»->initialize();
					«ENDFOR»
					
					«IF !noTimer»
						do{
					«ELSE»
						while(1){
					«ENDIF»
					status = None;
					«FOR actor : networkPartition.get(partition)»
						act_«actor.name»->action_selection(status);
					«ENDFOR»
					«IF !noTimer»
						if(status == None){
							std::this_thread::yield();
							stop = TimePoint<Ms>(std::chrono::duration_cast<Ms>(Clock::now() - _start)) > TimePoint<Ms>(Ms(1000));
							if(stop){
								std::cout << "Time out occurred on partition «partition» !" << std::endl;
							}
						}else{
							_start = Clock::now();
						}
					«ENDIF»
					«IF !noTimer»
						} while (!stop);
					«ELSE»
						}
					«ENDIF»
				}
			«ENDFOR»
		'''
	}

	def getPartitions() {
		val instances = new ReversePostOrder(network).filter(typeof(Instance))
		'''
			// -- Partitions Prototype
			void partition_monocore();
			void partition(std::string name, std::vector<Actor*> actors);
			
			void partition_monocore(){
				«FOR instance : instances»
				act_«instance.simpleName»->initialize();
				«ENDFOR»
				
				bool run = false;
				EStatus status = None;
				do{
						run = false;
						«FOR instance : instances»
						run |= act_«instance.simpleName»->action_selection(status);
						«ENDFOR»
						
				}while(run);	
			}
			
			void partition(std::string name, std::vector<Actor*> actors) {
				EStatus status = None;
				Clock::time_point _start = Clock::now();
				bool stop = false;
				
				for(Actor* actor : actors){
					actor->initialize();
				}
			
				do {
					status = None;
					for(Actor* actor : actors){
						actor->action_selection(status);
					}
					if (status == None) {
						stop = TimePoint<Ms>(std::chrono::duration_cast < Ms > (Clock::now() - _start)) > TimePoint<Ms>(Ms(1000));
						std::this_thread::yield();
						if (stop) {
							std::cout << "Time out occurred on partition " << name << "!" << std::endl;
						}
					} else {
						_start = Clock::now();
					}
				} while (!stop);
			}
		'''
	}

	def getMainMultiNetwork() {
		'''
			«FOR partition : partitions»
				«IF partition.hasAttribute("network_shared_variables")»
					// -- Shared Variables
					«FOR v : partition.getAttribute("network_shared_variables").objectValue as HashSet<Var>»
						«v.type.doSwitch» «v.name»«FOR dim : v.type.dimensions»[«dim»]«ENDFOR»;
					«ENDFOR»
				«ENDIF»
			«ENDFOR»
			
			
			int main(int argc, char *argv[]){
				// -- Instantiate Actors and Fifos
				«FOR partition : partitions»
					«FOR instance : partition.children.filter(typeof(Instance))»
						act_«instance.simpleName» = new «instance.simpleName»(); // «partition.simpleName»
					«ENDFOR»
				«ENDFOR»
			
				// -- Launch Partitions «var i = 0»
				std::thread p[«partitions.size»];
				«FOR partition : partitions»
					p[«i»] = std::thread(«partition.simpleName»);«{i++;""}»
				«ENDFOR»
			
				// -- Join the partition threads with the main thread
				for(int i=0; i < «partitions.size»; i++){
					p[i].join();
				}
			
				return 0;
			}
			//EOF
		'''
	}

	def getMain() {
		var instances = new ReversePostOrder(network).filter(typeof(Instance))
		'''
			«IF network.hasAttribute("network_shared_variables")»
				// -- Shared Variables
					«FOR v : network.getAttribute("network_shared_variables").objectValue as HashSet<Var>»
						«v.type.doSwitch» «v.name»«FOR dim : v.type.dimensions»[«dim»]«ENDFOR»;
					«ENDFOR»
			«ENDIF»
			
			int main(int argc, char *argv[]){
				«instantiateActorsFifos»
				«connectActorsFifos»

				// -- Actors Map
				std::vector<Actor*> actors;
				«FOR instance : instances»
					actors.push_back(act_«instance.simpleName»);
				«ENDFOR»
				
				// -- Get Options
				GetOpt options = GetOpt(argc, argv);
				std::string application_name = "«network.simpleName»";
				options.setActors(actors);
				options.getOptions();
			
				«IF fifoTrace»
					// -- Connect FIFO trace to each instance
					fifoTrace = new FifoTrace(options.get_trace_instance_name());
					fifoTrace->enableTraces(actors);
					fifoTrace->open();
					«FOR instance : network.children.filter(typeof(Instance))»
						act_«instance.simpleName»->fifoTrace = fifoTrace;
					«ENDFOR»
				«ENDIF»
			
				// -- Partitions
				std::map<std::string, std::vector<Actor*>> partitions;
				MappingParser parser(config_file,actors);
				partitions = parser.getPartitions();	
			
				«IF definedMapping»
					// -- Launch Partitions «var i = 0»
					std::thread p[«networkPartition.keySet.size»];
					«FOR partition : networkPartition.keySet»
						p[«i»] = std::thread(«partition»);«{i++;""}»
					«ENDFOR»
					
					// -- Join the partition threads with the main thread
					for(int i = 0; i < «networkPartition.keySet.size»; i++){
						p[i].join();
					}
				«ELSE»
					int partitions_size = partitions.size();
					if(partitions_size > 1){
						std::vector<std::thread> workers(partitions_size);
						
						int i = 0;
						for (std::map<std::string, std::vector<Actor*>>::iterator it=partitions.begin(); it!=partitions.end(); ++it){
							workers[i] = std::thread( partition, it->first, it->second );
							i++;
						}
						
						// -- Join the partition threads with the main thread
						for (int i = 0; i < partitions_size; i++) {
							workers[i].join();
						}
					}else{
						partition_monocore();
					}
				«ENDIF»
				
				«IF logSchedulingMiss»
				// Log Scheduling Miss
				SchedulingMissLogger missLogger = SchedulingMissLogger("«network.name»");
				
				for (Actor* actor : actors) {
					missLogger.addSchedulingMiss(actor->name(), actor->get_scheduling_miss());
				}
				
				missLogger.write();
				
				«ENDIF»
				
				«IF lockFreeFifo»
					«FOR conn : network.connections»
						if( fifo_«conn.getAttribute("fifoName").stringValue»->count() > 0){
							std::cout << "!!! FIFO: fifo_«conn.getAttribute("fifoName").stringValue» is not Empty, Remaining Elements:" << fifo_«conn.getAttribute("fifoName").stringValue»->count() << std::endl;
						}
					«ENDFOR»
				«ENDIF»
				
				«IF fifoTrace»
					fifoTrace->close();
					delete fifoTrace;
				«ENDIF»
				
				«FOR instance : instances»
					act_«instance.simpleName»->getState();
				«ENDFOR»
				
				«terminate»
				
				«FOR instance : instances»
					delete act_«instance.simpleName»;
				«ENDFOR»
				
				«IF lockFreeFifo»
					«FOR conn : network.connections»
						delete fifo_«conn.getAttribute("fifoName").stringValue»;
					«ENDFOR»
				«ELSE»
					«FOR instance : instances»
						«FOR edges : instance.outgoingPortMap.values»
							delete fifo_«edges.get(0).getAttribute("idNoBcast").objectValue»;
						«ENDFOR»
					«ENDFOR»
				«ENDIF»
				
				«delete»
				
				return 0;
			}
			//EOF
		'''
	}
	
	protected def terminate() {
	}
	
	protected def delete() {
	}

	def instantiateActorsFifos() {
		'''
			// -- Instantiate Actors
			«FOR instance : network.children.filter(typeof(Instance))»
				«instatiateActor(instance)»
			«ENDFOR»
			
			
			// -- Instantiate Fifos
			«IF lockFreeFifo»
				«FOR conn : network.connections»
					fifo_«conn.getAttribute("fifoName").stringValue» = new Fifo<«conn.sourcePort.type.doSwitch»>(«getSize(conn)», «getSize(conn)»);
				«ENDFOR»
			«ELSE»
				«FOR instance : network.children.filter(typeof(Instance))»
					«FOR edges : instance.outgoingPortMap.values»
						«val conn = edges.get(0)»
						fifo_«conn.getAttribute("idNoBcast").objectValue» = new Fifo<«conn.sourcePort.type.doSwitch»>(«getSize(conn)», «conn.getAttribute("threshold").objectValue», «conn.getAttribute("nbReaders").objectValue»);
					«ENDFOR»
				«ENDFOR»
			«ENDIF»
		'''
	}
	
	def int getSize(Connection connection){
		if(connection.size !== null){
			return connection.size;
		}else{
			return fifoSize;
		}
	}
	
	def instatiateActor(Instance instance){
		val List<Var> parameters = instance.getActor.parameters
		'''
			act_«instance.simpleName» = new «instance.simpleName»(«FOR parameter : parameters SEPARATOR ", "»«IF instance.getArgument(parameter.name) !== null»«instance.getArgument(parameter.name).value.doSwitch»«ELSE»«parameter.initialValue.doSwitch»«ENDIF»«ENDFOR»);
		'''
	}

	def connectActorsFifos() {
		'''
				// -- Connect Actors - Fifo Queues
				«IF lockFreeFifo»
					«FOR instance : network.children.filter(typeof(Instance))»
						«FOR port : instance.getActor.inputs»
							act_«instance.simpleName»->fifo_«instance.incomingPortMap.get(port).getAttribute("fifoName").stringValue» = fifo_«instance.incomingPortMap.get(port).getAttribute("fifoName").stringValue»;
						«ENDFOR»
						«FOR port : instance.getActor.outputs»
							«IF instance.outgoingPortMap.get(port) !== null »
								«FOR conn : instance.outgoingPortMap.get(port)»
									act_«instance.simpleName»->fifo_«conn.getAttribute("fifoName").stringValue» = fifo_«conn.getAttribute("fifoName").stringValue»;
								«ENDFOR»
							«ENDIF»
						«ENDFOR»
					«ENDFOR»
				«ELSE»
					«FOR e : network.connections»
						«IF e.source !== null && e.sourcePort !== null»
							act_«(e.source as Instance).name»->port_«e.sourcePort.name» = fifo_«e.getAttribute("idNoBcast").objectValue»;
						«ENDIF»
						«IF e.target !== null && e.targetPort !== null»
							act_«(e.target as Instance).name»->port_«e.targetPort.name» = fifo_«e.getAttribute("idNoBcast").objectValue»;
						«ENDIF»
					«ENDFOR»
				«ENDIF»
		'''
	}

}
