/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.cpp.backend.templates.network

import exelixi.core.backends.templates.network.NetworkSource
import net.sf.orcc.df.Network
import net.sf.orcc.df.Instance

class NetworkSourcePrinter extends NetworkSource {
	
	new(Network network) {
		super(network)
	}
	
	override getIncludes(){
		'''
			#include <thread>
			#include <chrono>
			#include "«network.simpleName».h"
		'''
	}
	
	override getBody(){
		'''
			// -- Chrono
			using Clock = std::chrono::high_resolution_clock;
			using Ms = std::chrono::milliseconds;
			template<class Duration>
			using TimePoint = std::chrono::time_point<Clock, Duration>;
			
			void «network.simpleName»::initialize() {
				«FOR instance : network.children.filter(typeof(Instance))»
					act_«instance.simpleName»->initialize();
				«ENDFOR»
			}
			
			void «network.simpleName»::run() {
			
				EStatus status;
				Clock::time_point _start = Clock::now();
				bool stop = false;
				do {
					«FOR instance : network.children.filter(typeof(Instance))»
						act_«instance.simpleName»->action_selection(status);
					«ENDFOR»
					status = None;
					if (status == None) {
						stop = TimePoint<Ms>(std::chrono::duration_cast<Ms>(Clock::now() - _start)) > TimePoint<Ms>(Ms(1000));
						std::this_thread::yield();
						if (stop) {
							std::cout << "Time out occurred on partition " << name << "!" << std::endl;
						}
					} else {
						_start = Clock::now();
					}
				} while (!stop);
			}
		'''
	}
	
}