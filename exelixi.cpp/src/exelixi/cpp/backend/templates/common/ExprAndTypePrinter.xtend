/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.backend.templates.common

import net.sf.orcc.backends.CommonPrinter
import net.sf.orcc.ir.ExprBinary
import net.sf.orcc.ir.ExprBool
import net.sf.orcc.ir.ExprFloat
import net.sf.orcc.ir.ExprInt
import net.sf.orcc.ir.ExprList
import net.sf.orcc.ir.ExprString
import net.sf.orcc.ir.ExprUnary
import net.sf.orcc.ir.ExprVar
import net.sf.orcc.ir.Expression
import net.sf.orcc.ir.OpBinary
import net.sf.orcc.ir.Type
import net.sf.orcc.ir.TypeBool
import net.sf.orcc.ir.TypeFloat
import net.sf.orcc.ir.TypeInt
import net.sf.orcc.ir.TypeList
import net.sf.orcc.ir.TypeString
import net.sf.orcc.ir.TypeUint
import net.sf.orcc.ir.TypeVoid
import net.sf.orcc.util.util.EcoreHelper
import net.sf.orcc.ir.Var

/**
 * 
 * @author Endri Bezati
 */
class ExprAndTypePrinter extends CommonPrinter {
		
	override caseExprBinary(ExprBinary expr) {
		val op = expr.op
		val container = EcoreHelper.getContainerOfType(expr, typeof(Expression))
		var nextPrec = if (op == OpBinary::SHIFT_LEFT || op == OpBinary::SHIFT_RIGHT) {

				// special case, for shifts always put parentheses because compilers
				// often issue warnings
				Integer::MIN_VALUE;
			} else {
				op.precedence;
			}

		val resultingExpr = '''«expr.e1.printExpr(nextPrec, 0)» «op.stringRepresentation» «expr.e2.printExpr(nextPrec, 1)»'''

		if (op.needsParentheses(precedence, branch) || (container !== null && op.logical)) {
			'''(«resultingExpr»)'''
		} else {
			resultingExpr
		}
	}

	override caseExprBool(ExprBool expr) '''«IF expr.value»true«ELSE»false«ENDIF»'''
	
	override caseExprFloat(ExprFloat expr) '''«expr.value»'''

	override caseExprInt(ExprInt expr) '''«expr.value»«IF expr.long»LL«ENDIF»'''

	override caseExprList(ExprList expr) 
		'''{«FOR value: expr.value SEPARATOR ","» «value.doSwitch»«ENDFOR»}'''

	override caseExprString(ExprString expr) '''"«expr.value»"'''

	override caseExprUnary(ExprUnary expr) '''«expr.op.text»(«expr.expr.doSwitch»)'''

	override caseExprVar(ExprVar expr) '''«expr.use.variable.name»'''
	
	override caseTypeBool(TypeBool type)  '''bool'''
	
	override caseTypeFloat(TypeFloat type)  '''float'''
	
	override caseTypeInt(TypeInt type) {
		printInt(type.size)
	}

	override caseTypeList(TypeList type) {
		type.type.doSwitch
	}

	override caseTypeString(TypeString type)  '''std::string'''

	override caseTypeUint(TypeUint type) {
		"u" + printInt(type.size);
	}

	override caseTypeVoid(TypeVoid type) {
		"void";
	}
	
	def private printInt(int size) {
		if (size <= 8) {
			return "int8_t";
		} else if (size <= 16) {
			return "int16_t";
		} else if (size <= 32) {
			return "int32_t";
		} else if (size <= 64) {
			return "int64_t";
		} else {
			return null;
		}
	}
	
	override protected stringRepresentation(OpBinary op) {
		if (op == OpBinary::DIV_INT)
			"/"
		else
			super.stringRepresentation(op)
	}
	
	/**
	  * Print for a type, the corresponding formatted text to
	  * use inside a printf() call.
	  * @param type the type to print
	  * @return printf() type format
	  */
	def protected printfFormat(Type type) {
		switch type {
			case type.bool: "i"
			case type.float: "f"
			case type.int && (type as TypeInt).long: "lli"
			case type.int: "i"
			case type.uint && (type as TypeUint).long: "llu"
			case type.uint: "u"
			case type.list: "p"
			case type.string: "s"
			case type.void: "p"
		}
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Variables
	
	def protected varDecl(Var v) {
		'''«v.type.doSwitch» «v.name»«FOR dim : v.type.dimensions»[«dim»]«ENDFOR»'''
	}

	def protected varDeclWithInit(Var v) {
		'''«v.type.doSwitch» «v.name»«FOR dim : v.type.dimensions»[«dim»]«ENDFOR»«IF v.initialValue !== null» = «v.initialValue.doSwitch»«ENDIF»'''
	}
}
