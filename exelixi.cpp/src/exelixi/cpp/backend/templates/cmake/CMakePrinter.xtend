/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.backend.templates.cmake

import net.sf.orcc.df.Network
import java.util.Map
import static exelixi.cpp.backend.constants.Constants.*
import net.sf.orcc.df.Instance

/**
 * 
 * @author Endri Bezati
 */
class CMakePrinter {

	Network network;

	boolean linkNativeLib;
	String linkNativeLibFolder;

	new(Network network, Map<String, Object> options) {
		this.network = network

		linkNativeLib = options.getOrDefault(LINK_NATIVE_LIBRARY, false) as Boolean;
		linkNativeLibFolder = options.getOrDefault(LINK_NATIVE_LIBRARY_FOLDER, "") as String;

		if (linkNativeLib && linkNativeLibFolder != "")
			linkNativeLib = true
		else
			linkNativeLib = false
	}

	def getContentTop() {
		'''
			cmake_minimum_required (VERSION 2.6)
			
			project («network.simpleName»)
			
			set(extra_definitions)
			option(NO_DISPLAY "Disable SDL display." 0)
			if(NO_DISPLAY)
				list(APPEND extra_definitions -DNO_DISPLAY)
			else()
				find_package(SDL REQUIRED)
			endif()
			
			option(INTEL_CC "Use the Intel compiler." OFF) 
			
			if (INTEL_CC) 
			  find_program(CMAKE_C_COMPILER NAMES icc) 
			  find_program(CMAKE_CXX_COMPILER NAMES icpc) 
			  find_program(CMAKE_AR NAMES xiar) 
			  find_program(CMAKE_LINKER NAMES xild) 
			
			  if (CMAKE_C_COMPILER MATCHES CMAKE_C_COMPILER-NOTFOUND OR 
			      CMAKE_CXX_COMPILER MATCHES CMAKE_CXX_COMPILER-NOTFOUND OR 
			      CMAKE_AR MATCHES CMAKE_AR-NOTFOUND OR 
			      CMAKE_LINKER MATCHES CMAKE_LINKER-NOTFOUND) 
			    message(FATAL_ERROR "Cannot find Intel compiler. In Linux you may need to run eg. `. /opt/intel/bin/compilervars.sh intel64'") 
			  endif () 
			endif (INTEL_CC) 
			
			«IF linkNativeLib»
				set(external_definitions)
				set(external_include_paths)
				set(external_library_paths)
				set(external_libraries)
				
				# All external vars should be set by the CMakeLists.txt inside the following folder.
				add_subdirectory(«linkNativeLibFolder» «linkNativeLibFolder»)
			«ENDIF»
			
			# Requiered Packages
			find_package(Threads REQUIRED)
			
			# Place the executable on bin directory
			set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)
			
			if(CMAKE_COMPILER_IS_GNUCXX)
				set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g")
				set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")
				set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -std=c++11 -fpermissive -Wno-narrowing -w")
			elseif(CMAKE_CXX_COMPILER_ID STREQUAL Intel)
				set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g")
				set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")
				set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -std=c++11 -fpermissive -Wno-narrowing -w")
			endif()	
			
			«IF linkNativeLib»
				
				if(external_definitions)
					list(APPEND extra_definitions ${external_definitions})
				endif()
				add_definitions(${extra_definitions})
				
				if(external_library_paths)
					link_directories(${external_library_paths})
				endif()
			«ENDIF»
			
			include_directories(
				lib/include
				«IF linkNativeLib»
					${external_include_paths}
				«ENDIF»
			)
			
			# Compile Exelixi runtime
			add_subdirectory(lib)
			
			# Compiler dataflow CPP application
			add_subdirectory(src)
		'''
	}

	def getContentSrc() {
		'''
			# Generated from «network.simpleName»
			
			set(filenames
				«FOR instance : network.children.filter(typeof(Instance))»
					«instance.simpleName».cpp
				«ENDFOR»
				«network.simpleName».cpp
			)
			«IF linkNativeLib»
				
				if(external_definitions)
					add_definitions(${external_definitions})
				endif()	
				
				if(external_library_paths)
					link_directories(${external_library_paths})
				endif()
			«ENDIF»
			
			include_directories(./include«IF linkNativeLib» ${external_include_paths}«ENDIF»)
			
			add_executable(«network.simpleName» ${filenames})
			
			set(libraries«IF linkNativeLib» ${external_libraries}«ENDIF» exelixi-runtime)
			
			#Exelixi CPP library files
			if(NO_DISPLAY)
					add_definitions(-DNO_DISPLAY)
			else()
					find_package(SDL)
					include_directories(
						${SDL_INCLUDE_DIR} 
					)
					set(libraries ${libraries} ${SDL_LIBRARY})
			endif()
			
			set(libraries ${libraries} ${CMAKE_THREAD_LIBS_INIT})
			target_link_libraries(«network.simpleName» ${libraries})
		'''
	}

	def getContentLib() {
		'''
			#Exelixi CPP library files
			if(NO_DISPLAY)
				add_definitions(-DNO_DISPLAY)
			else()
				find_package(SDL)
				include_directories(
					${SDL_INCLUDE_DIR} 
				)
			endif()
			
			
			set(exelixi_runtime_sources
				«sources»
			)
			
			set(exelixi_runtime_header
				«headers»
			)
			
			add_library(exelixi-runtime STATIC ${exelixi_runtime_sources} ${exelixi_runtime_header})
		'''
	}

	def protected getSources() {
		'''
			src/file_utils.cpp
			src/file_read.cpp
			src/file_write.cpp
			src/get_opt.cpp
			src/mapping_parser.cpp
			src/sdl_display.cpp
			src/tinyxml2.cpp
		'''
	}

	def protected getHeaders() {
		'''
			include/actor.h
			include/fifo.h
			include/file_read.h
			include/file_write.h
			include/file_utils.h
			include/get_opt.h
			include/mapping_parser.h
			include/sdl_display.h
			include/tinyxml2.h
			include/scheduling_miss_logger.h
		'''
	}

}
