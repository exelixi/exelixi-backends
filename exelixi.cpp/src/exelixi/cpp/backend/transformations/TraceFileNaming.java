/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.backend.transformations;

import java.io.File;
import java.util.List;
import java.util.Map.Entry;

import net.sf.orcc.df.Connection;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.graph.Vertex;
import net.sf.orcc.util.OrccLogger;

/**
 * 
 * @author Endri Bezati
 */
public class TraceFileNaming extends DfVisitor<Void> {
	private File path;

	public TraceFileNaming(File root) {
		path = root;
	}

	public Void caseInstance(Instance instance) {
		String t = instance.getName()+": ";
		for(String s : instance.getHierarchicalId().subList(1, instance.getHierarchicalId().size())){
			t+=("/" +s);
		}
		OrccLogger.noticeln(t);
		
		String name = instance.getSimpleName();
		File iPath = new File(path, name);
		if (!iPath.exists()) {
			iPath.mkdir();
			OrccLogger.noticeln(iPath.toString() + " created");
		}

		for (Entry<Port, List<Connection>> eout : instance.getOutgoingPortMap().entrySet()) {
			Port out = eout.getKey();

			String filePath = iPath.getAbsolutePath() + File.separator + out.getName() + ".txt";
			out.setAttribute("fifoTrace", filePath);

			for (Connection c : eout.getValue()) {
				Vertex target = c.getTarget();
				if (target instanceof Port) {
					Port targetPort = (Port) target;
					String linkPath = path.getAbsolutePath() + File.separator + targetPort.getName() + ".txt";
					out.setAttribute("fifoTraceLink", linkPath);
				}
			}

		}

		for (Entry<Port, Connection> ein : instance.getIncomingPortMap().entrySet()) {
			Port in = ein.getKey();

			String filePath = iPath.getAbsolutePath() + File.separator + in.getName() + ".txt";
			in.setAttribute("fifoTrace", filePath);

			Vertex source = ein.getValue().getSource();
			if (source instanceof Port) {
				Port sourcePort = (Port) source;
				String linkPath = path.getAbsolutePath() + File.separator + sourcePort.getName() + ".txt";
				in.setAttribute("fifoTraceLink", linkPath);
			}

		}

		return null;
	}

	public Void caseNetwork(Network network) {
		path = new File(path, network.getSimpleName());
		if (!path.exists()) {
			path.mkdirs();
			OrccLogger.noticeln(path.toString() + " created");
		}

		for (Vertex v : network.getVertices()) {
			if (v instanceof Network) {
				Network sNetwork = (Network) v;
				new TraceFileNaming(path).doSwitch(sNetwork);
			} else if (v instanceof Instance) {
				doSwitch((Instance) v);
			}
		}
		return null;

	}

}
