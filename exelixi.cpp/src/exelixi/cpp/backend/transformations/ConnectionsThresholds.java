/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.backend.transformations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.orcc.df.Action;
import net.sf.orcc.df.Connection;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.util.DfVisitor;

/**
 * 
 * @author Endri Bezati
 */
public class ConnectionsThresholds extends DfVisitor<Map<Connection, Integer>> {

	public static String THRESHOLD = "threshold";

	private class PortVisitor extends DfVisitor<Void> {

		private Map<Port, Integer> portsMap = new HashMap<>();

		@Override
		public Void caseAction(Action action) {
			for (Entry<Port, Integer> e : action.getInputPattern().getNumTokensMap().entrySet()) {
				Port port = e.getKey();
				Integer value = getMaxValue(portsMap.get(port), e.getValue());

				if (value != null) {
					portsMap.put(port, value);
				}
			}

			for (Entry<Port, Integer> e : action.getOutputPattern().getNumTokensMap().entrySet()) {
				Port port = e.getKey();
				Integer value = getMaxValue(portsMap.get(port), e.getValue());

				if (value != null) {
					portsMap.put(port, value);
				}
			}
			return null;
		}

		@Override
		public Void caseInstance(Instance instance) {

			for (Action action : instance.getActor().getActions()) {
				doSwitch(action);
			}

			for (Entry<Port, Connection> e : instance.getIncomingPortMap().entrySet()) {
				Port port = e.getKey();
				Connection connection = e.getValue();
				Integer value = getMaxValue(thresholdMap.get(connection), portsMap.get(port));

				if (value != null) {
					thresholdMap.put(connection, value);
					connection.setAttribute(THRESHOLD, value);
				}
			}

			for (Entry<Port, List<Connection>> e : instance.getOutgoingPortMap().entrySet()) {
				Port port = e.getKey();
				for (Connection connection : e.getValue()) {
					Integer value = getMaxValue(thresholdMap.get(connection), portsMap.get(port));

					if (value != null) {
						thresholdMap.put(connection, value);
						connection.setAttribute(THRESHOLD, value);
					}
				}
			}

			return null;
		}

	}

	private Map<Connection, Integer> thresholdMap = new HashMap<>();

	@Override
	public Map<Connection, Integer> caseInstance(Instance instance) {
		new PortVisitor().doSwitch(instance);
		return thresholdMap;
	}

	@Override
	public Map<Connection, Integer> caseNetwork(Network network) {
		super.caseNetwork(network);

		for (Connection connection : network.getConnections()) {
			if (!connection.hasAttribute(THRESHOLD)) {
				thresholdMap.put(connection, 0);
				connection.setAttribute(THRESHOLD, 0);
			}
		}

		return thresholdMap;
	}

	private int roundPow2(int value) {
		double tmp = Math.log(value) / Math.log(2.0);
		return (int) Math.pow(2, Math.ceil(tmp));
	}

	private Integer getMaxValue(Integer oldValue, Integer newValue) {
		if (newValue == null) {
			return oldValue;
		}else{
			newValue = roundPow2(newValue);
		}
		
		if (oldValue == null) {
			return newValue;
		} else if (oldValue > newValue) {
			return oldValue;
		} else {
			return newValue;
		}
	}

}
