/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.backend.transformations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;

import net.sf.orcc.df.Connection;
import net.sf.orcc.df.DfFactory;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.graph.Vertex;
import net.sf.orcc.ir.Type;

/**
 * 
 * @author Endri Bezati
 */
public class Partitioner extends DfVisitor<Void> {

	private Map<String, String> mapping;

	public Partitioner(Map<String, String> mapping) {
		this.mapping = mapping;
	}

	@Override
	public Void caseNetwork(Network network) {

		Map<String, Network> partitions = new HashMap<String, Network>();

		// -- Retrieve Partition Names
		Set<String> partitionsNames = new HashSet<String>();
		for (Vertex vertex : network.getChildren()) {
			if (vertex instanceof Instance) {
				Instance instance = (Instance) vertex;
				String name = mapping.get(instance.getHierarchicalName());
				partitionsNames.add(name);
			}
		}

		// -- Copy the Network as many time as partitions are found
		for (String name : partitionsNames) {
			Network copy = EcoreUtil.copy(network);
			partitions.put(name, copy);
		}

		// -- Delete actors not found in a partition
		for (String pName : partitions.keySet()) {
			Network net = partitions.get(pName);
			List<Vertex> vertexes = net.getChildren();
			int nbrInputs = 0;
			int nbrOutputs = 0;
			List<Vertex> vertexToBeRemoved = new ArrayList<Vertex>();
			// -- Find actors to be removed
			for (Vertex vertex : vertexes) {
				Instance instance = (Instance) vertex;
				// -- If actor is not in the partition then remove
				if (!mapping.get(instance.getHierarchicalName()).equals(pName)) {
					// -- Get Ports and Connections
					Map<Port, Connection> incoming = instance.getIncomingPortMap();
					Map<Port, List<Connection>> outgoing = instance.getOutgoingPortMap();

					// -- Treat Outgoing
					Map<Port, Port> oldNewPorts = new HashMap<Port, Port>();
					for (Port port : outgoing.keySet()) {
						for (Connection connection : outgoing.get(port)) {
							Instance tgtInstance = (Instance) connection.getTarget();

							if (!mapping.get(tgtInstance.getHierarchicalName())
									.equals(mapping.get(instance.getHierarchicalName()))) {
								Port srcPort = connection.getSourcePort();
								if (!oldNewPorts.keySet().contains(srcPort)) {
									Type type = EcoreUtil.copy(connection.getSourcePort().getType());
									String portName = pName + "_IN_" + nbrInputs;
									nbrInputs++;
									net.addInput(DfFactory.eINSTANCE.createPort(type, portName));
									Port newNetworkPort = net.getInput(portName);
									Connection newConnection = DfFactory.eINSTANCE.createConnection(newNetworkPort,
											null, connection.getTarget(), connection.getTargetPort());
									newConnection.setSize(connection.getSize());
									net.add(newConnection);
									oldNewPorts.put(srcPort, newNetworkPort);
								} else {
									Port newNetworkPort = oldNewPorts.get(srcPort);
									Connection newConnection = DfFactory.eINSTANCE.createConnection(newNetworkPort,
											null, connection.getTarget(), connection.getTargetPort());
									newConnection.setSize(connection.getSize());
									net.add(newConnection);
								}
							}
							net.remove(connection);
						}
					}

					// -- Treat Incoming
					for (Port port : incoming.keySet()) {
						Instance tgtInstance = (Instance) incoming.get(port).getSource(); 
						if (!mapping.get(tgtInstance.getHierarchicalName())
								.equals(mapping.get(instance.getHierarchicalName()))) {
							Connection connection = incoming.get(port);
							Port newPort = EcoreUtil.copy(connection.getSourcePort());
							newPort.setName(pName + "_OUT_" + nbrOutputs);
							nbrOutputs++;
							if (!net.getOutputs().contains(newPort)) {
								net.addOutput(newPort);
								Instance srcInstance = (Instance) connection.getSource();
								Connection newConnection = DfFactory.eINSTANCE.createConnection(srcInstance,
										connection.getSourcePort(), newPort, null);
								newConnection.setSize(connection.getSize());
								net.add(newConnection);
							}
						}
						net.remove(incoming.get(port));
					}

					// -- Remove the actor
					vertexToBeRemoved.add(instance);
				}
			}
			net.removeVertices(vertexToBeRemoved);

		}

		return null;
	}

}
