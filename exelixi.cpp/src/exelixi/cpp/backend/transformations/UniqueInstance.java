/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.cpp.backend.transformations;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import net.sf.orcc.df.Actor;
import net.sf.orcc.df.Connection;
import net.sf.orcc.df.DfFactory;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.graph.Edge;
import net.sf.orcc.graph.Vertex;
import net.sf.orcc.ir.Expression;
import net.sf.orcc.ir.Var;
import net.sf.orcc.ir.util.IrUtil;
import net.sf.orcc.util.SwitchUtil;
import net.sf.orcc.util.Void;

/**
 * This class creates a unique instance from instatiate (true) flatten network
 * @author Endri Bezati
 * 
 */
public class UniqueInstance extends DfVisitor<Void> {

	public UniqueInstance() {
	}

	@Override
	public Void caseNetwork(Network network) {
		// copy instances to entities/instances
		List<Vertex> children = new ArrayList<Vertex>(network.getChildren());
		for (Vertex vertex : children) {
			Actor actor = vertex.getAdapter(Actor.class);
			if (actor == null) {
				// cannot instantiate anything else than an instance
				continue;
			}

			instantiate(network, actor);

		}
		return SwitchUtil.DONE;
	}

	/**
	 * Replaces connections to the instance by connections to the entity.
	 * 
	 * @param copier
	 *            a copier object used to create the newEntity
	 * @param instance
	 *            an instance
	 * @param newEntity
	 *            the new entity
	 */
	private void connect(Copier copier, Actor actor, Vertex newEntity) {
		List<Edge> incoming = new ArrayList<Edge>(actor.getIncoming());
		for (Edge edge : incoming) {
			edge.setTarget(newEntity);
			Connection connection = (Connection) edge;
			connection.setTargetPort((Port) copier.get(connection.getTargetPort()));
		}

		List<Edge> outgoing = new ArrayList<Edge>(actor.getOutgoing());
		for (Edge edge : outgoing) {
			edge.setSource(newEntity);
			Connection connection = (Connection) edge;
			connection.setSourcePort((Port) copier.get(connection.getSourcePort()));
		}
	}

	/**
	 * Instantiates the object referenced by the instance in the given network.
	 * 
	 * @param network
	 *            network that contains instance
	 * @param instance
	 *            an instance that references an actor or sub-network
	 */
	private void instantiate(Network network, Actor actor) {
		// copy object
		Copier copier = new Copier(true);
		Vertex newEntity = (Vertex) IrUtil.copy(copier, actor);
		Vertex newInstace = DfFactory.eINSTANCE.createInstance(actor.getName(), newEntity);

		// instantiate sub network
		doSwitch(newEntity);

		// rename sub network
		newEntity.setLabel(actor.getName());

		// replace connections of instance
		network.add(newInstace);
		connect(copier, actor, newInstace);

		// assigns arguments' values to network's variables
		for (Var var : actor.getParameters()) {
			Var cvar = (Var) copier.get(var);
			// If instance's parameter correspond to an actor's parameter
			if (cvar != null) {
				Expression value = var.getInitialValue();
				cvar.setInitialValue(value);
			} else {
				// TODO : Display a warning ?
			}
		}

		// remove instance
		network.remove(actor);
	}

}
