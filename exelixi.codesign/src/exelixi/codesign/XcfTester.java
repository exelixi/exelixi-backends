/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.codesign;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import exelixi.core.xcf.XcfReader;
import exelixi.core.xcf.model.XcfCodeGenerator;
import exelixi.core.xcf.model.XcfCodeGeneratorParameter;
import exelixi.core.xcf.model.XcfCodeGenerators;
import exelixi.core.xcf.model.XcfConnections;
import exelixi.core.xcf.model.XcfFifoConnection;
import exelixi.core.xcf.model.XcfInstance;
import exelixi.core.xcf.model.XcfMedia;
import exelixi.core.xcf.model.XcfMemory;
import exelixi.core.xcf.model.XcfMemoryConnection;
import exelixi.core.xcf.model.XcfModel;
import exelixi.core.xcf.model.XcfNetwork;
import exelixi.core.xcf.model.XcfPartition;
import exelixi.core.xcf.model.XcfPartitioning;

public class XcfTester {
	private static final String XCF_XML = "/home/endrix/test.xcf";

	public static void main(String[] args) throws JAXBException, IOException {

		System.out.println("Simple XCF Model");
		System.out.println("");
		XcfModel xcf = new XcfModel();

		xcf.setName("One Partition");
		xcf.setVersion("1.0");
		
		// -- XDF
		XcfNetwork xdf = new XcfNetwork("/home/endrix/test.xdf");
		xdf.setFileFormat("xdf");
		xcf.setNetwork(xdf);
		

		
		XcfCodeGenerator codeGenerator = new XcfCodeGenerator();
		codeGenerator.setId("CPP for X86");
		codeGenerator.setBackend("CPP");
		XcfCodeGeneratorParameter p1 = new XcfCodeGeneratorParameter();
		p1.setKey("FPGA Part");
		p1.setValue("xc7z020clg484-1");
		codeGenerator.addParameter(p1);
		
		XcfCodeGenerators codeGenerators = new XcfCodeGenerators();
		codeGenerators.addCodeGenerator(codeGenerator);
		xcf.setCodeGenerators(codeGenerators);
		

		XcfPartitioning partitions = new XcfPartitioning();
		
		// -- Partition 0
		XcfPartition p0 = new XcfPartition();
		p0.setId("Partition 0");
		p0.setPe("X86");
		p0.setHost("");

		XcfInstance a = new XcfInstance("A", false);
		XcfInstance b = new XcfInstance("B", true);
		p0.addInstance(a);
		p0.addInstance(b);
		p0.setCodeGenerationId("CPP for X86");

		
		XcfMemory mem = new XcfMemory();
		mem.setMedium("DDR");
		mem.setSize(1000);
		mem.setStartAddress("0x100");
		
		partitions.addPartition(p0);
		
		xcf.setPartitioning(partitions);
		
		
		XcfConnections connections = new XcfConnections();
		
		XcfFifoConnection f0 = new XcfFifoConnection();
		f0.setSrc("A");
		f0.setSrcPort("OUT");
		f0.setDst("B");
		f0.setDstPort("IN");
		f0.setSize(4096);
		f0.setMedium("USB3");
		connections.addFifoConnection(f0);
		
		XcfMemoryConnection m0 = new XcfMemoryConnection();
		m0.setInstance("A");
		m0.setMedium("DDR3");
		m0.setVar("toto");
		connections.addMemoryConnection(m0);
		
		xcf.setConnections(connections);
		
		
		XcfMedia media = new XcfMedia();
		media.addMemory(mem);
		xcf.setMedia(media);
		

		JAXBContext context = JAXBContext.newInstance(XcfModel.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		m.marshal(xcf, System.out);

		m.marshal(xcf, new File(XCF_XML));
		
		//SchemaOutputResolver sor = new XcfSchemaOutputResolver();
		//context.generateSchema(sor);
		
		
		System.out.println("");
		
		
		System.out.println("Parsing File : " + args[0]);
		System.out.println("");
		
		File xcfTest = new File(args[0]);
		XcfReader parser = new XcfReader(xcfTest);
		XcfModel m2 = parser.getXcfModel();
		
		m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		m.marshal(m2, System.out);
		
	}
}
