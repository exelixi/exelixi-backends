/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.codesign.backend.transformations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.util.EcoreUtil;

import exelixi.core.xcf.model.XcfInstance;
import exelixi.core.xcf.model.XcfPartition;
import net.sf.orcc.df.Actor;
import net.sf.orcc.df.Connection;
import net.sf.orcc.df.DfFactory;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.Port;
import net.sf.orcc.graph.Edge;
import net.sf.orcc.graph.Vertex;
import net.sf.orcc.ir.Type;

public class NetworkPartitioner {

	private Network network;

	private Map<XcfPartition, Network> childNetworks;

	

	/**
	 * 
	 * Top Connections maps
	 */

	private Map<Port, List<Port>> partitionedNetworkInputs;
	private Map<Port, Port> partitionedNetworkOutputs;

	public NetworkPartitioner(Network network) {
		this.network = network;
		childNetworks = new HashMap<XcfPartition, Network>();
		
		partitionedNetworkOutputs = new HashMap<Port, Port>();
		partitionedNetworkInputs  = new HashMap<Port, List<Port>>();
	}

	public Network getNetwork(XcfPartition partition) {
		if (childNetworks.containsKey(partition)) {
			return childNetworks.get(partition);
		} else {
			Map<Port, List<Port>> networkInputToActorPortMap = new HashMap<Port, List<Port>>();
			Map<Port, Port> actorPortToNetworkInputMap = new HashMap<Port, Port>();
			Map<Port, Port> actorPortToNetworkOutputMap = new HashMap<Port, Port>();
			// -- Create a network for the given partition
			Network pNetwork = DfFactory.eINSTANCE.createNetwork();
			pNetwork.setName(partition.getId());

			// -- I/O port counters
			int iCounter = 0;
			int oCounter = 0;

			// -- Copy Actors of the partition to the new partitioned network
			List<Actor> actors = new ArrayList<Actor>();
			for (XcfInstance instance : partition.getInstances()) {
				String instanceId = instance.getId();
				Actor actor = (Actor) network.getVertex(instanceId);
				actors.add(actor);

				Actor copyActor = EcoreUtil.copy(actor);
				pNetwork.getChildren().add(copyActor);
			}

			// -- Create Connections and IO Ports
			for (Vertex vertex : pNetwork.getChildren()) {
				Actor actor = (Actor) vertex;
				String name = actor.getName();

				// -- Actor from Original Network
				Actor originalActor = (Actor) network.getChild(name);

				// -- Incoming Edges
				List<Edge> incoming = originalActor.getIncoming();
				for (Edge edge : incoming) {
					Connection originalConnection = (Connection) edge;
					Vertex oSource = originalConnection.getSource();
					Port oSourcePort = originalConnection.getSourcePort();
					Port oTargetPort = originalConnection.getTargetPort();

					Port targetPort = actor.getInput(oTargetPort.getName());
					if (oSource != null && oSourcePort != null) {
						Actor source = (Actor) pNetwork.getChild(((Actor) oSource).getName());
						if (source != null) {
							// -- Source actor is in the same partition
							Port sourcePort = actor.getOutput(oSourcePort.getName());
							Connection connection = DfFactory.eINSTANCE.createConnection(source, sourcePort, actor,
									targetPort, originalConnection.getSize());
							pNetwork.add(connection);
						} else {
							// -- Source actor is in another partition
							if (!networkInputToActorPortMap.containsKey(oSourcePort)) {
								List<Port> inputPortList = new ArrayList<Port>();
								inputPortList.add(targetPort);
								networkInputToActorPortMap.put(oSourcePort, inputPortList);
								if (!actorPortToNetworkInputMap.containsKey(oSourcePort)) {
									Type type = EcoreUtil.copy(targetPort.getType());
									String networkInputPortName = "I_" + iCounter;
									Port inputPort = DfFactory.eINSTANCE.createPort(type, networkInputPortName);
									actorPortToNetworkInputMap.put(oSourcePort, inputPort);
									// -- Top Connections
									if (partitionedNetworkInputs.containsKey(oSourcePort)) {
										partitionedNetworkInputs.get(oSourcePort).add(inputPort);
									} else {
										List<Port> portList = new ArrayList<Port>();
										portList.add(inputPort);
										partitionedNetworkInputs.put(oSourcePort, portList);
									}
									
									pNetwork.addInput(inputPort);
									Connection connection = DfFactory.eINSTANCE.createConnection(inputPort, null, actor,
											targetPort, originalConnection.getSize());
									pNetwork.add(connection);
									iCounter++;
								} else {
									Port inputPort = actorPortToNetworkInputMap.get(oSourcePort);
									Connection connection = DfFactory.eINSTANCE.createConnection(inputPort, null, actor,
											targetPort, originalConnection.getSize());
									pNetwork.add(connection);
								}
							} else {
								List<Port> inputPortList = networkInputToActorPortMap.get(oSourcePort);
								inputPortList.add(targetPort);
								networkInputToActorPortMap.put(oSourcePort, inputPortList);
								Port inputPort = actorPortToNetworkInputMap.get(oSourcePort);
								if (partitionedNetworkInputs.containsKey(oSourcePort)) {
									partitionedNetworkInputs.get(oSourcePort).add(inputPort);
								} else {
									List<Port> portList = new ArrayList<Port>();
									portList.add(inputPort);
									partitionedNetworkInputs.put(oSourcePort, portList);
								}
								Connection connection = DfFactory.eINSTANCE.createConnection(inputPort, null, actor,
										targetPort, originalConnection.getSize());
								pNetwork.add(connection);
							}
						}
					} else if (oSourcePort == null) {
						if (!networkInputToActorPortMap.containsKey(oSourcePort)) {
							List<Port> inputPortList = new ArrayList<Port>();
							inputPortList.add(targetPort);
							networkInputToActorPortMap.put(oSourcePort, inputPortList);
							if (!actorPortToNetworkInputMap.containsKey(oSourcePort)) {
								Port originalInputPort = (Port) oSource;
								Type inputPortType = EcoreUtil.copy(originalInputPort.getType());
								String inputPortName = originalInputPort.getName();
								Port inputPort = DfFactory.eINSTANCE.createPort(inputPortType, inputPortName);
								actorPortToNetworkInputMap.put(oSourcePort, inputPort);
								pNetwork.addInput(inputPort);
								Connection connection = DfFactory.eINSTANCE.createConnection(inputPort, null, actor,
										targetPort);
								pNetwork.add(connection);
							}
						} else {
							Port inputPort = actorPortToNetworkInputMap.get(oSourcePort);
							Connection connection = DfFactory.eINSTANCE.createConnection(inputPort, null, actor,
									targetPort, originalConnection.getSize());
							pNetwork.add(connection);
						}
					}
				}

				// -- Outgoing Edges
				List<Edge> outgoing = originalActor.getOutgoing();
				for (Edge edge : outgoing) {
					Connection originalConnection = (Connection) edge;

					Port oSourcePort = originalConnection.getSourcePort();
					Vertex oTarget = originalConnection.getTarget();
					Port oTargetPort = originalConnection.getTargetPort();

					Port sourcePort = actor.getOutput(oSourcePort.getName());

					if (oTarget != null && oTargetPort != null) {
						// -- Actor to Actor Connection
						Actor target = (Actor) pNetwork.getChild(((Actor) oTarget).getName());
						if (target != null) {
							// -- Target actor is in the same partition
							Port targetPort = target.getInput(oTargetPort.getName());
							Connection connection = DfFactory.eINSTANCE.createConnection(actor, sourcePort, target,
									targetPort, originalConnection.getSize());
							pNetwork.add(connection);
						} else {
							// -- Target actor is in another partition
							// -- Permit only one connection to network putput port from the same actor
							// output port
							if (!actorPortToNetworkOutputMap.containsKey(sourcePort)) {
								// -- Create a new Port
								Type type = EcoreUtil.copy(sourcePort.getType());
								String networkOutputPortName = "O_" + oCounter;
								Port outputPort = DfFactory.eINSTANCE.createPort(type, networkOutputPortName);
								actorPortToNetworkOutputMap.put(sourcePort, outputPort);
								// -- Top Connections
								partitionedNetworkOutputs.put(oSourcePort, outputPort);

								pNetwork.addOutput(outputPort);
								Connection connection = DfFactory.eINSTANCE.createConnection(actor, sourcePort,
										outputPort, null);
								pNetwork.add(connection);

								oCounter++;
							}
						}
					} else if (oTargetPort == null) {
						Port originalOutputPort = (Port) oTarget;
						Type outputPorttype = EcoreUtil.copy(originalOutputPort.getType());
						String outputPortName = originalOutputPort.getName();
						Port outputPort = DfFactory.eINSTANCE.createPort(outputPorttype, outputPortName);
						pNetwork.addOutput(outputPort);
						Connection connection = DfFactory.eINSTANCE.createConnection(actor, sourcePort, outputPort,
								null);
						pNetwork.add(connection);
					}
				}

			}

			childNetworks.put(partition, pNetwork);

			return pNetwork;
		}
	}

	public Network getTopNetwork(List<XcfPartition> partitions) {
		// -- Create a network for the given partition
		Network topNetwork = DfFactory.eINSTANCE.createNetwork();
		topNetwork.setName(network.getName());

		// -- Add children Network
		for (XcfPartition partition : partitions) {
			Network child = getNetwork(partition);
			topNetwork.add(child);
		}

		// -- Connections
		for (Port originalActorOutputPort : partitionedNetworkOutputs.keySet()) {
			if (partitionedNetworkInputs.containsKey(originalActorOutputPort)) {
				Port sourcePort = partitionedNetworkOutputs.get(originalActorOutputPort);
				Vertex source = (Vertex) sourcePort.eContainer();
				for (Port targetPort : partitionedNetworkInputs.get(originalActorOutputPort)) {
					Vertex target = (Vertex) targetPort.eContainer();
					Connection connection = DfFactory.eINSTANCE.createConnection(source, sourcePort, target,
							targetPort);
					topNetwork.add(connection);
				}
			}
		}

		return topNetwork;
	}

}
