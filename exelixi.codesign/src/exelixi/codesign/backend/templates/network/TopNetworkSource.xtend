/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.codesign.backend.templates.network

import exelixi.core.backends.templates.network.NetworkSource
import net.sf.orcc.df.Network

class TopNetworkSource extends NetworkSource {
	
	new(Network network) {
		super(network)
	}
	
	override getIncludes(){
		'''
			#include <thread>
			#include "«network.simpleName».h"
		'''
	}
	
	
	def getInitialize(){
		'''
			void «network.simpleName»::initialize() {
				«FOR network : network.children.filter(typeof(Network))»
					net_«network.simpleName»->initialize();
				«ENDFOR»
			}
		'''
	}
	
	def getActors(){
		'''
			std::vector<Actor*> «network.simpleName»::get_actors() {
				«FOR network : network.children.filter(typeof(Network))»
					for(auto& actor : net_«network.simpleName»->get_actors()){
						actors.push_back(actor);
					}
				«ENDFOR»
			
			    return actors;
			}
		'''
	}
	
	def getRun(){
		'''
			void «network.simpleName»::run() {
			
				«FOR network : network.children.filter(typeof(Network))»
					std::thread t_«network.simpleName»(&«network.simpleName»::run, net_«network.simpleName»);
				«ENDFOR»
				
				«FOR network : network.children.filter(typeof(Network))»
					t_«network.simpleName».join();
				«ENDFOR»
			
				return true;
			}
		'''
	}
	
	override getBody(){
		'''
			«initialize»
			
			«actors»
			
			«run»
		'''
	}
	
	
}