/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.codesign.backend.templates.cmake

import exelixi.core.backends.templates.cmake.CmakePrinter
import exelixi.core.backends.templates.ExelixiPrinter
import java.util.List

class ExelixiRuntimeCmake extends CmakePrinter {

	private List<ExelixiPrinter> printers

	new(List<ExelixiPrinter> printers) {
		this.printers = printers
	}

	override getContent() {
		'''
			«fileHeader»
			if (NO_DISPLAY)
			    add_definitions(-DNO_DISPLAY)
			else ()
			    find_package(SDL)
			    include_directories(
			            ${SDL_INCLUDE_DIR}
			    )
			endif ()
			
			set(exelixi_runtime_sources
			        «FOR printer : printers»
			        	src/«printer.fileName»
			        «ENDFOR»
			        )
			
			add_library(exelixi-runtime STATIC ${exelixi_runtime_sources})
			
			target_include_directories(exelixi-runtime PUBLIC include)
		'''
	}

}
