/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.codesign.backend.templates.cmake

import exelixi.core.backends.templates.cmake.CmakePrinter
import exelixi.core.xcf.model.XcfPartitioning

class ProjectCmake extends CmakePrinter {

	private String projectName

	private XcfPartitioning partitioning

	new(String projectName, XcfPartitioning partitioning) {
		this.projectName = projectName
		this.partitioning = partitioning
	}

	override getContent() {
		'''
			«fileHeader»
			
			cmake_minimum_required (VERSION 2.6)
			
			project («projectName»)
			
			set(extra_definitions)
			option(NO_DISPLAY "Disable SDL display." ON)
			if(NO_DISPLAY)
			    list(APPEND extra_definitions -DNO_DISPLAY)
			else()
			    find_package(SDL REQUIRED)
			endif()
			
			set (CMAKE_CXX_STANDARD 11)
			
			# Requiered Packages
			find_package(Threads REQUIRED)
			
			# Place the executable on bin directory
			set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)
			
			if(CMAKE_COMPILER_IS_GNUCXX)
			    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g")
			    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")
			    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -std=c++11 -fpermissive -Wno-narrowing -w")
			elseif(CMAKE_CXX_COMPILER_ID STREQUAL Intel)
			    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g")
			    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")
			    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -std=c++11 -fpermissive -Wno-narrowing -w")
			endif()
			
			# -- Compile Exelixi Runtime
			add_subdirectory(lib/exelixi-runtime)
			
			«FOR partition : partitioning.partitions»
				# -- Compile Partition «partition.id»
				add_subdirectory(lib/partitions/«partition.id»)
			«ENDFOR»
			
			# -- Compile «projectName»
			add_subdirectory(«projectName»)
		'''
	}

}
