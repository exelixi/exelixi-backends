/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.codesign.backend;

import static exelixi.core.ExelixiConstants.CODESIGN_MODE;
import static exelixi.core.ExelixiConstants.EXELIXI_BACKEND;
import static exelixi.core.ExelixiConstants.PARTITIONED_NETWORK;
import static exelixi.codesign.CodesignConstants.PARTITIONED_NETWORK_IS_HOST;
import static exelixi.codesign.CodesignConstants.PARTITIONED_NETWORK_PE;
import static net.sf.orcc.OrccLaunchConstants.DEBUG_MODE;
import static net.sf.orcc.OrccLaunchConstants.DEFAULT_DEBUG;
import static net.sf.orcc.OrccLaunchConstants.DEFAULT_FIFO_SIZE;
import static net.sf.orcc.OrccLaunchConstants.FIFO_SIZE;
import static net.sf.orcc.OrccLaunchConstants.OUTPUT_FOLDER;
import static net.sf.orcc.OrccLaunchConstants.PROJECT;
import static net.sf.orcc.backends.BackendsConstants.XCF_FILE;
import static net.sf.orcc.util.OrccUtil.getFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.osgi.framework.Bundle;

import exelixi.codesign.backend.libraries.cpp.CPPLibrary;
import exelixi.codesign.backend.templates.cmake.ExelixiRuntimeCmake;
import exelixi.codesign.backend.templates.cmake.ProjectCmake;
import exelixi.codesign.backend.templates.cmake.TopCmake;
import exelixi.codesign.backend.templates.main.MainPrinter;
import exelixi.codesign.backend.templates.network.TopNetworkHeader;
import exelixi.codesign.backend.templates.network.TopNetworkSource;
import exelixi.codesign.backend.transformations.NetworkPartitioner;
import exelixi.codesign.backend.util.FolderInformation;
import exelixi.core.Activator;
import exelixi.core.backends.ExelixiBackend;
import exelixi.core.backends.templates.ExelixiPrinter;
import exelixi.core.backends.templates.exelixirunime.ActorHeader;
import exelixi.core.backends.templates.exelixirunime.FifoHeader;
import exelixi.core.backends.templates.exelixirunime.GetOptHeader;
import exelixi.core.backends.templates.exelixirunime.GetOptSource;
import exelixi.core.backends.templates.exelixirunime.NativeHeader;
import exelixi.core.backends.templates.exelixirunime.NetworkHeader;
import exelixi.core.backends.templates.exelixirunime.SdlDisplayHeader;
import exelixi.core.backends.templates.exelixirunime.SdlDisplaySource;
import exelixi.core.backends.transformations.ConnectionReaders;
import exelixi.core.xcf.XcfReader;
import exelixi.core.xcf.model.XcfCodeGenerator;
import exelixi.core.xcf.model.XcfCodeGenerators;
import exelixi.core.xcf.model.XcfModel;
import exelixi.core.xcf.model.XcfPartition;
import exelixi.core.xcf.model.XcfPartitioning;
import exelixi.cpp.backend.ExelixiCPP;
import net.sf.orcc.OrccRuntimeException;
import net.sf.orcc.backends.Backend;
import net.sf.orcc.backends.BackendFactory;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.transform.Instantiator;
import net.sf.orcc.df.transform.NetworkFlattener;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.util.FilesManager;
import net.sf.orcc.util.OrccLogger;
import net.sf.orcc.util.OrccUtil;
import net.sf.orcc.util.Result;
import net.sf.orcc.util.util.EcoreHelper;

/**
 * Exelixi CoDesign, a backend for heterogeneous platforms.
 *
 * @author Endri Bezati
 *
 */
public class CodesignBackend extends ExelixiBackend {

	/**
	 * Defined backends in XCF
	 */
	private Map<String, Backend> backends;

	private Map<Backend, Map<String, Object>> backendOptions;

	/**
	 * A map of hosts that contains a Map of backends
	 */
	private Map<String, List<Backend>> backendsInHost;

	private String topPath;

	private String topSrcPath;

	private String topInlcudePath;

	private String libPath;

	private String partitionsPath;

	private String outputPath;
	
	private FolderInformation folderInformation;

	/**
	 * List of transformations to apply on each network
	 */
	protected List<DfVisitor<?>> topNetworkTransfos;

	private void createFolders(Network network) {
		outputPath = getOption(OUTPUT_FOLDER, "");

		// -- Library path
		libPath = outputPath + File.separator + "lib";
		createDirectory(libPath);

		// -- Partitions path
		partitionsPath = libPath + File.separator + "partitions";
		createDirectory(partitionsPath);

		// -- Top Network path
		topPath = outputPath + File.separator + network.getSimpleName();
		createDirectory(topPath);

		// -- Top Source Path
		topSrcPath = topPath + File.separator + "src";
		createDirectory(topSrcPath);

		// -- Top Include Path
		topInlcudePath = topPath + File.separator + "include";
		createDirectory(topInlcudePath);

	}

	@Override
	protected void doInitializeOptions() {
		// -- Initialize Arrays
		topNetworkTransfos = new ArrayList<>();
		// -- Add Option codesign mode
		getOptions().put(CODESIGN_MODE, true);

	}

	@Override
	public void compile(IProgressMonitor progressMonitor) {
		// -- New ResourceSet for a new compilation
		currentResourceSet = new ResourceSetImpl();

		// -- Initialize the monitor. Can be used to stop the back-end
		// execution and provide feedback to user
		monitor = progressMonitor;

		// -- Initialize backends
		backends = new HashMap<String, Backend>();
		backendOptions = new HashMap<>();
		backendsInHost = new HashMap<>();

		// -- Get Xml Configuration File
		final String xcfFile = getOption(XCF_FILE, "");
		XcfReader xcfReader = new XcfReader(new File(xcfFile));
		XcfModel xcf = xcfReader.getXcfModel();
		
		

		// -- Get Network
		String networkName = xcf.getNetwork().getQualifiedId();
		final Network network;
		final IFile xdfFile = getFile(project, networkName, OrccUtil.NETWORK_SUFFIX);
		if (xdfFile == null) {
			throw new OrccRuntimeException(
					"Unable to find the XDF file " + "corresponding to the network " + networkName + ".");
		} else {
			network = EcoreHelper.getEObject(currentResourceSet, xdfFile);
		}
		
		// -- Get Partitions
		NetworkPartitioner partitioner = new NetworkPartitioner(network);
		XcfPartitioning partitioning = xcf.getPartitioning();
		
		// -- Create Folders
		folderInformation = new FolderInformation(partitioning, getOption(OUTPUT_FOLDER, ""));
		folderInformation.createFolders();

		// -- Print Information
		String exelixiVersion = "<unknown>";
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		if (bundle != null) {
			exelixiVersion = bundle.getHeaders().get("Bundle-Version");
		}

		OrccLogger.traceln("*********************************************" + "************************************");
		OrccLogger.traceln("* Exelixi Co-Design: " + exelixiVersion);
		OrccLogger.traceln("* Project:           " + project.getName());
		OrccLogger.traceln("* Network:           " + network.getName());
		OrccLogger.traceln("* Output folder:     " + outputPath);
		OrccLogger.traceln("*********************************************" + "************************************");

		// -- Flatten the network
		new Instantiator(true).doSwitch(network);
		new NetworkFlattener().doSwitch(network);

		// -- Get code generators
		XcfCodeGenerators codeGenerators = xcf.getCodeGenerators();
		for (XcfCodeGenerator cg : codeGenerators.getCodeGenerators()) {
			String cgId = cg.getId();
			Backend backend = BackendFactory.getInstance().getBackend(cg.getBackend());
			if (backend == null) {
				throw new OrccRuntimeException("Unable to find backend : " + cg.getBackend());
			}
			Map<String, Object> options = new HashMap<String, Object>();
			// -- Basic Orcc options
			options.put(PROJECT, getOption(PROJECT, ""));
			options.put(CODESIGN_MODE, true);
			options.put(EXELIXI_BACKEND, cg.getBackend());
			options.put(FIFO_SIZE, getOption(FIFO_SIZE, DEFAULT_FIFO_SIZE));
			options.put(DEBUG_MODE, getOption(DEBUG_MODE, DEFAULT_DEBUG));

			backendOptions.put(backend, options);

			backends.put(cgId, backend);
		}

		// -- Interfaces
		// -- TODO: Parse Interfaces

		
		Network topPartitionedNetwork = partitioner.getTopNetwork(partitioning.getPartitions());

		OrccLogger.traceln("* Available Partitions: " + partitioning.getPartitions().size());
		
		if (partitioning.getPartitions().size() > 1) {
			for (XcfPartition partition : partitioning.getPartitions()) {
				String partitionId = partition.getId();
				String codeGenerationId = partition.getCodeGenerationId();
				String partitionPe = partition.getPe();
				String host = partition.getHost();

				Network pNetwork = partitioner.getNetwork(partition);
				Backend backend = backends.get(codeGenerationId);
				String outputFolder = folderInformation.getPartitionPath(partition);
				backendOptions.get(backend).put(OUTPUT_FOLDER, outputFolder);
				backendOptions.get(backend).put(PARTITIONED_NETWORK, pNetwork);
				backendOptions.get(backend).put(PARTITIONED_NETWORK_IS_HOST, host);
				backendOptions.get(backend).put(PARTITIONED_NETWORK_PE, partitionPe);

				backend.setOptions(backendOptions.get(backend));
				OrccLogger.traceln(
						"*********************************************" + "************************************");
				OrccLogger.traceln("* Partition: " + partitionId);
				OrccLogger.traceln("* Code Generation ID: " + codeGenerationId);

				backend.compile(progressMonitor);

				if (backendsInHost.containsKey(host)) {
					backendsInHost.get(host).add(backend);
				} else {
					List<Backend> backends = new ArrayList<>();
					backends.add(backend);
					backendsInHost.put(host, backends);
				}
			}
		} else {

		}

		// -- Transform partitioned Network
		topNetworkTransfos.add(new ConnectionReaders());
		applyTransformations(topPartitionedNetwork, topNetworkTransfos, false);

		// -- Generate CoDesign libraries
		generateLibraries();

		// -- Top CMake
		// generateTopCmake(network.getSimpleName(), partitioning);

		// -- Generate Partitioned Network
		// doGenerateNetwork(topPartitionedNetwork);

	}

	void generateLibraries() {
		final Result result = Result.newInstance();
		Map<String, Boolean> hostContainsCPU = folderInformation.getHostContainsCPU();
		Map<String, String> hostLibpaths = folderInformation.getHostLibPaths();
		
		for(String host : hostContainsCPU.keySet()) {
			if(hostContainsCPU.get(host)) {
				String outputPath = hostLibpaths.get(host);
				CPPLibrary library = new CPPLibrary(outputPath);
				result.merge(library.generateLibrary());
			}
		}	
	}

	void generateTopCmake(String projectName, XcfPartitioning partitioning) {

		final Result result = Result.newInstance();
		// -- Generate Top CMake
		ProjectCmake topCmake = new ProjectCmake(projectName, partitioning);
		result.merge(FilesManager.writeFile(topCmake.getContent(), outputPath, topCmake.getFileName()));

	}

	// -- Helper Methods

	@Override
	protected Result doGenerateNetwork(Network network) {
		final Result result = Result.newInstance();

		// -- Header
		TopNetworkHeader topNetworkHeader = new TopNetworkHeader(network);
		result.merge(
				FilesManager.writeFile(topNetworkHeader.getContent(), topInlcudePath, topNetworkHeader.getFileName()));

		// -- Source
		TopNetworkSource topNetworkSource = new TopNetworkSource(network);
		result.merge(FilesManager.writeFile(topNetworkSource.getContent(), topSrcPath, topNetworkSource.getFileName()));

		// -- Main
		MainPrinter mainPrinter = new MainPrinter(network);
		result.merge(FilesManager.writeFile(mainPrinter.getContent(), topSrcPath, mainPrinter.getFileName()));

		// -- CMake
		TopCmake topCmake = new TopCmake(network);
		result.merge(FilesManager.writeFile(topCmake.getContent(), topPath, topCmake.getFileName()));

		return result;
	}

	/**
	 * Check if a backend class exists in the backends map
	 * 
	 * @param backends The backends Map
	 * @param c        The backend class
	 * @return
	 */
	private static boolean checkBackend(Map<String, Backend> backends, Class<?> c) {
		return backends.values().stream().anyMatch(p -> p.getClass() == c);
	}
}
