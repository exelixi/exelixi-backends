/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.codesign.backend.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import exelixi.core.util.tree.Node;
import exelixi.core.xcf.model.XcfPartition;
import exelixi.core.xcf.model.XcfPartitioning;

import static exelixi.codesign.CodesignConstants.EXELIXI_CODESIGN_PE_CPU;

public class FolderInformation {

	private static final String DEFAULT_HOST = "default";

	/**
	 * Root node
	 */
	private Node<PathContainer> root;

	/**
	 * Host, list of partition map
	 */
	private Map<String, List<XcfPartition>> hostPartitionMap;

	private Map<String, Boolean> hostContainsCPU;

	private Map<String, String> hostLibPath;

	/**
	 * FolderInformation constructor, creates a folder tree representation for the
	 * code-generation given a partitioning
	 * 
	 * @param partitioning
	 * @param outputFolder
	 */
	public FolderInformation(XcfPartitioning partitioning, String outputFolder) {
		// -- intialize the hostPartitionMap
		hostPartitionMap = new HashMap<String, List<XcfPartition>>();
		hostContainsCPU = new HashMap<String, Boolean>();
		hostLibPath = new HashMap<String, String>();

		// -- Construct the root node
		root = new Node<PathContainer>(PathContainer.createRootKind(outputFolder));

		// -- Search if a partition has defined a host and order them by host to
		// hostPartitionMap
		for (XcfPartition partition : partitioning.getPartitions()) {
			String host = DEFAULT_HOST;
			if (partition.getHost() != null) {
				host = partition.getHost();
			}

			if (hostPartitionMap.containsKey(host)) {
				hostPartitionMap.get(host).add(partition);
			} else {
				List<XcfPartition> partitionsList = new ArrayList<XcfPartition>();
				partitionsList.add(partition);
				hostPartitionMap.put(host, partitionsList);
			}
		}

		// -- Fill Map of host containing CPUs
		for (String host : hostPartitionMap.keySet()) {
			Boolean containsCPU = false;
			for (XcfPartition partition : hostPartitionMap.get(host)) {
				String pe = partition.getPe();
				if (pe.equals(EXELIXI_CODESIGN_PE_CPU)) {
					containsCPU = true;
					break;
				}
			}
			hostContainsCPU.put(host, containsCPU);
		}

		// -- Construct Tree
		if (hostPartitionMap.keySet().size() > 1) {
			for (String host : hostPartitionMap.keySet()) {
				// -- Create Host Node
				String path = root.getData().getPath() + File.separator + host;

				Node<PathContainer> parent = null;
				if (hostPartitionMap.get(host).size() > 1) {
					parent = new Node<PathContainer>(PathContainer.createHostKind(host, path));
				} else {
					parent = new Node<PathContainer>(PathContainer.createHostSinglePartitionKind(host, path));
				}

				root.addChild(parent);

				if (hostContainsCPU.get(host)) {
					// -- Lib Node
					String libPath = parent.getData().getPath() + File.separator + "lib";
					hostLibPath.put(host, libPath);
					Node<PathContainer> libNode = new Node<PathContainer>(PathContainer.createLibKind(libPath));
					parent.addChild(libNode);

					// -- Tob Node
					Node<PathContainer> topNode = new Node<PathContainer>(
							PathContainer.createTopKind(parent.getData().getPath() + File.separator + "top"));
					parent.addChild(topNode);

					// -- Add partition node on host node
					Node<PathContainer> partitionsNode = new Node<PathContainer>(PathContainer
							.createPartitionsFolderKind(libNode.getData().getPath() + File.separator + "partitions"));
					parent.addChild(partitionsNode);

					for (XcfPartition partition : hostPartitionMap.get(host)) {
						// -- Create node for partition
						String partitionId = partition.getId();

						Node<PathContainer> partitionNode = new Node<PathContainer>(PathContainer.createPartitionKind(
								partitionId, partitionsNode.getData().getPath() + File.separator + partitionId));
						partitionsNode.addChild(partitionNode);
					}
				}
			}
		} else {
			Iterator<String> it = hostPartitionMap.keySet().iterator();
			String host = it.next();
			Node<PathContainer> parent = null;
			if (host.equals(DEFAULT_HOST)) {
				parent = root;
			} else {
				if (hostPartitionMap.get(host).size() > 1) {
					parent = new Node<PathContainer>(
							PathContainer.createHostKind(host, root.getData().getPath() + File.separator + host));
				} else {
					parent = new Node<PathContainer>(PathContainer.createHostSinglePartitionKind(host,
							root.getData().getPath() + File.separator + host));
				}
				root.addChild(parent);
			}

			if (hostContainsCPU.get(host)) {
				// -- Lib Node
				Node<PathContainer> libNode = new Node<PathContainer>(
						PathContainer.createLibKind(parent.getData().getPath() + File.separator + "lib"));
				parent.addChild(libNode);

				// -- Tob Node
				Node<PathContainer> topNode = new Node<PathContainer>(
						PathContainer.createTopKind(parent.getData().getPath() + File.separator + "top"));
				parent.addChild(topNode);

				// -- Add partition node on host node
				Node<PathContainer> partitionsNode = new Node<PathContainer>(PathContainer
						.createPartitionsFolderKind(libNode.getData().getPath() + File.separator + "partitions"));
				parent.addChild(partitionsNode);

				for (XcfPartition partition : hostPartitionMap.get(host)) {
					// -- Create node for partition
					String partitionId = partition.getId();

					Node<PathContainer> partitionNode = new Node<PathContainer>(PathContainer.createPartitionKind(
							partitionId, partitionsNode.getData().getPath() + File.separator + partitionId));
					partitionsNode.addChild(partitionNode);
				}
			}
		}

		printTree(root, "|- ");
	}

	/**
	 * Get code-generation path for the partition
	 * 
	 * @param partition
	 * @return
	 */
	public String getPartitionPath(XcfPartition partition) {
		String hostId = partition.getHost();
		String partitionId = partition.getId();

		Node<PathContainer> node = findPartitionNode(root, hostId, partitionId);

		return node.getData().getPath();
	}

	
	public Map<String, String> getHostLibPaths(){
		return hostLibPath;
	}
	
	public Map<String, Boolean> getHostContainsCPU(){
		return hostContainsCPU;
	}
	
	/**
	 * Breadth First Search for finding the partition node
	 * 
	 * @param nodesToCheck
	 * @param hostId
	 * @param partitionId
	 * @return
	 */
	Node<PathContainer> findPartitionNode(Node<PathContainer> node, String hostId, String partitionId) {
		boolean isMatch = false;
		PathContainer pathContainer = node.getData();
		if (pathContainer.isHostSinglePartition() || pathContainer.isPartition()) {
			String nextHostId = pathContainer.getId();
			if ((nextHostId.equals(hostId) && !hostContainsCPU.get(hostId)) || nextHostId.equals(partitionId)) {
				isMatch = true;
			}
		}

		if (isMatch) {
			return node;
		} else {
			for (Node<PathContainer> child : node.getChildren()) {
				Node<PathContainer> result = findPartitionNode(child, hostId, partitionId);
				if (result != null) {
					return result;
				}
			}
		}
		return null;
	}

	/**
	 * Create the folder for a give configuration
	 */
	public void createFolders() {
		createFolders(root);
	}

	/**
	 * Create folders by Node
	 * 
	 * @param node
	 */
	private void createFolders(Node<PathContainer> node) {
		File dir = new File(node.getData().getPath());
		if (!dir.exists()) {
			dir.mkdir();
		}
		node.getChildren().forEach(each -> createFolders(each));
	}

	public static <T> void printTree(Node<T> node, String appender) {
		System.out.println(appender + node.getData());
		node.getChildren().forEach(each -> printTree(each, appender + appender));
	}

}
