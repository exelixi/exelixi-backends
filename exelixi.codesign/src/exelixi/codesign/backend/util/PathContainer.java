/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.codesign.backend.util;

public class PathContainer {

	public enum Kind {
		ROOT, HOST, HOST_SINGLE_PARTITION, TOP, LIB, PARTITIONS_FOLDER, PARTITION
	}

	public static PathContainer createRootKind(String path) {
		return new PathContainer(Kind.ROOT, path);
	}

	public static PathContainer createHostKind(String id, String path) {
		return new PathContainer(Kind.HOST, id, path);
	}
	
	public static PathContainer createHostSinglePartitionKind(String id, String path) {
		return new PathContainer(Kind.HOST_SINGLE_PARTITION, id, path);
	}

	public static PathContainer createTopKind(String path) {
		return new PathContainer(Kind.TOP, path);
	}

	public static PathContainer createLibKind(String path) {
		return new PathContainer(Kind.LIB, path);
	}

	public static PathContainer createPartitionsFolderKind(String path) {
		return new PathContainer(Kind.PARTITIONS_FOLDER, path);
	}

	public static PathContainer createPartitionKind(String id, String path) {
		return new PathContainer(Kind.PARTITION, id, path);
	}

	private Kind kind;

	private String id;

	private String path;

	public PathContainer(Kind kind, String path) {
		this.kind = kind;
		this.path = path;
		this.id = "";
	}

	public PathContainer(Kind kind, String id, String path) {
		this.kind = kind;
		this.path = path;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Kind getKind() {
		return kind;
	}

	public void setKind(Kind partition) {
		this.kind = partition;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isRoot() {
		return this.kind == Kind.ROOT;
	}

	public boolean isHost() {
		return this.kind == Kind.HOST;
	}
	
	public boolean isHostSinglePartition() {
		return this.kind == Kind.HOST_SINGLE_PARTITION;
	}

	public boolean isTop() {
		return this.kind == Kind.TOP;
	}

	public boolean isLib() {
		return this.kind == Kind.LIB;
	}

	public boolean isPartitionsFolder() {
		return this.kind == Kind.PARTITIONS_FOLDER;
	}

	public boolean isPartition() {
		return this.kind == Kind.PARTITION;
	}

	@Override
	public String toString() {
		return path + " : (" + kind +")";
	}

}
