/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.codesign.backend.libraries.cpp;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import exelixi.codesign.backend.templates.cmake.ExelixiRuntimeCmake;
import exelixi.core.backends.templates.ExelixiPrinter;
import exelixi.core.backends.templates.exelixirunime.ActorHeader;
import exelixi.core.backends.templates.exelixirunime.FifoHeader;
import exelixi.core.backends.templates.exelixirunime.GetOptHeader;
import exelixi.core.backends.templates.exelixirunime.GetOptSource;
import exelixi.core.backends.templates.exelixirunime.NativeHeader;
import exelixi.core.backends.templates.exelixirunime.NetworkHeader;
import exelixi.core.backends.templates.exelixirunime.SdlDisplayHeader;
import exelixi.core.backends.templates.exelixirunime.SdlDisplaySource;
import exelixi.core.util.ExelixiCoreUtils;
import net.sf.orcc.util.FilesManager;
import net.sf.orcc.util.Result;

public class CPPLibrary {

	/**
	 * Output Path
	 */
	String path;

	/**
	 * Output path for runtime
	 */
	String runtime;
	
	/**
	 * Output source path of the runtime
	 */
	String srcPath;

	/**
	 * Output header path of the runtime 
	 */
	String headerPath;
	

	/**
	 * Library headers
	 */
	List<ExelixiPrinter> headerPrinters;

	/**
	 * Library sources
	 */
	List<ExelixiPrinter> srcPrinters;

	public CPPLibrary(String path) {
		this.path = path;

		// -- Headers
		headerPrinters = new ArrayList<ExelixiPrinter>();
		headerPrinters.add(new ActorHeader());
		headerPrinters.add(new NetworkHeader());
		headerPrinters.add(new FifoHeader());
		headerPrinters.add(new GetOptHeader());
		headerPrinters.add(new NativeHeader());
		headerPrinters.add(new SdlDisplayHeader());

		// -- Sources
		srcPrinters = new ArrayList<ExelixiPrinter>();
		srcPrinters.add(new GetOptSource());
		srcPrinters.add(new SdlDisplaySource());

		// -- Source and header folders
		runtime = path + File.separator + "exelixi-runtime";
		srcPath = runtime + File.separator + "src";
		headerPath = runtime + File.separator + "include";
		ExelixiCoreUtils.createDirectory(runtime);
		ExelixiCoreUtils.createDirectory(srcPath);
		ExelixiCoreUtils.createDirectory(headerPath);
	}

	public Result generateLibrary() {
		final Result result = Result.newInstance();

		// -- Print headers
		headerPrinters.stream().forEach(
				(p -> result.merge(FilesManager.writeFile(p.getContent(), headerPath, p.getFileName()))));

		// -- Print sources
		srcPrinters.stream().forEach(
				(p -> result.merge(FilesManager.writeFile(p.getContent(), srcPath, p.getFileName()))));
		
		// -- Exelixi Runtime CmakeLists.txt
		ExelixiRuntimeCmake runtimeCmake = new ExelixiRuntimeCmake(srcPrinters);
		result.merge(FilesManager.writeFile(runtimeCmake.getContent(), runtime, runtimeCmake.getFileName()));
		
		return result;
	}
}
