/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package exelixi.codesign.ui.editors;

import static exelixi.codesign.CodesignConstants.EXELIXI_NETWORK_CONFIGURATION_FILE;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.FileEditorInput;

import exelixi.core.xcf.XcfReader;
import exelixi.core.xcf.XcfWriter;
import exelixi.core.xcf.model.XcfCodeGenerator;
import exelixi.core.xcf.model.XcfFifoConnection;
import exelixi.core.xcf.model.XcfInstance;
import exelixi.core.xcf.model.XcfMedia;
import exelixi.core.xcf.model.XcfMemoryConnection;
import exelixi.core.xcf.model.XcfModel;
import exelixi.core.xcf.model.XcfPartition;
import net.sf.orcc.OrccException;
import net.sf.orcc.util.OrccLogger;

public class ConfigurationEditor extends EditorPart {

	private XcfModel xcf;

	private List<XcfPartition> xcfPartitions;

	private Map<XcfPartition, XcfInstance> xcfInstances;

	private List<XcfMedia> xcfMedia;

	private List<XcfMemoryConnection> xcfMemoryConnections;

	private List<XcfFifoConnection> xcfFifoConnections;

	private boolean isDirty;

	// -- UI Widgets

	/**
	 * Partition Table
	 */

	private Table partitionTable;
	private TableViewer partitionViewer;

	/**
	 * Instance Table
	 */
	private Table instancesTable;
	private TableViewer instancesViewer;

	/**
	 * Code Generation
	 */
	private Table codeGenerationTable;
	private TableViewer codeGenerationViewer;

	/**
	 * Memories
	 */
	private Table memoriesTable;
	private TableViewer memoriesTableViewer;

	/**
	 * Interfaces
	 */
	private Table interfacesTable;
	private TableViewer interfacesTableViewer;

	/**
	 * FIFO Connections
	 */
	private Table fifoConnectionsTable;
	private TableViewer fifoConnectionsTableViewer;

	/**
	 * Memory Connections
	 */
	private Table memoryConnectionsTable;
	private TableViewer memoryConnectionsTableViewer;

	//  -- Column names

	// -- paritions
	private final int P_PARTITION_ID = 0;
	private final int P_PROCESSING_ELEMENT = 1;
	private final int P_CODE_GENERATOR = 2;
	private final int P_HOST = 3;

	private final String[] partitioningColumNames = new String[] { "Partition ID", "Processing Element",
			"Code Generator ID", "Host" };

	// -- instance
	private final int I_INSTANCE = 0;
	private final int I_EXTERNAL = 1;
	private final int I_PARTITION_ID = 2;

	private final String[] instancesColumnNames = new String[] { "Instance", "External", "Partition ID", };

	// -- code generation
	private final int C_CODE_GENERATOR_ID = 0;
	private final int C_BACKEND = 1;
	private final int C_OPTIONS = 2;

	private final String[] codeGenerationColumNames = new String[] { "Code Generator ID", "Backend", "Options" };

	// -- memories
	private final int M_MEMORY_ID = 0;
	private final int M_SIZE = 1;
	private final int M_START_ADDRESS = 2;
	private final int M_SHARED = 3;
	private final int M_RESERVE = 4;

	private final String[] memoriesColumNames = new String[] { "Medium ID", "Size", "Start Address", "Shared",
			"Reserve" };

	// -- interfaces
	private final int IF_MEDIUM_ID = 0;
	private final int IF_EXTERNAL = 1;

	private final String[] interfacesColumNames = new String[] { " Medium ID", "External" };

	// -- fifo connections
	private final int FC_SRC = 0;
	private final int FC_SRC_PORT = 1;
	private final int FC_DST = 2;
	private final int FC_DST_PORT = 3;
	private final int FC_SIZE = 4;
	private final int FC_MEDIUM_ID = 5;

	private final String[] fifoConnectionsColumNames = new String[] { "Source", "Source port", "Destination",
			"Destination port", "Size", "Medium ID" };

	// -- memory connections
	private final int MC_INSTANCE = 0;
	private final int MC_VARIABLE = 1;
	private final int MC_MEDIUM_ID = 2;

	private final String[] memoryConnectionsColumNames = new String[] { "Instance", "Variable", "Medium ID" };

	private class PartitioningContentProvider implements IStructuredContentProvider {

		@Override
		public Object[] getElements(Object element) {
			if (element instanceof XcfModel) {
				return xcf.getPartitioning().getPartitions().toArray();
			} else {
				return new Object[0];
			}
		}

	}

	private class PartitioningTableLabelProvider extends LabelProvider implements ITableLabelProvider {

		@Override
		public Image getColumnImage(Object o, int columIndex) {
			return null;
		}

		@Override
		public String getColumnText(Object o, int columIndex) {
			String result = "";
			XcfPartition partition = (XcfPartition) o;

			switch (columIndex) {
			case P_PARTITION_ID:
				result = partition.getId();
				break;
			case P_PROCESSING_ELEMENT:
				result = partition.getPe();
				break;
			case P_CODE_GENERATOR:
				result = partition.getCodeGenerationId();
				break;
			case P_HOST:
				if (partition.getHost() == null) {
					result = "";
				} else {
					result = partition.getHost().toString();
				}
				break;
			default:
				break;
			}

			return result;

		}

	}

	private class InstanceContentProvider implements IStructuredContentProvider {

		@Override
		public Object[] getElements(Object element) {
			if (element instanceof XcfModel) {
				return xcf.getPartitionInstanceMap().entrySet().toArray();
			} else {
				return new Object[0];
			}
		}

	}

	private class InstanceTableLabelProvider extends LabelProvider implements ITableLabelProvider {

		@Override
		public Image getColumnImage(Object o, int columIndex) {
			return null;
		}

		@Override
		public String getColumnText(Object o, int columIndex) {
			String result = "";
			@SuppressWarnings("unchecked")
			Map.Entry<XcfInstance, XcfPartition> e = (Map.Entry<XcfInstance, XcfPartition>) o;

			switch (columIndex) {
			case I_INSTANCE:
				result = e.getKey().getId();
				break;
			case I_PARTITION_ID:
				result = e.getValue().getId();
				break;
			case I_EXTERNAL:
				if (e.getKey().getExternal() == null) {
					result = "";
				} else {
					result = e.getKey().getExternal().toString();
				}
				break;

			default:
				break;
			}

			return result;

		}

	}

	private class CodeGeneratorContentProvider implements IStructuredContentProvider {

		@Override
		public Object[] getElements(Object element) {
			if (element instanceof XcfModel) {
				return xcf.getCodeGenerators().getCodeGenerators().toArray();
			} else {
				return new Object[0];
			}
		}

	}

	private class CodeGeneratorTableLabelProvider extends LabelProvider implements ITableLabelProvider {

		@Override
		public Image getColumnImage(Object o, int columnIndex) {
			return null;
		}

		@Override
		public String getColumnText(Object o, int columnIndex) {
			String result = "";
			XcfCodeGenerator cg = (XcfCodeGenerator) o;

			switch (columnIndex) {
			case C_CODE_GENERATOR_ID:
				result = cg.getId();
				break;
			case C_BACKEND:
				result = cg.getBackend();
				break;
			default:
				break;
			}

			return result;
		}

	}

	@Override
	public void doSave(IProgressMonitor arg0) {
		try {
			IEditorInput input = getEditorInput();
			if (input instanceof FileEditorInput) {
				File file = getFile(((FileEditorInput) input).getFile());
				if (getExtension(file).equals(EXELIXI_NETWORK_CONFIGURATION_FILE)) {
					new XcfWriter(xcf).writeXcf(file);
					setDirty(false);
				}
			}
		} catch (Exception e) {
			OrccLogger.severeln("Partitiong file size save error: " + e.getMessage());
			setDirty(false);
		}
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		isDirty = false;
		try {
			if (input instanceof FileEditorInput) {
				File file = getFile(((FileEditorInput) input).getFile());
				if (getExtension(file).equals(EXELIXI_NETWORK_CONFIGURATION_FILE)) {
					xcf = new XcfReader(file).getXcfModel();
					setPartName(file.getName());
				} else {
					throw new Exception("Invalid XCF file");
				}
			}
		} catch (Exception e) {
			throw new PartInitException("Input file is corrupted or not valid");
		}
		setSite(site);
		setInput(input);
	}

	@Override
	public boolean isDirty() {
		return isDirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		parent.setLayout(gridLayout);
		Label lbl = new Label(parent, SWT.NONE);
		lbl.setFont(parent.getFont());
		String network = xcf != null ? (xcf.getNetwork() != null ? xcf.getNetwork().getQualifiedId() : "") : "";
		lbl.setText("XDF Network file: \"" + network + "\"");
		lbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		// -- separator
		Label separator = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
		separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		{ // -- Partitioning Table
			Label partitionLabel = new Label(parent, SWT.NONE);
			partitionLabel.setFont(parent.getFont());
			partitionLabel.setText("Partitions");

			partitionTable = new Table(parent, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
			partitionViewer = new TableViewer(partitionTable);

			GridData gridData = new GridData(GridData.FILL_BOTH);
			gridData.grabExcessVerticalSpace = true;
			gridData.horizontalSpan = 3;
			gridData.heightHint = 300;
			partitionTable.setLayoutData(gridData);

			partitionTable.setLinesVisible(true);
			partitionTable.setHeaderVisible(true);

			// -- column : partition id
			TableColumn column = new TableColumn(partitionTable, SWT.LEFT, P_PARTITION_ID);
			column.setText(partitioningColumNames[P_PARTITION_ID]);
			column.setWidth(100);
			// column.addSelectionListener(getPartitionSelectionAdapter(column,
			// P_PARTITION_ID));

			// -- column : processing element
			column = new TableColumn(partitionTable, SWT.LEFT, P_PROCESSING_ELEMENT);
			column.setText(partitioningColumNames[P_PROCESSING_ELEMENT]);
			column.setWidth(180);
			// column.addSelectionListener(getPartitionSelectionAdapter(column,
			// P_PROCESSING_ELEMENT));

			// -- column : code generator
			column = new TableColumn(partitionTable, SWT.LEFT, P_CODE_GENERATOR);
			column.setText(partitioningColumNames[P_CODE_GENERATOR]);
			column.setWidth(180);
			// column.addSelectionListener(getPartitionSelectionAdapter(column,
			// P_CODE_GENERATION));

			// -- column : host
			column = new TableColumn(partitionTable, SWT.LEFT, P_HOST);
			column.setText(partitioningColumNames[P_HOST]);
			column.setWidth(50);
			// column.addSelectionListener(getPartitionSelectionAdapter(column, P_HOST));

			// -- create the partition viewer
			partitionViewer.setUseHashlookup(true);
			partitionViewer.setColumnProperties(partitioningColumNames);
			partitionViewer.setContentProvider(new PartitioningContentProvider());
			partitionViewer.setLabelProvider(new PartitioningTableLabelProvider());

			partitionViewer.setInput(xcf);

			// -- separator
			separator = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
			separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}

		{ // -- Instance Partition Mapping Table
			Label instanceLabelSeprator = new Label(parent, SWT.NONE);
			instanceLabelSeprator.setFont(parent.getFont());
			instanceLabelSeprator.setText("Instances");

			instancesTable = new Table(parent, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
			instancesViewer = new TableViewer(instancesTable);

			GridData gridData = new GridData(GridData.FILL_BOTH);
			gridData.grabExcessVerticalSpace = true;
			gridData.horizontalSpan = 3;
			gridData.heightHint = 300;
			instancesTable.setLayoutData(gridData);

			instancesTable.setLinesVisible(true);
			instancesTable.setHeaderVisible(true);

			// -- column : instance
			TableColumn column = new TableColumn(instancesTable, SWT.LEFT, I_INSTANCE);
			column.setText(instancesColumnNames[I_INSTANCE]);
			column.setWidth(300);
			// column.addSelectionListener(getInstancesSelectionAdapter(column,
			// I_INSTANCE));

			// -- column : external
			column = new TableColumn(instancesTable, SWT.LEFT, I_EXTERNAL);
			column.setText(instancesColumnNames[I_EXTERNAL]);
			column.setWidth(100);
			// column.addSelectionListener(getInstancesSelectionAdapter(column,
			// I_EXTERNAL));

			// -- column : partition id
			column = new TableColumn(instancesTable, SWT.LEFT, I_PARTITION_ID);
			column.setText(instancesColumnNames[I_PARTITION_ID]);
			column.setWidth(100);
			// column.addSelectionListener(getInstancesSelectionAdapter(column,
			// I_PARTITION_ID));

			instancesViewer.setUseHashlookup(true);
			instancesViewer.setColumnProperties(instancesColumnNames);
			instancesViewer.setContentProvider(new InstanceContentProvider());
			instancesViewer.setLabelProvider(new InstanceTableLabelProvider());

			instancesViewer.setInput(xcf);

			// -- separator
			separator = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
			separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		}

		{// -- Code Generation
			Label instanceLabelSeprator = new Label(parent, SWT.NONE);
			instanceLabelSeprator.setFont(parent.getFont());
			instanceLabelSeprator.setText("Code Generators");

			codeGenerationTable = new Table(parent, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
			codeGenerationViewer = new TableViewer(codeGenerationTable);

			GridData gridData = new GridData(GridData.FILL_BOTH);
			gridData.grabExcessVerticalSpace = true;
			gridData.horizontalSpan = 3;
			gridData.heightHint = 300;
			codeGenerationTable.setLayoutData(gridData);

			codeGenerationTable.setLinesVisible(true);
			codeGenerationTable.setHeaderVisible(true);

			// -- column : backend
			TableColumn column = new TableColumn(codeGenerationTable, SWT.LEFT, C_CODE_GENERATOR_ID);
			column.setText(codeGenerationColumNames[C_CODE_GENERATOR_ID]);
			column.setWidth(100);
			// column.addSelectionListener(getCodeGenerationSelectionAdapter(column,
			// C_BACKEND));

			// -- column : backend
			column = new TableColumn(codeGenerationTable, SWT.LEFT, C_BACKEND);
			column.setText(codeGenerationColumNames[C_BACKEND]);
			column.setWidth(100);
			// column.addSelectionListener(getCodeGenerationSelectionAdapter(column,
			// C_BACKEND));

			// -- column : optins
			column = new TableColumn(codeGenerationTable, SWT.LEFT, C_OPTIONS);
			column.setText(codeGenerationColumNames[C_OPTIONS]);
			column.setWidth(100);
			// column.addSelectionListener(getCodeGenerationSelectionAdapter(column,
			// C_OPTIONS));

			codeGenerationViewer.setUseHashlookup(true);
			codeGenerationViewer.setColumnProperties(codeGenerationColumNames);
			codeGenerationViewer.setContentProvider(new CodeGeneratorContentProvider());
			codeGenerationViewer.setLabelProvider(new CodeGeneratorTableLabelProvider());

			codeGenerationViewer.setInput(xcf);

			// -- separator
			separator = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
			separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}
		{ // -- interfaces
			Label instanceLabelSeprator = new Label(parent, SWT.NONE);
			instanceLabelSeprator.setFont(parent.getFont());
			instanceLabelSeprator.setText("Interfaces");

			interfacesTable = new Table(parent, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
			interfacesTableViewer = new TableViewer(interfacesTable);

			GridData gridData = new GridData(GridData.FILL_BOTH);
			gridData.grabExcessVerticalSpace = true;
			gridData.horizontalSpan = 3;
			gridData.heightHint = 300;
			interfacesTable.setLayoutData(gridData);

			interfacesTable.setLinesVisible(true);
			interfacesTable.setHeaderVisible(true);

			// -- column : medium id
			TableColumn column = new TableColumn(interfacesTable, SWT.LEFT, IF_MEDIUM_ID);
			column.setText(interfacesColumNames[IF_MEDIUM_ID]);
			column.setWidth(100);

			// -- column : external
			column = new TableColumn(interfacesTable, SWT.LEFT, IF_EXTERNAL);
			column.setText(interfacesColumNames[IF_EXTERNAL]);
			column.setWidth(100);

			// -- separator
			separator = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
			separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}
		{ // -- Memories
			Label instanceLabelSeprator = new Label(parent, SWT.NONE);
			instanceLabelSeprator.setFont(parent.getFont());
			instanceLabelSeprator.setText("Memories");

			memoriesTable = new Table(parent, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
			memoriesTableViewer = new TableViewer(memoriesTable);

			GridData gridData = new GridData(GridData.FILL_BOTH);
			gridData.grabExcessVerticalSpace = true;
			gridData.horizontalSpan = 3;
			gridData.heightHint = 300;
			memoriesTable.setLayoutData(gridData);

			memoriesTable.setLinesVisible(true);
			memoriesTable.setHeaderVisible(true);

			// -- column : memory id
			TableColumn column = new TableColumn(memoriesTable, SWT.LEFT, M_MEMORY_ID);
			column.setText(memoriesColumNames[M_MEMORY_ID]);
			column.setWidth(100);
			// column.addSelectionListener(getMemoriesSelectionAdapter(column,
			// M_MEMORY_ID));

			// -- column : size
			column = new TableColumn(memoriesTable, SWT.LEFT, M_SIZE);
			column.setText(memoriesColumNames[M_SIZE]);
			column.setWidth(100);
			// column.addSelectionListener(getMemoriesSelectionAdapter(column, M_SIZE));

			// -- column : start address
			column = new TableColumn(memoriesTable, SWT.LEFT, M_START_ADDRESS);
			column.setText(memoriesColumNames[M_START_ADDRESS]);
			column.setWidth(150);
			// column.addSelectionListener(getMemoriesSelectionAdapter(column,
			// M_START_ADDRESS));

			// -- column : shared
			column = new TableColumn(memoriesTable, SWT.LEFT, M_SHARED);
			column.setText(memoriesColumNames[M_SHARED]);
			column.setWidth(100);
			// column.addSelectionListener(getMemoriesSelectionAdapter(column, M_SHARED));

			// -- column : reserve
			column = new TableColumn(memoriesTable, SWT.LEFT, M_RESERVE);
			column.setText(memoriesColumNames[M_RESERVE]);
			column.setWidth(100);
			// column.addSelectionListener(getMemoriesSelectionAdapter(column, M_RESERVE));
			// -- separator
			separator = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
			separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		}
		{// -- fifo connections
			Label instanceLabelSeprator = new Label(parent, SWT.NONE);
			instanceLabelSeprator.setFont(parent.getFont());
			instanceLabelSeprator.setText("Fifo Connections");

			fifoConnectionsTable = new Table(parent, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
			fifoConnectionsTableViewer = new TableViewer(fifoConnectionsTable);

			GridData gridData = new GridData(GridData.FILL_BOTH);
			gridData.grabExcessVerticalSpace = true;
			gridData.horizontalSpan = 3;
			gridData.heightHint = 300;
			fifoConnectionsTable.setLayoutData(gridData);

			fifoConnectionsTable.setLinesVisible(true);
			fifoConnectionsTable.setHeaderVisible(true);

			// -- column : src
			TableColumn column = new TableColumn(fifoConnectionsTable, SWT.LEFT, FC_SRC);
			column.setText(fifoConnectionsColumNames[FC_SRC]);
			column.setWidth(100);

			// -- column : src-port
			column = new TableColumn(fifoConnectionsTable, SWT.LEFT, FC_SRC_PORT);
			column.setText(fifoConnectionsColumNames[FC_SRC_PORT]);
			column.setWidth(100);

			// -- column : dst
			column = new TableColumn(fifoConnectionsTable, SWT.LEFT, FC_DST);
			column.setText(fifoConnectionsColumNames[FC_DST]);
			column.setWidth(100);

			// -- column : dst-port
			column = new TableColumn(fifoConnectionsTable, SWT.LEFT, FC_DST_PORT);
			column.setText(fifoConnectionsColumNames[FC_DST_PORT]);
			column.setWidth(150);

			// -- column : size
			column = new TableColumn(fifoConnectionsTable, SWT.LEFT, FC_SIZE);
			column.setText(fifoConnectionsColumNames[FC_SIZE]);
			column.setWidth(100);

			// -- column : medium id
			column = new TableColumn(fifoConnectionsTable, SWT.LEFT, FC_MEDIUM_ID);
			column.setText(fifoConnectionsColumNames[FC_MEDIUM_ID]);
			column.setWidth(100);

			// -- separator
			separator = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
			separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}
		{// -- memory connections
			Label instanceLabelSeprator = new Label(parent, SWT.NONE);
			instanceLabelSeprator.setFont(parent.getFont());
			instanceLabelSeprator.setText("Memory Connections");

			memoryConnectionsTable = new Table(parent, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
			memoryConnectionsTableViewer = new TableViewer(memoryConnectionsTable);

			GridData gridData = new GridData(GridData.FILL_BOTH);
			gridData.grabExcessVerticalSpace = true;
			gridData.horizontalSpan = 3;
			gridData.heightHint = 300;
			memoryConnectionsTable.setLayoutData(gridData);

			memoryConnectionsTable.setLinesVisible(true);
			memoryConnectionsTable.setHeaderVisible(true);

			// -- column : instance
			TableColumn column = new TableColumn(memoryConnectionsTable, SWT.LEFT, MC_INSTANCE);
			column.setText(memoryConnectionsColumNames[MC_INSTANCE]);
			column.setWidth(100);

			// -- column : var
			column = new TableColumn(memoryConnectionsTable, SWT.LEFT, MC_VARIABLE);
			column.setText(memoryConnectionsColumNames[FC_SRC_PORT]);
			column.setWidth(100);

			// -- column : medium id
			column = new TableColumn(memoryConnectionsTable, SWT.LEFT, MC_MEDIUM_ID);
			column.setText(memoryConnectionsColumNames[FC_DST]);
			column.setWidth(100);

			// -- separator
			separator = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
			separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		}
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	private void setDirty(boolean value) {
		this.isDirty = value;
	}

	private File getFile(IFile file) throws OrccException{
		try {
			java.net.URI uri = file.getLocationURI();

			if (file.isLinked()) {
				uri = file.getRawLocationURI();
			}

			File jfile;

			jfile = EFS.getStore(uri).toLocalFile(0, new NullProgressMonitor());
			return jfile;
		} catch (Exception e) {
			throw new OrccException("Impossible to obtain the File from \"" + file + "\"");
		}
	}
	
	private String getExtension(File file) throws OrccException {
		try {
			if (!file.getName().isEmpty()) {
				String fileName = file.getName();
				int mid = fileName.lastIndexOf(".");
				return fileName.substring(mid + 1, fileName.length());
			} else {
				throw new OrccException(
						"Impossible to obtain the file extension of \"" + file + "\": the file name is empty");
			}
		} catch (Exception e) {
			throw new OrccException("Impossible to obtain the file extension of \"" + file + "\"");
		}
	}

}
