/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.backend;

/**
 * Xronos-Exelixi Constants
 * @author Endri Bezati
 */
public class Constants {
	// -- External tools
	public static final String P_VIVADO = "exelixi.xronos.options.vivadoPath";

	public static final String P_VIVADO_HLS = "exelixi.xronos.options.vivadoHLSPath";

	public static final String P_XILINXD_LICENSE_FILE = "exelixi.xronos.options.xilinxLicenceFile";

	public static final String P_LM_LICENSE_FILE = "exelixi.xronos.options.modelsimLicenceFile";
	
	public static String C_VIVADO = "exelixi.xronos.options.vivadoPath";

	public static String C_VIVADO_HLS = "exelixi.xronos.options.vivadoHLSPath";

	public static String C_XILINXD_LICENSE_FILE = "exelixi.xronos.options.xilinxLicenceFile";

	public static String C_LM_LICENSE_FILE = "exelixi.xronos.options.modelsimLicenceFile";

	// -- Partitioning names
	
	public static String ZYNQ_PS_PARTITION = "PS";
	
	public static String ZYNQ_PL_PARTITION = "PL";
	
	public static String ADDITIONAL_OPTIONS_SW = "exelixi.xronos.options.additionalOptionsSW"; 
	
	public static String ADDITIONAL_OPTIONS_HW = "exelixi.xronos.options.additionalOptionsHW"; 
	
	public static String EXELIXI_CLK_NAME = "ap_clk";
	
	public static String EXELIXI_RESET_NAME = "ap_rst_n";
	
	
	// -- Backend Options GUI

	public static String LAUNCH_HLS = "exelixi.xronos.options.launchHLS";

	public static String PACKAGE_SIZE = "exelixi.xronos.options.burstSize";

	public static String BIT_ACCURATE_TYPES = "exelixi.xronos.options.bitAccurateTypes";

	public static String INLINE_ACTIONS = "exelixi.xronos.options.inilineActions";

	public static String CLOCK_PERIOD = "exelixi.xronos.options.period";

	public static String CLOCK_PERIOD_VALUE = "exelixi.xronos.options.periodValue";
	
	public static String FPGA_BOARD = "exelixi.xronos.options.fpgaBoard"; 
	
	public static String PROFILE_DATAFLOW_NETWORK = "exelixi.xronos.options.profileNetwork";
	
	public static String EVERY_INSTANCE_AN_IP_CORE = "exelixi.xronos.options.everyInstanceIpCore";
	
	public static String AXI_STREAM_ACTOR_COMMUNICATION = "exelixi.xronos.options.axiStreamActorCommunication";
	
	public static final String ZYNQ_OPTION_AXI_LITE_CONTROL = "exelixi.xronos.options.axiLiteControl";
	
	public static String NATIVE_IP_CORES = "exelixi.xronos.options.nativeIpCores";
	
	public static String NATIVE_IP_CORES_FOLDER = "exelixi.xronos.options.nativeIpCoresFolder";

	// -- Backend Options

	public static String RTL_PATH = "exelixi.xronos.options.rtlPath";
	
	public static String TB_RTL_PATH = "exelixi.xronos.options.tbRtlPath";
	
	public static String XDC_PATH = "exelixi.xronos.options.xdcPath";
	
	public static String WCFG_PATH = "exelixi.xronos.options.wcfgPath";

	public static String FIFO_TRACE_PATH = "exelixi.xronos-hls.options.fifoTracePath";
	
	public static String PROJECTS_PATH = "exelixi.xronos-hls.options.projectsPath";
	
	public static String CODE_GEN_PATH_SW = "exelixi.xronos-hls.options.codeGenPathSW";
	
	public static String CODE_GEN_PATH_HW = "exelixi.xronos-hls.options.codeGenPathHW";
	
	public static String ACTOR_COMPLEX_GUARDS = "exelixi.xronos.options.complexActor";
	
	public static String ACTOR_ACTION_SELECTION_PEEKS = "exelixi.xronos.options.actorActionSelectionPeeks";
	
	public static String ACTION_SCHEDULER_PEEKS = "exelixi.xronos.options.actionSchedulerPeeks";
	
	public static String PORT_COMPLEXITY = "exelixi.xronos.options.complexPort";
	
	public static String DEVICE = "exelixi.xronos.options.device";
	
	public static String VARIABLE_BASE_ADDRESS = "exelixi.xronos.options.variableBaseAddress";
	
	public static String AVAILABLE_DDR_SPACE = "exelixi.xronos.options.availbaleDdrSpace";
	
	public static String NETWORK_EXTERNAL_VARIABLES = "exelixi.xronos.options.networkExternalVariables";
	
	public static String INSTANCE_EXTERNAL_VARIABLES = "exelixi.xronos.options.instanceExternalVariables";
	
	public static String ACTOR_EXTERNAL_VARIABLES = "exelixi.xronos.options.actorExternalVariables";
	
	public static String ACTOR_GURAD_STATE_VARIABLES = "exelixi.xronos.options.actorGuardStateVariables";
	
	public static String ACTION_EXTERNAL_VARIABLES = "exelixi.xronos.options.actionExternalVariables";
	
	public static String PROCEDURE_EXTERNAL_VARIABLES = "exelixi.xronos.options.procedureExternalVariables";
	
	// -- CoDesign mode
	public static String CODESIGN_MODE = "exelixi.codesign";
	
	public static String PARTITIONED_NETWORK = "exelixi.codesign.partitionedNetwork";

	
	
}
