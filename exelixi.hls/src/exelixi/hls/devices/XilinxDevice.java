/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.devices;

/**
 * A simple util class for retrieving the name and the board of Xilinx
 * device/board.
 * 
 * @author Endri Bezati
 *
 */
public class XilinxDevice {

	private String name;

	private String board;

	private String fpga;

	public XilinxDevice(String name) {
		this.name = name;
		if (name.equals("AC701 - Artix 7")) {
			board = "xilinx.com:ac701:part0:1.2";
			fpga = "xc7a200tfbg676-2";
		} else if (name.equals("KC705 - Kintex 7")) {
			board = "xilinx.com:kc705:part0:1.2";
			fpga = "xc7k325tffg900-2";
		} else if (name.equals("KCU105 - Kintex Ultrscale")) {
			board = "xilinx.com:kcu105:part0:1.1";
			fpga = "xcku040-ffva1156-2-e";
		} else if (name.equals("VC707 - Virtex 7")) {
			board = "xilinx.com:vc707:part0:1.2";
			fpga = "xc7vx485tffg1761-2";
		} else if (name.equals("VC709 - Virtex 7")) {
			board = "xilinx.com:vc709:part0:1.7";
			fpga = "xc7vx690tffg1761-2";
		} else if (name.equals("VCU 108 - Virtex Ultrascale")) {
			board = "xilinx.com:vcu108:part0:1.4";
			fpga = "xcvu095-ffva2104-2-e";
		} else if (name.equals("ZC702 - Zynq 7")) {
			board = "xilinx.com:zc702:part0:1.2";
			fpga = "xc7z020clg484-1";
		} else if (name.equals("ZC706 - Zynq 7")) {
			board = "xilinx.com:zc706:part0:1.2";
			fpga = "xc7z045ffg900-2";
		} else if (name.equals("Zedboard - Zynq 7")) {
			board = "em.avnet.com:zed:part0:1.3";
			fpga = "xc7z020clg484-1";
		} else if (name.equals("PYNQ Z1 - Zynq 7")) {
			board = "www.digilentinc.com:pynq-z1:part0:1.0";
			fpga = "xc7z020clg400-1";	
		} else if (name.equals("OZ745 - Zynq 7")) {
			board = "";
			fpga = "xc7z045ffg900-3";	
		} else if (name.equals("ZCU102 - Zynq Ultrascale+ MPSoC ES1")) {
			board = "xilinx.com:zcu102:part0:1.4";
			fpga = "xczu9eg-ffvb1156-2L-e-es1";
		}else if (name.equals("Opal Kelly - XEM7310-A200")){
			board = "";
			fpga = "xc7a200tfbg484-1";
		}else if (name.equals("XUVP5 - Virtex 5")){
			board = "";
			fpga = " xc5vlx110tff1136-1";
		}
		

	}

	public String getFpga() {
		return fpga;
	}

	public String getBoard() {
		return board;
	}
	
	public String getName() {
		return name;
	}

	public boolean isZynq() {
		if (name.equals("ZC702 - Zynq 7")) {
			return true;
		} else if (name.equals("ZC706 - Zynq 7")) {
			return true;
		} else if (name.equals("Zedboard - Zynq 7")) {
			return true;
		} else if (name.equals("PYNQ Z1 - Zynq 7")) {
			return true;
		} else if (name.equals("OZ745 - Zynq 7")) {
			return true;
		} else if (name.equals("ZCU102 - Zynq Ultrascale+ MPSoC ES1")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isZynqUltrascale(){
		return name.equals("ZCU102 - Zynq Ultrascale+ MPSoC ES1");
	}
	
	public int getDDRLength() {
		if (name.equals("ZC702 - Zynq 7")) {
			return 0x3FF00000;
		} else if (name.equals("ZC706 - Zynq 7")) {
			return 0x3FF00000;
		} else if (name.equals("Zedboard - Zynq 7")) {
			return 0x1FF00000;
		} else if (name.equals("PYNQ Z1 - Zynq 7")) {
			return 0x1FF00000;	
		} else if (name.equals("OZ745 - Zynq 7")) {
			return 0x3FF00000;	
		} else if (name.equals("ZCU102 - Zynq Ultrascale+ MPSoC ES1")) {
			return 0x3FF00000;
		}
		return 0;
	}

}
