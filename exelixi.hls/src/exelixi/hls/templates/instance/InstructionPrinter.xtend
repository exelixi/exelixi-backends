/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.templates.instance

import exelixi.hls.templates.types.TypePrinterVisitor
import net.sf.orcc.backends.ir.InstCast
import net.sf.orcc.df.Action
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Port
import net.sf.orcc.ir.Arg
import net.sf.orcc.ir.ArgByRef
import net.sf.orcc.ir.ArgByVal
import net.sf.orcc.ir.ExprInt
import net.sf.orcc.ir.InstAssign
import net.sf.orcc.ir.InstCall
import net.sf.orcc.ir.InstLoad
import net.sf.orcc.ir.InstReturn
import net.sf.orcc.ir.InstStore
import net.sf.orcc.ir.Instruction
import net.sf.orcc.ir.Param
import net.sf.orcc.ir.Procedure
import net.sf.orcc.ir.TypeList
import net.sf.orcc.ir.Var
import net.sf.orcc.util.util.EcoreHelper

import static exelixi.hls.backend.Constants.ACTOR_COMPLEX_GUARDS

/**
 * 
 * @author Endri Bezati
 */
class InstructionPrinter extends ExpressionPrinter {

	new(TypePrinterVisitor typePrinter) {
		super(typePrinter)
	}

	override caseInstAssign(InstAssign inst) {
		'''
			«inst.target.variable.name» = «inst.value.doSwitch»;
		'''
	}

	override caseInstCall(InstCall call) {
		'''
			«IF call.print»
				#ifndef __SYNTHESIS__
					std::cout << «FOR arg : call.arguments SEPARATOR " << "»(«arg.coutArg»)«ENDFOR»;
				#endif
			«ELSE»
				«IF call.target !== null»«call.target.variable.name» = «ENDIF»«call.procedure.name»(«IF !call.procedure.parameters.empty»«FOR j : 0 .. call.procedure.parameters.size - 1 SEPARATOR ", "»«printWithCast(call.procedure.parameters.get(j), call.arguments.get(j) )»«ENDFOR»«ENDIF»);
			«ENDIF»
		'''
	}

	override caseInstLoad(InstLoad load) {
		val target = load.target.variable
		val source = load.source.variable
		val action = EcoreHelper.getContainerOfType(load, Action)
		val actor = EcoreHelper.getContainerOfType(action, Actor)
		val port = getPort(source, action)
		var isInScheduler = false
		var isComplex = false
		if (actor !== null) {
			if (actor.hasAttribute(ACTOR_COMPLEX_GUARDS)) {
				isComplex = actor.getAttribute(ACTOR_COMPLEX_GUARDS).getObjectValue() as Boolean;
			}
		}

		if (action !== null) {
			isInScheduler = if(action.scheduler == EcoreHelper.getContainerOfType(load, Procedure)) true else false
		}
		'''
			«IF port !== null»
				«IF isComplex»
					«IF actor.inputs.contains(port)»
						«target.name» = p_«source.name»[(p_«port.name»_head + «load.indexes.get(0).doSwitch») & (p_«port.name»_size - 1)];
					«ELSE»
						«target.name» = p_«source.name»«load.indexes.printArrayIndexes»;
					«ENDIF»
				«ELSE»
					«IF !isInScheduler»
						«target.name» = p_«source.name»«load.indexes.printArrayIndexes»;
					«ELSE»
						«getPeek(port, target, source, (load.indexes.get(0) as ExprInt).intValue)»
					«ENDIF»
				«ENDIF»
			«ELSE»
				«target.name» = «source.name»«load.indexes.printArrayIndexes»;
			«ENDIF»
		'''
	}

	def getRead(Port port, Var target, Var source) {
		'''«target.name» = «source.name».read();'''
	}

	def getPeek(Port port, Var target, Var source, int index) {
		'''«target.name» = «source.name»_peek«IF index> 0»_«index»«ENDIF»;'''
	}

	def caseInstCast(InstCast cast) {
		var target = cast.target.variable
		var source = cast.source.variable
		'''
			«IF target.type.list && source.type.list»
				«FOR i : 0 .. (target.type as TypeList).size - 1»
					«target.name»[«i»] = («target.type.doSwitch») «source.name»[«i»];
				«ENDFOR»
			«ELSE»
				«target.name» = («target.type.doSwitch») «source.name»;
			«ENDIF»
		'''
	}

	override caseInstruction(Instruction instr) {
		if (instr instanceof InstCast)
			return caseInstCast(instr as InstCast)
		else
			super.caseInstruction(instr)
	}

	override caseInstReturn(InstReturn ret) {
		var procedure = EcoreHelper.getContainerOfType(ret, Procedure)
		var type = procedure.returnType
		'''
			«IF ret.value !== null»
				return ( «type.doSwitch» ) («ret.value.doSwitch»);
			«ENDIF»
		'''
	}

	override caseInstStore(InstStore store) {
		val target = store.target.variable
		val action = EcoreHelper.getContainerOfType(store, Action)
		val actor = EcoreHelper.getContainerOfType(action, Actor)
		val port = getPort(target, action)
		'''
			«IF port !== null»
				«IF actor.outputs.contains(port)»
					p_«target.name»«store.indexes.printArrayIndexes» = «store.value.doSwitch»;
				«ENDIF»
			«ELSE»
				«target.name»«store.indexes.printArrayIndexes» = «store.value.doSwitch»;
			«ENDIF»
		'''
	}

	def dispatch coutArg(ArgByRef arg) {
		'''&«arg.use.variable.doSwitch»«FOR index : arg.indexes»[«index.doSwitch»]«ENDFOR»'''
	}

	def dispatch coutArg(ArgByVal arg) {
		arg.value.doSwitch
	}

	def print(Arg arg) {
		if (arg.byRef) {
			"&" + (arg as ArgByRef).use.variable.name + (arg as ArgByRef).indexes.printArrayIndexes
		} else {
			(arg as ArgByVal).value.doSwitch
		}
	}

	def printWithCast(Param param, Arg arg) {
		val variable = param.variable
		if (arg.byRef) {
			'''(«variable.type.doSwitch») &«(arg as ArgByRef).use.variable.name»«(arg as ArgByRef).indexes.printArrayIndexes»'''
		} else {
			'''«IF (arg as ArgByVal).value.isExprBinary»(«variable.type.doSwitch») («(arg as ArgByVal).value.doSwitch»)«ELSE»«(arg as ArgByVal).value.doSwitch»«ENDIF»'''
		}
	}

	def protected getPort(Var variable, Action currentAction) {
		if (currentAction === null) {
			null
		} else if (currentAction?.inputPattern.contains(variable)) {
			currentAction.inputPattern.getPort(variable)
		} else if (currentAction?.outputPattern.contains(variable)) {
			currentAction.outputPattern.getPort(variable)
		} else if (currentAction?.peekPattern.contains(variable)) {
			currentAction.peekPattern.getPort(variable)
		} else {
			null
		}
	}

}
