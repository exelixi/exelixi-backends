/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.templates.instance

import exelixi.hls.templates.types.TypePrinterVisitor
import net.sf.orcc.backends.ir.BlockFor
import net.sf.orcc.ir.BlockBasic
import net.sf.orcc.ir.BlockIf
import net.sf.orcc.ir.BlockWhile

/**
 * 
 * @author Endri Bezati
 */
class BlockPrinter extends InstructionPrinter {
	
	new(TypePrinterVisitor typePrinter) {
		super(typePrinter)
	}
	
	override caseBlockBasic(BlockBasic block) '''
		«FOR instr : block.instructions»
			«instr.doSwitch»
		«ENDFOR»
	'''

	override caseBlockIf(BlockIf block) '''
		if («block.condition.doSwitch») {
			«FOR thenBlock : block.thenBlocks»
				«thenBlock.doSwitch»
			«ENDFOR»
		}«IF block.elseRequired» else {
			«FOR elseBlock : block.elseBlocks»
				«elseBlock.doSwitch»
			«ENDFOR»
		}
		«ENDIF»
	'''

	override caseBlockWhile(BlockWhile blockWhile) {
		val Integer loopIndex = blockWhile.getAttribute("loopLabel").objectValue as Integer
		'''
			loop_«loopIndex»: while («blockWhile.condition.doSwitch») {
				«FOR block : blockWhile.blocks»
					«block.doSwitch»
				«ENDFOR»
			}
		'''
	}

	override caseBlockFor(BlockFor blockFor) {
		val Integer loopIndex = blockFor.getAttribute("loopLabel").objectValue as Integer
		'''
			loop_«loopIndex»: for («blockFor.init.join(", ")['''«toExpression»''']» ; «blockFor.condition.doSwitch» ; «blockFor.step.join(", ")['''«toExpression»''']») {
				«FOR contentBlock : blockFor.blocks»
					«contentBlock.doSwitch»
				«ENDFOR»
			}
		'''
	}
}
