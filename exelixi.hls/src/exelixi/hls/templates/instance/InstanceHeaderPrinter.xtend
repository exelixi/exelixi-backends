/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.templates.instance

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.List
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.df.Connection
import java.util.Map
import net.sf.orcc.df.Actor

/**
 * 
 * @author Endri Bezati
 */
class InstanceHeaderPrinter extends HLSTemplate {

	Instance instance
	
	protected Actor actor

	String name

	List<Port> inputs

	List<Port> outputs
	
	TypePrinterVisitor typePrinter
	
	protected Map<Port,List<Connection>> outgoingPortMap

	new(Instance instance, TypePrinterVisitor typePrinter) {
		super(typePrinter)
		this.typePrinter = typePrinter
		this.instance = instance;
		this.actor = instance.getActor
		this.name = instance.simpleName
		this.inputs = instance.getActor.inputs
		this.outputs = instance.getActor.outputs
		this.outgoingPortMap = instance.outgoingPortMap
	}

	def getContent() {
		'''
			«getFileHeader("Vivado HLS Actor Header Code Generation, Instance: " + instance.label)»
			#include <«typePrinter.intIncludeHeaderFile»>
			#include <«typePrinter.channelIncludeHeaderFile»>
			
			void «instance.simpleName»(«FOR port : actor.inputs SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDFOR»«IF !actor.inputs.empty», «ENDIF»«FOR port : actor.outputs SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»_«i»«ENDFOR»«ELSE»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDIF»«ENDFOR»);
		'''
	}

}
