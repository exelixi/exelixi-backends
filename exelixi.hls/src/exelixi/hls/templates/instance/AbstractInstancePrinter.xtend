/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
package exelixi.hls.templates.instance

import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.ArrayList
import java.util.Arrays
import java.util.HashMap
import java.util.List
import java.util.Map
import net.sf.orcc.df.Action
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.ir.Param
import net.sf.orcc.ir.Procedure
import net.sf.orcc.ir.TypeList
import net.sf.orcc.ir.Var

import static exelixi.hls.backend.Constants.INLINE_ACTIONS
import org.eclipse.emf.common.util.EMap
import static exelixi.hls.backend.Constants.ACTOR_COMPLEX_GUARDS

/**
 * 
 * @author Endri Bezati
 */
abstract class AbstractInstancePrinter extends BlockPrinter {

	private TypePrinterVisitor typePrinter
	
	protected var Actor actor

	protected var Instance instance

	protected String name

	protected var Map<Port, List<String>> fanoutPortConenction

	protected var boolean inlineActions
	
	protected var boolean typeAccuracy
	
	private  Map<String, Object> options
	
	protected boolean isActorComplex

	protected Map<Port,List<Connection>> outgoingPortMap

	new(Instance instance, TypePrinterVisitor typePrinter,Map<String, Object> options) {
		super(typePrinter)
		this.typePrinter = typePrinter
		this.options = options
		this.instance = instance
		this.actor = instance.getActor()
		this.name = instance.simpleName
		this.inlineActions = options.getOrDefault(INLINE_ACTIONS, true) as Boolean;
		this.outgoingPortMap = instance.outgoingPortMap
		getFanoutPortNames
		if (actor.hasAttribute(ACTOR_COMPLEX_GUARDS)) {
			isActorComplex = actor.getAttribute(ACTOR_COMPLEX_GUARDS).getObjectValue() as Boolean;
		}
	}
	
	
	abstract def String getContent()
	
	def getHeaders() {
		'''
			#include <«typePrinter.intIncludeHeaderFile»>
			#include <«typePrinter.channelIncludeHeaderFile»>
			#include "../../code-gen/include/«name».h"
		'''
	}
	
	def getUsing(){
		''''''
	}


		// -- Parameters
	def getInstanceParameters() {
		val List<Var> parameters = instance.getActor.parameters
		'''
			«IF ! parameters.empty»
				// ----------------------------------------------------------------------------
				// -- Parameters
				
				«FOR parameter : parameters»
					«IF parameter.type.list»
						static «IF (parameter.type as TypeList).innermostType.uint»ap_uint<«(parameter.type as TypeList).innermostType.sizeInBits»> «ENDIF»ap_int<«(parameter.type as TypeList).innermostType.sizeInBits»> «parameter.name»«parameter.type.dimensionsExpr.printArrayIndexes» = «parameter.initialValue.doSwitch»;
					«ELSE»
						#define «parameter.name» «parameter.initialValue.doSwitch»
					«ENDIF»
				«ENDFOR»
			«ENDIF»
		'''
	}
	
	// -- Variables
	def getStateVariables() {
		'''
			«IF !actor.stateVars.empty || actor.fsm.states.size > 1 || isActorComplex »
			// ----------------------------------------------------------------------------
			// -- State Variables
			«ENDIF»
			«FOR variable : actor.stateVars»
				«variable.declare»
			«ENDFOR»
			
			«IF actor.fsm.states.size > 1 || isActorComplex»
				// -- Action Selection States
				enum state_t_«instance.simpleName» {«FOR state : actor.fsm.states SEPARATOR ", "»s_«instance.simpleName»_«state.label»«ENDFOR»};
				
				static state_t_«instance.simpleName» state =  s_«instance.simpleName»_«actor.fsm.initialState.name»;
			«ENDIF»
			
			«IF isActorComplex»
				«IF !actor.inputs.empty»// -- Input Ports indexes«ENDIF»
				«FOR port : actor.inputs»
					static int p_«port.name»_token_index = 0;
					static int p_«port.name»_head = 0;
					const  int p_«port.name»_size = «(actor.getStateVar("p_" + port.name).type as TypeList).size»;
				«ENDFOR»
			«ENDIF»
		'''
	}

	override protected declare(Var variable) {
		if (variable.global && variable.initialized && !variable.assignable && !variable.type.list) {
			'''#define «variable.name» «variable.initialValue.doSwitch»'''
		} else {
			val const = if(!variable.assignable && variable.global) "const "
			val global = if(variable.global) "static "
			val type = variable.type
			val dims = variable.type.dimensionsExpr.printArrayIndexes
			val init = if(variable.initialized) " = " + variable.initialValue.doSwitch
			val end = if(variable.global) ";"

			'''«global»«const»«type.doSwitch» «variable.name»«dims»«init»«end»'''
		}
	}
	
	def declare(Var variable, String prefix) {
		if (variable.global && variable.initialized && !variable.assignable && !variable.type.list) {
			'''#define «variable.name» «variable.initialValue.doSwitch»'''
		} else {
			val const = if(!variable.assignable && variable.global) "const "
			val global = if(variable.global) "static "
			val type = variable.type
			val dims = variable.type.dimensionsExpr.printArrayIndexes
			val init = if(variable.initialized) " = " + variable.initialValue.doSwitch
			val end = if(variable.global) ";"

			'''«global»«const»«type.doSwitch» «prefix»_«variable.name»«dims»«init»«end»'''
		}
	}
	
	def declareDDRVariable(Var variable){
		val type = variable.type
		'''volatile «type.doSwitch» * «variable.name»'''
	}
	// -- Function / Procedures
	def getFuncitonsProcedures() {
		'''
			
			// ----------------------------------------------------------------------------
			// -- Function(s) / Procedure(s)
			«FOR proc : actor.procs»
				«IF proc.native»extern«ELSE»static«ENDIF» «proc.returnType.doSwitch» «proc.name»(«proc.parameters.join(", ",
			[variable.declare])»);
			«ENDFOR»
			
			«FOR proc : actor.procs.filter[!native]»
				«proc.print»
			«ENDFOR»
		'''
	}

	def protected declare(Param param) {
		val variable = param.variable
		'''«variable.type.doSwitch» «variable.name»«variable.type.dimensionsExpr.printArrayIndexes»'''
	}

	def protected print(Procedure proc) {
		'''
			static «proc.returnType.doSwitch» «proc.name»(«proc.parameters.join(", ")[declare]») {
				«FOR variable : proc.locals»
					«variable.declare»;
				«ENDFOR»
			
				«FOR block : proc.blocks»
					«block.doSwitch»
				«ENDFOR»
			}
		'''
	}	
	
	def protected print(Procedure proc, String pragma) {
		'''
			static «proc.returnType.doSwitch» «proc.name»(«proc.parameters.join(", ")[declare]») {
			«pragma»	
				«FOR variable : proc.locals»
					«variable.declare»;
				«ENDFOR»
			
				«FOR block : proc.blocks»
					«block.doSwitch»
				«ENDFOR»
			}
		'''
	}	
	
	// -- Actions 
	def getActions() {
		'''
			// ----------------------------------------------------------------------------
			// -- Actions
			«FOR action : actor.actions»
				«action.print»
			«ENDFOR»
		'''
	}

	def print(Action action) {
		var maxOutputRepeat = 1
		for(port : action.outputPattern.ports){
			val repeat = action.outputPattern.getNumTokens(port)
			if(repeat > maxOutputRepeat)
				maxOutputRepeat = repeat
		}
		val output = '''
			«printActionScheduler(action)»
			
			«IF isActorComplex»
				static void «action.body.name»(«FOR port : action.outputPattern.ports SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»_«i»«ENDFOR»«ELSE»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDIF»«ENDFOR»){
			«ELSE»
				static void «action.body.name»(«FOR port : action.inputPattern.ports SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDFOR»«IF !action.inputPattern.ports.empty && !action.outputPattern.empty», «ENDIF»«FOR port : action.outputPattern.ports SEPARATOR ", "»«IF outgoingPortMap.get(port).size > 1»«FOR int i : 0 .. outgoingPortMap.get(port).size - 1 SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»_«i»«ENDFOR»«ELSE»«typePrinter.channelClassName»< «port.type.doSwitch» > &«port.name»«ENDIF»«ENDFOR») {
			«ENDIF»
			«printActionBody(action)»
			}
		'''
		return output
	}
	
	
	def printActionScheduler(Action action){
		'''
			«action.scheduler.print»
		'''
	}
	
	def printActionBody(Action action){
		'''
			«IF !isActorComplex»
				«FOR variable : action.inputPattern.variables»
					«declare(variable, "p")»;
				«ENDFOR»
				«FOR variable : action.outputPattern.variables»
					«declare(variable, "p")»;
				«ENDFOR»
				«actionInputs(action)»
			«ENDIF»
			«FOR variable : action.body.locals»
				«variable.declare»;
			«ENDFOR»
			«FOR block : action.body.blocks»
				«block.doSwitch»
			«ENDFOR»
			«actionOutputs(action)»
		'''
	}
	
	def actionInputs(Action action){
		var maxInputRepeat = 1
		for(port : action.inputPattern.ports){
			val repeat = action.inputPattern.getNumTokens(port)
			if(repeat > maxInputRepeat)
				maxInputRepeat = repeat
		}
		'''
			«IF !action.inputPattern.ports.empty»
				«IF maxInputRepeat == 1»
					«FOR port : action.inputPattern.ports»
						«getRead( port, action.inputPattern.portToVarMap, "0")»
					«ENDFOR»
				«ELSE»
					for(int i = 0; i < «maxInputRepeat»; i++){
						«FOR port: action.inputPattern.ports»
							if( i < «action.inputPattern.getNumTokens(port)»){
								«getRead( port, action.inputPattern.portToVarMap, "i")»
							}
						«ENDFOR»
					}
				«ENDIF»
			«ENDIF»
		'''
	}
	
	def actionOutputs(Action action){
		var maxOutputRepeat = 1
		for(port : action.outputPattern.ports){
			val repeat = action.outputPattern.getNumTokens(port)
			if(repeat > maxOutputRepeat)
				maxOutputRepeat = repeat
		}
		'''
			«IF !action.outputPattern.ports.empty»
				«IF maxOutputRepeat == 1»
					«FOR port : action.outputPattern.ports»
						«IF outgoingPortMap.get(port).size > 1»
							«FOR connection : outgoingPortMap.get(port)»
								«getWriteWithBroadcast(port, action.outputPattern.portToVarMap, "0", outgoingPortMap.get(port).indexOf(connection))»
							«ENDFOR»
						«ELSE»
							«getWrite( port, action.outputPattern.portToVarMap, "0")»
						«ENDIF»
					«ENDFOR»
				«ELSE»
					for(int i = 0; i < «maxOutputRepeat»; i++){
						«FOR port: action.outputPattern.ports»
							«IF action.outputPattern.ports.size > 1»
								if(i < «action.outputPattern.getNumTokens(port)»){
									«IF outgoingPortMap.get(port).size > 1»
										«FOR connection : outgoingPortMap.get(port)»
											«getWriteWithBroadcast(port, action.outputPattern.portToVarMap, "i", outgoingPortMap.get(port).indexOf(connection))»
										«ENDFOR»
									«ELSE»
										«getWrite(port, action.outputPattern.portToVarMap, "i")»
									«ENDIF»
								}
							«ELSE»
								«IF outgoingPortMap.get(port).size > 1»
									«FOR connection : outgoingPortMap.get(port)»
										«getWriteWithBroadcast(port, action.outputPattern.portToVarMap, "i", outgoingPortMap.get(port).indexOf(connection))»
									«ENDFOR»
								«ELSE»
									«getWrite(port, action.outputPattern.portToVarMap, "i")»
								«ENDIF»
							«ENDIF»
						«ENDFOR»
					} 
				«ENDIF»
			«ENDIF»
			«IF isActorComplex»	
				«getActionComplexInput(action)»
			«ENDIF»
		'''
	}
	
	def getActionComplexInput(Action action){
		'''
			«IF !action.inputPattern.ports.empty»
				// -- Update Input Circular Buffer indexes
				«FOR port : action.inputPattern.ports»
					p_«port.name»_head = (p_«port.name»_head + «action.inputPattern.getNumTokens(port)») & (p_«port.name»_size - 1);
					p_«port.name»_token_index -= «action.inputPattern.getNumTokens(port)»; 
				«ENDFOR»
			«ENDIF»
		'''
	}
	
	def getRead(Port port, EMap<Port, Var> portToVarMap, String index){
		'''p_«portToVarMap.get(port).name»[«index»] = «port.name».read();'''
	}
	
	def getWrite(Port port, EMap<Port, Var> portToVarMap, String index){
		'''«port.name».write(p_«portToVarMap.get(port).name»[«index»]);'''
	}
	
	def getWriteWithBroadcast(Port port, EMap<Port, Var> portToVarMap, String index, int broadcastIndex){
		'''«port.name»_«broadcastIndex».write(p_«portToVarMap.get(port).name»[«index»]);'''
	}
	
	
	def getFanoutPortNames() {
		var Map<Port, List<Connection>> portConnection = instance.getOutgoingPortMap()
		fanoutPortConenction = new HashMap
		for (port : actor.outputs) {
			if (portConnection.get(port).size > 1) {
				var List<String> portNames = new ArrayList
				for (connection : portConnection.get(port)) {
					if (connection.target instanceof Instance) {
						portNames.add(port.name + "_f_" + (connection.target as Instance).name);
					} else {
						portNames.add(port.name + "_f_" + (connection.target as Port).name);
					}
				}
				fanoutPortConenction.put(port, portNames)
			} else {
				fanoutPortConenction.put(port, Arrays.asList(port.name))
			}
		}
	}
	

}
