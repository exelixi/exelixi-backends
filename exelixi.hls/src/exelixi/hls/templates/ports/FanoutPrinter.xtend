/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.templates.ports

import exelixi.hls.templates.HLSTemplate
import exelixi.hls.templates.types.TypePrinterVisitor
import java.util.ArrayList
import java.util.List
import net.sf.orcc.df.Actor
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import net.sf.orcc.df.Port
import net.sf.orcc.graph.Edge

/**
 * 
 * @author Endri Bezati
 */
class FanoutPrinter extends HLSTemplate {

	protected var Instance instance

	protected var Actor actor

	protected var Port port

	protected var List<String> fanoutPorts

	private TypePrinterVisitor typePrinter

	new(Instance instance, Port port, TypePrinterVisitor typePrinter) {
		super(typePrinter)
		this.typePrinter = typePrinter
		this.instance = instance;
		this.port = port
		getFanoutPortNames(instance.getOutgoingPortMap().get(port))
	}

	new(Port port, TypePrinterVisitor typePrinter) {
		super(typePrinter)
		this.typePrinter = typePrinter
		this.instance = null;
		this.port = port
		getFanoutInputPortNames(port.outgoing)
	}

	def getContent() {
		'''
			«getFileHeader("Exelixi HLS Port Actor Fanout Code Generation," + if (instance !== null )" Instance: " + instance.name + " Port" + port.name)»
			#include <«typePrinter.intIncludeHeaderFile»>
			#include <«typePrinter.channelIncludeHeaderFile»>
			
			void fanout_«IF instance !== null»«instance.name»_«ENDIF»«port.name»(«typePrinter.channelClassName»< «port.type.doSwitch» > «port.name», «actionSelectionPorts»){
				if(«typePrinter.getEmptyN(port)»){
					«port.type.doSwitch» data = «port.name».read();
					«FOR fport : fanoutPorts»
						«fport».write(data);
					«ENDFOR»
				}
			}
		'''
	}

	def actionSelectionPorts() {
		'''«FOR fport : fanoutPorts SEPARATOR ", "»«typePrinter.channelClassName»< «port.type.doSwitch» > &«fport»«ENDFOR»'''
	}

	def getFanoutPortNames(List<Connection> conenctions) {
		fanoutPorts = new ArrayList
		for (Connection connection : conenctions) {
			if (connection.target instanceof Instance) {
				fanoutPorts.add(port.name + "_" + (connection.target as Instance).name + "_" +
					(connection.targetPort as Port).name);
			} else {
				fanoutPorts.add(port.name + "_" + (connection.target as Port).name);
			}
		}
	}

	def getFanoutInputPortNames(List<Edge> edges) {
		fanoutPorts = new ArrayList
		for (Edge edge : edges) {
			var Connection connection = edge as Connection
			if (connection.target instanceof Instance) {
				fanoutPorts.add(port.name + "_" + (connection.target as Instance).name + "_" +
					(connection.targetPort as Port).name);
			} else {
				fanoutPorts.add(port.name + "_" + (connection.target as Port).name);
			}
		}
	}

}
