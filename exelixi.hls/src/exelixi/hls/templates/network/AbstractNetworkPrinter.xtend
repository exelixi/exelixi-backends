/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.templates.network

import static exelixi.hls.backend.Constants.BIT_ACCURATE_TYPES
import static exelixi.hls.backend.Constants.PROFILE_DATAFLOW_NETWORK
import static net.sf.orcc.OrccLaunchConstants.*
import net.sf.orcc.df.Network
import java.util.Map
import java.text.SimpleDateFormat
import java.util.Date
import net.sf.orcc.df.Port
import net.sf.orcc.df.Connection
import net.sf.orcc.df.Instance
import java.util.HashMap

/**
 * 
 * @author Endri Bezati
 */
abstract class AbstractNetworkPrinter {

	protected var Network network

	protected var boolean typeAccuracy
	
	protected var Map<Connection, String> queueNames
	
	protected var int fifoSize

	protected var boolean enableProfiling

	new(Network network, Map<String, Object> options) {
		this.network = network
		if (options.containsKey(BIT_ACCURATE_TYPES)) {
			this.typeAccuracy = options.get(BIT_ACCURATE_TYPES) as Boolean
		}else{
			this.typeAccuracy = false
		}
		if (options.containsKey(FIFO_SIZE)) {
			fifoSize = options.get(FIFO_SIZE) as Integer
		} else {
			fifoSize = DEFAULT_FIFO_SIZE
		}
		if (options.containsKey(PROFILE_DATAFLOW_NETWORK)) {
			this.enableProfiling = options.get(PROFILE_DATAFLOW_NETWORK) as Boolean
		}else{
			this.enableProfiling = false
		}
		
		retrieveQueueNames
	}

	def getContent() {
		'''
			«getFileHeader("Exelixi Network Code Generation: " + network.label)»
			`timescale 1ns/1ps
			//`default_nettype none
			
			«composition»
		'''
	}
	
	
	def getComposition() {
		'''
			module «network.simpleName»(
				«getModulePortsNames()»
			);
				«getParameters»
			
				«getWires()»
				
				«getQueues»
			
				«getReadersWriters()»
				
				«getInstances»
				
				«getIdle»

			endmodule
		'''
	}


	// ------------------------------------------------------------------------
	// -- Network Input - Output Ports 
	def getModulePortsNames() {
		'''
			«IF !network.inputs.empty»
				// -- Actor Composition Input«IF network.inputs.size > 1»s«ENDIF»
				«FOR port: network.inputs»
					«getPortDeclaration(port,true)»
				«ENDFOR»
			«ENDIF»
			«IF !network.outputs.empty»
				// -- Actor Composition Output«IF network.outputs.size > 1»s«ENDIF»
				«FOR port: network.outputs»
					«getPortDeclaration(port,false)»
				«ENDFOR»
			«ENDIF»
			
			// -- System Signals 
			«getSystemSignals»
		'''
	}
	
	abstract def String getSystemSignals()
	
	abstract def String getPortDeclaration(Port port, boolean isInput)


	// ------------------------------------------------------------------------
	// -- Wires
	abstract def String getWires()
	
	// ------------------------------------------------------------------------
	// -- Parameters
	abstract def String getParameters()

	// ------------------------------------------------------------------------
	// -- FIFOs
	def getQueues() {
		'''
			// --------------------------------------------------------------------------
			// -- FIFOs
			«FOR connection : network.connections SEPARATOR "\n"»
				«getQueue(connection)»
			«ENDFOR»
		'''
	}

	abstract def String getQueue(Connection connection)

	// ------------------------------------------------------------------------
	// -- Port Reader / Writer
	def getReadersWriters() {
		'''
			«IF !network.inputs.empty»
				// --------------------------------------------------------------------------
				// -- Port Readers
				«FOR port: network.inputs SEPARATOR "\n"»
					«getPortReader(port)»
				«ENDFOR»
			«ENDIF»
			
			«IF !network.outputs.empty»
				// --------------------------------------------------------------------------
				// -- Port Writers
				«FOR port: network.outputs SEPARATOR "\n"»
					«getPortWriter(port)»
				«ENDFOR»
			«ENDIF»
		'''
	}
	
	abstract def String getPortReader(Port port)
	
	abstract def String getPortWriter(Port port)


	// ------------------------------------------------------------------------
	// -- Instances
	def getInstances() {
		'''
			// --------------------------------------------------------------------------
			// -- Actor Instance
			«FOR instance : network.children.filter(typeof(Instance)) SEPARATOR "\n"»
				«getInstance(instance)»
			«ENDFOR»
		'''
	}

	abstract def String getInstance(Instance instance)

	// ------------------------------------------------------------------------
	// -- Done

	abstract def String getIdle()

	// ------------------------------------------------------------------------
	// -- Helper definitions
	def getFileHeader(String comment) {
		var dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		var date = new Date();
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- Exelixi Design Systems 
			// -- Copyright (C) 2015 Exelixi. All rights reserved.
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- «comment»
			// -- Date: «dateFormat.format(date)»
			// ----------------------------------------------------------------------------
		'''
	}
	
	// -- Helper Methods
	def retrieveQueueNames() {
		queueNames = new HashMap<Connection, String>
		for (connection : network.connections) {
			if (connection.source instanceof Port) {
				if (connection.target instanceof Instance) {
					queueNames.put(connection,
						"q_" + (connection.source as Port).name + "_" + (connection.target as Instance).name + "_" +
							connection.targetPort.name)
				}
			} else if (connection.source instanceof Instance) {
				if (connection.target instanceof Port) {
					queueNames.put(connection,
						"q_" + (connection.source as Instance).name + "_" + connection.sourcePort.name + "_" +
							(connection.target as Port).name)
				} else if (connection.target instanceof Instance) {
					queueNames.put(connection,
						"q_" + (connection.source as Instance).name + "_" + connection.sourcePort.name + "_" +
							(connection.target as Instance).name + "_" + connection.targetPort.name)
				}

			}
		}
	}
}
