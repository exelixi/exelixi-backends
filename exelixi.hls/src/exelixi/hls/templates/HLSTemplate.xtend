/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.templates

import exelixi.hls.templates.types.TypePrinterVisitor
import net.sf.orcc.backends.c.CTemplate
import net.sf.orcc.ir.TypeBool
import net.sf.orcc.ir.TypeFloat
import net.sf.orcc.ir.TypeInt
import net.sf.orcc.ir.TypeUint
import net.sf.orcc.ir.Var
import net.sf.orcc.ir.Type
import net.sf.orcc.df.Port

/**
 * 
 * @author Endri Bezati
 */
class HLSTemplate extends CTemplate {

	protected TypePrinterVisitor typePrinter

	new(TypePrinterVisitor typePrinter) {
		this.typePrinter = typePrinter
	}

	def getFileHeader(String comment) {
		'''
			// ----------------------------------------------------------------------------
			//  _____          _ _      _ 
			// | ____|_  _____| (_)_  _(_)
			// |  _| \ \/ / _ \ | \ \/ / |
			// | |___ >  <  __/ | |>  <| |
			// |_____/_/\_\___|_|_/_/\_\_|
			// ----------------------------------------------------------------------------
			// -- Exelixi Design Systems 
			// -- Copyright (C) 2016 Exelixi. All rights reserved.
			// ----------------------------------------------------------------------------
			// -- This file is generated automatically by Exelixi, please do not modify
			// -- «comment»
			// ----------------------------------------------------------------------------
		'''
	}

	override caseTypeBool(TypeBool type) {
		'''«typePrinter.doSwitch(type)»'''
	}

	override caseTypeInt(TypeInt type) {
		'''«typePrinter.doSwitch(type)»'''
	}

	override caseTypeUint(TypeUint type) {
		'''«typePrinter.doSwitch(type)»'''
	}

	override caseTypeFloat(TypeFloat type) {
		'''«typePrinter.doSwitch(type)»'''
	}

	def declareExternalVariable(Var variable) {
		val type = variable.type
		'''volatile «type.doSwitch» * «variable.name»'''
	}

	def int getFlatListDepth(Type type) {
		var int r = 1;
		if (type.isList) {
			for(int dim : type.getDimensions){
				r = r*dim;
			}
		}
		return r;
	}

	def getTypeAXI(Port port){
		'''ap_axi«IF port.type.isUint»u«ELSE»s«ENDIF»<«port.type.sizeInBits», 4, 5, 5>'''
	}
}
