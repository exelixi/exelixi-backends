/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.partitioner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.sf.orcc.df.Connection;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Network;
import net.sf.orcc.df.Port;
import net.sf.orcc.graph.Edge;
import net.sf.orcc.graph.Vertex;

/**
 * 
 * This class offers methods to retrieves the instances from a given partition
 * and the list of connection from a partition to another.
 * 
 * @author Endri Bezati
 *
 */
public class Partitioner {

	private Boolean hasHeterogeneousPartitions;

	private Map<String, String> mapping;

	private Map<String, List<Connection>> paritionToPartitionConnection;

	private Map<String, List<Instance>> partitionInstance;

	public Partitioner(Network network, Map<String, String> mapping) {
		this.mapping = new HashMap<String, String>(mapping);
		this.partitionInstance = new LinkedHashMap<String, List<Instance>>();
		this.paritionToPartitionConnection = new LinkedHashMap<String, List<Connection>>();
		this.hasHeterogeneousPartitions = false;

		if (!mapping.isEmpty()) {
			String tmpName = "";
			for (String name : mapping.values()) {
				if (tmpName.isEmpty()) {
					tmpName = name;
				} else {
					if (!name.equals(tmpName)) {
						hasHeterogeneousPartitions = true;
						break;
					}
				}
			}
			// -- Fill Partition Instance Map
			for (String instanceHierarchicalName : mapping.keySet()) {

				Instance instance = getInstance(network, instanceHierarchicalName);
				if (instance != null) {
					String platform = mapping.get(instanceHierarchicalName);
					if (partitionInstance.containsKey(platform)) {
						partitionInstance.get(platform).add(instance);
					} else {
						List<Instance> instances = new ArrayList<Instance>();
						instances.add(instance);
						partitionInstance.put(platform, instances);
					}
				}
			}

			if (network.getInputs().isEmpty() && network.getOutputs().isEmpty()) {
				// -- Fill Partition To Partition Connections
				for (Edge edge : network.getEdges()) {
					Connection connection = (Connection) edge;
					Instance srcInstance = (Instance) connection.getSource();
					Instance tgtInstance = (Instance) connection.getTarget();
					String partionA = mapping.get(srcInstance.getHierarchicalName());
					String partionB = mapping.get(tgtInstance.getHierarchicalName());
					String paritionToPartition = partionA + "_to_" + partionB;
					if (paritionToPartitionConnection.containsKey(paritionToPartition)) {
						paritionToPartitionConnection.get(paritionToPartition).add(connection);
					} else {
						List<Connection> connections = new ArrayList<Connection>();
						connections.add(connection);
						paritionToPartitionConnection.put(paritionToPartition, connections);
					}
				}
			}

		} else {
			hasHeterogeneousPartitions = false;
		}

	}

	/**
	 * Returns a list of connection from a partition source to a partition
	 * target
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public List<Connection> getConnections(String source, String target) {
		String paritionToPartition = source + "_to_" + target;
		if (paritionToPartitionConnection.containsKey(paritionToPartition)) {
			return paritionToPartitionConnection.get(paritionToPartition);
		} else {
			return new ArrayList<Connection>();
		}
	}

	/**
	 * Get connections with Broadcast
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public Map<Connection, List<Connection>> getConnectionToConnectionBd(String source, String target) {
		List<Instance> instances = partitionInstance.get(source);
		if (instances == null) {
			instances = new ArrayList<Instance>();
		}
		Map<Connection, List<Connection>> connectionToConnectionBd = new HashMap<>();
		for (Instance instance : instances) {
			for (Port port : instance.getActor().getOutputs()) {
				if (instance.getOutgoingPortMap().get(port).size() > 1) {
					List<Connection> connections = new ArrayList<>();
					for (Connection connection : instance.getOutgoingPortMap().get(port)) {
						Instance tgt = (Instance) connection.getTarget();
						if (partitionInstance.containsKey(target)) {
							if (partitionInstance.get(target).contains(tgt)) {
								connections.add(connection);
							}
						}
					}
					if (!connections.isEmpty()) {
						connectionToConnectionBd.put(connections.get(0), connections);
					}
				} else {
					List<Connection> connections = new ArrayList<>();
					Connection connection = instance.getOutgoingPortMap().get(port).get(0);
					Instance tgt = (Instance) connection.getTarget();
					if (partitionInstance.containsKey(target)) {
						if (partitionInstance.get(target).contains(tgt)) {
							// connections.add(connection);
							connections.add(instance.getOutgoingPortMap().get(port).get(0));
							connectionToConnectionBd.put(connection, connections);
						}
					}
				}
			}
		}

		return connectionToConnectionBd;
	}

	/**
	 * Returns true if the partitioning is done on heterogeneous platforms
	 * 
	 * @return
	 */
	public Boolean getHasHeterogeneousPartitions() {
		return hasHeterogeneousPartitions;
	}

	/**
	 * Get the instance based on the Hierarchical Name
	 * 
	 * @param network
	 * @param instanceHierarchicalName
	 * @return
	 */
	private Instance getInstance(Network network, String instanceHierarchicalName) {
		for (Vertex vertex : network.getChildren()) {
			if (vertex instanceof Instance) {
				Instance instance = (Instance) vertex;
				if (instance.getHierarchicalName().equals(instanceHierarchicalName)) {
					return instance;
				}
			}
		}

		return null;
	}

	/**
	 * Get the list of instances from a given partition
	 * 
	 * @param partition
	 * @return
	 */
	public List<Instance> getInstances(String partition) {

		if (partitionInstance.containsKey(partition)) {
			return partitionInstance.get(partition);
		}
		return null;
	}

	/**
	 * Return true if an instance is on a given partition
	 * 
	 * @param instance
	 * @param partition
	 * @return
	 */
	public Boolean isInstanceOnPartition(Instance instance, String partition) {
		if (mapping.containsKey(instance.getHierarchicalName())) {
			if (mapping.get(instance.getHierarchicalName()).equals(partition)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Returns true is Mapping is Empty
	 * 
	 * @return
	 */
	public Boolean isMappingEmpty() {
		if (!mapping.isEmpty()) {
			for (String partition : mapping.values()) {
				if (partition.equals("")) {
					return true;
				}
			}
			return false;
		} else {
			return true;
		}
	}

	public Boolean containsPartition(String name) {
		if (mapping.containsValue(name)) {
			return true;
		}
		return false;
	}

}
