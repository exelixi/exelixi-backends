/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.transformations;

import java.util.Iterator;

import net.sf.orcc.df.Action;
import net.sf.orcc.df.Actor;
import net.sf.orcc.df.DfFactory;
import net.sf.orcc.df.FSM;
import net.sf.orcc.df.State;
import net.sf.orcc.df.Transition;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.graph.Edge;
import net.sf.orcc.ir.util.IrUtil;

import org.eclipse.emf.common.util.EList;

/**
 * This visitor will add an FSM to an actor if it does not have one. Will also
 * add all initialized actions to a state that is going to be executed only
 * once.
 * 
 * @author Endri Bezati
 *
 */
public class ActorAddFSM extends DfVisitor<Void> {

	@Override
	public Void caseActor(Actor actor) {

		// -- Add a finite state machine if an actor does not have one
		if (!actor.hasFsm()) {
			FSM fsm = DfFactory.eINSTANCE.createFSM();
			State sActor = DfFactory.eINSTANCE.createState(actor.getSimpleName());

			if (!actor.getInitializes().isEmpty()) {
				State init = DfFactory.eINSTANCE.createState(actor.getSimpleName() + "Init");
				fsm.getStates().add(init);

				Iterator<Action> iter = actor.getInitializes().listIterator();

				while (iter.hasNext()) {
					Action action = iter.next();
					Transition t = null;
					Action cAction = IrUtil.copy(action);
					if (iter.hasNext()) {
						// FIXME : If actor has two initializes
						t = DfFactory.eINSTANCE.createTransition(init, cAction, init);
					} else {
						t = DfFactory.eINSTANCE.createTransition(init, cAction, sActor);
					}
					fsm.add(t);
					actor.getActions().add(cAction);
				}

				fsm.setInitialState(init);
				actor.getInitializes().clear();

				fsm.getStates().add(sActor);
				for (Action action : actor.getActionsOutsideFsm()) {
					Transition t = DfFactory.eINSTANCE.createTransition(sActor, action, sActor);
					fsm.add(t);
				}
				actor.setFsm(fsm);
				actor.getActionsOutsideFsm().clear();
			} else {
				fsm.getStates().add(sActor);
				for (Action action : actor.getActionsOutsideFsm()) {
					Transition t = DfFactory.eINSTANCE.createTransition(sActor, action, sActor);
					fsm.add(t);
				}
				fsm.setInitialState(sActor);
				actor.setFsm(fsm);
				actor.getActionsOutsideFsm().clear();
			}
		} else {
			FSM fsm = actor.getFsm();
			if (!actor.getInitializes().isEmpty()) {
				State init = DfFactory.eINSTANCE.createState(actor.getSimpleName() + "Init");
				fsm.getStates().add(init);
				State oldInitState = fsm.getInitialState();

				Iterator<Action> iter = actor.getInitializes().listIterator();

				while (iter.hasNext()) {
					Action action = iter.next();
					Transition t = null;
					Action cAction = IrUtil.copy(action);
					if (iter.hasNext()) {
						t = DfFactory.eINSTANCE.createTransition(init, cAction, init);
					} else {
						t = DfFactory.eINSTANCE.createTransition(init, cAction, oldInitState);
					}
					fsm.add(t);
					actor.getActions().add(cAction);
				}

				fsm.setInitialState(init);
				actor.getInitializes().clear();
			}

			for (State state : fsm.getStates()) {
				int index = 0;
				for (Action action : actor.getActionsOutsideFsm()) {
					Transition t = DfFactory.eINSTANCE.createTransition(state, action, state);
					EList<Edge> edges = state.getOutgoing();
					int oldIndex = edges.indexOf(t);
					edges.move(index, oldIndex);
					actor.getActions().move(index, action);
					index++;
				}
			}
			actor.getActionsOutsideFsm().clear();
		}

		return super.caseActor(actor);
	}

}
