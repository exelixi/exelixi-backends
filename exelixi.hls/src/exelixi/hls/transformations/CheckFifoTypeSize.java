/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.transformations;

import net.sf.orcc.df.Connection;
import net.sf.orcc.df.Instance;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.ir.Type;
import net.sf.orcc.util.OrccLogger;

/**
 * This visitor checks if the bit size of the source and destination port are
 * the same, otherwise a warning is given
 * 
 * @author Endri Bezati
 */
public class CheckFifoTypeSize extends DfVisitor<Void> {

	@Override
	public Void caseConnection(Connection connection) {
		if (connection.getSource() instanceof Port) {
			if (connection.getTarget() instanceof Instance) {
				Type sourceType = ((Port) connection.getSource()).getType();
				Type targetType = connection.getTargetPort().getType();
				if (sourceType.getSizeInBits() != targetType.getSizeInBits()) {
					OrccLogger.warnln("Connection :" + connection + " has different size in Bits !!!");
					OrccLogger.warnln("Source :" + sourceType);
					OrccLogger.warnln("Target :" + targetType);
				}
			}
		} else if (connection.getSource() instanceof Instance) {
			if (connection.getTarget() instanceof Port) {
				Type sourceType = connection.getSourcePort().getType();
				Type targetType = ((Port) connection.getTarget()).getType();

				if (sourceType.getSizeInBits() != targetType.getSizeInBits()) {
					OrccLogger.warnln("Connection :" + connection + " has different size in Bits !!!");
					OrccLogger.warnln("Source :" + sourceType);
					OrccLogger.warnln("Target :" + targetType);
				}

			} else if (connection.getTarget() instanceof Instance) {
				Type sourceType = connection.getSourcePort().getType();
				Type targetType = connection.getTargetPort().getType();

				if (sourceType.getSizeInBits() != targetType.getSizeInBits()) {
					OrccLogger.warnln("Connection :" + connection + " has different size in Bits !!!");
					OrccLogger.warnln("Source :" + sourceType);
					OrccLogger.warnln("Target :" + targetType);
				}
			}
		}
		return null;
	}

}
