/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.transformations;

import net.sf.orcc.df.Actor;
import net.sf.orcc.ir.BlockIf;
import net.sf.orcc.ir.BlockWhile;
import net.sf.orcc.ir.ExprBinary;
import net.sf.orcc.ir.Instruction;
import net.sf.orcc.ir.OpBinary;
import net.sf.orcc.ir.Procedure;
import net.sf.orcc.ir.Type;
import net.sf.orcc.ir.util.AbstractIrVisitor;
import net.sf.orcc.util.OrccLogger;
import net.sf.orcc.util.util.EcoreHelper;

/**
 * 
 * @author Endri Bezati
 */
public class LimitedTypeChacking extends AbstractIrVisitor<Void> {

	public LimitedTypeChacking() {
		super(true);
	}

	@Override
	public Void caseExprBinary(ExprBinary expr) {
		OpBinary op = expr.getOp();
		Type e1 = expr.getE1().getType();
		Type e2 = expr.getE2().getType();
		int bitSizeE1 = e1.getSizeInBits();
		int bitSizeE2 = e2.getSizeInBits();
		boolean isIntE1 = e1.isInt();
		boolean isIntE2 = e2.isInt();
		
		Actor actor = EcoreHelper.getContainerOfType(expr, Actor.class);
		String actorName = actor.getSimpleName();
		
		
		int lineNumber = -1;
		Procedure procedure = EcoreHelper.getContainerOfType(expr, Procedure.class);
		String procedureName = "";
		if(procedure !=null)
			procedureName = procedure.getName();
		
		if(expr.eContainer() instanceof Instruction){
			Instruction inst = (Instruction) expr.eContainer();
			lineNumber = inst.getLineNumber();
		} else if (expr.eContainer() instanceof BlockIf){
			BlockIf blockIf = (BlockIf) expr.eContainer(); 
			lineNumber = blockIf.getLineNumber();
		}  else if (expr.eContainer() instanceof BlockWhile){
			BlockWhile blockWhile = (BlockWhile) expr.eContainer();
			lineNumber = blockWhile.getLineNumber();

		}
		
		if (op.isComparison()) {
			if (bitSizeE1 != bitSizeE2 || isIntE1 != isIntE2) {
				OrccLogger.warnln("Actor: " + actorName + 
						", Procedure: " + procedureName + 
						", line: " + lineNumber +
						", E1 type: " + (isIntE1 ? "int" : "uint") + ", size: " + bitSizeE1 + 
						", E2 type: " + (isIntE2 ? "int" : "uint") + ", size: " + bitSizeE2);
			}

		} else if (op == OpBinary.SHIFT_LEFT) {
			if (bitSizeE1 != bitSizeE2 || isIntE1 != isIntE2) {
				OrccLogger.warnln("Actor: " + actorName + 
						", Procedure: " + procedureName + 
						", line: " + lineNumber +
						", E1 type: " + (isIntE1 ? "int" : "uint") + ", size: " + bitSizeE1 + 
						", E2 type: " + (isIntE2 ? "int" : "uint") + ", size: " + bitSizeE2);
			}
		} else if (op == OpBinary.SHIFT_RIGHT) {
			if (bitSizeE1 != bitSizeE2 || isIntE1 != isIntE2) {
				OrccLogger.warnln("Actor: " + actorName + 
						", Procedure: " + procedureName + 
						", line: " + lineNumber +
						", E1 type: " + (isIntE1 ? "int" : "uint") + ", size: " + bitSizeE1 + 
						", E2 type: " + (isIntE2 ? "int" : "uint") + ", size: " + bitSizeE2);
			}
		}

		return null;
	}

}
