/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.transformations;

import net.sf.orcc.backends.ir.BlockFor;
import net.sf.orcc.df.Actor;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.ir.BlockWhile;
import net.sf.orcc.ir.util.AbstractIrVisitor;

import org.eclipse.emf.ecore.EObject;

/**
 * This transformation lables evry loop in an actor
 * 
 * @author Endri Bezati
 *
 */
public class LoopLabeler extends DfVisitor<Void> {

	private class Labeler extends AbstractIrVisitor<Void> {

		@Override
		public Void caseBlockWhile(BlockWhile blockWhile) {
			blockWhile.setAttribute("loopLabel", loopIndex);
			loopIndex++;
			return super.caseBlockWhile(blockWhile);
		}

		public Void caseBlockFor(BlockFor blockFor) {
			blockFor.setAttribute("loopLabel", loopIndex);
			loopIndex++;
			doSwitch(blockFor.getBlocks());
			doSwitch(blockFor.getJoinBlock());
			return null;
		}

		@Override
		public Void defaultCase(EObject object) {
			if (object instanceof BlockFor) {
				return caseBlockFor((BlockFor) object);
			}
			return super.defaultCase(object);
		}

	}
	
	/** Loop Counter **/
	private Integer loopIndex;

	public LoopLabeler(){
		Labeler labeler =  new Labeler();
		this.irVisitor = labeler;
	}

	@Override
	public Void caseActor(Actor actor) {
		loopIndex = 0;
		return super.caseActor(actor);
	}
	
	
}
