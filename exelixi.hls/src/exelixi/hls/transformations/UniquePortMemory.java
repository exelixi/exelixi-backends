/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.transformations;

import static exelixi.hls.backend.Constants.ACTOR_COMPLEX_GUARDS;

import java.util.Comparator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import exelixi.hls.util.MathUtil;
import net.sf.orcc.df.Action;
import net.sf.orcc.df.Actor;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.impl.PatternImpl;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.ir.Def;
import net.sf.orcc.ir.InstLoad;
import net.sf.orcc.ir.InstStore;
import net.sf.orcc.ir.IrFactory;
import net.sf.orcc.ir.Use;
import net.sf.orcc.ir.Var;
import net.sf.orcc.ir.util.AbstractIrVisitor;
import net.sf.orcc.ir.util.IrUtil;
import net.sf.orcc.util.Void;
import net.sf.orcc.util.util.EcoreHelper;

/**
 * This transformations replaces all port variables with a unique one. With the
 * purpose of the scheduler to attribute values on this new variables.
 * 
 * @author Endri Bezati
 *
 */
public class UniquePortMemory extends DfVisitor<Void> {

	protected Integer maxRepeatSize;

	private boolean chnageVariable;

	private boolean toThePwoOfTwo;

	public UniquePortMemory(Integer fifosize, boolean toThePwoOfTwo) {
		this.maxRepeatSize = fifosize;
		this.chnageVariable = false;
		this.toThePwoOfTwo = toThePwoOfTwo;
	}

	public UniquePortMemory(Integer fifosize) {
		this(fifosize, true);
	}

	private class ReplaceVar extends AbstractIrVisitor<Void> {

		@Override
		public Void caseInstLoad(InstLoad load) {
			Var source = load.getSource().getVariable();
			PatternImpl pattern = EcoreHelper.getContainerOfType(source, PatternImpl.class);
			if (pattern != null) {
				Port port = (Port) pattern.getVarToPortMap().get(source);
				if (newPortVar.containsKey(port)) {
					Var var = newPortVar.get(port);
					Use use = IrFactory.eINSTANCE.createUse(var);
					load.setSource(use);
				}
			}
			return null;
		}

		@Override
		public Void caseInstStore(InstStore store) {
			Var target = store.getTarget().getVariable();
			PatternImpl pattern = EcoreHelper.getContainerOfType(target, PatternImpl.class);
			if (pattern != null) {
				Port port = (Port) pattern.getVarToPortMap().get(target);
				if (newPortVar.containsKey(port)) {
					Var var = newPortVar.get(port);
					Def def = IrFactory.eINSTANCE.createDef(var);
					store.setTarget(def);
				}
			}

			return null;
		}

	}

	private class MaxPortDepth extends DfVisitor<TreeMap<Port, Integer>> {

		/**
		 * Maximum number of the repeat depth for each port
		 */
		private TreeMap<Port, Integer> portDepth;

		@Override
		public TreeMap<Port, Integer> caseAction(Action action) {

			for (Port port : action.getInputPattern().getPorts()) {
				int numTokens = action.getInputPattern().getNumTokensMap().get(port);
				maxDepth(port, numTokens);
			}

			for (Port port : action.getOutputPattern().getPorts()) {
				int numTokens = action.getOutputPattern().getNumTokensMap().get(port);
				maxDepth(port, numTokens);
			}
			return null;
		}

		@Override
		public TreeMap<Port, Integer> caseActor(Actor actor) {
			portDepth = new TreeMap<Port, Integer>(new PortNameComp());
			super.caseActor(actor);
			return portDepth;
		}

		private void maxDepth(Port port, int numTokens) {
			if (portDepth.containsKey(port)) {
				if (portDepth.get(port) < numTokens) {
					portDepth.put(port, numTokens);
				}
			} else {
				portDepth.put(port, numTokens);
			}
		}

	}

	SortedMap<Port, Var> newPortVar;

	@Override
	public Void caseActor(Actor actor) {
		boolean isActorComplex = false;
		if (actor.hasAttribute(ACTOR_COMPLEX_GUARDS)) {
			isActorComplex = (boolean) actor.getAttribute(ACTOR_COMPLEX_GUARDS).getObjectValue();
		}

		if (isActorComplex) {
			// -- Retrieve the port depth for each
			Map<Port, Integer> portDepth = new MaxPortDepth().doSwitch(actor);

			newPortVar = new TreeMap<Port, Var>(new PortNameComp());
			for (Port port : portDepth.keySet()) {
				int nbrElements = portDepth.get(port);
				int size = toThePwoOfTwo ? MathUtil.nearestPowTwo(nbrElements) : nbrElements;
				Var var = IrFactory.eINSTANCE.createVar(
						IrFactory.eINSTANCE.createTypeList(size, IrUtil.copy(port.getType())), "p_" + port.getName(),
						true, 0);
				var.setAttribute("portStateVar", true);
				actor.getStateVars().add(var);
				newPortVar.put(port, var);
			}

			// -- Now modify each Load and store associate to a Port
			if (chnageVariable) {
				for (Action action : actor.getActions()) {
					new ReplaceVar().doSwitch(action.getScheduler());
					new ReplaceVar().doSwitch(action.getBody());
				}
			}
			actor.setAttribute("portDepth", portDepth);

		}
		return null;
	}

	class PortNameComp implements Comparator<Port> {

		@Override
		public int compare(Port o1, Port o2) {
			return o1.getName().compareTo(o2.getName());
		}

	}

}
