/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.transformations;

import static exelixi.hls.backend.Constants.PORT_COMPLEXITY;

import java.util.ArrayList;
import java.util.List;

import net.sf.orcc.df.Action;
import net.sf.orcc.df.Actor;
import net.sf.orcc.df.FSM;
import net.sf.orcc.df.Pattern;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.State;
import net.sf.orcc.df.Transition;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.graph.Edge;

/**
 * A port is considered complex if and only if there is a peek on the port or if
 * it exist a state with multiple transitions and one of those transitions have
 * a repeat.
 * 
 * @author Endri Bezati
 *
 */
public class PortComplexity extends DfVisitor<Void> {

	@Override
	public Void caseActor(Actor actor) {

		// -- Check for Actions with Peek Patterns
		for (Action action : actor.getActions()) {
			Pattern peekPattern = action.getPeekPattern();

			if (!peekPattern.isEmpty()) {
				List<Port> ports = peekPattern.getPorts();
				for (Port port : ports) {
					port.setAttribute(PORT_COMPLEXITY, true);
				}
				break;
			}
		}

		// -- Check if it exist a transition in a state with many transitions
		// that contains a repeat
		if (actor.hasFsm()) {
			FSM fsm = actor.getFsm();
			for (State state : fsm.getStates()) {
				List<Edge> outgoing = state.getOutgoing();

				List<Action> actions = new ArrayList<Action>();
				for (Edge edge : outgoing) {
					Transition transition = (Transition) edge;
					actions.add(transition.getAction());
				}

				for (Action action : actions) {
					Pattern inputPattern = action.getInputPattern();
					for (Port port : inputPattern.getPorts()) {
						if (inputPattern.getNumTokens(port) > 1) {
							if (!port.hasAttribute(PORT_COMPLEXITY)) {
								port.setAttribute(PORT_COMPLEXITY, true);
							}
						}
					}
				}
			}

		}
		
		return null;
	}

}
