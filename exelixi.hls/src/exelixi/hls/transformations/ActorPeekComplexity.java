/*
 * Software License Agreement (BSD License)
 * Copyright (c) 2011-2018
 * Endri Bezati
 * web   : http://gramm.epfl.ch
 * email : endri.bezati@epfl.ch
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of the EPFL nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package exelixi.hls.transformations;

import static exelixi.hls.backend.Constants.ACTION_SCHEDULER_PEEKS;
import static exelixi.hls.backend.Constants.ACTOR_ACTION_SELECTION_PEEKS;
import static exelixi.hls.backend.Constants.ACTOR_COMPLEX_GUARDS;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import net.sf.orcc.df.Action;
import net.sf.orcc.df.Actor;
import net.sf.orcc.df.Pattern;
import net.sf.orcc.df.Port;
import net.sf.orcc.df.util.DfVisitor;
import net.sf.orcc.ir.ExprInt;
import net.sf.orcc.ir.Expression;
import net.sf.orcc.ir.InstLoad;
import net.sf.orcc.ir.Procedure;
import net.sf.orcc.ir.Var;
import net.sf.orcc.ir.util.AbstractIrVisitor;

/**
 * This visitor checks if an actor has peek and input repeats, if yes then this
 * actor is considered as a complex actor.
 * 
 * @author Endri Bezati
 *
 */
public class ActorPeekComplexity extends DfVisitor<Void> {

	private Pattern peekPattern;

	private Boolean isComplex;

	private Map<Port, List<Integer>> peekIndexPort;

	private Map<Port, List<Integer>> actorPeekIndexPorts;

	class InnerVisitor extends AbstractIrVisitor<Void> {

		@Override
		public Void caseInstLoad(InstLoad load) {
			Var variable = load.getSource().getVariable();
			if (peekPattern.getVariables().contains(variable)) {
				List<Expression> indexes = load.getIndexes();
				if (indexes.size() > 1) {
					isComplex = false;
				} else {
					Expression index_0 = indexes.get(0);
					if (index_0.isExprInt()) {
						int index = ((ExprInt) index_0).getIntValue();
						Port port = peekPattern.getVarToPortMap().get(variable);
						if (peekIndexPort.containsKey(port)) {
							if (!peekIndexPort.get(port).contains(index)) {
								peekIndexPort.get(port).add(index);
							}
						} else {
							List<Integer> peekIndexes = new ArrayList<Integer>();
							peekIndexes.add(index);
							peekIndexPort.put(port, peekIndexes);
						}
						isComplex = false;
					} else {
						isComplex = true;
					}
				}

			}

			return null;
		}

	}

	@Override
	public Void caseActor(Actor actor) {
		actorPeekIndexPorts = new HashMap<Port, List<Integer>>();
		isComplex = false;

		for (Action action : actor.getActions()) {
			peekPattern = action.getPeekPattern();

			if (!peekPattern.isEmpty()) {
				peekIndexPort = new HashMap<Port, List<Integer>>();
				Procedure scheduler = action.getScheduler();
				new InnerVisitor().doSwitch(scheduler);

				if (!isComplex) {
					Map<Port, List<Integer>> treeMap = new TreeMap<Port, List<Integer>>(new Comparator<Port>() {

						@Override
						public int compare(Port o1, Port o2) {
							if (o1.getName().equals(o2.getName())) {
								return 0;
							}
							return o1.getName().compareTo(o2.getName());
						}
					}

					);

					for (Port port : peekIndexPort.keySet()) {
						treeMap.put(port, peekIndexPort.get(port));
					}

					scheduler.setAttribute(ACTION_SCHEDULER_PEEKS, treeMap);
					for (Port port : treeMap.keySet()) {
						if (actorPeekIndexPorts.containsKey(port)) {
							for (Integer integer : treeMap.get(port)) {
								if (!actorPeekIndexPorts.get(port).contains(integer)) {
									actorPeekIndexPorts.get(port).add(integer);
								}
							}
						} else {
							List<Integer> indexes = new ArrayList<Integer>(treeMap.get(port));
							actorPeekIndexPorts.put(port, indexes);
						}
					}
				}

			}

			if (isComplex) {
				break;
			}
		}

		actor.setAttribute(ACTOR_COMPLEX_GUARDS, isComplex);
		if (!isComplex && !actorPeekIndexPorts.isEmpty()) {
			Map<Port, List<Integer>> treeMap = new TreeMap<Port, List<Integer>>(new Comparator<Port>() {

				@Override
				public int compare(Port o1, Port o2) {
					if (o1.getName().equals(o2.getName())) {
						return 0;
					}
					return o1.getName().compareTo(o2.getName());
				}
			}
 
			);

			for (Port port : actorPeekIndexPorts.keySet()) {
				treeMap.put(port, actorPeekIndexPorts.get(port));
			}
			
			actor.setAttribute(ACTOR_ACTION_SELECTION_PEEKS, treeMap);
		}

		return null;
	}

}
